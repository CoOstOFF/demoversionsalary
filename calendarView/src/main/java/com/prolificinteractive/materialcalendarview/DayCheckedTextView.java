package com.prolificinteractive.materialcalendarview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckedTextView;

import java.util.ArrayList;
import java.util.List;

public class DayCheckedTextView extends CheckedTextView {

    private final int fadeTime;
    private List<OnDrawListener> onDrawListeners = new ArrayList<>();
    private CalendarDay date = new CalendarDay();
    private int selectionColor = Color.GRAY;
    private int backgroundColor = Color.TRANSPARENT;

    public DayCheckedTextView(Context context) {
        this(context, null);
    }

    public DayCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        fadeTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        initBackground();
        setIncludeFontPadding(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setTextAlignment(TEXT_ALIGNMENT_GRAVITY);
        }
    }

    private static Drawable generateRectDrawable(final int color) {
        ShapeDrawable drawable = new ShapeDrawable(new RectShape());
        drawable.setShaderFactory(new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, 0, 0, color, color, Shader.TileMode.REPEAT);
            }
        });
        return drawable;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Drawable generateRippleDrawable(final int color) {
        ColorStateList list = ColorStateList.valueOf(color);
        Drawable mask = generateRectDrawable(Color.WHITE);
        return new RippleDrawable(list, null, mask);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!onDrawListeners.isEmpty())
            for (OnDrawListener onDrawListener : onDrawListeners)
                onDrawListener.onDraw(canvas, getHeight(), getWidth(), getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
    }

    public void setDay(CalendarDay date) {
        this.date = date;
        setText(String.valueOf(date.getDay()));
    }

    public CalendarDay getDate() {
        return date;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    private void initBackground() {
        StateListDrawable drawable = new StateListDrawable();
        drawable.setExitFadeDuration(fadeTime);
        drawable.addState(new int[]{android.R.attr.state_checked}, generateRectDrawable(selectionColor));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable.addState(new int[]{android.R.attr.state_pressed}, generateRippleDrawable(selectionColor));
        } else {
            drawable.addState(new int[]{android.R.attr.state_pressed}, generateRectDrawable(selectionColor));
        }
        drawable.addState(new int[]{}, generateRectDrawable(backgroundColor));

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            setBackgroundDrawable(drawable);
        } else {
            setBackground(drawable);
        }
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(@ColorInt int color) {
        this.backgroundColor = color;
        initBackground();
    }

    public int getSelectionColor() {
        return selectionColor;
    }

    public void setSelectionColor(int color) {
        this.selectionColor = color;
        initBackground();
    }

    protected void setupSelection(boolean showOtherDates, boolean inRange, boolean inMonth) {
        boolean enabled = inMonth && inRange;
        setEnabled(enabled);
        setVisibility(enabled || showOtherDates ? View.VISIBLE : View.INVISIBLE);
    }

    public void removeOnDrawListener(OnDrawListener onDrawListener) {
        this.onDrawListeners.remove(onDrawListener);
        postInvalidate();
    }

    public void addOnDrawListener(OnDrawListener onDrawListener) {
        this.onDrawListeners.add(onDrawListener);
        postInvalidate();
    }

    public void clearOnDrawListener() {
        this.onDrawListeners.clear();
        postInvalidate();
    }

    public interface OnDrawListener {
        void onDraw(Canvas canvas, int height, int width, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom);
    }
}
