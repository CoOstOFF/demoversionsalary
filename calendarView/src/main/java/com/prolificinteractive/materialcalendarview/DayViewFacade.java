package com.prolificinteractive.materialcalendarview;

import android.content.res.Resources;

public class DayViewFacade {

    private DayCheckedTextView dayCheckedTextView;

    private DayViewFacade() {
    }

    public DayViewFacade(DayCheckedTextView dayCheckedTextView) {
        this.dayCheckedTextView = dayCheckedTextView;
    }

    public void setGravity(int gravity) {
        dayCheckedTextView.setGravity(gravity);
    }

    public void addOnDrawListener(DayCheckedTextView.OnDrawListener onDrawListener) {
        dayCheckedTextView.addOnDrawListener(onDrawListener);
    }

    public void removeOnDrawListener(DayCheckedTextView.OnDrawListener onDrawListener) {
        dayCheckedTextView.removeOnDrawListener(onDrawListener);
    }

    public void clearOnDrawListener() {
        dayCheckedTextView.clearOnDrawListener();
    }

    public void setSelectionColor(int selectionColor) {
        dayCheckedTextView.setSelectionColor(selectionColor);
    }

    public void setBackgroundColor(int backgroundColor) {
        dayCheckedTextView.setBackgroundColor(backgroundColor);
    }

    public int getGravity(){
        return dayCheckedTextView.getGravity();
    }

    public int getBackgroundColor() {
        return dayCheckedTextView.getBackgroundColor();
    }

    public int getSelectionColor() {
        return dayCheckedTextView.getSelectionColor();
    }

    public int getCurrentTextColor() {
        return dayCheckedTextView.getCurrentTextColor();
    }

    public float getTextSize() {
        return dayCheckedTextView.getTextSize();
    }

    public Resources getResource() {
        return dayCheckedTextView.getResources();
    }
}
