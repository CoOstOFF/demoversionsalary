package com.prolificinteractive.materialcalendarview;

public interface OnLongClickListener {

    void onLongClick(CalendarDay day);
}
