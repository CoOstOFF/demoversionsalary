package com.prolificinteractive.materialcalendarview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;

/**
 * Display one day of a {@linkplain MaterialCalendarView}
 */
public class DayView extends FrameLayout {

    private DayCheckedTextView checkedView;
    private float maxTileSize;

    public DayView(Context context) {
        this(context, null);
    }

    public DayView(Context context, AttributeSet attrs) {
        super(context, attrs);

        checkedView = new DayCheckedTextView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        checkedView.setLayoutParams(params);
        checkedView.setPadding(dpToPx(3), 0, dpToPx(3), 0);

        addView(checkedView);
    }

    public float getMaxTileSize() {
        return maxTileSize;
    }

    public void setMaxTileSize(float maxTileSize) {
        this.maxTileSize = maxTileSize;
    }

    private int dpToPx(float dp) {
        return (int) (dp * getContext().getResources().getDisplayMetrics().density);
    }

    public DayCheckedTextView getCheckedView() {
        return checkedView;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec) / MonthView.COLUMN_COUNT;
        if (width > maxTileSize && maxTileSize > 0) width = (int) maxTileSize;
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY));

        int padding = dpToPx(7f);
        checkedView.getLayoutParams().width = width - padding;
        checkedView.getLayoutParams().height = width - padding;
        checkedView.requestLayout();
    }
}
