package com.prolificinteractive.materialcalendarview;

public interface DayViewDecorator {

    boolean decorate(CalendarDay calendarDay, DayViewFacade dayViewFacade);
}
