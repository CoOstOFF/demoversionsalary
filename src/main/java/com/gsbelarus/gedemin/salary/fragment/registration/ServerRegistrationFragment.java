package com.gsbelarus.gedemin.salary.fragment.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.ui.BackEditText;
import com.gsbelarus.gedemin.salary.ui.FocusableTextInputLayout;

import io.realm.Realm;

public class ServerRegistrationFragment extends RegistrationPagerItemFragment implements View.OnClickListener {

    private BackEditText inputServerAddress;
    private FocusableTextInputLayout inputServerAddressLayout;

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setContentView(R.layout.fragment_registration_server);

        inputServerAddressLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_server_address_layout);
        inputServerAddressLayout.setErrorEnabled(true);
        inputServerAddress = (BackEditText) rootView.findViewById(R.id.input_server_address);

        setupEditText(inputServerAddress, false, new Validator() {
            @Override
            public boolean validate(EditText editText) {
                return validateServerInput();
            }
        });

        rootView.findViewById(R.id.scan_qrcode_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.scan_qrcode_button) {
            if (onScanQrCodeListener != null)
                onScanQrCodeListener.onScanQrCode();
        }
    }

    private boolean validateServerInput() {
        String url = inputServerAddress.getText().toString();
        String error = null;

        if (url.replaceAll(" +", "").isEmpty()) {
            error = getContext().getString(R.string.response_error_empty_code);
        }
        inputServerAddressLayout.setError(error);

        return error == null;
    }

    @Override
    public boolean validatePageInputs() {
        return validateServerInput();
    }

    @Override
    public void savePageFields() {
        getRealmHelper().getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                View rootView = getRootView();
                //Assert.assertNotNull(rootView);

                String inputText = ((EditText) rootView.findViewById(R.id.input_server_address)).getText().toString();

                String address = inputText;
                if (!Patterns.WEB_URL.matcher(inputText).matches()) {
                    address = ServerURL.findAddressByCompany(inputText);
                    if (address.isEmpty()) {
                        address = ServerURL.RELAY_SERVER_URL;
                        getRealmHelper().getSyncMetaData(false).setAlias(ServerURL.formatServerName(inputText));
                    }
                }
                getRealmHelper().getSyncMetaData(false).setAddressServer(address);
                getRealmHelper().getSyncMetaData(false).setScheme("http");
            }
        });
    }

    @Override
    protected void loadPageFields(final View rootView, @Nullable Bundle bundle) {
        String addressServer;
        if (BundleHelper.containsArguments(bundle, RegistrationPagerFragment.TAG_ADDRESS_SERVER))
            addressServer = BundleHelper.get(bundle, RegistrationPagerFragment.TAG_ADDRESS_SERVER);
        else
            addressServer = getRealmHelper().getSyncMetaData(true).getAddressServer();

        String company = ServerURL.findCompanyByAddress(addressServer);
        if (company.isEmpty()) {
            company = getRealmHelper().getSyncMetaData(true).getAlias();
            if (company.isEmpty()) company = addressServer;
        }
        setTextToEdit((EditText) rootView.findViewById(R.id.input_server_address), company);
    }
}
