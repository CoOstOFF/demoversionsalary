package com.gsbelarus.gedemin.salary.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.adapter.AuthDevicesAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.ui.DividerItemDecoration;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;

import io.realm.Realm;

public class AuthDevicesFragment extends BaseFragment implements AuthDevicesAdapter.OnLockDeviceClickListener {

    private SwipeRefreshLayout refreshLayout;
    private AuthDevicesAdapter recyclerAdapter;
    private RealmHelper realmHelper;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_auth_devices;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        realmHelper.getDeviceInfo(true);
        recyclerAdapter = new AuthDevicesAdapter(getContext(), realmHelper.getRealm().where(DeviceInfoModel.class).findAll(), this);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        refreshLayout.setColorSchemeColors(getContext().getResources().getIntArray(R.array.swipe_refresh));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (PermissionHelper.ReadPhoneState.checkAndRequest(AuthDevicesFragment.this))
                    getContext().startService(SyncServiceModel.getIntentWithTask(new Intent(getContext(), SyncDataService.class),
                            SyncServiceTask.TypeOfTask.SERIES_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_REQUEST)));
                else
                    refreshLayout.setRefreshing(false);
            }
        });

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.auth_devices_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null, false, true));
        recyclerView.setAdapter(recyclerAdapter);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new MaterialDialog.Builder(getActivity())
//                        .title(R.string.confirm_request_new_invite_title)
//                        .content(R.string.confirm_request_new_invite)
//                        .negativeText(R.string.gdmnlib_dialog_cancel)
//                        .positiveText(R.string.gdmnlib_dialog_ok)
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
//                                getContext().startService(SyncDataService.getIntentWithTask(
//                                        new Intent(getContext(), SyncDataService.class),
//                                        SyncServiceTask.TypeOfTask.SERIES_SYNC,
//                                        new SyncRequestType(SyncRequestType.TypeOfRequest.INVITE_REQUEST)));
//                            }
//                        })
//                        .show();
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.confirm_request_new_invite_title)
                        .setMessage(R.string.confirm_request_new_invite)
                        .setNegativeButton(R.string.gdmnlib_dialog_cancel, null)
                        .setPositiveButton(R.string.gdmnlib_dialog_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getContext().startService(SyncDataService.getIntentWithTask(
                                        new Intent(getContext(), SyncDataService.class),
                                        SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                        new SyncRequestType(SyncRequestType.TypeOfRequest.INVITE_REQUEST)));
                            }
                        })
                        .create()
                        .show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View rootView) {
        if (refreshLayout != null)
            refreshLayout.setRefreshing(false);

        if (realmHelper != null) {
//            realmHelper.getRealm().refresh(); // FIXME: 15.06.16
        }

        if (recyclerAdapter != null)
            recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, final DeviceInfoModel deviceInfoModel) {
        if (view == null) return;

        if (view.getId() == R.id.icon_lock) {
//            new MaterialDialog.Builder(getActivity())
//                    .title(R.string.confirm_lock_device_title)
//                    .content(R.string.confirm_lock_device)
//                    .negativeText(R.string.gdmnlib_dialog_cancel)
//                    .positiveText(R.string.gdmnlib_dialog_ok)
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
//                            getContext().startService(SyncDataService.getIntentWithTask(
//                                    new Intent(getContext(), SyncDataService.class),
//                                    SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DENY_ACCESS_REQUEST, deviceInfoModel.getUid())));
//                        }
//                    })
//                    .show();
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.confirm_lock_device_title)
                    .setMessage(R.string.confirm_lock_device)
                    .setNegativeButton(R.string.gdmnlib_dialog_cancel, null)
                    .setPositiveButton(R.string.gdmnlib_dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getContext().startService(SyncDataService.getIntentWithTask(
                                    new Intent(getContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DENY_ACCESS_REQUEST, deviceInfoModel.getUid())));
                        }
                    })
                    .create()
                    .show();
        }
    }
}
