package com.gsbelarus.gedemin.salary.fragment.payslip;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.adapter.payslip.InternalClickListener;
import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipDetailAdapter;
import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipGeneralAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.PayslipDetailCard;
import com.gsbelarus.gedemin.salary.entity.PayslipSubItem;
import com.gsbelarus.gedemin.salary.entity.model.PayslipBenefitModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipDeductionModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipItemModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.ui.AutofitEmptyRecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class PayslipTabDetailFragment extends BaseFragment {

    private PayslipDetailAdapter adapter;
    private RealmHelper realmHelper;
    private AppCompatButton workModeButton;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_payslip_detail;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        adapter = new PayslipDetailAdapter(getContext(), realmHelper);

        AutofitEmptyRecyclerView cardsRecView = (AutofitEmptyRecyclerView) rootView.findViewById(R.id.payslip_recycler_view);
        final StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        cardsRecView.setLayoutManager(manager);
        cardsRecView.setHasFixedSize(true);
        cardsRecView.setColumnWidth(getActivity().getResources().getDimension(R.dimen.payslip_detail_card_width));
        cardsRecView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int space = (int) (2f * getContext().getResources().getDisplayMetrics().density);

                outRect.top = space;
                outRect.left = space;
                outRect.right = space;
                outRect.bottom = space;
            }
        });
        cardsRecView.setAdapter(adapter);
        cardsRecView.setEmptyView(rootView.findViewById(R.id.empty_rec_view_detail));

        adapter.setOnClickInfoButtons(new InternalClickListener() {
            @Override
            public void onInternalViewClick(View view, View internalView, int position) {
                new AlertDialog.Builder(getActivity())
                        .setPositiveButton(R.string.dialog_ok, null)
                        .setMessage(adapter.getList().get(position).getInfo())
                        .create()
                        .show();
            }
        });

        workModeButton = (AppCompatButton) rootView.findViewById(R.id.payslip_detail_work_mode_button);
        workModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RegistrationPager.class));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View rootView) {
        adapter.clear();
        Calendar date;
        if (!BundleHelper.containsArguments(getArguments(), PayslipFragment.TAG_DATE)) {
            List<Calendar> possibleDates = realmHelper.getPayslipPossibleDatesList();
            date = possibleDates.get(possibleDates.size() - 1);
        } else
            date = BundleHelper.get(getArguments(), PayslipFragment.TAG_DATE);

        PayslipModel payslipModel = realmHelper.getPayslip(date);

        if (payslipModel != null) {
            List<PayslipDetailCard> list = new ArrayList<>();
            RealmResults<PayslipItemModel> debitList = realmHelper.getDebitList(payslipModel);
            RealmResults<PayslipItemModel> creditList = realmHelper.getCreditList(payslipModel);
            RealmResults<PayslipItemModel> taxList = realmHelper.getTaxesList(payslipModel);
            RealmResults<PayslipItemModel> prepaymentList = realmHelper.getPrepaymentList(payslipModel);

            //TODO long
            double deductionSum = 0;
            double benefitSum = 0;

            /** общая сумма начислений и удержаний */   //TODO refactoring
            if (payslipModel.getDeductions() != null)
                deductionSum = payslipModel.getDeductions().where().sum("sum").doubleValue();
            if (payslipModel.getBenefits() != null)
                benefitSum = payslipModel.getBenefits().where().findAll().sum("sum").doubleValue();

            if (!debitList.isEmpty()) {
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipItemModel item : debitList) {
                    items.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getDebit()));
                }
                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_detail_debit_sum),
                        getString(R.string.payslip_detail_debit_info),
                        debitList.sum("debit").doubleValue(),
                        sortByComparatorValues(items, false)));
            }
            if(!prepaymentList.isEmpty()) {
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipItemModel item : prepaymentList) {
                    String prepDateString = "";
                    if (item.getDate() != null) {
                        prepDateString = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(item.getDate());
                    }

                    items.add(new PayslipSubItem(prepDateString, PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getCredit()));
                }

                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_general_prepayment_sum),
                        getString(R.string.payslip_detail_prepayment_info),
                        prepaymentList.sum("credit").doubleValue(),
                        sortByComparatorValues(items, false)));
            }

            if (!creditList.isEmpty()) {
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipItemModel item : creditList) {
                    items.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getCredit()));
                }
                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_detail_credit_sum),
                        getString(R.string.payslip_detail_credit_info),
                        creditList.sum("credit").doubleValue(),
                        sortByComparatorValues(items, false)));
            }
            if (!taxList.isEmpty()) {
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipItemModel item : taxList) {
                    items.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getCredit()));
                }
                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_detail_taxes_sum),
                        getString(R.string.payslip_detail_taxes_info),
                        taxList.sum("credit").doubleValue(),
                        sortByComparatorValues(items, false)));
            }
            if (!payslipModel.getDeductions().where().greaterThan("sum", 0.0).findAll().isEmpty()) { //TODO
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipDeductionModel item : payslipModel.getDeductions().where().greaterThan("sum", 0.0).findAll()) {
                    items.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getSum()));
                }
                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_detail_deductions_sum),
                        getString(R.string.payslip_detail_deductions_info),
                        deductionSum,
                        sortByComparatorValues(items, false)));
            }
            if (!payslipModel.getBenefits().where().greaterThan("sum", 0.0).findAll().isEmpty()) {
                List<PayslipSubItem> items = new ArrayList<>();
                for (PayslipBenefitModel item : payslipModel.getBenefits().where().greaterThan("sum", 0.0).findAll()) {
                    items.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, item.getName(), item.getSum()));
                }
                list.add(new PayslipDetailCard(
                        getString(R.string.payslip_detail_benefits_sum),
                        getString(R.string.payslip_detail_benefits_info),
                        benefitSum,
                        sortByComparatorValues(items, false)));
            }

            adapter.setList(date, list);
        }

        if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty())
            workModeButton.setVisibility(View.VISIBLE);
        else
            workModeButton.setVisibility(View.GONE);

    }

    private List<PayslipSubItem> sortByComparatorValues(List<PayslipSubItem> list, final boolean orderASC) { // true - ASC, false - DESC
        Collections.sort(list, new Comparator<PayslipSubItem>() {
            @Override
            public int compare(PayslipSubItem lhs, PayslipSubItem rhs) {
                if (orderASC) {
                    return ((Double) lhs.getSum()).compareTo(rhs.getSum());
                } else {
                    return ((Double) rhs.getSum()).compareTo(lhs.getSum());

                }
            }
        });
        return list;
    }

    @Override
    public void onResume() {
        super.onResume();

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }
}
