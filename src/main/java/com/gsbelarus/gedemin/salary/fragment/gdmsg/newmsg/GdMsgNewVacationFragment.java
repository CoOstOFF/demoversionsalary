package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.GdMsgNew;
import com.gsbelarus.gedemin.salary.database.ModelFactory;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationModel;
import com.gsbelarus.gedemin.salary.service.SendDataService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GdMsgNewVacationFragment extends GdMsgNewPeriodFragment implements View.OnClickListener {

    private static final String DATE_FORMAT = "d MMM yyyy";

    private TextView tvDateBegin;
    private TextView tvDateEnd;
    private RadioGroup rgVacationType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null && BundleHelper.containsArguments(getArguments(), GdMsgNew.TAG_MSG_DATE))
            setDateBegin(BundleHelper.<Date>get(getArguments(), GdMsgNew.TAG_MSG_DATE));

        if (getDateBegin() == null)
            setDateBegin(CalendarHelper.initCalendarFields(Calendar.getInstance(), Calendar.HOUR_OF_DAY, 0).getTime());

        if (getDateEnd() == null) {
            Calendar begin = Calendar.getInstance();
            begin.setTime(getDateBegin());
            Calendar end = CalendarHelper.initCalendarFields(begin, Calendar.HOUR_OF_DAY, 0);
            end.add(Calendar.DATE, 1);
            setDateEnd(end.getTime());
        }
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setTitleFragment(R.string.request_vacation);
        setContentView(R.layout.fragment_gdmsg_new_vacation);

        tvDateBegin = (TextView) rootView.findViewById(R.id.tv_date_begin);
        tvDateEnd = (TextView) rootView.findViewById(R.id.tv_date_end);
        rgVacationType = (RadioGroup) rootView.findViewById(R.id.rg_vacation_type);

        setDateText(tvDateBegin, getDateBegin());
        setDateText(tvDateEnd, getDateEnd());

        tvDateBegin.setOnClickListener(this);
        tvDateEnd.setOnClickListener(this);
    }

    @Override
    protected void onDateChanged(DatePickerType type) {
        switch (type) {
            case DATE_BEGIN:
                setDateText(tvDateBegin, getDateBegin());
                break;
            case DATE_END:
                setDateText(tvDateEnd, getDateEnd());
                break;
        }
    }

    private void setDateText(TextView textView, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        textView.setText(dateFormat.format(date));
    }

    @Override
    public void onClick(View v) {
        DatePickerDialog datePickerDialog = null;
        if (v == tvDateBegin)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_BEGIN);
        else if (v == tvDateEnd)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_END);

        if (datePickerDialog != null) {
            Calendar bufCal = Calendar.getInstance();
            datePickerDialog.setYearRange(bufCal.get(Calendar.YEAR), bufCal.get(Calendar.YEAR) + 5);
            datePickerDialog.setMinDate(bufCal);
        }
    }

    private GdMsgVacationModel.Type getCheckedType() {
        switch (rgVacationType.getCheckedRadioButtonId()) {
            case R.id.rb_regular_vacation:
                return GdMsgVacationModel.Type.REGULAR;
            case R.id.rb_unpaid_vacation:
                return GdMsgVacationModel.Type.UNPAID;
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:

                if (getDateBegin().before(getDateEnd())) {
                    GdMsgVacationModel gdMsgVacationModel = new GdMsgVacationModel();
                    gdMsgVacationModel.setUid(getRealmHelper().generateUID(GdMsgVacationModel.class));
                    GdMsgModel tmpGdMsgModel = new GdMsgModel();
                    tmpGdMsgModel.setUid(getRealmHelper().generateUID(GdMsgModel.class));
                    GdMsgModel gdMsgModel = ModelFactory.newInstanceGdMsgVacationModel(
                            gdMsgVacationModel,
                            tmpGdMsgModel,
                            getComment(),
                            new Date(),
                            getDateBegin(),
                            getDateEnd(),
                            getCheckedType());

                    Intent intent = new Intent(getActivity(), SendDataService.class);
                    intent.setAction(SendDataService.SEND_GD_MSG_ACTION);
                    intent.putExtra(SendDataService.EXTRA_KEY_GD_MSG, gdMsgModel);
                    getActivity().startService(intent);

                    getActivity().finish();
                } else
                    new AlertDialog.Builder(getActivity())
                            .setPositiveButton(R.string.dialog_ok, null)
                            .setTitle(getString(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification))
                            .setMessage(R.string.incorrect_dates_message)
                            .create()
                            .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
