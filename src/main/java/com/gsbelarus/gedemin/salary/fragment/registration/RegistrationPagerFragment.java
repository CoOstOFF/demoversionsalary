package com.gsbelarus.gedemin.salary.fragment.registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.IntroPager;
import com.gsbelarus.gedemin.salary.activity.QrCodeCapture;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.ui.NoSwipeViewPager;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import me.zhanghai.android.materialprogressbar.IndeterminateHorizontalProgressDrawable;

import static com.gsbelarus.gedemin.salary.activity.Main.CONTACT_EMAIL;
import static com.gsbelarus.gedemin.salary.activity.Main.CONTACT_SUBJECT;

public class RegistrationPagerFragment extends BaseFragment implements RegistrationPagerItemFragment.OnNavButtonClickListener, RegistrationPagerItemFragment.OnScanQrCodeListener {

    private static String INVITE_CODE_PATTERN = "^[\\-0-9a-zA-Z]*$";

    private static final int REQUEST_CODE_QR_CODE_CAPTURE = 9001;
    public static final String TAG_EMPLOYEE_MODEL = "employee";
    public static final String TAG_ADDRESS_SERVER = "address_server";

    private FragmentPagerAdapter pageAdapter;
    private NoSwipeViewPager pager;

    private ProgressBar progressBar;

    private boolean performingAuthRequest;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_registration_pager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final List<Class<? extends RegistrationPagerItemFragment>> fragmentsClass = new ArrayList<>();
        fragmentsClass.add(ServerRegistrationFragment.class);
        fragmentsClass.add(PassportRegistrationFragment.class);
        fragmentsClass.add(PayslipRegistrationFragment.class);

        pageAdapter = new FragmentPagerAdapter(getFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                try {
                    RegistrationPagerItemFragment fragment = fragmentsClass.get(position).newInstance();
                    fragment.setArguments(new BundleHelper.Builder()
                            .putBoolean(RegistrationPagerItemFragment.TAG_LOGIN_BUTTON, position == pageAdapter.getCount() - 1)
                            .putBoolean(RegistrationPagerItemFragment.TAG_NEXT_BUTTON, position < pageAdapter.getCount() - 1)
                            .putBoolean(RegistrationPagerItemFragment.TAG_PREV_BUTTON, position > 0)
                            .build());
                    fragment.setOnNavButtonClickListener(RegistrationPagerFragment.this);
                    fragment.setOnScanQrCodeListener(RegistrationPagerFragment.this);
                    return fragment;
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public int getCount() {
                return fragmentsClass.size();
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (performingAuthRequest) {
            getContext().startService(SyncServiceModel.getIntentWithTask(new Intent(getContext(), SyncDataService.class),
                    SyncServiceTask.TypeOfTask.STOP_SYNC));
            SettingsFragment.putPrefMode(getContext(), SettingsFragment.PrefMode.UNDEFINED_MODE);
        }
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        progressBar = (ProgressBar) rootView.findViewById(R.id.reg_progress_bar);
        progressBar.setIndeterminateDrawable(new IndeterminateHorizontalProgressDrawable(getActivity()));

        pager = (NoSwipeViewPager) rootView.findViewById(R.id.reg_viewpager);
        pager.setAdapter(pageAdapter);
        pager.setPagingEnabled(false);
        pager.setOffscreenPageLimit(pageAdapter.getCount());
    }

    @Override
    public void onClick(View view, String tag) {
        RegistrationPagerItemFragment fragment;
        switch (tag) {
            case RegistrationPagerItemFragment.TAG_LOGIN_BUTTON:
                fragment = getPagerFragment(pager.getCurrentItem());
                if (fragment != null && fragment.validatePageInputs()) {
                    if (PermissionHelper.ReadPhoneState.checkAndRequest(RegistrationPagerFragment.this)) {
                        login(null, false);
                    }
                }
                break;
            case RegistrationPagerItemFragment.TAG_NEXT_BUTTON:
                fragment = getPagerFragment(pager.getCurrentItem());
                if (fragment != null && fragment.validatePageInputs()) {
                    pager.setCurrentItem(getPagerItem(+1), true);
                }
                break;
            case RegistrationPagerItemFragment.TAG_PREV_BUTTON:
                pager.setCurrentItem(getPagerItem(-1), true);
                break;
        }
    }

    @Override
    public void onScanQrCode() {
        Intent intent = new Intent(getActivity(), QrCodeCapture.class);
        startActivityForResult(intent, REQUEST_CODE_QR_CODE_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_QR_CODE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                for (RegistrationPagerItemFragment frgm : getPagerFragments()) {
                    frgm.reloadPageFields(data.getExtras());
                }
                pager.setCurrentItem(pager.getAdapter().getCount() - 1, true);
            }
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            for (RegistrationPagerItemFragment itemFragment : getPagerFragments()) {
                itemFragment.setOnNavButtonClickListener(this);
                itemFragment.setOnScanQrCodeListener(this);
            }
        }
    }

    private void login(@Nullable final String inviteCode, final boolean lostMode) {  //TODO красивее
        hideKeyboard(getBaseActivity());
        showAppBar(getBaseActivity());

        RealmHelper.executeTransactionWithClosing(new RealmHelper.RealmHelperExecutor() {
            @Override
            public void execute(RealmHelper realmHelper) {
                realmHelper.clearDB(false);
                realmHelper.getSyncMetaData(false).setInviteCode(inviteCode);
                realmHelper.getSyncMetaData(false).setLostMode(lostMode);
            }
        });

        for (RegistrationPagerItemFragment frgm : getPagerFragments())
            frgm.savePageFields();

        performingAuthRequest = true;
        progressBar.setVisibility(View.VISIBLE);
        getBaseActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        SettingsFragment.putPrefMode(getContext(), SettingsFragment.PrefMode.WORK_MODE);

        getContext().startService(SyncDataService.getIntentWithTask(
                new Intent(getContext(), SyncDataService.class),
                SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.AUTH_REQUEST)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionHelper.REQUEST_CODE && grantResults.length > 0)
            for (int i = 0; i < permissions.length; i++) {
                switch (permissions[i]) {
                    case Manifest.permission.READ_PHONE_STATE:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                            RealmHelper.executeWithClosing(new RealmHelper.RealmHelperExecutor() {
                                @Override
                                public void execute(RealmHelper realmHelper) {
                                    login(realmHelper.getSyncMetaData(true).getInviteCode(), false);
                                }
                            });
                        break;
                }
            }
    }

    private int getPagerItem(int i) {
        return pager.getCurrentItem() + i;
    }

    @Nullable
    private RegistrationPagerItemFragment getPagerFragment(int position) {
        return (RegistrationPagerItemFragment) getFragmentManager().findFragmentByTag(getPagerFragmentTag(pager, pageAdapter.getItemId(position)));
    }

    private List<RegistrationPagerItemFragment> getPagerFragments() {
        List<RegistrationPagerItemFragment> fragments = new ArrayList<>();
        for (int i = 0; i < pageAdapter.getCount(); i++) {
            RegistrationPagerItemFragment fragment = (RegistrationPagerItemFragment) getFragmentManager().findFragmentByTag(getPagerFragmentTag(pager, pageAdapter.getItemId(i)));
            if (fragment != null)
                fragments.add(fragment);
        }
        return fragments;
    }

    private String getPagerFragmentTag(ViewPager viewPager, long id) {
        return "android:switcher:" + viewPager.getId() + ":" + id;              ////TODO переделать
    }

    public boolean onBackPressed() {
        if (pager != null && pager.getCurrentItem() != 0) {
            pager.setCurrentItem(getPagerItem(-1));
            return true;
        }
        return false;
    }

    public boolean finishSync(SyncServiceStatus syncServiceStatus) {
        boolean showStandardErrorDialog = true;
        if (syncServiceStatus.getStatus().getTypeOfStatus() == SyncStatus.TypeOfStatus.SUCCESSFUL) {
            getBaseActivity().putSharedBoolean(IntroPager.PREF_SHOW_INTRO, false);
            getBaseActivity().setResult(Activity.RESULT_OK);
            getBaseActivity().finish();

            getContext().startService(SyncServiceModel.getIntentWithTask(new Intent(getContext(), SyncDataService.class),
                    SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));
        } else {
            ResponseErrorCode responseErrorCode = ResponseErrorCode.NEED_INVITE;
            SettingsFragment.putPrefMode(getContext(), SettingsFragment.PrefMode.UNDEFINED_MODE);
            if (syncServiceStatus.getStatus().getMessage().equals(responseErrorCode.getMessage())) {
                showRequestInviteDialog();
                showStandardErrorDialog = false;
            }
        }
        performingAuthRequest = false;
        progressBar.setVisibility(View.GONE);
        getBaseActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        return showStandardErrorDialog;
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard(getBaseActivity());
        showAppBar(getBaseActivity());
    }

    private void showRequestInviteDialog() {
        getInputTextMaterialDialog(
                getContext().getString(R.string.registration_payslip_need_invite_dlgtitle),
                getContext().getString(R.string.registration_payslip_need_invite_dlgcontent),
                getContext().getString(R.string.registration_payslip_need_invite_hint),
                false,
                getContext().getString(R.string.registration_payslip_need_invite_btnlogin),
                getContext().getString(R.string.registration_payslip_need_invite_btnlost),
                new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        if (!Pattern.compile(INVITE_CODE_PATTERN).matcher(input.toString()).matches()) {
                            dialog.setContent(getContext().getString(R.string.registration_payslip_invalid_invitecode_error)); //TODO test
                            dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                        } else {
                            dialog.setContent(getContext().getString(R.string.registration_payslip_need_invite_dlgcontent));
                            dialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                        }
                    }
                },
                new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull final MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        if (materialDialog.getInputEditText() != null)
                            login(materialDialog.getInputEditText().getText().toString(), false);
                    }
                },
                new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull final MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        StringBuilder clientInfo = new StringBuilder();
                        //clientInfo.append("------------------------------------------");
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", CONTACT_EMAIL, null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "GedeminSalary support: invite");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, clientInfo.toString());

                        try {
                            startActivity(Intent.createChooser(emailIntent, getString(R.string.help_send_mail_choose) + "..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            try {
                                //TODO передать info
                                Intent browserIntent = new Intent();
                                browserIntent.setAction(Intent.ACTION_VIEW);
                                browserIntent.setData(Uri.parse(SettingsFragment.GS_CONTACT_URL + SettingsFragment.GS_CONTACT_ANALYTICS_CAMPAIGN_PARAMETERS));
                                browserIntent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                                startActivity(browserIntent);
                            } catch (Exception e) {
                                try {
                                    Intent browserIntent = new Intent();
                                    browserIntent.setAction(Intent.ACTION_VIEW);
                                    browserIntent.setData(Uri.parse(SettingsFragment.GS_CONTACT_URL + SettingsFragment.GS_CONTACT_ANALYTICS_CAMPAIGN_PARAMETERS));
                                    startActivity(browserIntent);
                                } catch (Exception ignored) {
                                }
                            }
                        }
                        //TODO dialog confirm + info
//                        new MaterialDialog.Builder(getActivity())
//                                .title(R.string.lost_device_confirm)
//                                .content(R.string.lost_device_info)
//                                .positiveText(R.string.confirm)
//                                .negativeText(R.string.cancel_text)
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        login("", true);
//                                    }
//                                })
//                                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        dialog.dismiss();
//                                    }
//                                })
//                                .build()
//                                .show();

                    }
                }
        ).show();
    }

    public static void showAppBar(BaseActivity baseActivity) {
        if (baseActivity != null && baseActivity.getAppBarLayout() != null) {
            baseActivity.getAppBarLayout().setExpanded(true);
            baseActivity.setScrollFlagsToolbar(0);
        }
    }

    public static void hideAppBar(BaseActivity baseActivity) {
        if (baseActivity != null && baseActivity.getAppBarLayout() != null) {
            baseActivity.setScrollFlagsToolbar(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
            baseActivity.getAppBarLayout().setExpanded(false);
        }
    }

    public static void hideKeyboard(BaseActivity baseActivity) {
        View view = baseActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager im = (InputMethodManager) baseActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private MaterialDialog getInputTextMaterialDialog(String title, String message, String hint, boolean allowEmptyInput,
                                                      String positiveText, String neutralText,
                                                      MaterialDialog.InputCallback onInputCallback,
                                                      MaterialDialog.SingleButtonCallback onPositiveCallback,
                                                      MaterialDialog.SingleButtonCallback onNeutralCallback) {

        return new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(message)
                .positiveText(positiveText)
                .neutralText(neutralText)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .alwaysCallInputCallback()
                .input(hint, null, allowEmptyInput, onInputCallback)
                .onPositive(onPositiveCallback)
                .onNeutral(onNeutralCallback)
                .build();

    }

    private BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }
}