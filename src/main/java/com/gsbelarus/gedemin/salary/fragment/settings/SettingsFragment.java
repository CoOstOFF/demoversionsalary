package com.gsbelarus.gedemin.salary.fragment.settings;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.content.IntentCompat;

import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.AuthDevices;
import com.gsbelarus.gedemin.salary.activity.Main;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.fragment.dialogs.LicensesDialogFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;

import io.realm.Realm;

public class SettingsFragment extends PrefFragment {

    public static final String PLAY_STORE_ANALYTICS_CAMPAIGN_PARAMETERS = "&referrer=utm_source%3Dapplication%26utm_medium%3DGedeminPayslip%2520app%26utm_content%3Drate%2520app%26utm_campaign%3DGedeminPayslip%2520app";
    public static final String GS_URL = "http://gsbelarus.com/pw/";
    public static final String GS_CONTACT_URL = GS_URL + "contact-us/";
    public static final String GS_CONTACT_ANALYTICS_CAMPAIGN_PARAMETERS = "?utm_source=application&utm_medium=GedeminPayslip%20app&utm_content=learn%20more&utm_campaign=GedeminPayslip%20app";

    public enum PrefMode {
        WORK_MODE, DEMO_MODE, UNDEFINED_MODE;

        public static String getPrefString() {
            return "mode";
        }
    }

    public enum ThemeMode {
        LIGHT, DARK;

        public static String getPrefString() {
            return "theme";
        }
    }

    public enum NotifyBoardFilter {
        NO_NOTIFICATIONS, COMPANY_NOTIFICATIONS, ALL_NOTIFICATIONS;
    }

    private enum BoardSyncPeriodType {
        DAYS_30, DAYS_60, DAYS_120
    }

    private static final String PREF_WORK_MODE = "working_mode";

    public static final String LANG_LIST = "lang_listPref";
    public static final String THEME_LIST = "themes_listPref";
    public static final String CURRENCY_LIST = "currency_listPref";
    public static final String BOARD_SYNC_PERIOD_LIST = "board_sync_period_listPref";

    public static final String NOTIFY_NEW_PAYSLIP = "notify_new_payslip_checkBoxPref";
    public static final String NOTIFY_BOARD_FILTER_LIST = "notify_board_filter_listPref";

    public static final String AUTH = "auth_button";
    public static final String SYNC = "sync_button";
    public static final String DEVICES_LIST = "devices_list_button";

    public static final String ABOUT = "about";
    public static final String CHANGELOG = "changelog";
    public static final String RATE_APP = "rate_app";
    public static final String LICENSES = "licenses";

    public static final int DEFAULT_LANG_PREF_ITEM = 0;
    public static final int DEFAULT_THEME_PREF_ITEM = 0;
    public static final int DEFAULT_BOARD_SYNC_PERIOD_PREF_ITEM = 1;
    public static final int DEFAULT_NOTIFY_BOARD_FILTER_PREF_ITEM = 1;

    private RealmHelper realmHelper;
    private CurrencyHelper currencyHelper;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (!key.equals("pref_dark_theme")) {
                    return;
                }

                getActivity().finish();
                final Intent intent = getActivity().getIntent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currencyHelper = CurrencyHelper.getInstance(context);
        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        addPreferencesFromResource(R.xml.pref_settings);

        initListSummary(findPreference(LANG_LIST),
                getResources().getStringArray(R.array.language_list_title),
                DEFAULT_LANG_PREF_ITEM)
                .setOnPreferenceClickListener(getClickListener());

        initListSummary(findPreference(THEME_LIST),
                getResources().getStringArray(R.array.theme_list_title),
                DEFAULT_THEME_PREF_ITEM)
                .setOnPreferenceClickListener(getClickListener());

        initListSummary(findPreference(CURRENCY_LIST),
                getResources().getStringArray(R.array.currency_list),
                currencyHelper.getLastChoice().ordinal())
                .setOnPreferenceClickListener(getClickListener());

        initListSummary(findPreference(BOARD_SYNC_PERIOD_LIST),
                getResources().getStringArray(R.array.board_sync_period_list_values),
                DEFAULT_BOARD_SYNC_PERIOD_PREF_ITEM)
                .setOnPreferenceClickListener(getClickListener());

        initListSummary(findPreference(NOTIFY_BOARD_FILTER_LIST),
                getResources().getStringArray(R.array.notify_board_filter_list_values),
                DEFAULT_NOTIFY_BOARD_FILTER_PREF_ITEM)
                .setOnPreferenceClickListener(getClickListener());

        findPreference(AUTH).setOnPreferenceClickListener(getClickListener());
        findPreference(SYNC).setOnPreferenceClickListener(getClickListener());
        findPreference(DEVICES_LIST).setOnPreferenceClickListener(getClickListener());

        findPreference(ABOUT).setOnPreferenceClickListener(getClickListener());
        findPreference(CHANGELOG).setOnPreferenceClickListener(getClickListener());
        findPreference(RATE_APP).setOnPreferenceClickListener(getClickListener());
        findPreference(LICENSES).setOnPreferenceClickListener(getClickListener());

        finishSync();
        /** Проверяем запущена ли синхронизации, если запущена срабатывает BroadcastReceiver
         и выключает кнопки раздела Аутентификация */
        getActivity().startService(SyncServiceModel.getIntentWithTask(
                new Intent(context, SyncDataService.class), SyncServiceTask.TypeOfTask.REQUEST_STATUS_SYNC));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realmHelper.getRealm().close();
    }

    private Preference.OnPreferenceClickListener getClickListener() {
        return new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                if (preference.getKey().equals(LANG_LIST)) {
                    showListDialog(preference,
                            preference.getTitle().toString(),
                            getString(R.string.cancel_text),
                            getResources().getStringArray(R.array.language_list_title),
                            DEFAULT_LANG_PREF_ITEM,
                            new OnItemListChanged() {
                                @Override
                                public void onItemListChanged(String value, int position) {

                                    ((App) getActivity().getApplication()).changeLocale();
                                    Intent restartIntent = new Intent(context, Main.class);
                                    restartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(restartIntent);

                                    android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            });

                } else if (preference.getKey().equals(THEME_LIST)) {
                    showListDialog(preference,
                            preference.getTitle().toString(),
                            getString(R.string.cancel_text),
                            getResources().getStringArray(R.array.theme_list_title),
                            DEFAULT_THEME_PREF_ITEM,
                            new OnItemListChanged() {
                                @Override
                                public void onItemListChanged(String value, int position) {
                                    putThemeMode(context, ThemeMode.values()[position]);

                                    Intent restartIntent = new Intent(context, Main.class);
                                    restartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(restartIntent);

                                    android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            });

                } else if (preference.getKey().equals(CURRENCY_LIST)) {
                    currencyHelper.showChoiceDialog(getActivity(), new CurrencyHelper.OnCurrencySwitcherChanged() {
                        @Override
                        public void onChanged(CurrencyHelper.Kind kind) {
                            preference.setSummary(getResources().getStringArray(R.array.currency_list)
                                    [currencyHelper.getLastChoice().ordinal()]);
                        }
                    });

                } else if (preference.getKey().equals(BOARD_SYNC_PERIOD_LIST)) {
                    showListDialog(preference,
                            preference.getTitle().toString(),
                            getString(R.string.cancel_text),
                            getResources().getStringArray(R.array.board_sync_period_list_values),
                            DEFAULT_BOARD_SYNC_PERIOD_PREF_ITEM,
                            new OnItemListChanged() {
                                @Override
                                public void onItemListChanged(String value, int position) {
                                    int prevPeriodDays = getBoardSyncPeriodDaysPref(context);
                                    PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(BOARD_SYNC_PERIOD_LIST, BoardSyncPeriodType.values()[position].ordinal()).commit();

                                    int newPeriodDays = getBoardSyncPeriodDaysPref(context);

                                    if (getPrefMode(context)!= SettingsFragment.PrefMode.DEMO_MODE && newPeriodDays != prevPeriodDays) {
                                        if (newPeriodDays > prevPeriodDays) {
                                            realmHelper.getRealm().beginTransaction();
                                            realmHelper.getSyncMetaData(false).setLastSyncPostsDate("");
                                            realmHelper.getRealm().commitTransaction();
                                        }

                                        getActivity().startService(
                                                SyncServiceModel.getIntentWithTask(
                                                        new Intent(getActivity(), SyncDataService.class),
                                                        SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                                        new SyncRequestType(SyncRequestType.TypeOfRequest.BOARD_ITEM_REQUEST)
                                                )
                                        );
                                    }
                                }
                            });

                } else if (preference.getKey().equals(NOTIFY_BOARD_FILTER_LIST)) {
                    showListDialog(preference,
                            preference.getTitle().toString(),
                            getString(R.string.cancel_text),
                            getResources().getStringArray(R.array.notify_board_filter_list_values),
                            DEFAULT_NOTIFY_BOARD_FILTER_PREF_ITEM,
                            new OnItemListChanged() {
                                @Override
                                public void onItemListChanged(String value, int position) {
                                    PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(NOTIFY_BOARD_FILTER_LIST, NotifyBoardFilter.values()[position].ordinal()).commit();
                                }
                            });
                } else if (preference.getKey().equals(AUTH)) {
                    startActivity(new Intent(context, RegistrationPager.class));

                } else if (preference.getKey().equals(SYNC)) {
                    if (PermissionHelper.ReadPhoneState.checkAndRequest(getActivity()))
                        getActivity().startService(SyncServiceModel.getIntentWithTask(new Intent(getActivity(), SyncDataService.class),
                                SyncServiceTask.TypeOfTask.SINGLE_SYNC,
                                new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));

                } else if (preference.getKey().equals(DEVICES_LIST)) {
                    startActivity(new Intent(context, AuthDevices.class));

                } else if (preference.getKey().equals(LICENSES)) {
                    showLicensesDialog();
                }

                return false;
            }
        };
    }

    public static void showMarket(Context context) {
        try {
            context.startActivity(getMarketIntent(context));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" +
                    context.getPackageName() + PLAY_STORE_ANALYTICS_CAMPAIGN_PARAMETERS)));
        }
    }

    private void showLicensesDialog() {
        LicensesDialogFragment dialog = LicensesDialogFragment.newInstance();
        dialog.show(getFragmentManager(), "LicensesDialog");
    }

    public static Intent getMarketIntent(Context context) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName() +
                PLAY_STORE_ANALYTICS_CAMPAIGN_PARAMETERS)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    private void setEnabledPrefSync(boolean flag) {
        findPreference(LANG_LIST).setEnabled(flag);
        findPreference(THEME_LIST).setEnabled(flag);
        findPreference(AUTH).setEnabled(flag);
        findPreference(SYNC).setEnabled(flag);
        findPreference(DEVICES_LIST).setEnabled(flag);
        findPreference(BOARD_SYNC_PERIOD_LIST).setEnabled(flag);
        findPreference(NOTIFY_BOARD_FILTER_LIST).setEnabled(flag);
    }

    public void startSync() {
        setEnabledPrefSync(false);
    }

    public void finishSync() {
        setEnabledPrefSync(true);
        if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty()) {
            findPreference(SYNC).setSummary("");
            findPreference(AUTH).setSummary(getString(R.string.pref_account_auth_summary_demo));
        } else {

            String syncDate = realmHelper.getSyncMetaData(true).getLastSyncDate();
            if (syncDate.equals(SyncMetaDataModel.DEFAULT_SYNC_DATE))
                findPreference(SYNC).setSummary(getString(R.string.pref_sync_summary));
            else
                findPreference(SYNC).setSummary(getString(R.string.pref_sync_summary) + " " + syncDate);
            findPreference(AUTH).setSummary(getString(R.string.pref_account_auth_summary_work));
        }
    }

    public static PrefMode getPrefMode(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.contains(PREF_WORK_MODE)) {
            if (preferences.getBoolean(PREF_WORK_MODE, false))
                putPrefMode(context, PrefMode.WORK_MODE);
            else
                putPrefMode(context, PrefMode.DEMO_MODE);
            preferences.edit().remove(PREF_WORK_MODE).apply();
        }
        return PrefMode.values()[PreferenceManager.getDefaultSharedPreferences(context).getInt(PrefMode.getPrefString(), PrefMode.UNDEFINED_MODE.ordinal())];
    }

    public static void putPrefMode(Context context, PrefMode prefMode) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(PrefMode.getPrefString(), prefMode.ordinal()).apply();
    }

    public static ThemeMode getThemeMode(Context context) {
        return ThemeMode.values()[PreferenceManager.getDefaultSharedPreferences(context).getInt(ThemeMode.getPrefString(), ThemeMode.LIGHT.ordinal())];
    }

    public static void putThemeMode(Context context, ThemeMode themeMode) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(ThemeMode.getPrefString(), themeMode.ordinal()).commit();
    }

    public static NotifyBoardFilter getNotifyBoardFilterPref(Context context) {
        return NotifyBoardFilter.values()[PreferenceManager.getDefaultSharedPreferences(context).getInt(NOTIFY_BOARD_FILTER_LIST, DEFAULT_NOTIFY_BOARD_FILTER_PREF_ITEM)];
    }

    public static Integer getBoardSyncPeriodDaysPref(Context context) {
        switch (BoardSyncPeriodType.values()[PreferenceManager.getDefaultSharedPreferences(context).getInt(BOARD_SYNC_PERIOD_LIST, DEFAULT_BOARD_SYNC_PERIOD_PREF_ITEM)]) {
            case DAYS_30:
                return 30;
            case DAYS_60:
                return 60;
            case DAYS_120:
                return 120;
            default:
                return null;
        }
    }

}
