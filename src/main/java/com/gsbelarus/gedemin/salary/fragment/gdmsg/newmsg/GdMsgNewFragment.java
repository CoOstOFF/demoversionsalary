package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;

import io.realm.Realm;

public class GdMsgNewFragment extends BaseFragment {

    private TextView tvTitle;
    private EditText etComment;
    private FrameLayout flContent;

    private RealmHelper realmHelper;

    @Override
    final protected int getLayoutResource() {
        return R.layout.fragment_gdmsg_new;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setHasOptionsMenu(true);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
        etComment = (EditText) rootView.findViewById(R.id.et_comment);
        flContent = (FrameLayout) rootView.findViewById(R.id.fl_content);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    protected void setContentView(@LayoutRes int viewResource) {
        if (flContent != null)
            LayoutInflater.from(flContent.getContext()).inflate(viewResource, flContent, true);
    }

    protected void setTitleFragment(CharSequence text) {
        if (tvTitle != null)
            tvTitle.setText(text);
    }

    protected void setTitleFragment(@StringRes int textResource) {
        if (tvTitle != null)
            tvTitle.setText(textResource);
    }

    protected String getComment() {
        if (etComment != null)
            return etComment.getText().toString();
        return "";
    }

    protected EditText getCommentView() {
        return etComment;
    }

    protected TextView getTitleView() {
        return tvTitle;
    }

    protected RealmHelper getRealmHelper() {
        return realmHelper;
    }
}
