package com.gsbelarus.gedemin.salary.fragment.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.webkit.WebView;

import com.gsbelarus.gedemin.salary.R;

public class LicensesDialogFragment extends DialogFragment {

    public static LicensesDialogFragment newInstance() {
        return new LicensesDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        WebView view = (WebView) LayoutInflater.from(getActivity()).inflate(R.layout.dialog_license, null);
        view.loadUrl("file:///android_asset/open_source_licenses.html");

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.pref_info_licenses))
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }
}
