package com.gsbelarus.gedemin.salary.fragment.gdmsg;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;

import io.realm.Realm;

public class GdMsgDetailFragment extends BaseFragment {

    private static final String TAG_GD_MSG_ID = "tag_gd_msg_id";

    private Integer gdMsgId;
    private RealmHelper realmHelper;

    public static GdMsgDetailFragment newInstance(GdMsgModel gdMsg) {
        Bundle args = new Bundle();

        args.putInt(TAG_GD_MSG_ID, gdMsg.getUid());

        GdMsgDetailFragment fragment = new GdMsgDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_gdmsg_detail;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        if (BundleHelper.containsArguments(getArguments(), TAG_GD_MSG_ID))
            gdMsgId = BundleHelper.get(getArguments(), TAG_GD_MSG_ID);

        else if (BundleHelper.containsArguments(savedInstanceState, TAG_GD_MSG_ID))
            gdMsgId = BundleHelper.get(savedInstanceState, TAG_GD_MSG_ID);

        GdMsgModel gdMsg = realmHelper.getObjectByUid(GdMsgModel.class, gdMsgId);

        if (gdMsg != null) {
            ImageView avatarImageView = (ImageView) rootView.findViewById(R.id.iv_avatar);
            TextView typeTextView = (TextView) rootView.findViewById(R.id.txt_type);
            TextView dateTextView = (TextView) rootView.findViewById(R.id.txt_date);
            TextView subjectTextView = (TextView) rootView.findViewById(R.id.txt_subject);
            TextView msgTextView = (TextView) rootView.findViewById(R.id.txt_msg);
            TextView titleReplyTextView = (TextView) rootView.findViewById(R.id.tv_reply_title);
            TextView msgReplyTextView = (TextView) rootView.findViewById(R.id.tv_reply_msg);
            TextView titleCommentTextView = (TextView) rootView.findViewById(R.id.txt_title_comment);
            TextView commentTextView = (TextView) rootView.findViewById(R.id.txt_comment);

            //TODO setImageDrawable
            avatarImageView.setImageResource(gdMsg.getSenderIconRes());
            ((GradientDrawable) avatarImageView.getBackground()).setColor(gdMsg.getRequestStatusColor());

            typeTextView.setText(gdMsg.getSenderName());
            dateTextView.setText(gdMsg.getLongFormattedTimestamp());
            subjectTextView.setText(gdMsg.getSubjectName());

            String status = gdMsg.getRequestStatusName();
            String data = gdMsg.getData();
            if (!data.isEmpty())
                data += ". ";
            if (!status.isEmpty() && !data.isEmpty())
                status += "\n";

            String msg = status + data;
            if (!msg.isEmpty()) {
                msgTextView.setText(Html.fromHtml(msg));
                msgTextView.setVisibility(View.VISIBLE);
            } else {
                msgTextView.setVisibility(View.GONE);
            }

            if (!gdMsg.getComment().isEmpty()) {
                commentTextView.setText(Html.fromHtml(gdMsg.getComment()));

                titleCommentTextView.setVisibility(View.VISIBLE);
                commentTextView.setVisibility(View.VISIBLE);
            } else {
                titleCommentTextView.setVisibility(View.GONE);
                commentTextView.setVisibility(View.GONE);
            }

            GdMsgModel requestGdMsg = gdMsg.getRequestGdMsg();
            if (requestGdMsg != null) {
                titleReplyTextView.setVisibility(View.VISIBLE);
                msgReplyTextView.setVisibility(View.VISIBLE);

                titleReplyTextView.setText(getResources().getString(R.string.reply_title, requestGdMsg.getLongFormattedTimestamp()));

                String requestData = requestGdMsg.getData();
                if (!requestData.isEmpty()) {
                    requestData += ". ";
                }
                msgReplyTextView.setText(Html.fromHtml(requestData + requestGdMsg.getComment()));
            } else {
                titleReplyTextView.setVisibility(View.GONE);
                msgReplyTextView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (gdMsgId != null)
            outState.putInt(TAG_GD_MSG_ID, gdMsgId);
    }
}
