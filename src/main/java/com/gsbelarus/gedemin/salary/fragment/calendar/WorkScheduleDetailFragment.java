package com.gsbelarus.gedemin.salary.fragment.calendar;

import android.os.Bundle;
import android.view.View;

import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.WorkScheduleModel;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.realm.Realm;

public class WorkScheduleDetailFragment extends CalendarDetailFragment {

    private RealmHelper realmHelper;

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        WorkScheduleModel workScheduleModel = realmHelper.getObjectByUid(WorkScheduleModel.class, getDayId());

        String day = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(workScheduleModel.getDate());

        if (!day.isEmpty())
            getTvDay().setText(day.substring(0, 1).toUpperCase() + day.substring(1).toLowerCase());
        getTvDayType().setText(DayType.getName(workScheduleModel.getDayType()).toUpperCase());

        getEventAdapter().setItems(workScheduleModel.getEvents());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }
}
