package com.gsbelarus.gedemin.salary.fragment.registration;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.ui.BackEditText;

import io.realm.Realm;

abstract public class RegistrationPagerItemFragment extends BaseFragment {

    public static final String TAG_LOGIN_BUTTON = "login_button";
    public static final String TAG_NEXT_BUTTON = "next_button";
    public static final String TAG_PREV_BUTTON = "prev_button";

    private RealmHelper realmHelper;

    private FrameLayout regContent;

    private OnNavButtonClickListener onNavButtonClickListener;
    protected OnScanQrCodeListener onScanQrCodeListener;

    abstract public boolean validatePageInputs();

    abstract public void savePageFields();

    abstract protected void loadPageFields(View rootView, @Nullable Bundle bundle);

    @Override
    final protected int getLayoutResource() {
        return R.layout.fragment_registration_item;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        regContent = (FrameLayout) rootView.findViewById(R.id.reg_content_layout);
        Button loginButtonVP = (Button) rootView.findViewById(R.id.login_button_vp);
        Button nextPageVP = (Button) rootView.findViewById(R.id.next_page_vp);
        Button prevPageVP = (Button) rootView.findViewById(R.id.prev_page_vp);

        if (BundleHelper.containsArguments(getArguments(), TAG_LOGIN_BUTTON))
            loginButtonVP.setVisibility(BundleHelper.<Boolean>get(getArguments(), TAG_LOGIN_BUTTON) ? View.VISIBLE : View.INVISIBLE);
        if (BundleHelper.containsArguments(getArguments(), TAG_NEXT_BUTTON))
            nextPageVP.setVisibility(BundleHelper.<Boolean>get(getArguments(), TAG_NEXT_BUTTON) ? View.VISIBLE : View.INVISIBLE);
        if (BundleHelper.containsArguments(getArguments(), TAG_PREV_BUTTON))
            prevPageVP.setVisibility(BundleHelper.<Boolean>get(getArguments(), TAG_PREV_BUTTON) ? View.VISIBLE : View.INVISIBLE);

        loginButtonVP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNavButtonClickListener != null)
                    onNavButtonClickListener.onClick(v, TAG_LOGIN_BUTTON);
            }
        });
        nextPageVP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNavButtonClickListener != null)
                    onNavButtonClickListener.onClick(v, TAG_NEXT_BUTTON);
            }
        });
        prevPageVP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNavButtonClickListener != null)
                    onNavButtonClickListener.onClick(v, TAG_PREV_BUTTON);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (SettingsFragment.getPrefMode(getContext()) != SettingsFragment.PrefMode.DEMO_MODE)
            loadPageFields(getView(), null);
    }

    public void reloadPageFields(Bundle bundle) {
        if (getView() != null)
            loadPageFields(getView(), bundle);
    }

    protected void setContentView(@LayoutRes int resource) {
        if (regContent != null) {
            View view = getBaseActivity().getLayoutInflater().inflate(resource, regContent, false);
            if (view != null) {
                regContent.removeAllViews();
                regContent.addView(view);
            }
        }
    }

    protected void setupEditText(final @NonNull BackEditText editText, final Validator validator) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    editText.setSelection(editText.getText().length());
            }
        });
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                    RegistrationPagerFragment.hideAppBar(getBaseActivity());
                return false;
            }
        });
        editText.setOnBackPressedListener(new BackEditText.OnBackPressedListener() {
            @Override
            public void onBackPressed() {
                RegistrationPagerFragment.showAppBar(getBaseActivity());
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (validator == null) return false;

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validator.validate(editText)) {
                        RegistrationPagerFragment.hideKeyboard(getBaseActivity());
                        RegistrationPagerFragment.showAppBar(getBaseActivity());
                    }
                    return true;

                } else if (actionId == EditorInfo.IME_ACTION_NEXT)
                    return !validator.validate(editText);

                return false;
            }
        });
    }

    protected void setupEditText(final @NonNull BackEditText editText, boolean validateOnTextChanged, final Validator validator) {
        setupEditText(editText, validator);

        if (validator == null) return;

        if (validateOnTextChanged)
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    validator.validate(editText);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
    }

    protected void setTextToEdit(EditText editText, String text) {
        if (text != null && !text.isEmpty())
            editText.setText(text);
    }

    public void setOnNavButtonClickListener(OnNavButtonClickListener onNavButtonClickListener) {
        this.onNavButtonClickListener = onNavButtonClickListener;
    }

    public void setOnScanQrCodeListener(OnScanQrCodeListener onScanQrCodeListener) {
        this.onScanQrCodeListener = onScanQrCodeListener;
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected RealmHelper getRealmHelper() {
        return realmHelper;
    }

    public interface Validator {
        boolean validate(EditText editText);
    }

    public interface OnNavButtonClickListener {
        void onClick(View view, String tag);
    }

    public interface OnScanQrCodeListener {
        void onScanQrCode();
    }
}
