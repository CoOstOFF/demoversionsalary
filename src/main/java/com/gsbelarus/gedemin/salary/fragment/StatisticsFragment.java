package com.gsbelarus.gedemin.salary.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.DatePickerDialog;
import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.interfaces.OnDateSet;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.activity.StatisticDetail;
import com.gsbelarus.gedemin.salary.adapter.StatisticCardsAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.StatisticPayslip;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipFragment;
import com.gsbelarus.gedemin.salary.ui.AutofitEmptyRecyclerView;
import com.gsbelarus.gedemin.salary.ui.StatisticCardDecoration;
import com.gsbelarus.gedemin.salary.ui.TextDrawable;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import java.util.Calendar;
import java.util.List;

import io.realm.Realm;

public class StatisticsFragment extends BaseFragment implements
        OnDateSet,
        CurrencyHelper.OnCurrencySwitcherChanged,
        StatisticCardsAdapter.OnItemClickListener {

    public static final String TAG_FIRST_DATE = "tag_first_date";
    public static final String TAG_SECOND_DATE = "tag_second_date";

    private DatePickerDialog dialog;
    private Calendar firstDate;
    private Calendar secondDate;

    private TextView firstDateButton;
    private TextView secondDateButton;

    private CurrencyHelper currencyHelper;
    private RealmHelper realmHelper;

    private StatisticCardsAdapter adapterCardsRecView;

    private Button workModeButton;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_statistics;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setHasOptionsMenu(true);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        currencyHelper = CurrencyHelper.getInstance(getContext());
        currencyHelper.addOnCurrencySwitcherChanged(this);
        adapterCardsRecView = new StatisticCardsAdapter(getActivity(), getContext(), realmHelper);

        firstDateButton = (TextView) rootView.findViewById(R.id.statistic_dates_first_date_button);
        secondDateButton = (TextView) rootView.findViewById(R.id.statistic_dates_second_date_button);

        firstDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setDate(firstDate);
                dialog.show(TAG_FIRST_DATE);
            }
        });
        secondDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setDate(secondDate);
                dialog.show(TAG_SECOND_DATE);
            }
        });

        AutofitEmptyRecyclerView cardsRecView = (AutofitEmptyRecyclerView) rootView.findViewById(R.id.payslip_statistic_cards);
        final GridLayoutManager manager = new GridLayoutManager(getContext(), 1);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (adapterCardsRecView.getItemViewType(position) == StatisticCardsAdapter.ItemTypes.HEADER.ordinal()) ?
                        manager.getSpanCount() : 1;
            }
        });
        cardsRecView.setLayoutManager(manager);
        cardsRecView.setColumnWidth(getActivity().getResources().getDimension(R.dimen.statistic_card_width));
        cardsRecView.addItemDecoration(new StatisticCardDecoration(getContext(), 2f));
        cardsRecView.setAdapter(adapterCardsRecView);

        adapterCardsRecView.setOnItemClickListener(this);
        cardsRecView.setEmptyView(rootView.findViewById(R.id.empty_rec_view_statistics));

        workModeButton = (Button) rootView.findViewById(R.id.payslip_statistics_work_mode_button);
        workModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RegistrationPager.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        currencyHelper.removeOnCurrencySwitcherChanged(this);
        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View view) {
        List<Calendar> dates = realmHelper.getPayslipPossibleDatesList();

        firstDate = dates.get(0);
        secondDate = dates.get(dates.size() - 1);

        /** отображаем данные на год */
        Calendar cal = (Calendar) secondDate.clone();
        cal.add(Calendar.MONTH, -11);
        if (firstDate.before(cal))
            firstDate = cal;

        dialog = new DatePickerDialog(getActivity(), dates, Calendar.MONTH);
        dialog.setOnDateSetListeners(this);

        choiceDate(firstDateButton, firstDate);
        choiceDate(secondDateButton, secondDate);

        initCardsRecView();

        if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty())
            workModeButton.setVisibility(View.VISIBLE);
        else
            workModeButton.setVisibility(View.GONE);
    }

    private void initCardsRecView() {
        Calendar leftDate = (Calendar) firstDate.clone();
        Calendar rightDate = (Calendar) secondDate.clone();

        CalendarHelper.initCalendarFields(leftDate, Calendar.MONTH, 1);
        CalendarHelper.initCalendarFields(rightDate, Calendar.MONTH, 1);
        if (firstDate.after(secondDate)) {
            leftDate = secondDate;
            rightDate = firstDate;
        }

        List<StatisticPayslip> statisticPayslips = realmHelper.getPayslipStatistics(leftDate, rightDate, false);
        Calendar beforeFirstDate = (Calendar) leftDate.clone();
        beforeFirstDate.add(Calendar.MONTH, -1);

        Double startSum = null;
        PayslipModel payslipModel = realmHelper.getPayslip(beforeFirstDate);
        if (payslipModel != null)
            startSum = realmHelper.getDebitSum(payslipModel);

        adapterCardsRecView.setStartSum(startSum);
        adapterCardsRecView.setList(statisticPayslips);
        if (statisticPayslips.size() > 1)
            adapterCardsRecView.showHeader(null);
        else
            adapterCardsRecView.hideHeader();
    }

    @Override
    public void updateDate(String tag, int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        switch (tag) {
            case TAG_FIRST_DATE:
                firstDate = cal;
                break;
            case TAG_SECOND_DATE:
                secondDate = cal;
                break;
            case DatePickerDialog.DEFAULT_TAG:
                LogUtil.d("tag must not be " + DatePickerDialog.DEFAULT_TAG);
                return;
        }

        choiceDate(firstDateButton, firstDate);
        choiceDate(secondDateButton, secondDate);

        initCardsRecView();
    }

    @SuppressLint("SetTextI18n")
    private void choiceDate(TextView dateTextView, Calendar date) {
        dateTextView.setText(CalendarHelper.getShortMonthName(getContext(), date.get(Calendar.MONTH)).toUpperCase() + " " + date.get(Calendar.YEAR));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (firstDate != null && secondDate != null) {
            outState.putSerializable(TAG_FIRST_DATE, firstDate);
            outState.putSerializable(TAG_SECOND_DATE, secondDate);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (BundleHelper.containsArguments(savedInstanceState, TAG_FIRST_DATE, TAG_SECOND_DATE)) {
            firstDate = BundleHelper.get(savedInstanceState, TAG_FIRST_DATE);
            secondDate = BundleHelper.get(savedInstanceState, TAG_SECOND_DATE);
            choiceDate(firstDateButton, firstDate);
            choiceDate(secondDateButton, secondDate);
            initCardsRecView();
        }
    }

    @Override
    public void onClick(View view, StatisticPayslip item) {
        Intent intent = new Intent(getContext(), StatisticDetail.class);
        intent.putExtra(PayslipFragment.TAG_DATE, item.getDate());
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    private BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onChanged(CurrencyHelper.Kind kind) {
        initCardsRecView();
        changeIcon(getBaseActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.currency_switcher, menu);
        changeIcon(getBaseActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_currency_switch:
                currencyHelper.showChoiceDialog(getActivity(), null);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeIcon(BaseActivity activity) {
        if (activity != null && activity.getToolbar() != null && activity.getToolbar().getMenu() != null) {
            MenuItem item = activity.getToolbar().getMenu().findItem(R.id.action_currency_switch);
            if (item != null)
                item.setIcon(new TextDrawable(getResources(), currencyHelper.getSymbolCurrencyText("")));
        }
    }
}
