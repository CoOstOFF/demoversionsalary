package com.gsbelarus.gedemin.salary.fragment.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;

public class RateItDialogFragment extends DialogFragment {

    private static final int LAUNCHES_UNTIL_PROMPT = 35;
    private static final String PREF_NAME = "APP_RATER";
    private static final String LAUNCHES = "LAUNCHES";
    private static final String DISABLED = "DISABLED";

    @SuppressLint("CommitPrefEdits")
    public static void show(Context context, FragmentManager fragmentManager) {
        boolean shouldShow = false;
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        LogUtil.dtag("RateDialog", sharedPreferences.getBoolean(DISABLED, false));
        if (!sharedPreferences.getBoolean(DISABLED, false)) {
            int launches = sharedPreferences.getInt(LAUNCHES, 0) + 1;
            LogUtil.dtag("RateDialog", "launches = " + launches);
            if (launches > LAUNCHES_UNTIL_PROMPT) {
                    shouldShow = true;
            }
            editor.putInt(LAUNCHES, launches);
        }

        if (shouldShow) {
            new RateItDialogFragment().show(fragmentManager, null);
        } else {
            editor.commit();
        }
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, 0);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialogWrapper.Builder(getActivity())
                .setTitle(R.string.rate_title)
                .setMessage(R.string.rate_content)
                .setPositiveButton(R.string.rate_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getActivity().getApplicationContext().getPackageName() +
                                SettingsFragment.PLAY_STORE_ANALYTICS_CAMPAIGN_PARAMETERS)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        getSharedPreferences(getActivity()).edit().putBoolean(DISABLED, true).commit();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.rate_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getSharedPreferences(getActivity()).edit().putBoolean(DISABLED, true).commit();
                        dialog.dismiss();
                    }
                })
                .setNeutralButton(R.string.rate_neutral, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getSharedPreferences(getActivity()).edit().putInt(LAUNCHES, 0).commit();
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
