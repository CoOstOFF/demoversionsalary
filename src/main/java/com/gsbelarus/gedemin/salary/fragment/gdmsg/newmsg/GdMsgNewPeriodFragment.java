package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

public abstract class GdMsgNewPeriodFragment extends GdMsgNewFragment implements DatePickerDialog.OnDateSetListener {

    public enum DatePickerType {

        DATE_BEGIN(TAG_DATE_PICKER_BEGIN),
        DATE_END(TAG_DATE_PICKER_END);

        private String tag;

        DatePickerType(String tag) {
            this.tag = tag;
        }
    }

    public static final String TAG_DATE_PICKER_BEGIN = "date_picker_begin";
    public static final String TAG_DATE_PICKER_END = "date_picker_end";

    private Date dateBegin;
    private Date dateEnd;

    protected abstract void onDateChanged(DatePickerType type);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BundleHelper.containsArguments(savedInstanceState, TAG_DATE_PICKER_BEGIN, TAG_DATE_PICKER_END)) {
            dateBegin = BundleHelper.get(savedInstanceState, TAG_DATE_PICKER_BEGIN);
            dateEnd = BundleHelper.get(savedInstanceState, TAG_DATE_PICKER_END);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        DatePickerType type = null;

        switch (view.getTag()) {
            case TAG_DATE_PICKER_BEGIN:
                calendar.setTime(dateBegin);
                calendar.set(year, monthOfYear, dayOfMonth);
                dateBegin.setTime(calendar.getTimeInMillis());
                type = DatePickerType.DATE_BEGIN;

                break;
            case TAG_DATE_PICKER_END:
                calendar.setTime(dateEnd);
                calendar.set(year, monthOfYear, dayOfMonth);
                dateEnd.setTime(calendar.getTimeInMillis());
                type = DatePickerType.DATE_END;

                break;
        }
        onDateChanged(type);
    }

    protected DatePickerDialog showDatePickerDialog(DatePickerType type) {
        switch (type) {
            case DATE_BEGIN:
                return showDatePickerDialog(type.tag, dateBegin);
            case DATE_END:
                return showDatePickerDialog(type.tag, dateEnd);
            default:
                return null;
        }
    }

    /**
     * если используется кастомный тег, нужно восстанавливать callback после смены ориентации с открытым диалогом
     * <br> либо вызвать {@code dialog.dismissOnPause()}
     */
    protected DatePickerDialog showDatePickerDialog(String tag, Date date) {
        Calendar bufCal = Calendar.getInstance();
        bufCal.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this,
                bufCal.get(Calendar.YEAR), bufCal.get(Calendar.MONTH), bufCal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.vibrate(false);
        datePickerDialog.show(getActivity().getFragmentManager(), tag);
        return datePickerDialog;
    }

    protected void restoreCallbackDatePicker(String tag) {
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag(tag);
        if (dpd != null)
            dpd.setOnDateSetListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        restoreCallbackDatePicker(TAG_DATE_PICKER_BEGIN);
        restoreCallbackDatePicker(TAG_DATE_PICKER_END);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(TAG_DATE_PICKER_BEGIN, dateBegin);
        outState.putSerializable(TAG_DATE_PICKER_END, dateEnd);
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }
}
