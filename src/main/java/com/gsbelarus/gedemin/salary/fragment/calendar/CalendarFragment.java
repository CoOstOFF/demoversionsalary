package com.gsbelarus.gedemin.salary.fragment.calendar;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.activity.BaseNavigationDrawerActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.adapter.calendar.CalendarDetailFragmentAdapter;
import com.gsbelarus.gedemin.salary.adapter.calendar.TimesheetDetailFragmentAdapter;
import com.gsbelarus.gedemin.salary.adapter.calendar.WorkScheduleDetailFragmentAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.ui.calendar.CalendarDayViewDecorator;
import com.gsbelarus.gedemin.salary.ui.calendar.TimesheetDayViewDecorator;
import com.gsbelarus.gedemin.salary.ui.calendar.WorkScheduleDayViewDecorator;
import com.gsbelarus.gedemin.salary.util.GdMsgNewHelper;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateChangedListener;
import com.prolificinteractive.materialcalendarview.OnLongClickListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import io.realm.Realm;

public class CalendarFragment extends BaseFragment implements
        OnDateChangedListener, DatePickerDialog.OnDateSetListener, OnLongClickListener, View.OnKeyListener {

    private static final String TAG_CALENDAR_TYPE = "calendar_type_tag";
    private static final String TAG_DATE_PICKER = "date_picker_tag";

    public enum CalendarType {WORK_SCHEDULE, TIMESHEET}

    private RealmHelper realmHelper;
    private MaterialCalendarView calendarView;
    private ViewPager daysViewPager;
    private ViewGroup toolbarTitleLayout;
    private SlidingUpPanelLayout slidingLayout;

    private Calendar minDate;
    private Calendar maxDate;

    private CalendarDetailFragmentAdapter adapter;
    private CalendarDayViewDecorator dayViewDecorator;
    private CalendarType calendarType = CalendarType.WORK_SCHEDULE;

    private int scrollFlagsToolbar = 0;

    private ViewPager.OnPageChangeListener viewPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            calendarView.setSelectedDate(adapter.getCalendarByPosition(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    public static CalendarFragment getInstance(CalendarType calendarType) {
        CalendarFragment calendarFragment = new CalendarFragment();
        calendarFragment.setArguments(new BundleHelper.Builder()
                .putSerializable(TAG_CALENDAR_TYPE, calendarType)
                .build());
        return calendarFragment;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_calendar;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        if (savedInstanceState == null)
            initCalendarType(getArguments());
        else
            initCalendarType(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        realmHelper.getRealm().close();
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        rootView.setFocusableInTouchMode(true);
        rootView.setOnKeyListener(this);
        rootView.requestFocus();

        if (getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getSupportActionBar() != null &&
                getBaseNavigationDrawerActivity().getToolbar() != null) {
            getBaseNavigationDrawerActivity().getToolbar().addView(toolbarTitleLayout = createToolbarTitle());
            getBaseNavigationDrawerActivity().getSupportActionBar().setDisplayShowTitleEnabled(false);
            getBaseNavigationDrawerActivity().setVisibilityAppBarShadow(false);
            scrollFlagsToolbar = getBaseNavigationDrawerActivity().getScrollFlagsToolbar();
            getBaseNavigationDrawerActivity().setScrollFlagsToolbar(0);
        }

        slidingLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);

        calendarView = (MaterialCalendarView) rootView.findViewById(R.id.material_calendar_view);
        calendarView.setSelectedDate(Calendar.getInstance());
        calendarView.setTitleLayoutEnabled(false);
        calendarView.setOnDateChangedListener(this);
        calendarView.setOnLongClickListener(this);
        calendarView.setMinimumDate(minDate);
        calendarView.setMaximumDate(maxDate);

        daysViewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        daysViewPager.addOnPageChangeListener(viewPagerListener);

        setDateToolbar(calendarView.getSelectedDate().getMonth(), calendarView.getSelectedDate().getYear());

        switch (calendarType) {
            case WORK_SCHEDULE:
                adapter = new WorkScheduleDetailFragmentAdapter(getFragmentManager(), realmHelper);
                dayViewDecorator = new WorkScheduleDayViewDecorator(getContext(), realmHelper);
                break;
            case TIMESHEET:
                adapter = new TimesheetDetailFragmentAdapter(getFragmentManager(), realmHelper);
                dayViewDecorator = new TimesheetDayViewDecorator(getContext(), realmHelper);
                break;
        }
        daysViewPager.setAdapter(adapter);
        setViewPagerDate(calendarView.getSelectedDate().getCalendar());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getSupportActionBar() != null &&
                getBaseNavigationDrawerActivity().getToolbar() != null) {
            getBaseNavigationDrawerActivity().getToolbar().removeView(toolbarTitleLayout);
            getBaseNavigationDrawerActivity().getSupportActionBar().setDisplayShowTitleEnabled(true);
            getBaseNavigationDrawerActivity().setVisibilityAppBarShadow(true);
            getBaseNavigationDrawerActivity().setScrollFlagsToolbar(scrollFlagsToolbar);
        }

        daysViewPager.removeOnPageChangeListener(viewPagerListener);
    }

    private ViewGroup createToolbarTitle() {
        Toolbar toolbar = getBaseNavigationDrawerActivity().getToolbar();
        if (toolbar != null) {
            ViewGroup toolBarTitleLayout = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.calendar_clickable_toolbar, toolbar, false);

            TextView tvTitle = (TextView) toolBarTitleLayout.findViewById(R.id.title);
            tvTitle.setText(getBaseNavigationDrawerActivity().getToolbar().getTitle());

            TextView tvSubtitle = (TextView) toolBarTitleLayout.findViewById(R.id.subtitle);
            tvSubtitle.getCompoundDrawables()[2].setColorFilter(new PorterDuffColorFilter(tvSubtitle.getCurrentTextColor(), PorterDuff.Mode.MULTIPLY));

            ViewGroup clickableContainer = (ViewGroup) toolBarTitleLayout.findViewById(R.id.clickable_container);
            clickableContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (calendarView != null) {
                        Calendar day = calendarView.getSelectedDate().getCalendar();
                        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(CalendarFragment.this, day.get(Calendar.YEAR), day.get(Calendar.MONTH), day.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.vibrate(false);
                        datePickerDialog.showYearPickerFirst(true);
                        datePickerDialog.show(getActivity().getFragmentManager(), TAG_DATE_PICKER);
                        minDate = datePickerDialog.getMinDate();
                        maxDate = datePickerDialog.getMaxDate();
                    }
                }
            });
            return toolBarTitleLayout;
        }
        return null;
    }

    /**
     * DatePickerDialog
     */
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int day) {
        CalendarDay selectDay = new CalendarDay(year, month, day);
        if (calendarView != null)
            calendarView.setSelectedDate(selectDay);
        setViewPagerDate(selectDay.getCalendar());
        collapseSlidingLayout();
    }

    @Override
    public void initView(View view) {
        calendarView.setDayViewDecorator(dayViewDecorator);
        setViewPagerDate(calendarView.getSelectedDate().getCalendar());
        updateMonthInfo(calendarView.getSelectedDate().getCalendar());
        collapseSlidingLayout();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            setViewPagerDate(calendarView.getSelectedDate().getCalendar());
        }
    }

    @SuppressLint("SetTextI18n")
    private void setDateToolbar(int month, int year) {
        if (toolbarTitleLayout != null) {
            TextView tv = (TextView) toolbarTitleLayout.findViewById(R.id.subtitle);
            tv.setText(CalendarHelper.getMonthName(getActivity().getApplicationContext(), month) + " " + year);
        }
    }

    private void updateMonthInfo(Calendar date) {
        if (getRootView() != null) {
            CalendarDetailFragmentAdapter.MonthInfo monthInfo = adapter.getMonthInfo(date);
            int workingDaysCount = monthInfo.getWorkingDaysCount();
            double workingHours = monthInfo.getWorkingHours();
            TextView tvMonthInfo = (TextView) getRootView().findViewById(R.id.tv_month_info);
            if (tvMonthInfo != null) {
                if (workingDaysCount == 0 && workingHours == 0)
                    tvMonthInfo.setText("");
                else
                    tvMonthInfo.setText(getContext().getString(monthInfo.getDescription(), workingDaysCount, getHoursString(workingHours)));
            }
        }
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        setDateToolbar(date.getMonth(), date.getYear());
        updateMonthInfo(date.getCalendar());
        collapseSlidingLayout();
    }

    @Override
    public void onDateChanged(MaterialCalendarView widget, CalendarDay date) {
        setViewPagerDate(date.getCalendar());
        collapseSlidingLayout();
    }

    @Override
    public void onLongClick(CalendarDay day) {
        if (calendarType == CalendarType.WORK_SCHEDULE) {
            if (day.isBefore(new CalendarDay()))
                GdMsgNewHelper.showGdMsgNewDialog(getActivity(), day.getCalendar(), getContext(),
                        GdMsgModel.Subject.INCOME);
            else
                GdMsgNewHelper.showGdMsgNewDialog(getActivity(), day.getCalendar(), getContext(),
                        GdMsgModel.Subject.VACATION,
                        GdMsgModel.Subject.ABSENCE);
        }
    }

    private void setViewPagerDate(Calendar date) {
        int index = adapter.getPositionByDate(date);
        if (index != CalendarDetailFragmentAdapter.UNDEFINED_INDEX) {
            boolean animate = true;
            if (adapter.isShowEmpty()) {
                animate = false;
                adapter.setShowEmpty(false);
                adapter.notifyDataSetChanged();
            }
            daysViewPager.removeOnPageChangeListener(viewPagerListener);
            daysViewPager.setCurrentItem(index, animate);
            daysViewPager.addOnPageChangeListener(viewPagerListener);
        } else {
            adapter.setShowEmpty(true);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (slidingLayout != null && slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                collapseSlidingLayout();
                return true;
            }
        }
        return false;
    }

    private void collapseSlidingLayout() {
        if (slidingLayout != null)
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_today:
                calendarView.setSelectedDate(Calendar.getInstance());
                setViewPagerDate(Calendar.getInstance());
                collapseSlidingLayout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initCalendarType(Bundle bundle) {
        if (BundleHelper.containsArguments(bundle, TAG_CALENDAR_TYPE))
            calendarType = BundleHelper.get(bundle, TAG_CALENDAR_TYPE);
        else
            calendarType = CalendarType.WORK_SCHEDULE;
    }

    private String getHoursString(double hours) {
        if (hours - (int) hours > 0)
            return String.valueOf(hours);
        else
            return String.valueOf((int) hours);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(TAG_CALENDAR_TYPE, calendarType);
    }

    @Override
    public void onResume() {
        super.onResume();

        restoreCallbackPicker(TAG_DATE_PICKER);

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    private void restoreCallbackPicker(String tag) {
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag(tag);
        if (dpd != null)
            dpd.setOnDateSetListener(this);
    }

    private BaseNavigationDrawerActivity getBaseNavigationDrawerActivity() {
        return (BaseNavigationDrawerActivity) getActivity();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.calendar, menu);
    }
}
