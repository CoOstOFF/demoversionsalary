package com.gsbelarus.gedemin.salary.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.adapter.LinearLayoutAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.DrawerPersonInfoItem;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import io.realm.Realm;

public class AboutEmployeeFragment extends BaseFragment {

    private LinearLayoutAdapter adapter;
    private TextView nameView;
    private RealmHelper realmHelper;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_about_employee;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        nameView = (TextView) rootView.findViewById(R.id.name_text_view);

        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.about_employee_content_view);
        adapter = new LinearLayoutAdapter(linearLayout);
        adapter.setOnItemLongClickListener(new LinearLayoutAdapter.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(View view, int position) {

                View RootView = getActivity().getWindow().getDecorView().getRootView().findViewById(android.R.id.content);
                LogUtil.d("Snackbar" + RootView);

                LogUtil.dtag("Snackbar", "personInfoText " + adapter.getItem(position).getPersonInfoItem(),
                        "personInfoItem " + adapter.getItem(position).getPersonInfoItem());
                clipboard(adapter.getItem(position).getPersonInfoItem());

                LogUtil.d("Snackbar" + getContext().getString(R.string.person_made_copy, adapter.getItem(position)
                        .getPersonInfoItem()));
                Snackbar.make(RootView, getContext().getString(R.string.person_made_copy, adapter.getItem(position)
                        .getPersonInfoItem()), Snackbar.LENGTH_SHORT).show();

                return true;
            }

            private void clipboard(String message_copy) {

                ClipboardManager clipboard = (ClipboardManager) getActivity().getApplicationContext()
                        .getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", message_copy);
                clipboard.setPrimaryClip(clip);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View view) {
        PayslipModel lastPayslipModel = realmHelper.getLastPayslip();

        String name;
        String listNumber;
        String department;
        String position;
        String titleSum;
        String sum;
        String passportId;

        if (lastPayslipModel == null) {

            EmployeeModel employeeModel = realmHelper.getLastEmployee(true);
            if (!realmHelper.getSyncMetaData(true).getAuthKey().isEmpty() &&
                    !employeeModel.getFirstname().isEmpty() &&
                    !employeeModel.getSurname().isEmpty()) // для рабочих данных
                name = employeeModel.getFirstname() + " " + employeeModel.getSurname();
            else
                name = getString(R.string.navview_header_profile_name_default);
            listNumber = "...";
            department = "...";
            position = "...";
            titleSum = getString(R.string.person_salary);
            sum = "...";
            passportId = "...";

        } else {
            name = lastPayslipModel.getEmployee().getFirstname() + " " +
                    lastPayslipModel.getEmployee().getSurname();
            listNumber = lastPayslipModel.getEmployee().getListNumber();
            department = lastPayslipModel.getDepartment();
            position = lastPayslipModel.getPosition();

            CurrencyHelper currencyHelper = CurrencyHelper.getInstance(getContext());
            if (lastPayslipModel.getSalary() == 0) {
                titleSum = getString(R.string.person_hour_rate);
                sum = currencyHelper.getFormattedSum(lastPayslipModel.getHourRate()) + " руб."; //TODO res
            } else {
                titleSum = getString(R.string.person_salary);
                sum = currencyHelper.getFormattedSum(lastPayslipModel.getSalary()) + " руб.";
            }

            passportId = lastPayslipModel.getEmployee().getPassportId();
        }

        adapter.clear();
        adapter.add(new DrawerPersonInfoItem(getString(R.string.person_department),
                department, R.drawable.ic_pin_drop_36dp))
                .add(new DrawerPersonInfoItem(getString(R.string.person_position),
                        position, R.drawable.ic_readability_36dp))
                .add(new DrawerPersonInfoItem(titleSum,
                        sum, R.drawable.ic_coin_36dp))
                .add(new DrawerPersonInfoItem(getString(R.string.person_number),
                        listNumber, R.drawable.ic_assignment_ind_36dp))
                .add(new DrawerPersonInfoItem((getString(R.string.person_passport_id)),
                        passportId, R.drawable.ic_account_key_36dp));
        nameView.setText(name);
        adapter.notifyDateChange();
    }
}
