package com.gsbelarus.gedemin.salary.fragment.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.RectF;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.analytics.InstallTracker;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.ui.camera.BarcodeGraphic;
import com.gsbelarus.gedemin.salary.ui.camera.CameraSourcePreview;
import com.gsbelarus.gedemin.salary.ui.camera.GraphicOverlay;
import com.gsbelarus.gedemin.salary.ui.camera.ViewFinderView;
import com.gsbelarus.gedemin.salary.util.RxPermissions;
import com.gsbelarus.gedemin.salary.vision.BarcodeTrackerFactory;
import com.gsbelarus.gedemin.salary.vision.VisionCallback;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

import rx.functions.Action1;

public class QrCodeCaptureFragment extends BaseFragment {

    //    // intent request code to handle updating play services if needed.
//    private static final int REQUEST_CODE_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int REQUEST_CODE_HANDLE_CAMERA_PERM = 2;
    private static final String QR_URL_EMPLOYEE_MODEL_PARAM = "empl";
    private static final String QR_URL_SERVER_PARAM = "server";       //TODO временно здесь company name !!!
    private static final String EXTRA_KEY_FLASH_ENABLED = "flash_enabled";

    private ViewFinderView viewFinderView;
    private CameraSource cameraSource;
    private CameraSourcePreview cameraSourcePreview;
    private GraphicOverlay<BarcodeGraphic> graphicOverlay;
    private Barcode prevBarcode;
    private boolean scanning;
    private boolean flashEnabled;
    private float widthScaleFactor;
    private float heightScaleFactor;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_qrcode_capture;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private final RxPermissions.PermissionsRequester mPermissionsRequester = new RxPermissions.PermissionsRequester() {
        @Override
        public void performRequestPermissions(String[] permissions) {
            requestPermissions(permissions, REQUEST_CODE_HANDLE_CAMERA_PERM);
        }
    };

    @Override
    public void onCreateView(View rootView, final Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        viewFinderView = (ViewFinderView) rootView.findViewById(R.id.view_finder);

        FloatingActionButton fabInfo = (FloatingActionButton) rootView.findViewById(R.id.fab_info);
        fabInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                        .setMessage(R.string.qr_code_info)
                        .show();
            }
        });

        cameraSourcePreview = (CameraSourcePreview) rootView.findViewById(R.id.preview);
        graphicOverlay = (GraphicOverlay<BarcodeGraphic>) rootView.findViewById(R.id.graphicOverlay);
        RxPermissions.get(getActivity())
                .request(mPermissionsRequester, Manifest.permission.CAMERA)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean granted) {
                        if (granted) {
                            initBarcodeDetector();
                            if (savedInstanceState == null)
                                Snackbar.make(graphicOverlay, R.string.qrcode_help_focus, Snackbar.LENGTH_LONG).show();

                        } else {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                                Snackbar.make(graphicOverlay, R.string.permission_camera_rationale, Snackbar.LENGTH_INDEFINITE)
                                        .setAction(com.gedemin.gsbelarus.lib.R.string.gdmnlib_dialog_ok, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                mPermissionsRequester.request();
                                            }
                                        })
                                        .show();
                            } else {
                                showOkMaterialDialog(
                                        R.string.app_name,
                                        R.string.no_camera_permission,
                                        new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                getActivity().finish();
                                            }
                                        });
                            }
                        }
                    }
                });

//        flashEnabled = savedInstanceState != null && savedInstanceState.getBoolean(EXTRA_KEY_FLASH_ENABLED);
        flashEnabled = false;
//        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
//        if(resultCode == ConnectionResult.SUCCESS) {
//            initBarcodeDetector();
//        } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
//                resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
//                resultCode == ConnectionResult.SERVICE_DISABLED) {
//            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
//            dialog.show();
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_HANDLE_CAMERA_PERM)
            mPermissionsRequester.onRequestPermissionsResult(permissions, grantResults);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(EXTRA_KEY_FLASH_ENABLED, flashEnabled); //TODO ERR
    }

    @SuppressLint("InlinedApi")
    private void initBarcodeDetector() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(new VisionCallback() {
            @Override
            public void onFound(final Barcode barcode) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Size size = cameraSource.getPreviewSize();
                        widthScaleFactor = (float) viewFinderView.getWidth() / (float) size.getHeight();
                        heightScaleFactor = (float) viewFinderView.getHeight() / (float) size.getWidth();
                        RectF rect = new RectF(barcode.getBoundingBox());
                        if (scanning && (barcode.format == Barcode.QR_CODE)
                                && translateY(rect.top) > viewFinderView.getFramingRect().top
                                && translateX(rect.left) > viewFinderView.getFramingRect().left
                                && translateX(rect.right) < viewFinderView.getFramingRect().right
                                && translateY(rect.bottom) < viewFinderView.getFramingRect().bottom) {
                            scanning = false;
                            if (prevBarcode == null || !(prevBarcode.rawValue.equals(barcode.rawValue))) {
                                prevBarcode = barcode;

                                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                                v.vibrate(100);

                                Log.d("QRcode", barcode.rawValue);

                                EmployeeModel employeeModel = null;
                                String server = null;
                                Map<String, String> referralParams = qrCodeReferralParamsParse(barcode);
                                if (referralParams != null) {
                                    employeeModel = getReferralEmployeeModel(referralParams);
                                    server = getReferralAddressServer(referralParams);
                                }
                                if (employeeModel != null && server != null) {

                                    viewFinderView.setBorderColor(ContextCompat.getColor(getContext(), R.color.md_blue_500));
                                    viewFinderView.setupViewFinder();

                                    Snackbar.make(graphicOverlay, R.string.qrcode_capture_help_read_success, Snackbar.LENGTH_LONG)
                                            .show();

//                                    RealmHelper realmHelper = new RealmHelper(Realm.getDefaultInstance());
//                                    realmHelper.getRealm().beginTransaction();
//
//                                    realmHelper.save(employeeModel, false);
//                                    realmHelper.getSyncMetaData(false).setAddressServer(server);
//
//                                    realmHelper.getRealm().commitTransaction();
//                                    realmHelper.getRealm().close();

                                    Intent intent = new Intent();
                                    intent.putExtras(new BundleHelper.Builder()
                                            .putString(RegistrationPagerFragment.TAG_ADDRESS_SERVER, server)
                                            .putSerializable(RegistrationPagerFragment.TAG_EMPLOYEE_MODEL, employeeModel)
                                            .build());

                                    getActivity().setResult(Activity.RESULT_OK, intent);
                                    getActivity().finish();
                                } else {
                                    Snackbar.make(graphicOverlay, R.string.qrcode_capture_help_read_fail, Snackbar.LENGTH_LONG)
                                            .show();
                                }
                            }
                            scanning = true;
                        }
                    }
                });
            }
        });
        barcodeDetector.setProcessor(new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational() && getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //Detector dependencies are not yet available

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = (getActivity().registerReceiver(null, lowStorageFilter) != null);

            if (hasLowStorage) {
                Snackbar.make(graphicOverlay, R.string.low_storage_error, Snackbar.LENGTH_LONG)
                        .show();
            } else {
                showOkMaterialDialog(
                        R.string.dialog_detector_error_title,
                        R.string.dialog_detector_error_text,
                        new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });
            }
        }

        viewFinderView.setupViewFinder();
        CameraSource.Builder builder = new CameraSource
                .Builder(getActivity().getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);
        builder = builder.setAutoFocusEnabled(true);

        cameraSource = builder.build();
        cameraSourcePreview.requestLayout();

    }

    private Map<String, String> qrCodeReferralParamsParse(Barcode barcode) {

        if (barcode.valueFormat != Barcode.URL) {
            return null;
        }

        Map<String, String> referralParams;
        try {
            String qrReferrer = Uri.parse(barcode.rawValue).getQueryParameter("referrer");
            referralParams = InstallTracker.extractUrlParams(qrReferrer);
        } catch (Exception e) {
            return null;
        }

        return referralParams;
    }

    private String getReferralAddressServer(@NonNull Map<String, String> referralParams) {
        if (!(referralParams.containsKey(QR_URL_SERVER_PARAM))) {
            return null;
        }

        String qrRawServer = referralParams.get(QR_URL_SERVER_PARAM);
        if (qrRawServer == null || qrRawServer.isEmpty()) {
            return null;
        }

        qrRawServer = ServerURL.findAddressByCompany(qrRawServer);
        if (qrRawServer.isEmpty()) {
            return null;
        }

        return qrRawServer;
    }

    private EmployeeModel getReferralEmployeeModel(@NonNull Map<String, String> referralParams) { //TODO gen exception

        if (!(referralParams.containsKey(QR_URL_EMPLOYEE_MODEL_PARAM))) {
            return null;
        }

        String qrRawEmployee = referralParams.get(QR_URL_EMPLOYEE_MODEL_PARAM);
        if (qrRawEmployee == null || qrRawEmployee.isEmpty()) {
            return null;
        }

        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat(ServerURL.SERVER_DATE_FORMAT);
        Gson gsonModelParser = builder.excludeFieldsWithoutExposeAnnotation().create();

        EmployeeModel employeeModel;
        try {
            employeeModel = gsonModelParser.fromJson(qrRawEmployee, EmployeeModel.class);
        } catch (Exception e) {
            return null;
        }

        return employeeModel;
    }

    private void showOkMaterialDialog(@StringRes int title, @StringRes int message, DialogInterface.OnClickListener onClickListener) {
        //TODO move to lib
        if (getActivity() != null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.dialog_ok, onClickListener)
                    .show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        startCameraSource();
        viewFinderView.setupViewFinder();
        flashSwitch(getCamera(cameraSource), flashEnabled);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (cameraSourcePreview != null) {
            cameraSourcePreview.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (cameraSourcePreview != null) {
            cameraSourcePreview.release();
        }
    }

    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
//        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
//                getActivity().getApplicationContext());
//        if (code != ConnectionResult.SUCCESS) {
//            Dialog dlg =
//                    GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), code, RC_HANDLE_GMS);
//            dlg.show();
//        }
        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            if (cameraSource != null) {
                try {
                    cameraSourcePreview.start(cameraSource, graphicOverlay);
                    scanning = true;
                } catch (IOException e) {
                    // Unable to start camera source

                    showOkMaterialDialog(
                            R.string.dialog_camera_error_title,
                            R.string.dialog_camera_error_text,
                            new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });

                    cameraSource.release();
                    cameraSource = null;
                }
            }
        } else {
            showOkMaterialDialog(
                    R.string.dialog_camera_error_title,
                    R.string.no_camera,
                    new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

//        if (isFlashEnabled(getCamera(cameraSource))) {  // TODO: 23.02.2016 Check flash available
        inflater.inflate(R.menu.qrcode_capture, menu);
        switchFlashMenuItem(menu.findItem(R.id.action_flash_switch), flashEnabled);
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_flash_switch:
                if (item.getTitle().equals(getString(R.string.action_flash_switch_on))) {
                    switchFlashMenuItem(item, false);
                    flashSwitch(getCamera(cameraSource), false);
                } else {
                    switchFlashMenuItem(item, true);
                    flashSwitch(getCamera(cameraSource), true);
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchFlashMenuItem(MenuItem menuItem, boolean enabled) {
        menuItem.setTitle(enabled ? R.string.action_flash_switch_on : R.string.action_flash_switch_off)
                .setIcon(enabled ? R.drawable.ic_flash_on_24dp : R.drawable.ic_flash_off_24dp);
    }

    private void flashSwitch(Camera camera, boolean enabled) {
        if (camera != null) {
            Camera.Parameters params = camera.getParameters();
            params.setFlashMode(enabled ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            flashEnabled = enabled;
        }
    }

    private Camera getCamera(CameraSource cameraSource) {
        Field[] declaredFields = CameraSource.class.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.getType() == Camera.class) {
                field.setAccessible(true);
                try {
                    if (cameraSource != null) {
                        Camera camera = (Camera) field.get(cameraSource);
                        if (camera != null) {
                            return camera;
                        } else return null;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                break;
            }
        }
        return null;
    }

//    private boolean isFlashEnabled(Camera camera) {
//        if (camera != null) {
//            Camera.Parameters params = camera.getParameters();
//            List<String> supportedFlashModes = params.getSupportedFlashModes();
//            return (!(params.getFlashMode() == null || supportedFlashModes == null || supportedFlashModes.isEmpty() ||
//                    supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)));
//        }
//        return false;
//    }

    /**
     * Adjusts the x coordinate from the preview's coordinate system to the view coordinate
     * system.
     */
    public float translateX(float x) {
        return x * widthScaleFactor;
    }

    /**
     * Adjusts the y coordinate from the preview's coordinate system to the view coordinate
     * system.
     */
    public float translateY(float y) {
        return y * heightScaleFactor;
    }
}