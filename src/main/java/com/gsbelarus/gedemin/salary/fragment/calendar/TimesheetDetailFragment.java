package com.gsbelarus.gedemin.salary.fragment.calendar;

import android.os.Bundle;
import android.view.View;

import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.TimesheetModel;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.realm.Realm;

public class TimesheetDetailFragment extends CalendarDetailFragment {

    private RealmHelper realmHelper;

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        TimesheetModel timesheetModel = realmHelper.getObjectByUid(TimesheetModel.class, getDayId());

        String day = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(timesheetModel.getDate());

        if (!day.isEmpty())
            getTvDay().setText(day.substring(0, 1).toUpperCase() + day.substring(1).toLowerCase());
        getTvDayType().setText(DayType.getName(timesheetModel.getDayType()).toUpperCase());

        getEventAdapter().setItems(timesheetModel.getEvents());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }
}
