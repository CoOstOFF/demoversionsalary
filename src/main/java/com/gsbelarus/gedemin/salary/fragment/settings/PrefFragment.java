package com.gsbelarus.gedemin.salary.fragment.settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsbelarus.gedemin.salary.R;

public class PrefFragment extends PreferenceFragment {

    protected Context context;

    public interface OnPositiveListener {
        void onPositive();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getBaseContext();
    }

    @SuppressWarnings("unused")
    protected Preference initListSummary(Preference preference, String[] items, int defaultItem) {
        preference.setSummary(items[getInt(preference.getKey(), defaultItem)]);
        return preference;
    }

    @SuppressWarnings("unused")
    protected void showListDialog(final Preference preference, String title,
                                  String negativeText, final String[] items,
                                  int defaultValue, final OnItemListChanged onItemListChanged) {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .items(items)
                .title(title)
                .negativeText(negativeText)
                .itemsCallbackSingleChoice(getInt(preference.getKey(), defaultValue), new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                        putInt(preference.getKey(), i);
                        preference.setSummary(items[i]);
                        if (onItemListChanged != null)
                            onItemListChanged.onItemListChanged(items[i], i);
                        return false;
                    }
                })
                .build();
        dialog.show();
    }

    protected interface OnItemListChanged {
        void onItemListChanged(String value, int position);
    }

    @SuppressWarnings("unused")
    protected Preference initEditSummary(Preference preference, String defaultValue) {
        String summary = getString(preference.getKey(), defaultValue);
        if (summary.isEmpty()) summary = defaultValue;
        preference.setSummary(summary);
        return preference;
    }


    protected void showEditDialog(final Preference preference, String title, String hint,
                                  String positiveText, String negativeText, InputFilter filter, @Nullable final Integer inputType, final OnPositiveListener listener) {
        @SuppressLint("InflateParams") ViewGroup layout = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.dialog_edit_text, null);
        final EditText editText = (EditText) layout.findViewById(android.R.id.edit);
        if (filter != null) {
            editText.setFilters(new InputFilter[]{filter});
        }
        if (inputType != null) editText.setInputType(inputType);
        editText.setText(getString(preference.getKey(), ""));
        editText.setSelection(editText.length());
        editText.setHint(hint);
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .customView(layout, false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {

                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        if (editText.getText().toString().isEmpty())
                            preference.setSummary(R.string.not_specified_text);
                        else
                            preference.setSummary(editText.getText());
                        putString(preference.getKey(), editText.getText().toString());
                        if (listener != null)
                            listener.onPositive();
                    }
                })
                .build();
        dialog.show();

        Window window = dialog.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @SuppressWarnings("unused")
    protected MaterialDialog showAlertDialog(Spanned title, String message,
                                             String positiveButtonText, String negativeButtonText, String neutralButtonText,
                                             MaterialDialog.SingleButtonCallback callback) {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getActivity());
        if (title != null)
            dialogBuilder.title(title);
        if (message != null)
            dialogBuilder.content(message);
        if (positiveButtonText != null)
            dialogBuilder.positiveText(positiveButtonText);
        if (negativeButtonText != null)
            dialogBuilder.negativeText(negativeButtonText);
        if (neutralButtonText != null)
            dialogBuilder.neutralText(neutralButtonText);
        if (callback != null)
            dialogBuilder.onAny(callback);

        MaterialDialog dialog = dialogBuilder.build();
        if (getActivity().isFinishing()) return null;
        dialog.show();

        return dialog;
    }

    @SuppressWarnings("unused")
    protected void putString(String prefKey, String value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(prefKey, value);
        editor.apply();
    }

    @SuppressWarnings("unused")
    protected void putInt(String prefKey, int value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(prefKey, value);
        editor.apply();
    }

    @SuppressWarnings("unused")
    protected void putBoolean(String prefKey, boolean value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(prefKey, value);
        editor.apply();
    }

    @SuppressWarnings("unused")
    protected String getString(String prefKey, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(prefKey, defaultValue);
    }

    @SuppressWarnings("unused")
    protected int getInt(String prefKey, int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(prefKey, defaultValue);
    }

    @SuppressWarnings("unused")
    protected boolean getBoolean(String prefKey, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(prefKey, defaultValue);
    }
}
