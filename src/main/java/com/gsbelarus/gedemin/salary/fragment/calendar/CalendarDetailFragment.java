package com.gsbelarus.gedemin.salary.fragment.calendar;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.adapter.calendar.CalendarRecyclerViewAdapter;
import com.gsbelarus.gedemin.salary.ui.EmptyRecyclerView;

public abstract class CalendarDetailFragment extends BaseFragment {

    protected static final String CALENDAR_DETAIL_TAG = "calendar_detail_tag";

    protected static final String DATE_FORMAT = "EEEE, d MMMM";

    private CalendarRecyclerViewAdapter eventAdapter;
    private TextView tvDay;
    private TextView tvDayType;
    private int dayId;

    public static <T extends CalendarDetailFragment> T newInstance(Class<T> cl, int gdMsgId) {
        T fragment = null;
        try {
            fragment = cl.newInstance();
            fragment.setArguments(new BundleHelper.Builder()
                    .putInt(CALENDAR_DETAIL_TAG, gdMsgId)
                    .build());
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_calendar_detail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            initGdMsgId(getArguments());
        else
            initGdMsgId(savedInstanceState);
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        eventAdapter = new CalendarRecyclerViewAdapter();
        EmptyRecyclerView recyclerView = (EmptyRecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(eventAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setEmptyView(rootView.findViewById(R.id.empty_rec_view_schedule));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        tvDay = (TextView) rootView.findViewById(R.id.tv_day);
        tvDayType = (TextView) rootView.findViewById(R.id.tv_day_type);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(CALENDAR_DETAIL_TAG, dayId);
    }

    private void initGdMsgId(Bundle bundle) {
        if (BundleHelper.containsArguments(bundle, CALENDAR_DETAIL_TAG))
            dayId = BundleHelper.get(bundle, CALENDAR_DETAIL_TAG);
    }

    public int getDayId() {
        return dayId;
    }

    public CalendarRecyclerViewAdapter getEventAdapter() {
        return eventAdapter;
    }

    public TextView getTvDay() {
        return tvDay;
    }

    public TextView getTvDayType() {
        return tvDayType;
    }
}
