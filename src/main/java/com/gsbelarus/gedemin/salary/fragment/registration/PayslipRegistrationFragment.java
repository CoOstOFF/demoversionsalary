package com.gsbelarus.gedemin.salary.fragment.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.ui.BackEditText;
import com.gsbelarus.gedemin.salary.ui.FocusableTextInputLayout;

import io.realm.Realm;

public class PayslipRegistrationFragment extends RegistrationPagerItemFragment implements RegistrationPagerItemFragment.Validator {

    private BackEditText inputListNumber;
    private FocusableTextInputLayout inputListNumberLayout;

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setContentView(R.layout.fragment_registration_payslip);

        inputListNumberLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_list_number_layout);
        inputListNumberLayout.setErrorEnabled(true);
        inputListNumber = (BackEditText) rootView.findViewById(R.id.input_list_number);

        setupEditText(inputListNumber, true, this);
    }

    @Override
    public boolean validate(EditText editText) {
        if (editText == inputListNumber)
            return validateListNumberInput();
        return false;
    }

    private boolean validateListNumberInput() {
        String listNumber = inputListNumber.getText().toString();
        String error = null;

//        if (listNumber.isEmpty())
//            error = getContext().getString(R.string.registration_payslip_empty_listnumber_error);
        inputListNumberLayout.setError(error);

        return error == null;
    }

    @Override
    public boolean validatePageInputs() {
        return validateListNumberInput();
    }

    @Override
    public void savePageFields() {
        getRealmHelper().getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                View rootView = getRootView();
                assert rootView != null;
                getRealmHelper().getLastEmployee(false).setListNumber(((EditText) rootView.findViewById(R.id.input_list_number)).getText().toString());
            }
        });
    }

    @Override
    protected void loadPageFields(final View rootView, @Nullable Bundle bundle) {
        EmployeeModel employeeModel;
        if (BundleHelper.containsArguments(bundle, RegistrationPagerFragment.TAG_EMPLOYEE_MODEL))
            employeeModel = BundleHelper.get(bundle, RegistrationPagerFragment.TAG_EMPLOYEE_MODEL);
        else
            employeeModel = getRealmHelper().getLastEmployee(true);

        if (employeeModel != null)
            setTextToEdit((EditText) rootView.findViewById(R.id.input_list_number), employeeModel.getListNumber());
    }
}
