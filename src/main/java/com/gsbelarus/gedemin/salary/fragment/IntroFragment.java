package com.gsbelarus.gedemin.salary.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IntroFragment extends android.support.v4.app.Fragment {

    private static final String ARG_LAYOUT_RES_ID = "layoutResId";

    public interface OnCreateViewListener {
        void onCreateView(View view);
    }

    public static IntroFragment newInstance(int layoutResId) {
        IntroFragment introFragment = new IntroFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        introFragment.setArguments(args);

        return introFragment;
    }

    public static IntroFragment newInstance(int layoutResId, OnCreateViewListener onCreateViewListener) {
        IntroFragment introFragment = newInstance(layoutResId);
        introFragment.onCreateViewListener = onCreateViewListener;
        return introFragment;
    }

    private int layoutResId;
    private OnCreateViewListener onCreateViewListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID))
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(layoutResId, container, false);
        if (onCreateViewListener != null)
            onCreateViewListener.onCreateView(rootView);
        return rootView;
    }
}