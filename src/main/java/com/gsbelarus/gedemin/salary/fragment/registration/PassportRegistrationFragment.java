package com.gsbelarus.gedemin.salary.fragment.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.ui.BackEditText;
import com.gsbelarus.gedemin.salary.ui.FocusableTextInputLayout;
import com.gsbelarus.gedemin.salary.util.Transliterator;

import java.util.regex.Pattern;

import io.realm.Realm;

public class PassportRegistrationFragment extends RegistrationPagerItemFragment implements RegistrationPagerItemFragment.Validator {

    private static final String PASSPORT_ID_PATTERN = "^[a-zA-Z0-9 ]*$";
    private static final String PERSON_NAME_PATTERN = "^[а-яА-ЯёЁa-zA-Z0-9\\- ]*$";

    private BackEditText inputSurName;
    private BackEditText inputFirstName;
    private BackEditText inputMiddleName;
    private BackEditText inputPassportId;

    private FocusableTextInputLayout inputSurNameLayout;
    private FocusableTextInputLayout inputFirstNameLayout;
    private FocusableTextInputLayout inputMiddleNameLayout;
    private FocusableTextInputLayout inputPassportIdLayout;

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setContentView(R.layout.fragment_registration_passport);

        inputSurNameLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_sur_name_layout);
        inputSurNameLayout.setErrorEnabled(true);
        inputSurName = (BackEditText) rootView.findViewById(R.id.input_sur_name);

        inputFirstNameLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_first_name_layout);
        inputFirstNameLayout.setErrorEnabled(true);
        inputFirstName = (BackEditText) rootView.findViewById(R.id.input_first_name);

        inputMiddleNameLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_middle_name_layout);
        inputMiddleNameLayout.setErrorEnabled(true);
        inputMiddleName = (BackEditText) rootView.findViewById(R.id.input_middle_name);

        inputPassportIdLayout = (FocusableTextInputLayout) rootView.findViewById(R.id.input_passport_id_layout);
        inputPassportIdLayout.setErrorEnabled(true);
        inputPassportId = (BackEditText) rootView.findViewById(R.id.input_passport_id);

        setupEditText(inputSurName, true, this);
        setupEditText(inputFirstName, true, this);
        setupEditText(inputMiddleName, true, this);
        setupEditText(inputPassportId, true, this);
    }

    @Override
    public boolean validate(EditText editText) {
        if (editText == inputSurName)
            return validateSurnameInput();
        else if (editText == inputFirstName)
            return validateFirstNameInput();
        else if (editText == inputMiddleName)
            return validateMiddleNameInput();
        else if (editText == inputPassportId)
            return validatePassportIdInput();
        return false;
    }

    private boolean isValidPersonName(String name) {
        return Pattern.compile(PERSON_NAME_PATTERN).matcher(name).matches();
    }

    private boolean validateSurnameInput() {
        String surname = inputSurName.getText().toString();
        String error = null;

        if (surname.isEmpty())
            error = getContext().getString(R.string.registration_passport_empty_surname_error);
        else if (!isValidPersonName(surname))
            error = getContext().getString(R.string.registration_passport_invalid_surname_error);
        inputSurNameLayout.setError(error);

        return error == null;
    }

    private boolean validateFirstNameInput() {
        String firstName = inputFirstName.getText().toString();
        String error = null;

        if (firstName.isEmpty())
            error = getContext().getString(R.string.registration_passport_empty_name_error);
        else if (!isValidPersonName(firstName))
            error = getContext().getString(R.string.registration_passport_invalid_name_error);
        inputFirstNameLayout.setError(error);

        return error == null;
    }

    private boolean validateMiddleNameInput() {
        String middleName = inputMiddleName.getText().toString();
        String error = null;

        if (!isValidPersonName(middleName))
            error = getContext().getString(R.string.registration_passport_invalid_middlename_error);
        inputMiddleNameLayout.setError(error);

        return error == null;
    }

    private boolean validatePassportIdInput() {
        String passportId = inputPassportId.getText().toString();
        String error = null;

        if (Transliterator.containsSimilarCyr(passportId)) {
            passportId = Transliterator.transliterateSimilarCyrToLat(passportId, true);
            inputPassportId.setText(passportId);
            inputPassportId.setSelection(passportId.length());
        }

        if (passportId.isEmpty())
            error = getContext().getString(R.string.registration_passport_empty_id_error);
        else if (!Pattern.compile(PASSPORT_ID_PATTERN).matcher(passportId).matches())
            error = getContext().getString(R.string.registration_passport_invalid_id_error);
        inputPassportIdLayout.setError(error);

        return error == null;
    }

    @Override
    public boolean validatePageInputs() {
        return validateSurnameInput() && validateFirstNameInput() && validateMiddleNameInput() && validatePassportIdInput();
    }

    @Override
    public void savePageFields() {
        getRealmHelper().getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                View rootView = getRootView();
                assert rootView != null;
                EmployeeModel employeeModel = getRealmHelper().getLastEmployee(false);
                employeeModel.setSurname(charSimilarConvent(((EditText) rootView.findViewById(R.id.input_sur_name)).getText().toString()));
                employeeModel.setFirstname(charSimilarConvent(((EditText) rootView.findViewById(R.id.input_first_name)).getText().toString()));
                employeeModel.setMiddlename(charSimilarConvent(((EditText) rootView.findViewById(R.id.input_middle_name)).getText().toString()));
                employeeModel.setPassportId(((EditText) rootView.findViewById(R.id.input_passport_id)).getText().toString());
            }
        });
    }

    @Override
    protected void loadPageFields(final View rootView, @Nullable Bundle bundle) {
        EmployeeModel employeeModel;
        if (BundleHelper.containsArguments(bundle, RegistrationPagerFragment.TAG_EMPLOYEE_MODEL))
            employeeModel = BundleHelper.get(bundle, RegistrationPagerFragment.TAG_EMPLOYEE_MODEL);
        else
            employeeModel = getRealmHelper().getLastEmployee(true);

        if (employeeModel != null) {
            setTextToEdit((EditText) rootView.findViewById(R.id.input_sur_name), employeeModel.getSurname());
            setTextToEdit((EditText) rootView.findViewById(R.id.input_first_name), employeeModel.getFirstname());
            setTextToEdit((EditText) rootView.findViewById(R.id.input_middle_name), employeeModel.getMiddlename());
            setTextToEdit((EditText) rootView.findViewById(R.id.input_passport_id), employeeModel.getPassportId());
        }
    }

    private String charSimilarConvent(String string) {
        char[] oldChars = {'ё', 'Ё', 'Ъ', 'ъ'};
        char[] newChars = {'е', 'е', 'Ь', 'ь'};
        if (oldChars.length == newChars.length) {
            for (int i = 0; i < oldChars.length; i++) {
                string = string.replace(oldChars[i], newChars[i]);
            }
        }
        return string;
    }
}