package com.gsbelarus.gedemin.salary.fragment.payslip;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.activity.Statistics;
import com.gsbelarus.gedemin.salary.adapter.payslip.InternalClickListener;
import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipGeneralAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.HorizontalBarData;
import com.gsbelarus.gedemin.salary.entity.PayslipGeneralItem;
import com.gsbelarus.gedemin.salary.entity.PayslipSubItem;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.PayslipItemModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.ui.AutofitEmptyRecyclerView;
import com.gsbelarus.gedemin.salary.util.DateFormatHelper;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class PayslipTabGeneralFragment extends BaseFragment {

    private PayslipGeneralAdapter adapter;
    private RealmHelper realmHelper;
    private AppCompatButton workModeButton;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_payslip_general;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        adapter = new PayslipGeneralAdapter(getContext(), realmHelper);

        AutofitEmptyRecyclerView generalRecView = (AutofitEmptyRecyclerView) rootView.findViewById(R.id.payslip_recycler_view);
        final StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        generalRecView.setLayoutManager(manager);
        generalRecView.setHasFixedSize(true);
        generalRecView.setColumnWidth(getActivity().getResources().getDimension(R.dimen.payslip_general_card_width));
        generalRecView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int space = (int) (2f * getContext().getResources().getDisplayMetrics().density);

                outRect.top = space;
                outRect.left = space;
                outRect.right = space;
                outRect.bottom = space;
            }
        });
        generalRecView.setAdapter(adapter);
        generalRecView.setEmptyView(rootView.findViewById(R.id.empty_rec_view_general));

        adapter.setOnClickButtons(new InternalClickListener() {
            @Override
            public void onInternalViewClick(View view, View internalView, int position) {
                switch (internalView.getId()) {
                    case R.id.payslip_general_sync_button:
                        if (PermissionHelper.ReadPhoneState.checkAndRequest(getActivity()))
                            getActivity().startService(SyncServiceModel.getIntentWithTask(new Intent(getContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));
                        break;
                    case R.id.payslip_general_statistics_button:
                        if (BundleHelper.containsArguments(getArguments(), PayslipFragment.TAG_DATE)) {
                            getActivity().finish();
                        } else {
                            Intent intent = new Intent(getActivity(), Statistics.class);
                            startActivity(intent);
                        }
                        break;
                    case R.id.payslip_general_cards_header_info_button:
                        new AlertDialog.Builder(getActivity())
                                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setMessage(adapter.getList().get(position).getInfo())
                                .show();
                        break;
                }
            }
        });

        workModeButton = (AppCompatButton) rootView.findViewById(R.id.payslip_general_work_mode_button);
        workModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RegistrationPager.class));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View view) {
        adapter.clear();
        Calendar date;
        if (!BundleHelper.containsArguments(getArguments(), PayslipFragment.TAG_DATE)) {
            List<Calendar> possibleDates = realmHelper.getPayslipPossibleDatesList();
            date = possibleDates.get(possibleDates.size() - 1);
        } else {
            date = BundleHelper.get(getArguments(), PayslipFragment.TAG_DATE);
        }

        PayslipModel payslipModel = realmHelper.getPayslip(date);

        if (payslipModel != null) {
            List<PayslipSubItem> itemsList = new ArrayList<>();
            List<PayslipSubItem> itemsTaxesList = new ArrayList<>();
            List<PayslipSubItem> itemsEmployeeList = new ArrayList<>();
            List<PayslipSubItem> itemsTotalCurYearList = new ArrayList<>();
            List<PayslipSubItem> itemsTotalYearList = new ArrayList<>();
            List<HorizontalBarData> barData = new ArrayList<>();

            double prepaymentSum;
            double totalDebitSum;
            double totalDebitSumCurYear;
            double totalDebitSumYear;
            double totalCreditSum;
            double totalTaxesSum;
            double totalTaxesSumCurYear;
            double totalTaxesSumYear;
            double salarySum;
            double hoursWorked;
            double daysWorked;
            double hoursPlanned;
            double daysPlanned;

            int dependentsSum;
            int payslipYear = date.get(Calendar.YEAR);
            int[] attrs = {R.attr.colorPayslipDebit, R.attr.colorPayslipCredit, R.attr.colorPayslipTax};

            TypedArray ta = getActivity().obtainStyledAttributes(attrs);
            // FIXME: 04.04.2016
            int debitColor = ta.getColor(0, Color.GREEN);
            @SuppressWarnings("ResourceType") int creditColor = ta.getColor(1, Color.RED);
            @SuppressWarnings("ResourceType") int taxColor = ta.getColor(2, Color.BLUE);

            Date prepaymentDate;

            String prepDateString = "";
            String salaryText;
            String department;
            String position;

            RealmResults<PayslipItemModel> taxList = realmHelper.getTaxesList(payslipModel);

            totalDebitSum = realmHelper.getDebitSum(payslipModel);
            totalCreditSum = realmHelper.getCreditSum(payslipModel);
            totalTaxesSum = realmHelper.getTaxesSum(payslipModel);

            prepaymentSum = realmHelper.getPrepaymentSum(payslipModel);
            prepaymentDate = realmHelper.getPrepaymentDate(payslipModel);

            department = payslipModel.getDepartment();
            position = payslipModel.getPosition();
            salarySum = payslipModel.getSalary() == 0 ? payslipModel.getHourRate() : payslipModel.getSalary();
            salaryText = payslipModel.getSalary() == 0 ? getContext().getString(R.string.person_hour_rate) :
                    getContext().getString(R.string.person_salary);
            dependentsSum = payslipModel.getDependents();
            hoursWorked = realmHelper.getTimesheetHours(date);
            daysWorked = realmHelper.getTimesheetWorkingDaysCount(date);
            hoursPlanned = realmHelper.getWorkScheduleHours(date);
            daysPlanned = realmHelper.getWorkScheduleWorkingDaysCount(date);

            Calendar yearBefore;
            yearBefore = (Calendar) date.clone();
            yearBefore.add(Calendar.MONTH, -12);

            totalDebitSumCurYear = realmHelper.getTotalDebitSum(new GregorianCalendar(payslipYear, 0, 1), date);
            totalDebitSumYear = realmHelper.getTotalDebitSum(yearBefore, date);
            totalTaxesSumCurYear = realmHelper.getTotalTaxesSum(new GregorianCalendar(payslipYear, 0, 1), date);
            totalTaxesSumYear = realmHelper.getTotalTaxesSum(yearBefore, date);

            barData.add(new HorizontalBarData(debitColor, (long) (totalDebitSum - totalTaxesSum)));
            barData.add(new HorizontalBarData(creditColor, (long) totalCreditSum));
            barData.add(new HorizontalBarData(taxColor, (long) totalTaxesSum));

            adapter.setDate(date);

            if (prepaymentDate != null)
                prepDateString = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(prepaymentDate);

            itemsList.add(new PayslipSubItem(prepDateString, getContext().getString(R.string.payslip_general_prepayment_sum),
                    prepaymentSum, debitColor));

            if (payslipModel.getEndSaldo() >= 0.0) {// (totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum > 0)
                itemsList.add(new PayslipSubItem(getContext().getString(R.string.payslip_general_cash_sum),
                        payslipModel.getEndSaldo(), debitColor)); // totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum, debitColor));

                double sumNegative = new BigDecimal(Math.abs(totalDebitSum - totalTaxesSum - payslipModel.getEndSaldo() - prepaymentSum -  totalCreditSum)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                if (sumNegative > 0.0) {  // чистые-квыдаче-аванс-удержания
                    itemsList.add(new PayslipSubItem(getContext().getString(R.string.payslip_general_cash_sum_negative), sumNegative, creditColor)); // Math.abs(totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum), creditColor));
                }

            } else if (payslipModel.getEndSaldo() < 0.0) {// (totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum < 0)
                itemsList.add(new PayslipSubItem(getContext().getString(R.string.payslip_general_cash_sum_negative),
                        Math.abs(payslipModel.getEndSaldo()), creditColor)); // Math.abs(totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum), creditColor));
            }
            itemsList.add(new PayslipSubItem(getContext().getString(R.string.payslip_general_credit_sum), totalCreditSum, creditColor));

            for (PayslipItemModel i : taxList) {
                itemsTaxesList.add(new PayslipSubItem(i.getName(), i.getCredit(), taxColor));
            }

            ta.recycle();

            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITHOUT_SUM, department, 0));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITHOUT_SUM, position, 0));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM, salaryText, salarySum));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_SUM,
                    getContext().getString(R.string.payslip_general_dependents), dependentsSum));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_SUM,
                    getContext().getString(R.string.payslip_general_worked_days), daysWorked));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_SUM,
                    getContext().getString(R.string.payslip_general_worked_hours), hoursWorked));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_SUM,
                    getContext().getString(R.string.payslip_general_planned_days), daysPlanned));
            itemsEmployeeList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_SUM,
                    getContext().getString(R.string.payslip_general_planned_hours), hoursPlanned));

            itemsTotalCurYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_debit_to_sum_final), totalDebitSumCurYear));
            itemsTotalCurYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_clean_sum), totalDebitSumCurYear - totalTaxesSumCurYear));
            itemsTotalCurYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_taxes_sum), totalTaxesSumCurYear));


            itemsTotalYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_debit_to_sum_final), totalDebitSumYear));
            itemsTotalYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_clean_sum), totalDebitSumYear - totalTaxesSumYear));
            itemsTotalYearList.add(new PayslipSubItem(PayslipGeneralAdapter.SubItemType.WITH_CONVERTED_SUM,
                    getContext().getString(R.string.payslip_general_taxes_sum), totalTaxesSumYear));


            //TODO res
            if (totalDebitSum != 0) {
                adapter.addItem(new PayslipGeneralItem(PayslipGeneralAdapter.ItemType.ITEM, getString(R.string.payslip_detail_debit_info), DateFormatHelper.getMonthString(getContext(), date),
                        getContext().getString(R.string.payslip_general_debit_sum), "(" + getContext().getString(R.string.payslip_general_clean_sum) + ")", totalDebitSum - totalTaxesSum, itemsList));
            }
            if (totalTaxesSum != 0)
                adapter.addItem(new PayslipGeneralItem(PayslipGeneralAdapter.ItemType.ITEM,
                        getString(R.string.payslip_detail_taxes_info), DateFormatHelper.getMonthString(getContext(), date),
                        getContext().getString(R.string.payslip_general_taxes_sum), null, totalTaxesSum, itemsTaxesList));
            adapter.addItem(new PayslipGeneralItem(PayslipGeneralAdapter.ItemType.SIMPLE_CARD_ITEM, getString(R.string.info), itemsEmployeeList));
            if (totalDebitSumCurYear != 0 || totalTaxesSumCurYear != 0)
                adapter.addItem(new PayslipGeneralItem(PayslipGeneralAdapter.ItemType.SIMPLE_CARD_ITEM,
                        getContext().getString(R.string.payslip_general_from_year_begin), itemsTotalCurYearList));
            if (totalDebitSumYear != 0 || totalTaxesSumYear != 0)
                adapter.addItem(new PayslipGeneralItem(PayslipGeneralAdapter.ItemType.SIMPLE_CARD_ITEM,
                        getContext().getString(R.string.payslip_general_per_year), itemsTotalYearList));
            if (totalDebitSum != 0) {
                String headerTitle;
                if (payslipModel.isFinal())
                    headerTitle = getString(R.string.payslip_general_debit_to_sum_final);
                else
                    headerTitle = getString(R.string.payslip_general_debit_to_sum);
                adapter.showHeader("(" + headerTitle + ")",
                        totalDebitSum, barData);
            }
        }

        if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty())
            workModeButton.setVisibility(View.VISIBLE);
        else
            workModeButton.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }
}
