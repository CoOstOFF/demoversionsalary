package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.GdMsgNew;
import com.gsbelarus.gedemin.salary.database.ModelFactory;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgIncomeModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.service.SendDataService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GdMsgNewIncomeFragment extends GdMsgNewPeriodFragment implements View.OnClickListener {

    private static final String DATE_FORMAT = "MMM yyyy";

    private TextView tvDateBegin;
    private TextView tvDateEnd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null && BundleHelper.containsArguments(getArguments(), GdMsgNew.TAG_MSG_DATE))
            setDateEnd(BundleHelper.<Date>get(getArguments(), GdMsgNew.TAG_MSG_DATE));

        if (getDateEnd() == null)
            setDateEnd(CalendarHelper.initCalendarFields(Calendar.getInstance(), Calendar.HOUR_OF_DAY, 0).getTime());

        if (getDateBegin() == null) {
            Calendar begin = Calendar.getInstance();
            begin.setTime(getDateEnd());
            begin.add(Calendar.MONTH, -1);
            setDateBegin(begin.getTime());
        }
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setTitleFragment(R.string.income_statement);
        setContentView(R.layout.fragment_gdmsg_new_income);

        tvDateBegin = (TextView) rootView.findViewById(R.id.tv_date_begin);
        tvDateEnd = (TextView) rootView.findViewById(R.id.tv_date_end);

        setDateText(tvDateBegin, getDateBegin());
        setDateText(tvDateEnd, getDateEnd());

        tvDateBegin.setOnClickListener(this);
        tvDateEnd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        DatePickerDialog datePickerDialog = null;
        if (v == tvDateBegin)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_BEGIN);
        else if (v == tvDateEnd)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_END);

        if (datePickerDialog != null) {
            Calendar bufCal = Calendar.getInstance();
            datePickerDialog.setYearRange(bufCal.get(Calendar.YEAR) - 100, bufCal.get(Calendar.YEAR));
            datePickerDialog.setMaxDate(bufCal);
        }
    }

    @Override
    protected void onDateChanged(DatePickerType type) {
        switch (type) {
            case DATE_BEGIN:
                setDateText(tvDateBegin, getDateBegin());
                break;
            case DATE_END:
                setDateText(tvDateEnd, getDateEnd());
                break;
        }
    }

    private void setDateText(TextView textView, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        textView.setText(dateFormat.format(date));
    }

    @Override
    public void onResume() {
        super.onResume();

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:

                getDateBegin().setDate(1);
                getDateEnd().setDate(1);
                if (getDateBegin().before(getDateEnd())) {
                    GdMsgIncomeModel gdMsgIncomeModel = new GdMsgIncomeModel();
                    gdMsgIncomeModel.setUid(getRealmHelper().generateUID(GdMsgIncomeModel.class));
                    GdMsgModel tmpGdMsgModel = new GdMsgModel();
                    tmpGdMsgModel.setUid(getRealmHelper().generateUID(GdMsgModel.class));

                    GdMsgModel gdMsgModel = ModelFactory.newInstanceGdMsgIncomeModel(
                            gdMsgIncomeModel,
                            tmpGdMsgModel,
                            getComment(),
                            new Date(),
                            getDateBegin(),
                            getDateEnd());

                    Intent intent = new Intent(getActivity(), SendDataService.class);
                    intent.setAction(SendDataService.SEND_GD_MSG_ACTION);
                    intent.putExtra(SendDataService.EXTRA_KEY_GD_MSG, gdMsgModel);
                    getActivity().startService(intent);

                    getActivity().finish();
                } else
                    new AlertDialog.Builder(getActivity())
                            .setPositiveButton(R.string.dialog_ok, null)
                            .setTitle(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification)
                            .setMessage(R.string.incorrect_dates_message)
                            .create()
                            .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
