package com.gsbelarus.gedemin.salary.fragment.gdmsg;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

public class GdMsgDetailPagerFragment extends BaseFragment {

    public static final String TAG_GD_MSG_ID = "gd_msg_id";
    public static final String TAG_SENDER_FILTER = "sender_filter";
    public static final String TAG_TEXT_FILTER = "text_filter";

    private RealmResults<GdMsgModel> gdMsgs;

    private ViewPager.OnPageChangeListener onPageChangeListener;
    private ViewPager mViewPager;
    private RealmHelper realmHelper;

    private GdMsgModel.Sender senderFilter;
    private String textFilter;

    private int gdMsgId;

    private Snackbar snackOnDelete;
    private boolean canRemoveItems;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_gdmsg_detail_pager;
    }

    public static GdMsgDetailPagerFragment newInstance(int gdMsgId, @Nullable GdMsgModel.Sender senderFilter, @Nullable String filter) {
        GdMsgDetailPagerFragment fragment = new GdMsgDetailPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TAG_GD_MSG_ID, gdMsgId);

        if (senderFilter != null)
            bundle.putSerializable(TAG_SENDER_FILTER, senderFilter);
        if (filter != null)
            bundle.putString(TAG_TEXT_FILTER, filter);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        if (BundleHelper.containsArguments(getArguments(), TAG_GD_MSG_ID))
            gdMsgId = BundleHelper.get(getArguments(), TAG_GD_MSG_ID);

        if (BundleHelper.containsArguments(getArguments(), TAG_TEXT_FILTER))
            textFilter = BundleHelper.get(getArguments(), TAG_TEXT_FILTER);
        else
            textFilter = null;

        if (BundleHelper.containsArguments(getArguments(), TAG_SENDER_FILTER))
            senderFilter = BundleHelper.get(getArguments(), TAG_SENDER_FILTER);
        else
            senderFilter = null;

        mViewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        mViewPager.setClipToPadding(false);
        mViewPager.setPageMargin(40);

        mViewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                return GdMsgDetailFragment.newInstance(gdMsgs.get(position));
            }

            @Override
            public int getCount() {
                return gdMsgs == null ? 0 : gdMsgs.size();
            }

            @Override
            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }

            @Override
            public long getItemId(int position) {
                return gdMsgs.get(position).getUid();
            }
        });
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                gdMsgId = gdMsgs.get(position).getUid();
                if (gdMsgs.get(position).isUnread())
                    realmHelper.setGdMsgViewedWithSync(gdMsgs.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mViewPager.clearOnPageChangeListeners();
        realmHelper.getRealm().close();
    }

    @Override
    public void initView(View rootView) {
        if (mViewPager != null && mViewPager.getAdapter() != null) {

            if (textFilter != null && !textFilter.isEmpty())
                gdMsgs = realmHelper.getAllGdMsgs().where()
                        .contains("gdMsgString", textFilter.toLowerCase(), Case.INSENSITIVE).findAllSorted("timestamp", Sort.DESCENDING);
            else
                gdMsgs = realmHelper.getAllGdMsgs();

            if (senderFilter != null) {
                gdMsgs = gdMsgs.where().equalTo("senderIndex", senderFilter.ordinal()).findAllSorted("timestamp", Sort.DESCENDING);
            }

            mViewPager.getAdapter().notifyDataSetChanged();

            for (int i = 0; i < gdMsgs.size(); i++) {
                if (gdMsgs.get(i).getUid() == gdMsgId) {
                    if (gdMsgs.get(i).isUnread())
                        realmHelper.setGdMsgViewedWithSync(gdMsgs.get(i));
                    mViewPager.setCurrentItem(i, false);
                    break;
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (snackOnDelete != null)
            snackOnDelete.dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:

                if (gdMsgs.isEmpty()) return true;

                if (snackOnDelete != null)
                    snackOnDelete.dismiss();

                final int positionForDelete = mViewPager.getCurrentItem();

                View view = getView();
                assert view != null;
                snackOnDelete = Snackbar.make(view, getString(R.string.msg_removed) + ".", Snackbar.LENGTH_LONG);
                snackOnDelete
                        .setAction(R.string.action_undo, new View.OnClickListener() {
                            public void onClick(View v) {
                                realmHelper.getRealm().cancelTransaction();
                                realmHelper.getRealm().beginTransaction();      // TODO костыль
                                realmHelper.getRealm().commitTransaction();
                                gdMsgs.addChangeListener(new RealmChangeListener<RealmResults<GdMsgModel>>() {
                                    @Override
                                    public void onChange(RealmResults<GdMsgModel> element) {
                                        mViewPager.getAdapter().notifyDataSetChanged();
                                        mViewPager.setCurrentItem(positionForDelete);
                                        element.removeChangeListener(this);
                                    }
                                });
                                canRemoveItems = false;
                            }
                        });

                snackOnDelete.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View v) {
                        realmHelper.getRealm().beginTransaction();
                        realmHelper.deleteObject(gdMsgs, positionForDelete, false);
                        mViewPager.getAdapter().notifyDataSetChanged();
                        canRemoveItems = true;
                    }

                    @Override
                    public void onViewDetachedFromWindow(View v) {

                        if (canRemoveItems) {
                            realmHelper.getRealm().commitTransaction();
                            canRemoveItems = false;
                        }
                    }
                });
                snackOnDelete.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ViewPager.OnPageChangeListener getOnPageChangeListener() {
        return onPageChangeListener;
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener;
        if (mViewPager != null && onPageChangeListener != null)
            mViewPager.addOnPageChangeListener(onPageChangeListener);
    }
}
