package com.gsbelarus.gedemin.salary.fragment.gdmsg;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.SpannableStringBuilder;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.ui.activity.BaseNavigationDrawerActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.GdMsgDetailPager;
import com.gsbelarus.gedemin.salary.activity.Main;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.adapter.BaseSimpleAdapter;
import com.gsbelarus.gedemin.salary.adapter.GdMsgRecyclerViewAdapter;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.service.SendDataService;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.ui.AnimatorListener;
import com.gsbelarus.gedemin.salary.ui.DividerItemDecoration;
import com.gsbelarus.gedemin.salary.ui.EmptyRecyclerView;
import com.gsbelarus.gedemin.salary.util.GdMsgNewHelper;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;
import com.gsbelarus.gedemin.salary.util.SparseBooleanArrayParcelable;

import java.lang.reflect.Field;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class GdMsgsListFragment extends BaseFragment implements ActionMode.Callback,
        BaseSimpleAdapter.OnItemClickListener<GdMsgModel, GdMsgRecyclerViewAdapter.RecyclerItemViewHolder>,
        BaseSimpleAdapter.OnItemLongClickListener<GdMsgModel, GdMsgRecyclerViewAdapter.RecyclerItemViewHolder>,
        View.OnClickListener,
        View.OnKeyListener,
        ViewPager.OnPageChangeListener,
        RealmChangeListener {

    private static final String TABLET_VIEWPAGER_FRAGMENT = "tablet_viewpager_fragment";
    private static final String TAG_TEXT_FILTER = "text_filter";
    private static final String TAG_SELECTED_ITEM = "selected_item";
    private static final String TAG_ACTIVATED_ITEMS = "activated_items";
    private static final String TAG_STARTED_ACTION_MODE = "started_action_mode";

    private GdMsgRecyclerViewAdapter recyclerAdapter;
    private EmptyRecyclerView recyclerView;
    private RecyclerView.OnScrollListener scrollListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton fab;
    private View coordinatorLayoutView;
    private Snackbar snackOnDelete;
    private Spinner mGdMsgFilterSpinner;
    private SearchView searchView;
    private ActionMode actionMode;
    private BroadcastReceiver receiver;

    private boolean isTwoPaneGdMsg = false;
    private boolean canRemoveItems;
    private Button workModeButton;

    private RealmHelper realmHelper;

    private int scrollFlagsToolbar = 0;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_gdmsgs_list;
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setHasOptionsMenu(true);

        realmHelper = new RealmHelper(Realm.getDefaultInstance());
        realmHelper.getRealm().addChangeListener(this);
        recyclerAdapter = new GdMsgRecyclerViewAdapter(getActivity(), realmHelper.getAllGdMsgs());

        rootView.setFocusableInTouchMode(true);
        rootView.setOnKeyListener(this);

        createBroadcast();

        if (getBaseNavigationDrawerActivity().getSupportActionBar() != null && getBaseNavigationDrawerActivity().getToolbar() != null) {
            getBaseNavigationDrawerActivity().getSupportActionBar().setDisplayShowTitleEnabled(false);
            getBaseNavigationDrawerActivity().getToolbar().addView(mGdMsgFilterSpinner = createFilterSpinner());
            scrollFlagsToolbar = getBaseNavigationDrawerActivity().getScrollFlagsToolbar();
            getBaseNavigationDrawerActivity().setScrollFlagsToolbar(0);
        }

        FrameLayout fragmentItemDetail = (FrameLayout) rootView.findViewById(R.id.flGdMsgDetailContainer);
        isTwoPaneGdMsg = fragmentItemDetail != null;
        recyclerAdapter.setSelectionVisible(isTwoPaneGdMsg);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (EmptyRecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(null);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null, false, true));
        recyclerView.setEmptyView(rootView.findViewById(R.id.empty_rec_view));
        recyclerAdapter.setOnItemClickListener(this);
        recyclerAdapter.setOnItemLongClickListener(this);

        // костыль, отключает swipeRefresh если список может скролиться вверх
        recyclerView.addOnScrollListener(scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = (recyclerView == null ||
                        recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });

        fab = (FloatingActionButton) rootView.findViewById(R.id.fab_add);
        fab.setOnClickListener(this);

        coordinatorLayoutView = rootView.findViewById(R.id.coordinatorLayout);

        swipeRefreshLayout.setColorSchemeColors(getContext().getResources().getIntArray(R.array.swipe_refresh));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (PermissionHelper.ReadPhoneState.checkAndRequest(GdMsgsListFragment.this))
                    getActivity().startService(SyncServiceModel.getIntentWithTask(new Intent(getActivity(), SyncDataService.class),
                            SyncServiceTask.TypeOfTask.SERIES_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.MSG_REQUEST)));
                else
                    swipeRefreshLayout.setRefreshing(false);
            }
        });

        workModeButton = (AppCompatButton) rootView.findViewById(R.id.gdmsgs_general_work_mode_button);
        workModeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RegistrationPager.class));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getActivity().unregisterReceiver(receiver);

        if (getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getSupportActionBar() != null
                && getBaseNavigationDrawerActivity().getToolbar() != null) {
            getBaseNavigationDrawerActivity().getSupportActionBar().setDisplayShowTitleEnabled(true);
            getBaseNavigationDrawerActivity().getToolbar().removeView(mGdMsgFilterSpinner);
            getBaseNavigationDrawerActivity().setScrollFlagsToolbar(scrollFlagsToolbar);
        }

        if (recyclerView != null && scrollListener != null)
            recyclerView.removeOnScrollListener(scrollListener);

        realmHelper.getRealm().removeChangeListener(this);
        realmHelper.getRealm().close();
    }

    private void createDetailFragment() {
        if (isTwoPaneGdMsg) {
            if (recyclerAdapter != null) {
                GdMsgDetailPagerFragment fragment = GdMsgDetailPagerFragment.newInstance(
                        recyclerAdapter.getItemCount() == 0 ? -1 : recyclerAdapter.getSelectedUid(),
                        recyclerAdapter.getSenderFilter(), recyclerAdapter.getTextFilter());
                fragment.setOnPageChangeListener(this);
                getChildFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.flGdMsgDetailContainer, fragment, TABLET_VIEWPAGER_FRAGMENT)
                        .commitAllowingStateLoss();
            }
        }
    }

    @Override
    public void initView(View rootView) {
        swipeRefreshLayout.setRefreshing(false);
        recyclerAdapter.notifyDataSetChanged();
        createDetailFragment();

        if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty())
            workModeButton.setVisibility(View.VISIBLE);
        else
            workModeButton.setVisibility(View.GONE);
    }

    public Spinner createFilterSpinner() {
        if (getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getSupportActionBar() != null) {
            Spinner mGdMsgFilterSpinner = (Spinner) LayoutInflater.from(getActivity()).inflate(R.layout.gdmsgs_spinner, getBaseNavigationDrawerActivity().getToolbar(), false);

            final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseNavigationDrawerActivity().getSupportActionBar().getThemedContext(),
                    R.array.title_msg_filter, R.layout.gdmsgs_spinner_drop_title);
            adapter.setDropDownViewResource(R.layout.gdmsgs_spinner_drop_list);
            mGdMsgFilterSpinner.setAdapter(adapter);

            mGdMsgFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long itemId) {
                    switch (position) {
                        case 0:
                            recyclerAdapter.setSenderFilter(null);
                            break;
                        case 1:
                            recyclerAdapter.setSenderFilter(GdMsgModel.Sender.SERVER);
                            break;
                        case 2:
                            recyclerAdapter.setSenderFilter(GdMsgModel.Sender.CLIENT);
                            break;
                        default:
                            recyclerAdapter.setSenderFilter(null);
                            break;
                    }
                    recyclerAdapter.notifyDataSetChangedWithFilters();
                    createDetailFragment();
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
            return mGdMsgFilterSpinner;
        }
        return null;
    }

    private void checked(View view, final int position) {
        int duration = 100;

        ObjectAnimator objectAnimatorStart = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0f).setDuration(duration);
        final ObjectAnimator objectAnimatorFinish = ObjectAnimator.ofFloat(view, "scaleX", 0f, 1f).setDuration(duration);
        objectAnimatorStart.addListener(new AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                toggleActivation(position);
                objectAnimatorFinish.start();
            }
        });
        objectAnimatorStart.start();
    }

    private void toggleActivation(int idx) {
        if (actionMode != null) {
            recyclerAdapter.toggleActivation(idx);
            actionMode.setTitle(getString(R.string.selected_count, recyclerAdapter.getActivatedItemCount()));
            boolean itemsSelected = recyclerAdapter.getActivatedItemCount() != 0;
            actionMode.getMenu().findItem(R.id.menu_read_all).setVisible(itemsSelected);
            actionMode.getMenu().findItem(R.id.menu_select_all).setVisible(recyclerAdapter.getActivatedItemCount() != recyclerAdapter.getItemCount());
            if (!itemsSelected)
                actionMode.finish();
        }
    }

    @Override
    public void onClick(View view, GdMsgModel item, GdMsgRecyclerViewAdapter.RecyclerItemViewHolder recyclerItemViewHolder) {
        if (actionMode != null) {
            checked(recyclerItemViewHolder.avatarImageView, recyclerItemViewHolder.getAdapterPosition());
        } else {
            if (snackOnDelete != null)
                snackOnDelete.dismiss();
            recyclerAdapter.setSelectedUid(item.getUid());

            if (isTwoPaneGdMsg) {
                createDetailFragment();
            } else {
                Intent i = new Intent(getActivity(), GdMsgDetailPager.class);
                i.putExtras(new BundleHelper.Builder()
                        .putInt(GdMsgDetailPagerFragment.TAG_GD_MSG_ID, item.getUid())
                        .putString(GdMsgDetailPagerFragment.TAG_TEXT_FILTER, recyclerAdapter.getTextFilter())
                        .putSerializable(GdMsgDetailPagerFragment.TAG_SENDER_FILTER, recyclerAdapter.getSenderFilter())
                        .build());
                startActivity(i);
            }
        }
    }

    @Override
    public boolean onLongClick(View view, GdMsgModel item, GdMsgRecyclerViewAdapter.RecyclerItemViewHolder recyclerItemViewHolder) {
        if (actionMode == null && getBaseNavigationDrawerActivity().getToolbar() != null) {
            actionMode = getBaseNavigationDrawerActivity().getToolbar().startActionMode(this);
            checked(recyclerItemViewHolder.avatarImageView, recyclerItemViewHolder.getAdapterPosition());
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;

        if (view.getId() == R.id.fab_add)
            GdMsgNewHelper.showGdMsgNewDialog(getActivity(),
                    getContext(),
                    GdMsgModel.Subject.INCOME,
                    GdMsgModel.Subject.VACATION,
                    GdMsgModel.Subject.ABSENCE,
                    GdMsgModel.Subject.VACATION_INFO);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.setTitle(getString(R.string.selected_count, recyclerAdapter.getActivatedItemCount()));
        getActivity().getMenuInflater().inflate(R.menu.gdmsgs_action, menu);
        if (fab != null)
            fab.setVisibility(View.GONE);
        getBaseNavigationDrawerActivity().getToolbar().setVisibility(View.INVISIBLE);
        getBaseNavigationDrawerActivity().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if (snackOnDelete != null)
            snackOnDelete.dismiss();
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:

                if (snackOnDelete != null)
                    snackOnDelete.dismiss();

                final SparseBooleanArray selectedUids = recyclerAdapter.getActivatedUids();
                snackOnDelete = Snackbar.make(coordinatorLayoutView, getResources().getString(R.string.msgs_removed, selectedUids.size()), Snackbar.LENGTH_LONG);
                snackOnDelete
                        .setAction(R.string.action_undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                realmHelper.getRealm().cancelTransaction();
                                realmHelper.getRealm().beginTransaction();      // TODO костыль
                                realmHelper.getRealm().commitTransaction();
                                recyclerAdapter.getGdMsgDataSet().addChangeListener(new RealmChangeListener<RealmResults<GdMsgModel>>() {
                                    @Override
                                    public void onChange(RealmResults<GdMsgModel> element) {
                                        recyclerAdapter.notifyDataSetChanged();
                                        createDetailFragment();
                                        element.removeChangeListener(this);
                                    }
                                });
                                canRemoveItems = false;
                            }
                        });

                snackOnDelete.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View v) {
                        realmHelper.getRealm().beginTransaction();
                        for (int i = 0; i < selectedUids.size(); i++) {
                            int position = recyclerAdapter.getPositionByUid(selectedUids.keyAt(i));
                            realmHelper.deleteObject((RealmResults<GdMsgModel>) recyclerAdapter.getItems(), position, false);
                        }
                        recyclerAdapter.notifyDataSetChanged();
                        createDetailFragment();

                        actionMode.finish();
                        canRemoveItems = true;
                    }

                    @Override
                    public void onViewDetachedFromWindow(View v) {
                        if (canRemoveItems) {
                            realmHelper.getRealm().commitTransaction();
                            canRemoveItems = false;
                        }
                        createDetailFragment();
                    }
                });

                snackOnDelete.show();

                return true;
            case R.id.menu_select_all:
                recyclerAdapter.setActivationAll();
                String title = getString(R.string.selected_count, recyclerAdapter.getActivatedItemCount());
                actionMode.setTitle(title);
                item.setVisible(false);
                return true;
            case R.id.menu_read_all:
                for (GdMsgModel gdMsg : recyclerAdapter.getActivatedItems()) {
                    if (gdMsg.isUnread())
                        realmHelper.setGdMsgViewedWithSync(gdMsg);
                }
                mode.finish();
                return true;
            default:
                return false;
        }
    }

    private void startDeleteByUid(int uid) {
        try {
            GdMsgModel gdMsg = realmHelper.getRealm()
                    .where(GdMsgModel.class).equalTo("uid", uid)
                    .findFirst();
            if (gdMsg != null)
                realmHelper.deleteGdMsgWithSync(gdMsg, false);
        } catch (Exception ex) {
            //Crashlytics.getInstance().core.logException(ex);
            LogUtil.e(ex.getMessage());
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        if (recyclerAdapter.getActivatedItemCount() != 0)
            recyclerAdapter.clearActivations();
        if (fab != null)
            fab.setVisibility(View.VISIBLE);
        getBaseNavigationDrawerActivity().getToolbar().setVisibility(View.VISIBLE);
        getBaseNavigationDrawerActivity().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (recyclerAdapter != null)
            recyclerAdapter.setSelectedPosition(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * вызывается при изменении базы из потока UI
     */
    @Override
    public void onChange(Object element) {
        recyclerAdapter.notifyDataSetChanged();
        ((Main) getBaseNavigationDrawerActivity()).updateNavMenuMailCounter();
        createDetailFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (snackOnDelete != null)
            snackOnDelete.dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    private BaseNavigationDrawerActivity getBaseNavigationDrawerActivity() {
        return (BaseNavigationDrawerActivity) getActivity();
    }

    /**
     * ловим уведомление об успешной отправке сообщения.
     * BroadcastReceiver регистрируется в onCreate(), удаляется в onDestroy()
     */
    private void createBroadcast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LogUtil.d("intent", intent.toString());
                if (intent.getExtras().getBoolean(SendDataService.EXTRA_KEY_GD_MSG_WAS_SENT, false)) {
                    //realmHelper.getRealm().waitForChange();
                    recyclerAdapter.notifyDataSetChanged();
                }
            }
        };
        IntentFilter intFilt = new IntentFilter(SendDataService.SEND_GD_MSG_BROADCAST_ACTION);
        getActivity().registerReceiver(receiver, intFilt);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_TEXT_FILTER, recyclerAdapter.getTextFilter());
        outState.putInt(TAG_SELECTED_ITEM, recyclerAdapter.getSelectedUid());
        outState.putParcelable(TAG_ACTIVATED_ITEMS, new SparseBooleanArrayParcelable(recyclerAdapter.getActivatedUids()));
        outState.putBoolean(TAG_STARTED_ACTION_MODE, actionMode != null);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            recyclerAdapter.setTextFilter(savedInstanceState.getString(TAG_TEXT_FILTER));
            recyclerAdapter.setSelectedUid(savedInstanceState.getInt(TAG_SELECTED_ITEM));
            recyclerAdapter.setActivatedUids((SparseBooleanArray) savedInstanceState.getParcelable(TAG_ACTIVATED_ITEMS));
            createDetailFragment();

            if (savedInstanceState.getBoolean(TAG_STARTED_ACTION_MODE, false)
                    && getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getToolbar() != null)
                actionMode = getBaseNavigationDrawerActivity().getToolbar().startActionMode(this);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!searchView.isIconified()) {
                closeAndClearSearch();
                animateToggle(1f, 0f);
                return true;
            }
        }
        return false;
    }

    private void closeAndClearSearch() {
        if (searchView != null && !searchView.isIconified()) {
            searchView.setQuery("", true);
            searchView.setIconified(true);
        }
    }

    private void openSearch(String textFilter) {
        if (searchView != null && searchView.isIconified()) {
            searchView.setQuery(textFilter, false);
            searchView.setIconified(false);
        }
    }

    private void animateToggle(float start, float end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ValueAnimator offsetAnimator = ValueAnimator.ofFloat(start, end);
            offsetAnimator.setDuration(300);
            offsetAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            offsetAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float offset = (Float) animation.getAnimatedValue();
                    if (getBaseNavigationDrawerActivity() != null && getBaseNavigationDrawerActivity().getActionBarDrawerToggle() != null)
                        getBaseNavigationDrawerActivity().getActionBarDrawerToggle().onDrawerSlide(null, offset);
                }
            });
            offsetAnimator.start();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.gdmsgs, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        searchView.setLayoutParams(new SearchView.LayoutParams(SearchView.LayoutParams.MATCH_PARENT,
                SearchView.LayoutParams.MATCH_PARENT));
        searchView.requestLayout();
        searchView.setMaxWidth(10000);

        /** убираем иконку из hint */
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        SpannableStringBuilder ssb = new SpannableStringBuilder("");
        ssb.append(getResources().getString(R.string.msg_search_placeholder));
        searchAutoComplete.setHint(ssb);

        try {
            /** меняем курсор */
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchAutoComplete, R.drawable.edittext_cursor_white);
        } catch (Exception ignored) {
        }

        /** убираем полосу снизу */
        View searchPlate = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        searchPlate.setBackgroundColor(Color.TRANSPARENT);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGdMsgFilterSpinner != null)
                    mGdMsgFilterSpinner.setVisibility(View.GONE);
                animateToggle(0f, 1f);
                getBaseNavigationDrawerActivity().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                if (recyclerAdapter.getTextFilter() == null) recyclerAdapter.setTextFilter("");
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (mGdMsgFilterSpinner != null)
                    mGdMsgFilterSpinner.setVisibility(View.VISIBLE);
                animateToggle(1f, 0f);
                getBaseNavigationDrawerActivity().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                recyclerAdapter.setTextFilter(null);
                recyclerAdapter.notifyDataSetChangedWithFilters();
                createDetailFragment();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                recyclerAdapter.setTextFilter(s);
                recyclerAdapter.notifyDataSetChangedWithFilters();
                createDetailFragment();
                return true;
            }
        });

        if (recyclerAdapter.getTextFilter() != null) {
            openSearch(recyclerAdapter.getTextFilter());
        } else {
            closeAndClearSearch();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!searchView.isIconified()) {
                    closeAndClearSearch();
                    animateToggle(1f, 0f);
                    return false;
                }
                /** т.к действие по кнопке home переопределено, следует "руками" открыть/закрыть drawer */
                if (getBaseNavigationDrawerActivity().isOpenedDrawer())
                    getBaseNavigationDrawerActivity().closeDrawer();
                else
                    getBaseNavigationDrawerActivity().openDrawer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}