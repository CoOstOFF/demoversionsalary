package com.gsbelarus.gedemin.salary.fragment.payslip;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipViewPagerAdapter;
import com.gsbelarus.gedemin.salary.ui.TextDrawable;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import java.util.Calendar;

public class PayslipFragment extends BaseFragment implements
        CurrencyHelper.OnCurrencySwitcherChanged {

    public static final String TAG_DATE = "tag_date";

    private PayslipViewPagerAdapter adapter;
    private CurrencyHelper currencyHelper;
    private TabLayout tabLayout;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_payslip;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        currencyHelper = CurrencyHelper.getInstance(getContext());
        currencyHelper.addOnCurrencySwitcherChanged(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        currencyHelper.removeOnCurrencySwitcherChanged(this);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        tabLayout = (TabLayout) getActivity().getLayoutInflater().inflate(R.layout.view_tabs, getBaseActivity().getAppBarLayout(), false);
        getBaseActivity().addToAppBar(tabLayout);

        adapter = new PayslipViewPagerAdapter(getContext(), getFragmentManager());
        if (BundleHelper.containsArguments(getArguments(), TAG_DATE))
            adapter.setDate(BundleHelper.<Calendar>get(getArguments(), TAG_DATE));

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.payslip_viewpager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getBaseActivity().removeFromAppBar(tabLayout);
    }

    @Override
    public void initView(final View rootView) {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    private BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onChanged(CurrencyHelper.Kind kind) {
        changeIcon(getBaseActivity());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.currency_switcher, menu);
        changeIcon(getBaseActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_currency_switch:
                currencyHelper.showChoiceDialog(getActivity(), null);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeIcon(BaseActivity activity) {
        if (activity != null && activity.getToolbar() != null && activity.getToolbar().getMenu() != null) {
            MenuItem item = activity.getToolbar().getMenu().findItem(R.id.action_currency_switch);
            if (item != null)
                item.setIcon(new TextDrawable(getResources(), currencyHelper.getSymbolCurrencyText("")));
        }
    }
}
