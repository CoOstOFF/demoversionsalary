package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.ModelFactory;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationInfoModel;
import com.gsbelarus.gedemin.salary.service.SendDataService;

import java.util.Date;

public class GdMsgNewVacationInfoFragment extends GdMsgNewFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setTitleFragment(R.string.request_vacation_info);
        setContentView(R.layout.fragment_gdmsg_new_vacation_info);
    }

    @Override
    public void onResume() {
        super.onResume();

        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:
                GdMsgVacationInfoModel gdMsgVacationInfoModel = new GdMsgVacationInfoModel();
                gdMsgVacationInfoModel.setUid(getRealmHelper().generateUID(GdMsgVacationInfoModel.class));
                GdMsgModel tmpGdMsgModel = new GdMsgModel();
                tmpGdMsgModel.setUid(getRealmHelper().generateUID(GdMsgModel.class));
                GdMsgModel gdMsgModel = ModelFactory.newInstanceGdMsgVacationInfoModel(
                        gdMsgVacationInfoModel,
                        tmpGdMsgModel,
                        getComment(),
                        new Date());

                Intent intent = new Intent(getActivity(), SendDataService.class);
                intent.setAction(SendDataService.SEND_GD_MSG_ACTION);
                intent.putExtra(SendDataService.EXTRA_KEY_GD_MSG, gdMsgModel);
                getActivity().startService(intent);

                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
