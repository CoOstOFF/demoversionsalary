package com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.GdMsgNew;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgAbsenceModel;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GdMsgNewAbsenceFragment extends GdMsgNewPeriodFragment implements View.OnClickListener {

    public static final String TAG_TIME_PICKER_BEGIN = "time_picker_begin";
    public static final String TAG_TIME_PICKER_END = "time_picker_end";

    private static final String DATE_FORMAT = "d MMMM yyyy";
    private static final String TIME_FORMAT = "HH:mm";

    private TextView tvDateBegin;
    private TextView tvDateEnd;
    private TextView tvTimeBegin;
    private TextView tvTimeEnd;
    private RadioGroup rgAbsenceType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null && BundleHelper.containsArguments(getArguments(), GdMsgNew.TAG_MSG_DATE))
            setDateBegin(BundleHelper.<Date>get(getArguments(), GdMsgNew.TAG_MSG_DATE));

        if (getDateBegin() == null)
            setDateBegin(CalendarHelper.initCalendarFields(Calendar.getInstance(), Calendar.HOUR_OF_DAY, 0).getTime());

        if (getDateEnd() == null) {
            Calendar begin = Calendar.getInstance();
            begin.setTime(getDateBegin());
            setDateEnd(CalendarHelper.initCalendarFields(begin, Calendar.MINUTE, 1).getTime());
        }
    }

    @Override
    public void onCreateView(View rootView, Bundle savedInstanceState) {
        super.onCreateView(rootView, savedInstanceState);

        setTitleFragment(R.string.absence);
        setContentView(R.layout.fragment_gdmsg_new_absence);

        setViewAndChildrenEnabled(rootView, false);

        tvDateBegin = (TextView) rootView.findViewById(R.id.tv_date_begin);
        tvDateEnd = (TextView) rootView.findViewById(R.id.tv_date_end);
        tvTimeBegin = (TextView) rootView.findViewById(R.id.tv_start_time);
        tvTimeEnd = (TextView) rootView.findViewById(R.id.tv_finish_time);
        rgAbsenceType = (RadioGroup) rootView.findViewById(R.id.rg_absence_type);

        setDateTimeText(tvDateBegin, DATE_FORMAT, getDateBegin());
        setDateTimeText(tvDateEnd, DATE_FORMAT, getDateEnd());
        setDateTimeText(tvTimeBegin, TIME_FORMAT, getDateBegin());
        setDateTimeText(tvTimeEnd, TIME_FORMAT, getDateEnd());

        tvDateBegin.setOnClickListener(this);
        tvDateEnd.setOnClickListener(this);
        tvTimeBegin.setOnClickListener(this);
        tvTimeEnd.setOnClickListener(this);

        if (savedInstanceState == null)
            showInNextVersionDialog();
    }

    @Override
    public void onClick(View v) {
        DatePickerDialog datePickerDialog = null;
        if (v == tvDateBegin)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_BEGIN);
        else if (v == tvDateEnd)
            datePickerDialog = showDatePickerDialog(DatePickerType.DATE_END);
        else if (v == tvTimeBegin)
            showTimePickerDialog(TAG_TIME_PICKER_BEGIN, getDateBegin());
        else if (v == tvTimeEnd)
            showTimePickerDialog(TAG_TIME_PICKER_END, getDateEnd());

        if (datePickerDialog != null) {
            Calendar bufCal = Calendar.getInstance();
            datePickerDialog.setYearRange(bufCal.get(Calendar.YEAR), bufCal.get(Calendar.YEAR) + 1);
            datePickerDialog.setMinDate(bufCal);
        }
    }

    @Override
    protected void onDateChanged(DatePickerType type) {
        switch (type) {
            case DATE_BEGIN:
                setDateTimeText(tvDateBegin, DATE_FORMAT, getDateBegin());
                break;
            case DATE_END:
                setDateTimeText(tvDateEnd, DATE_FORMAT, getDateEnd());
                break;
        }
    }

    public void onTimeSet(String tag, int hourOfDay, int minute, int second) {
        Calendar calendar = Calendar.getInstance();

        switch (tag) {
            case TAG_TIME_PICKER_BEGIN:
                calendar.setTime(getDateBegin());
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                getDateBegin().setTime(calendar.getTimeInMillis());

                setDateTimeText(tvTimeBegin, TIME_FORMAT, getDateBegin());
                break;
            case TAG_TIME_PICKER_END:
                calendar.setTime(getDateEnd());
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                getDateEnd().setTime(calendar.getTimeInMillis());

                setDateTimeText(tvTimeEnd, TIME_FORMAT, getDateEnd());
                break;
        }
    }

    private void showTimePickerDialog(final String tag, Date date) {
        Calendar bufCal = Calendar.getInstance();
        bufCal.setTime(date);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                        GdMsgNewAbsenceFragment.this.onTimeSet(tag, hourOfDay, minute, second);
                    }
                },
                bufCal.get(Calendar.HOUR_OF_DAY), bufCal.get(Calendar.MINUTE), true);
        timePickerDialog.enableSeconds(false);
        timePickerDialog.vibrate(false);
        timePickerDialog.dismissOnPause(true);
        timePickerDialog.show(getActivity().getFragmentManager(), tag);
    }

    private void setDateTimeText(TextView textView, String format, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        textView.setText(dateFormat.format(date));
    }

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((App) getActivity().getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    private GdMsgAbsenceModel.Type getCheckedType() {
        switch (rgAbsenceType.getCheckedRadioButtonId()) {
            case R.id.rb_absence_type_sick:
                return GdMsgAbsenceModel.Type.SICK;
            case R.id.rb_absence_type_donor_day:
                return GdMsgAbsenceModel.Type.DONOR_DAY;
            case R.id.rb_absence_type_military_commissariat:
                return GdMsgAbsenceModel.Type.MILITARY_COMMISSARIAT;
            case R.id.rb_absence_type_subpoena:
                return GdMsgAbsenceModel.Type.SUBPOENA;
            case R.id.rb_absence_type_other:
                return GdMsgAbsenceModel.Type.OTHER;
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:

//                if (getDateBegin().before(getDateEnd())) {
//                    GdMsgModel gdMsgModel = ModelFactory.newInstanceGdMsgAbsenceModel(
//                            getRealmHelper().generateUID(GdMsgAbsenceModel.class),
//                            new GdMsgAbsenceModel(),
//                            getRealmHelper().generateUID(GdMsgModel.class),
//                            new GdMsgModel(),
//                            getComment(),
//                            new Date(),
//                            getDateBegin(),
//                            getDateEnd(),
//                            getCheckedType());
//
//                    if (gdMsgModel.getGdMsgAbsenceModel().getType() == GdMsgAbsenceModel.Type.OTHER && getComment().replaceAll(" ", "").isEmpty()) {
//                        new MaterialDialog.Builder(getActivity())
//                                .title(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification)
//                                .content(getString(R.string.required, getString(R.string.et_comment)))
//                                .positiveText(R.string.introduce)
//                                .negativeText(R.string.cancel_text)
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
//                                        getCommentView().requestFocus();
//                                        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                                        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
//                                    }
//                                })
//                                .build()
//                                .show();
//
//                    } else {
//                        Intent intent = new Intent(getActivity(), SendDataService.class);
//                        intent.setAction(SendDataService.SEND_GD_MSG_ACTION);
//                        intent.putExtra(SendDataService.EXTRA_KEY_GD_MSG, gdMsgModel);
//                        getActivity().startService(intent);
//
//                        getActivity().finish();
//                    }
//                } else
//                    new DialogHelper(getActivity()).showMaterialDialog(
//                            getString(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification),
//                            getString(R.string.incorrect_dates_message),
//                            null);
                showInNextVersionDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    private void showInNextVersionDialog() {
        new AlertDialog.Builder(getActivity())
                .setPositiveButton(R.string.dialog_ok, null)
                .setMessage(R.string.available_in_next_release)
                .create()
                .show();

    }
}
