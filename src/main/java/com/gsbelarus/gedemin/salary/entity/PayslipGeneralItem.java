package com.gsbelarus.gedemin.salary.entity;

import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipGeneralAdapter;

import java.util.List;

public class PayslipGeneralItem {

    private String name;
    private String subName;
    private PayslipGeneralAdapter.ItemType id;
    private String title;
//    private int titleColor = R.color.md_black_1000;
    private String info;
    private double sum;
    private String formattedDate = "";
    private List<PayslipSubItem> payslipSubItems;

    public PayslipGeneralItem(PayslipGeneralAdapter.ItemType id, String info, String formattedDate, String name, String subName, double sum, List<PayslipSubItem> payslipSubItems) {
        this.id = id;
        this.formattedDate = formattedDate;
        this.name = name;
        this.subName = subName;
        this.info = info;
        this.sum = sum;
        this.payslipSubItems = payslipSubItems;
    }

    public PayslipGeneralItem(PayslipGeneralAdapter.ItemType id, String title, List<PayslipSubItem> payslipGeneralItemSubItems) {
        this.id = id;
        this.title = title;
        this.payslipSubItems = payslipGeneralItemSubItems;
    }

    public PayslipGeneralItem(PayslipGeneralAdapter.ItemType id, String title, int titleColor, List<PayslipSubItem> payslipGeneralItemSubItems) {
        this.id = id;
        this.title = title;
//        this.titleColor = titleColor;
        this.payslipSubItems = payslipGeneralItemSubItems;
    }
    // generated getters & setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public PayslipGeneralAdapter.ItemType getId() {
        return id;
    }

    public void setId(PayslipGeneralAdapter.ItemType id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
//
//    public int getTitleColor() {
//        return titleColor;
//    }
//
//    public void setTitleColor(int titleColor) {
//        this.titleColor = titleColor;
//    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

    public List<PayslipSubItem> getPayslipGeneralItemSubItems() {
        return payslipSubItems;
    }

    public void setPayslipGeneralItemSubItem(List<PayslipSubItem> payslipGeneralItemSubItems) {
        this.payslipSubItems = payslipGeneralItemSubItems;
    }
}
