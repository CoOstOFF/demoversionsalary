package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class TimesheetModel extends RealmObject implements Serializable {

    @Ignore
    private DayType dayType;        // наличие поля требует RealmObject

    @Expose
    private int dayTypeIndex;

    @PrimaryKey @Expose
    private int uid;

    @Expose
    private EmployeeModel employee;

    @Required @Expose
    private String description = "";

    @Expose
    private double hours;

    @Required @Expose
    private Date date;
    private RealmList<DayEventModel> events;

    public DayType getDayType() {
        int i = getDayTypeIndex();
        if (i < 0 || i + 1 > DayType.values().length) return DayType.UNKNOWN;
        return DayType.values()[i];
    }

    public void setDayType(DayType dayType) {
        setDayTypeIndex(dayType.ordinal());
    }

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getDayTypeIndex() {
        return dayTypeIndex;
    }

    public void setDayTypeIndex(int dayTypeIndex) {
        this.dayTypeIndex = dayTypeIndex;
    }

    public EmployeeModel getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeModel employee) {
        this.employee = employee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<DayEventModel> getEvents() {
        return events;
    }

    public void setEvents(RealmList<DayEventModel> events) {
        this.events = events;
    }
}
