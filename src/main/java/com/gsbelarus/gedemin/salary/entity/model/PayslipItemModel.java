package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;


public class PayslipItemModel extends RealmObject implements Serializable {

    public enum Category {DEBIT, CREDIT, TAX, PREPAYMENT}  // Начисления, Удержания, Налоги, Аванс

    @Ignore
    private Category category;      // наличие поля требует RealmObject

    @Expose
    private int categoryIndex;

    @PrimaryKey @Expose
    private int uid;

    @Expose
    private Date date;
    @Required @Expose
    private String name = "";
    @Expose
    private int code;

    @Expose
    private double debit;
    @Expose
    private double credit;

    public Category getCategory() {
        return Category.values()[getCategoryIndex()];
    }

    public void setCategory(Category category) {
        setCategoryIndex(category.ordinal());
    }

    // generated getters & setters

    public int getCategoryIndex() {
        return categoryIndex;
    }

    public void setCategoryIndex(int categoryIndex) {
        this.categoryIndex = categoryIndex;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
}
