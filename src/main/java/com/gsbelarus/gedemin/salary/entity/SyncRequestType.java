package com.gsbelarus.gedemin.salary.entity;

import java.io.Serializable;

public class SyncRequestType implements Serializable {

    public enum TypeOfRequest {
        AUTH_REQUEST, DATA_REQUEST, MSG_REQUEST, DATA_MSG_BOARDITEM_REQUEST, SEND_GD_MSG_STATES, CLEAN_DATA, INVITE_REQUEST, LOST_CLEAN_DATA, DENY_ACCESS_REQUEST, BOARD_ITEM_REQUEST
    }

    private TypeOfRequest typeOfRequest;
    private int data;

    public SyncRequestType(TypeOfRequest typeOfRequest) {
        this.typeOfRequest = typeOfRequest;
    }

    public SyncRequestType(TypeOfRequest typeOfRequest, int data) {
        this.typeOfRequest = typeOfRequest;
        this.data = data;
    }

    public TypeOfRequest getTypeOfRequest() {
        return typeOfRequest;
    }

    public SyncRequestType setTypeOfRequest(TypeOfRequest typeOfRequest) {
        this.typeOfRequest = typeOfRequest;
        return this;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
