package com.gsbelarus.gedemin.salary.entity;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

public class RealmSyncObject implements Serializable {

    @Expose
    private String cl;
    @Expose
    private List<Link> master;
    @Expose
    private List<Link> link;
    @Expose
    private String obj;
    @Expose
    private boolean deleted;

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public List<Link> getMaster() {
        return master;
    }

    public void setMaster(List<Link> master) {
        this.master = master;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(List<Link> link) {
        this.link = link;
    }

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
