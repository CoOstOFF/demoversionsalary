package com.gsbelarus.gedemin.salary.entity.model;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.google.gson.annotations.Expose;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewAbsenceFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewIncomeFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewVacationFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewVacationInfoFragment;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class GdMsgModel extends RealmObject implements Serializable {

    private static Context context = App.getContext();

    public enum RequestStatus {SUCCESS, ERROR, REJECTED, UNKNOWN}

    public enum Subject {INCOME, ABSENCE, VACATION, INFO, VACATION_INFO, UNKNOWN}

    public enum Sender {CLIENT, SERVER, UNKNOWN}

    ///TODO EmployeeModel
    @Ignore
    private RequestStatus requestStatus;
    @Expose
    private int requestStatusIndex;
    @Ignore
    private Subject subject;
    @Expose
    private int subjectIndex;
    @Ignore
    private Sender sender;
    @Expose
    private int senderIndex;

    @PrimaryKey
    @Expose
    private int uid;

    @Expose
    private boolean isUnread;
    @Required
    @Expose
    private Date timestamp;
    @Required
    @Expose
    private String msg = "";
    @Required
    @Expose
    private String comment = "";

    @Required
    private String gdMsgString = "";

    //TODO переделать
    @Expose
    private GdMsgAbsenceModel gdMsgAbsenceModel;
    @Expose
    private GdMsgIncomeModel gdMsgIncomeModel;
    @Expose
    private GdMsgVacationModel gdMsgVacationModel;
    @Expose
    private GdMsgInfoModel gdMsgInfoModel;
    @Expose
    private GdMsgVacationInfoModel gdMsgVacationInfoModel;

    @ColorInt
    public int getRequestStatusColor() {
        switch (getRequestStatus()) {
            case SUCCESS:
                return ContextCompat.getColor(context, R.color.messages_request_status_success);
            case ERROR:
                return ContextCompat.getColor(context, R.color.messages_request_status_error);
            case REJECTED:
                return ContextCompat.getColor(context, R.color.messages_request_status_rejected);
            case UNKNOWN:
            default:
                return ContextCompat.getColor(context, R.color.messages_request_status_unknown);
        }
    }

    public String getRequestStatusName() {
        switch (getRequestStatus()) {
            case SUCCESS:
                if (getSubject() == GdMsgModel.Subject.INCOME)
                    return context.getString(R.string.status_success_income) + ". ";
                else
                    return context.getString(R.string.status_success) + ". ";
            case REJECTED:
                return context.getString(R.string.status_rejected) + ". ";
            case ERROR:
                return context.getString(R.string.status_error) + ". ";
            case UNKNOWN:
                if (getSender() == GdMsgModel.Sender.SERVER &&
                        getSubject() != GdMsgModel.Subject.VACATION_INFO && getSubject() != GdMsgModel.Subject.INFO)
                    return context.getString(R.string.status_unprocessed) + ". ";
            default:
                return "";
        }
    }

    public GdMsgModel getRequestGdMsg() {
        if (getSender() == GdMsgModel.Sender.SERVER) {
            switch (getSubject()) {
                case ABSENCE:
                    if (getGdMsgAbsenceModel() == null) return null;
                    return getGdMsgAbsenceModel().getRequestGdMsgModel();
                case VACATION:
                    if (getGdMsgVacationModel() == null) return null;
                    return getGdMsgVacationModel().getRequestGdMsgModel();
                case INCOME:
                    if (getGdMsgIncomeModel() == null) return null;
                    return getGdMsgIncomeModel().getRequestGdMsgModel();
                case INFO:
                    if (getGdMsgInfoModel() == null) return null;
                    return getGdMsgInfoModel().getRequestGdMsgModel();
                case VACATION_INFO:
                    if (getGdMsgVacationInfoModel() == null) return null;
                    return getGdMsgVacationInfoModel().getRequestGdMsgModel();
            }
        }
        return null;
    }

    public String getSubjectName() {
        return getSubjectName(getSubject(), context);
    }

    public static String getSubjectName(GdMsgModel.Subject subject, Context context) {
        switch (subject) {
            case INCOME:
                return context.getString(R.string.income_statement);
            case ABSENCE:
                return context.getString(R.string.absence);
            case VACATION:
                return context.getString(R.string.request_vacation);
            case INFO:
                return context.getString(R.string.info);
            case VACATION_INFO:
                return context.getString(R.string.request_vacation_info);
            case UNKNOWN:
            default:
                return context.getString(R.string.unknown_data_type);
        }
    }

    /**
     * при добавлении новых типов запросов необходимо прописать для этих типов фрагменты
     */
    @Nullable
    public static Class<? extends BaseFragment> getSubjectFragmentClass(GdMsgModel.Subject subject) {
        switch (subject) {
            case INCOME:
                return GdMsgNewIncomeFragment.class;
            case ABSENCE:
                return GdMsgNewAbsenceFragment.class;
            case VACATION:
                return GdMsgNewVacationFragment.class;
            case VACATION_INFO:
                return GdMsgNewVacationInfoFragment.class;
            case INFO:
            case UNKNOWN:
            default:
                return null;
        }
    }

    public String getData() {
        String msgData = "";
        String dateBegin;
        String dateEnd;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("d MMMM", Locale.getDefault());
        switch (getSender()) {
            case CLIENT:
                SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
                SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("d MMMM HH:mm", Locale.getDefault());

                switch (getSubject()) {
                    case INCOME:
                        if (getGdMsgIncomeModel() != null) {
                            if (getGdMsgIncomeModel().getDateBegin() != null && getGdMsgIncomeModel().getDateEnd() != null) {
                                dateBegin = monthFormatter.format(getGdMsgIncomeModel().getDateBegin());
                                dateEnd = monthFormatter.format(getGdMsgIncomeModel().getDateEnd());
                                msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                            }
                        }
                        break;
                    case ABSENCE:
                        if (getGdMsgAbsenceModel() != null) {
                            if (getGdMsgAbsenceModel().getDateBegin() != null && getGdMsgAbsenceModel().getDateEnd() != null) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(getGdMsgAbsenceModel().getDateBegin());
                                if (calendar.get(Calendar.HOUR) != 0)
                                    dateBegin = dateTimeFormatter.format(getGdMsgAbsenceModel().getDateBegin());
                                else
                                    dateBegin = dateFormatter.format(getGdMsgAbsenceModel().getDateBegin());

                                calendar.setTime(getGdMsgAbsenceModel().getDateEnd());
                                if (calendar.get(Calendar.HOUR) != 0)
                                    dateEnd = dateTimeFormatter.format(getGdMsgAbsenceModel().getDateEnd());
                                else
                                    dateEnd = dateFormatter.format(getGdMsgAbsenceModel().getDateEnd());
                                msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                            }
                            String type = GdMsgAbsenceModel.getTypeName(getGdMsgAbsenceModel().getType());
                            if (!type.isEmpty())
                                msgData += (msgData.isEmpty() ? "" : ". ") + context.getString(R.string.absence_type) + " " + type;
                        }
                        break;
                    case VACATION:
                        if (getGdMsgVacationModel() != null) {
                            if (getGdMsgVacationModel().getDateBegin() != null && getGdMsgVacationModel().getDateEnd() != null) {
                                dateBegin = dateFormatter.format(getGdMsgVacationModel().getDateBegin().getTime());
                                dateEnd = dateFormatter.format(getGdMsgVacationModel().getDateEnd().getTime());
                                msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                            }
                            String type = "";
                            if (getGdMsgVacationModel().getType() == GdMsgVacationModel.Type.REGULAR)
                                type = context.getString(R.string.request_vacation_type_regular);
                            else if (getGdMsgVacationModel().getType() == GdMsgVacationModel.Type.UNPAID)
                                type = context.getString(R.string.request_vacation_type_unpaid);

                            if (!type.isEmpty())
                                msgData += (msgData.isEmpty() ? "" : " ") + type;
                        }
                        break;
                    case VACATION_INFO:
                        msgData = context.getString(R.string.request_vacation_info_message);
                        break;
                }
                break;
            case SERVER:
                if (getRequestStatus() == GdMsgModel.RequestStatus.SUCCESS) {
                    switch (getSubject()) {
                        case VACATION:
                            if (!getMsg().isEmpty())
                                msgData = context.getString(R.string.vacation_server_msg);
                            break;
                    }
                } else if (getRequestStatus() == GdMsgModel.RequestStatus.UNKNOWN) {
                    switch (getSubject()) {
                        case VACATION_INFO:
                            if (getGdMsgVacationInfoModel() != null) {
                                if (getGdMsgVacationInfoModel().getDateBegin() != null && getGdMsgVacationInfoModel().getDateEnd() != null) {
                                    dateBegin = dateFormatter.format(getGdMsgVacationInfoModel().getDateBegin().getTime());
                                    dateEnd = dateFormatter.format(getGdMsgVacationInfoModel().getDateEnd().getTime());
                                    msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                                } else {
                                    msgData = context.getString(R.string.vacation_info_empty);
                                }
                            }
                            break;
                    }
                }
                if (!getMsg().isEmpty())
                    msgData += (msgData.isEmpty() ? "" : ". ") + getMsg();
                break;
        }
        return msgData;
    }

    public String getSenderName() {
        switch (getSender()) {
            case CLIENT:
                return context.getString(R.string.msg_type_outgoing);
            case SERVER:
                return context.getString(R.string.msg_type_inbox);
            case UNKNOWN:
            default:
                return context.getString(R.string.unknown_data_type);
        }
    }

    public
    @DrawableRes
    int getSenderIconRes() {
        switch (getSender()) {
            case CLIENT:
                return R.drawable.ic_arrow_back_36dp;
            case SERVER:
                return R.drawable.ic_arrow_forward_36dp;
            case UNKNOWN:
            default:
                return R.drawable.ic_error_outline_36dp;
        }
    }

    public static String toString(GdMsgModel gdMsg) {
        if (gdMsg != null) {
            String result = gdMsg.getSenderName() +
                    gdMsg.getData() +
                    gdMsg.getSubjectName() +
                    gdMsg.getRequestStatusName() +
                    gdMsg.getComment() +
                    gdMsg.getFormattedTimestamp() +
                    gdMsg.getLongFormattedTimestamp();

            result += toString(gdMsg.getRequestGdMsg());

            return result.toLowerCase();
        } else {
            return "";
        }
    }

    public String getFormattedTimestamp() {
        Calendar currCalendar = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getTimestamp());

        SimpleDateFormat dateFormat;
        // current year
        if (calendar.get(Calendar.YEAR) == currCalendar.get(Calendar.YEAR)) {
            // current day
            if (calendar.get(Calendar.DAY_OF_YEAR) == currCalendar.get(Calendar.DAY_OF_YEAR)) {
                dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            } else {
                dateFormat = new SimpleDateFormat("d MMM", Locale.getDefault());
            }
        } else {
            dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        }

        return dateFormat.format(calendar.getTime());
    }

    public String getLongFormattedTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy, H:mm", Locale.getDefault());
        return dateFormat.format(getTimestamp().getTime());
    }

    public RequestStatus getRequestStatus() {
        int i = getRequestStatusIndex();
        if (i < 0 || i + 1 > RequestStatus.values().length) return RequestStatus.UNKNOWN;
        return RequestStatus.values()[i];
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        setRequestStatusIndex(requestStatus.ordinal());
    }

    public Subject getSubject() {
        int i = getSubjectIndex();
        if (i < 0 || i + 1 > Subject.values().length) return Subject.UNKNOWN;
        return Subject.values()[i];
    }

    public void setSubject(Subject subject) {
        setSubjectIndex(subject.ordinal());
    }

    public Sender getSender() {
        int i = getSenderIndex();
        if (i < 0 || i + 1 > Sender.values().length) return Sender.UNKNOWN;
        return Sender.values()[i];
    }

    public void setSender(Sender sender) {
        setSenderIndex(sender.ordinal());
    }

    // generated getters & setters

    public int getSenderIndex() {
        return senderIndex;
    }

    public void setSenderIndex(int senderIndex) {
        this.senderIndex = senderIndex;
    }

    public int getRequestStatusIndex() {
        return requestStatusIndex;
    }

    public void setRequestStatusIndex(int requestStatusIndex) {
        this.requestStatusIndex = requestStatusIndex;
    }

    public int getSubjectIndex() {
        return subjectIndex;
    }

    public void setSubjectIndex(int subjectIndex) {
        this.subjectIndex = subjectIndex;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setIsUnread(boolean isUnread) {
        this.isUnread = isUnread;
    }

    public boolean isUnread() {
        return isUnread;
    }

    public GdMsgAbsenceModel getGdMsgAbsenceModel() {
        return gdMsgAbsenceModel;
    }

    public void setGdMsgAbsenceModel(GdMsgAbsenceModel gdMsgAbsenceModel) {
        this.gdMsgAbsenceModel = gdMsgAbsenceModel;
    }

    public GdMsgIncomeModel getGdMsgIncomeModel() {
        return gdMsgIncomeModel;
    }

    public void setGdMsgIncomeModel(GdMsgIncomeModel gdMsgIncomeModel) {
        this.gdMsgIncomeModel = gdMsgIncomeModel;
    }

    public GdMsgVacationModel getGdMsgVacationModel() {
        return gdMsgVacationModel;
    }

    public void setGdMsgVacationModel(GdMsgVacationModel gdMsgVacationModel) {
        this.gdMsgVacationModel = gdMsgVacationModel;
    }

    public GdMsgInfoModel getGdMsgInfoModel() {
        return gdMsgInfoModel;
    }

    public void setGdMsgInfoModel(GdMsgInfoModel gdMsgInfoModel) {
        this.gdMsgInfoModel = gdMsgInfoModel;
    }

    public GdMsgVacationInfoModel getGdMsgVacationInfoModel() {
        return gdMsgVacationInfoModel;
    }

    public void setGdMsgVacationInfoModel(GdMsgVacationInfoModel gdMsgVacationInfoModel) {
        this.gdMsgVacationInfoModel = gdMsgVacationInfoModel;
    }

    public String getGdMsgString() {
        return gdMsgString;
    }

    public void setGdMsgString(String gdMsgString) {
        this.gdMsgString = gdMsgString;
    }
}
