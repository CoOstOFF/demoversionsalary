package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class SyncGdMsgStateModel extends RealmObject {

    public enum UpdatedStatus {READED, DELETED, UNKNOWN}

    @PrimaryKey @Required @Expose
    private String uid;
    @Ignore
    private UpdatedStatus status;
    @Expose
    private int statusIndex;
    @Expose
    private int gdMsgKey;

    public UpdatedStatus getStatus() {
        int i = getStatusIndex();
        if (i < 0 || i + 1 > UpdatedStatus.values().length) return UpdatedStatus.UNKNOWN;
        return UpdatedStatus.values()[i];
    }

    public void setStatus(UpdatedStatus status) {
        setStatusIndex(status.ordinal());
    }

    // generated getters & setters

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getGdMsgKey() {
        return gdMsgKey;
    }

    public void setGdMsgKey(int gdMsgKey) {
        this.gdMsgKey = gdMsgKey;
    }

    public int getStatusIndex() {
        return statusIndex;
    }

    public void setStatusIndex(int statusIndex) {
        this.statusIndex = statusIndex;
    }
}
