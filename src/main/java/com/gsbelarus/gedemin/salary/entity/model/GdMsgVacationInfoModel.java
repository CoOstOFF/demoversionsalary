package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;


public class GdMsgVacationInfoModel extends RealmObject implements Serializable {

    @PrimaryKey @Expose
    private int uid;

    @Expose
    private Date dateBegin;
    @Expose
    private Date dateEnd;

    @Ignore @Expose
    private int requestMsgKey;

    @Expose
    private GdMsgModel requestGdMsgModel;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getRequestMsgKey() {
        return requestMsgKey;
    }

    public void setRequestMsgKey(int requestMsgKey) {
        this.requestMsgKey = requestMsgKey;
    }

    public GdMsgModel getRequestGdMsgModel() {
        return requestGdMsgModel;
    }

    public void setRequestGdMsgModel(GdMsgModel requestGdMsgModel) {
        this.requestGdMsgModel = requestGdMsgModel;
    }
}
