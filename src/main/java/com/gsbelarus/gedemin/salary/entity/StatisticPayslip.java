package com.gsbelarus.gedemin.salary.entity;

import java.util.Calendar;

public class StatisticPayslip {
    private Calendar date;
    private double totalSum;
    private double difference;

    public StatisticPayslip() {
    }

    public StatisticPayslip(Calendar date, double totalSum) {
        this.date = date;
        this.totalSum = totalSum;
    }

    public StatisticPayslip(Calendar date, double totalSum, double difference) {
        this.date = date;
        this.totalSum = totalSum;
        this.difference = difference;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public double getDifference() {
        return difference;
    }

    public void setDifference(double difference) {
        this.difference = difference;
    }
}
