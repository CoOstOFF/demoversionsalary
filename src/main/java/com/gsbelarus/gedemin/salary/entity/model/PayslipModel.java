package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class PayslipModel extends RealmObject implements Serializable {

    @PrimaryKey @Expose
    private int uid;

    @Required @Expose
    private Date payDate;

    private RealmList<PayslipItemModel> items;              // позиции (Начисления, Удержания, Налоги)
    private RealmList<PayslipBenefitModel> benefits;        // Льготы
    private RealmList<PayslipDeductionModel> deductions;    // Вычеты

    @Expose
    private EmployeeModel employee;

    @Required @Expose
    private String department = "";
    @Required @Expose
    private String position = "";

    @Expose
    private boolean isFinal;
    @Expose
    private double endSaldo;

    @Expose
    private double salary;
    @Expose
    private double hourRate;
    @Expose
    private int dependents;

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public EmployeeModel getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeModel employee) {
        this.employee = employee;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getHourRate() {
        return hourRate;
    }

    public void setHourRate(double hourRate) {
        this.hourRate = hourRate;
    }

    public RealmList<PayslipItemModel> getItems() {
        return items;
    }

    public void setItems(RealmList<PayslipItemModel> items) {
        this.items = items;
    }

    public RealmList<PayslipBenefitModel> getBenefits() {
        return benefits;
    }

    public void setBenefits(RealmList<PayslipBenefitModel> benefits) {
        this.benefits = benefits;
    }

    public RealmList<PayslipDeductionModel> getDeductions() {
        return deductions;
    }

    public void setDeductions(RealmList<PayslipDeductionModel> deductions) {
        this.deductions = deductions;
    }

    public int getDependents() {
        return dependents;
    }

    public void setDependents(int dependents) {
        this.dependents = dependents;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public double getEndSaldo() {
        return endSaldo;
    }

    public void setEndSaldo(double endSaldo) {
        this.endSaldo = endSaldo;
    }

}
