package com.gsbelarus.gedemin.salary.entity;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class Link implements Serializable {

    @Expose
    private String cl;
    @Expose
    private int uid;
    @Expose
    private String field;

    public Link(String cl, int uid, String field) {
        this.cl = cl;
        this.uid = uid;
        this.field = field;
    }

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
