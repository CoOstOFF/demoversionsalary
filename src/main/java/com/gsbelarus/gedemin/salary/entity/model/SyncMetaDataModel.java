package com.gsbelarus.gedemin.salary.entity.model;

import android.support.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class SyncMetaDataModel extends RealmObject {

    public static final String DEFAULT_SYNC_DATE = "01.01.1900 00:00:00";

    @PrimaryKey
    private int uid;
    @Required
    private String scheme = "";
    @Required
    private String addressServer = "";
    @Required
    private String authKey = "";
    @Required
    private String lastSyncDate = "";
    @Required
    private String lastSyncMsgDate = "";
    @Required
    private String gcmRegistrationId = "";

    private String inviteCode = null;

    private boolean permits = true;

    @Required
    private String alias = "";
    private boolean lostMode;
    @Required
    private String lastSyncPostsDate = "";

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddressServer() {
        return addressServer;
    }

    public void setAddressServer(String addressServer) {
        this.addressServer = addressServer;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getLastSyncMsgDate() {
        return lastSyncMsgDate;
    }

    public void setLastSyncMsgDate(String lastSyncMsgDate) {
        this.lastSyncMsgDate = lastSyncMsgDate;
    }

    public String getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(String lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public String getGcmRegistrationId() {
        return gcmRegistrationId;
    }

    public void setGcmRegistrationId(String gcmRegistrationId) {
        this.gcmRegistrationId = gcmRegistrationId;
    }

    @Nullable
    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public boolean isPermits() {
        return permits;
    }

    public void setPermits(boolean permits) {
        this.permits = permits;
    }

    public boolean isLostMode() {
        return lostMode;
    }

    public void setLostMode(boolean lostMode) {
        this.lostMode = lostMode;
    }

    public String getLastSyncPostsDate() {
        return lastSyncPostsDate;
    }

    public void setLastSyncPostsDate(String lastSyncPostsDate) {
        this.lastSyncPostsDate = lastSyncPostsDate;
    }
}
