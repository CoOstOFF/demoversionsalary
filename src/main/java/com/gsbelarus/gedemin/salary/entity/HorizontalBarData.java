package com.gsbelarus.gedemin.salary.entity;

public class HorizontalBarData {

    private int color;
    private long sum;

    public HorizontalBarData() {
    }

    public HorizontalBarData(int color, long sum) {
        this.color = color;
        this.sum = sum;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }
}
