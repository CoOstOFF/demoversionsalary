package com.gsbelarus.gedemin.salary.entity.model;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;

import com.google.gson.annotations.Expose;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class DayEventModel extends RealmObject implements Serializable {

    private static Context context = App.getContext();

    public enum EventType {
        /**
         * Рабочее событие
         */
        WORKING,
        /**
         * Ночное событие
         */
        NIGHT,
        /**
         * Государственный праздник
         */
        PUBLIC_HOLIDAY,
        /**
         * Нерабочее событие
         */
        NOT_WORKING,
        /**
         * Отпуск
         */
        VACATION,
        /**
         * Больничный
         */
        SICK,
        /**
         * Прогул
         */
        ABSENTEEISM,
        /**
         * wtf???
         */
        UNKNOWN
    }

    @Expose
    private int eventTypeIndex;

    @PrimaryKey @Expose
    private int uid;

    @Expose
    private Date timeBegin;
    @Expose
    private Date timeEnd;

    @Expose
    private double hours;

    @Required @Expose
    private String description = "";
    @Expose
    private boolean isAllDay;

    public DayEventModel(int uid, EventType eventType, Date timeBegin, Date timeEnd, double hours, String description, boolean isAllDay) {
        this.eventTypeIndex = eventType.ordinal();
        this.uid = uid;
        this.timeBegin = timeBegin;
        this.timeEnd = timeEnd;
        this.hours = hours;
        this.description = description;
        this.isAllDay = isAllDay;
    }

    public DayEventModel() {

    }

    public EventType getEventType() {
        int i = eventTypeIndex;
        if (i < 0 || i + 1 > EventType.values().length) return EventType.UNKNOWN;
        return EventType.values()[i];
    }

    public void setEventType(EventType eventType) {
        eventTypeIndex = eventType.ordinal();
    }

    public String getFormatDuration() {
        if (getTimeBegin() != null && getTimeEnd() != null) {

            SimpleDateFormat formatter = new SimpleDateFormat("H:mm", Locale.getDefault());
            return formatter.format(getTimeBegin()) + " - " + formatter.format(getTimeEnd());

        } else {
            double hours = getHours();
            if (hours - (int) hours > 0)
                return String.valueOf(hours) + context.getResources().getString(R.string.hour_abbr);
            else
                return String.valueOf((int) hours) + context.getResources().getString(R.string.hour_abbr);
        }
    }

    @ColorInt
    public int getColor() {
        return getColor(getEventType());
    }

    @ColorInt
    public static int getColor(DayEventModel.EventType eventType) {
        switch (eventType) {
            case WORKING:
                return ContextCompat.getColor(context, R.color.calendar_event_working);
            case NOT_WORKING:
                return ContextCompat.getColor(context, R.color.calendar_event_not_working);
            case NIGHT:
                return ContextCompat.getColor(context, R.color.calendar_event_night);
            case VACATION:
                return ContextCompat.getColor(context, R.color.calendar_event_vacation);
            case PUBLIC_HOLIDAY:
                return ContextCompat.getColor(context, R.color.calendar_event_public_holiday);
            case SICK:
                return ContextCompat.getColor(context, R.color.calendar_event_sick);
            case ABSENTEEISM:
                return ContextCompat.getColor(context, R.color.calendar_event_absenteeism);
            case UNKNOWN:
            default:
                return ContextCompat.getColor(context, R.color.calendar_event_unknown);
        }
    }

    public String getName() {
        switch (getEventType()) {
            case WORKING:
                return context.getString(R.string.event_type_working);
            case NOT_WORKING:
                return context.getString(R.string.event_type_not_working);
            case NIGHT:
                return context.getString(R.string.event_type_night);
            case VACATION:
                return context.getString(R.string.event_type_vacation);
            case PUBLIC_HOLIDAY:
                return context.getString(R.string.event_type_public_holiday);
            case SICK:
                return context.getString(R.string.event_type_sick);
            case ABSENTEEISM:
                return context.getString(R.string.event_type_absenteeism);
            case UNKNOWN:
            default:
                return context.getString(R.string.unknown_data_type);
        }
    }

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(Date timeBegin) {
        this.timeBegin = timeBegin;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAllDay() {
        return isAllDay;
    }

    public void setAllDay(boolean isAllDay) {
        this.isAllDay = isAllDay;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }
}
