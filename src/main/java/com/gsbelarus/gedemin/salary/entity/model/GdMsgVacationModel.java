package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class GdMsgVacationModel extends RealmObject implements Serializable {

    public enum Type {REGULAR, UNPAID, UNKNOWN}

    @PrimaryKey @Expose
    private int uid;

    @Ignore
    private Type type;
    @Expose
    private int typeIndex;
    @Expose
    private Date dateBegin;
    @Expose
    private Date dateEnd;

    @Ignore @Expose
    private int requestMsgKey;

    @Expose
    private GdMsgModel requestGdMsgModel; // для серверных сообщений - сообщение на которое ответ

    public Type getType() {
        int i = getTypeIndex();
        if (i < 0 || i + 1 > Type.values().length) return Type.UNKNOWN;
        return Type.values()[i];
    }

    public void setType(Type type) {
        setTypeIndex(type.ordinal());
    }

    // generated getters & setters

    public int getTypeIndex() {
        return typeIndex;
    }

    public void setTypeIndex(int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public GdMsgModel getRequestGdMsgModel() {
        return requestGdMsgModel;
    }

    public void setRequestGdMsgModel(GdMsgModel requestGdMsgModel) {
        this.requestGdMsgModel = requestGdMsgModel;
    }

    public int getRequestMsgKey() {
        return requestMsgKey;
    }

    public void setRequestMsgKey(int requestMsgKey) {
        this.requestMsgKey = requestMsgKey;
    }
}
