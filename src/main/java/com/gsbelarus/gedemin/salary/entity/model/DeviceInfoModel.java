package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class DeviceInfoModel extends RealmObject implements Serializable {

    public static final int CURRENT_DEVICE_UID = -1;

    @PrimaryKey @Expose
    private int uid;

    @Required @Expose
    private String phoneNumber = "";
    @Required @Expose
    private String deviceId = "";
    @Required @Expose
    private String deviceModel = "";

    public DeviceInfoModel(int uid, String phoneNumber, String deviceId, String deviceModel) {
        this.uid = uid;
        this.phoneNumber = phoneNumber;
        this.deviceId = deviceId;
        this.deviceModel = deviceModel;
    }

    public DeviceInfoModel() {
    }

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }
}
