package com.gsbelarus.gedemin.salary.entity;

import java.util.List;

public class PayslipDetailCard {

    private List<PayslipSubItem> payslipSubItems;
    private double sum;
    private String name;
//    private int titleColor = R.color.md_black_1000;
    private String info;

    public PayslipDetailCard() {
    }

    public PayslipDetailCard(String name, String info, double sum, List<PayslipSubItem> payslipSubItems) {
        this.payslipSubItems = payslipSubItems;
        this.sum = sum;
        this.name = name;
        this.info = info;
    }

    public PayslipDetailCard(String name, String info, int titleColor, double sum, List<PayslipSubItem> payslipSubItems) {
        this.payslipSubItems = payslipSubItems;
        this.name = name;
//        this.titleColor = titleColor;
        this.sum = sum;
        this.info = info;
    }

    // generated getters & setters

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
//
//    public int getTitleColor() {
//        return titleColor;
//    }
//
//    public void setTitleColor(int titleColor) {
//        this.titleColor = titleColor;
//    }

    public List<PayslipSubItem> getPayslipSubItems() {
        return payslipSubItems;
    }

    public void setPayslipSubItems(List<PayslipSubItem> payslipSubItems) {
        this.payslipSubItems = payslipSubItems;
    }
}
