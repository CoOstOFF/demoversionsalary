package com.gsbelarus.gedemin.salary.entity;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.gsbelarus.gedemin.salary.entity.model.SyncGdMsgStateModel;

import java.lang.reflect.Type;

public class SyncGdMsgStateModelSerializer implements JsonSerializer<SyncGdMsgStateModel> {

    @Override
    public JsonElement serialize(SyncGdMsgStateModel model, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("uid", model.getUid());
        jsonObject.addProperty("gdMsgKey", model.getGdMsgKey());
        jsonObject.addProperty("statusIndex", model.getStatusIndex());
        return jsonObject;
    }
}
