package com.gsbelarus.gedemin.salary.entity.model;

import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;

import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;

public enum DayType {

    DAY_OFF, WORK, UNKNOWN;

    @ColorInt
    public static int getColor(DayType dayType) {
        switch (dayType) {
            case WORK:
                return ContextCompat.getColor(App.getContext(), R.color.calendar_work);
            case DAY_OFF:
                return ContextCompat.getColor(App.getContext(), R.color.calendar_day_off);
            case UNKNOWN:
            default:
                return ContextCompat.getColor(App.getContext(), R.color.calendar_unknown);
        }
    }

    public static String getName(DayType dayType) {
        switch (dayType) {
            case WORK:
                return App.getContext().getString(R.string.day_type_work);
            case DAY_OFF:
                return App.getContext().getString(R.string.day_type_day_off);
            case UNKNOWN:
            default:
                return App.getContext().getString(R.string.unknown_data_type);
        }
    }
}
