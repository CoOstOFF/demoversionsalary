package com.gsbelarus.gedemin.salary.entity.model;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class GdMsgAbsenceModel extends RealmObject implements Serializable {

    private static Context context = App.getContext();

    public enum Type {SICK, DONOR_DAY, MILITARY_COMMISSARIAT, SUBPOENA, OTHER, UNKNOWN}

    @PrimaryKey @Expose
    private int uid;

    @Ignore
    private Type type;
    @Expose
    private int typeIndex;
    @Expose
    private Date dateBegin;
    @Expose
    private Date dateEnd;

    @Ignore @Expose
    private int requestMsgKey;

    @Expose
    private GdMsgModel requestGdMsgModel; // для серверных сообщений - сообщение на которое ответ

    public Type getType() {
        int i = getTypeIndex();
        if (i < 0 || i + 1 > Type.values().length) return Type.UNKNOWN;
        return Type.values()[i];
    }

    public void setType(Type type) {
        setTypeIndex(type.ordinal());
    }

    public static String getTypeName(GdMsgAbsenceModel.Type type) {
        switch (type) {
            case SICK:
                return context.getString(R.string.request_absence_type_sick);
            case DONOR_DAY:
                return context.getString(R.string.request_absence_type_donor_day);
            case MILITARY_COMMISSARIAT:
                return context.getString(R.string.request_absence_type_military_commissariat);
            case SUBPOENA:
                return context.getString(R.string.request_absence_type_subpoena);
            case OTHER:
                return context.getString(R.string.request_absence_type_other);
            case UNKNOWN:
            default:
                return context.getString(R.string.unknown_data_type);
        }
    }

    // generated getters & setters

    public int getTypeIndex() {
        return typeIndex;
    }

    public void setTypeIndex(int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public GdMsgModel getRequestGdMsgModel() {
        return requestGdMsgModel;
    }

    public void setRequestGdMsgModel(GdMsgModel requestGdMsgModel) {
        this.requestGdMsgModel = requestGdMsgModel;
    }

    public int getRequestMsgKey() {
        return requestMsgKey;
    }

    public void setRequestMsgKey(int requestMsgKey) {
        this.requestMsgKey = requestMsgKey;
    }
}
