package com.gsbelarus.gedemin.salary.entity;

public class DrawerPersonInfoItem {

    private String personInfoText;
    private String personInfoItem;
    private int personInfoImage;


    public DrawerPersonInfoItem(String personInfoText, String personInfoItem, int personInfoImage) {

        this.personInfoText = personInfoText;
        this.personInfoItem = personInfoItem;
        this.personInfoImage = personInfoImage;
    }


    public String getPersonInfoText() {
        return personInfoText;
    }

    public void setPersonInfoText(String personInfoText) {
        this.personInfoText = personInfoText;
    }

    public String getPersonInfoItem() {
        return personInfoItem;
    }

    public void setPersonInfoItem(String personInfoItem) {
        this.personInfoItem = personInfoItem;
    }

    public int getPersonInfoImage() {
        return personInfoImage;
    }

    public void setPersonInfoImage(int personInfoImage) {
        this.personInfoImage = personInfoImage;
    }
}
