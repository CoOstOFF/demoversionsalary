package com.gsbelarus.gedemin.salary.entity;

import com.gsbelarus.gedemin.salary.adapter.payslip.PayslipGeneralAdapter;

public class PayslipSubItem {

    int color;
    private String name;
    private double sum;

    private PayslipGeneralAdapter.SubItemType id;
    private String formattedDate = "";

    public PayslipSubItem(String formattedDate, String name, double sum, int color) {
        this.formattedDate = formattedDate;
        this.name = name;
        this.sum = sum;
        this.color = color;
    }

    public PayslipSubItem(String name, double sum, int color) {
        this.name = name;
        this.sum = sum;
        this.color = color;
    }

    public PayslipSubItem(PayslipGeneralAdapter.SubItemType id, String name, double sum) {
        this.id = id;
        this.name = name;
        this.sum = sum;
    }

    public PayslipSubItem(String formattedDate, PayslipGeneralAdapter.SubItemType id, String name, double sum) {
        this.formattedDate = formattedDate;
        this.id = id;
        this.name = name;
        this.sum = sum;
    }

    // generated getters & setters


    public PayslipGeneralAdapter.SubItemType getId() {
        return id;
    }

    public void setId(PayslipGeneralAdapter.SubItemType id) {
        this.id = id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }
}
