package com.gsbelarus.gedemin.salary.entity.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class GdMsgInfoModel extends RealmObject implements Serializable {

    @PrimaryKey @Expose
    private int uid;

    @Ignore @Expose
    private int requestMsgKey;

    @Expose
    private GdMsgModel requestGdMsgModel; // для серверных сообщений - сообщение на которое ответ

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public GdMsgModel getRequestGdMsgModel() {
        return requestGdMsgModel;
    }

    public void setRequestGdMsgModel(GdMsgModel requestGdMsgModel) {
        this.requestGdMsgModel = requestGdMsgModel;
    }

    public int getRequestMsgKey() {
        return requestMsgKey;
    }

    public void setRequestMsgKey(int requestMsgKey) {
        this.requestMsgKey = requestMsgKey;
    }
}
