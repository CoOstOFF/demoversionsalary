package com.gsbelarus.gedemin.salary.entity.model;


import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class PayslipDeductionModel extends RealmObject implements Serializable {

    @PrimaryKey @Expose
    private int uid;

    @Expose
    private Date date;
    @Required @Expose
    private String name = "";
    @Required @Expose
    private String deductionKind = ""; // вид вычета
    @Expose
    private double sum;
    @Expose
    private int code;

    // generated getters & setters

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeductionKind() {
        return deductionKind;
    }

    public void setDeductionKind(String deductionKind) {
        this.deductionKind = deductionKind;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
