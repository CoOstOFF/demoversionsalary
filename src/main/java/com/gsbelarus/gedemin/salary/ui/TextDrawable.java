package com.gsbelarus.gedemin.salary.ui;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

public class TextDrawable extends Drawable {

    private static final int DEFAULT_COLOR = Color.WHITE;
    private static final int DEFAULT_TEXT_SIZE = 20;

    private final String text;
    private final Paint paint;

    private int mIntrinsicWidth;
    private int mIntrinsicHeight;

    public TextDrawable(Resources res, String text) {
        this.text = text;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(DEFAULT_COLOR);
        paint.setTextAlign(Paint.Align.CENTER);
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                DEFAULT_TEXT_SIZE, res.getDisplayMetrics());
        paint.setTextSize(textSize);
        mIntrinsicWidth = (int) (paint.measureText(text, 0, text.length()) + .5);
        mIntrinsicHeight = paint.getFontMetricsInt(null);
    }

    @Override
    public void draw(Canvas canvas) {
        Rect r = getBounds();
        canvas.drawText(text, r.width() / 2, r.height() / 2 - ((paint.descent() + paint.ascent()) / 2), paint);
    }

    @Override
    public int getOpacity() {
        return paint.getAlpha();
    }

    @Override
    public int getIntrinsicWidth() {
        return mIntrinsicWidth;
    }

    @Override
    public int getIntrinsicHeight() {
        return mIntrinsicHeight;
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter filter) {
        paint.setColorFilter(filter);
    }
}
