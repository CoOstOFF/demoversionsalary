package com.gsbelarus.gedemin.salary.ui;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.EditText;

// костыль для фокуса на поле ввода вместе с ошибкой
public class FocusableTextInputLayout extends TextInputLayout {

    public FocusableTextInputLayout(Context context) {
        super(context);
    }

    public FocusableTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FocusableTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setError(CharSequence error) {
        super.setError(error);
        if (error != null) {
            EditText editText = getEditText();
            if (editText == null) return;
            if (editText.hasFocus()) editText.clearFocus();
            setFocusable(true);
            setFocusableInTouchMode(true);
            requestFocus();
            setFocusable(false);
            setFocusableInTouchMode(false);
            editText.requestFocus();
        }
    }
}
