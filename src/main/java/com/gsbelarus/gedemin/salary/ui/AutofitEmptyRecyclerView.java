package com.gsbelarus.gedemin.salary.ui;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;

public class AutofitEmptyRecyclerView extends EmptyRecyclerView {
    private float columnWidth = -1;

    public AutofitEmptyRecyclerView(Context context) {
        super(context);
    }

    public AutofitEmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutofitEmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (columnWidth > 0 && getLayoutManager() != null) {
            int spanCount = (int) Math.max(1, getMeasuredWidth() / columnWidth);
            if (getLayoutManager() instanceof GridLayoutManager)
                ((GridLayoutManager) getLayoutManager()).setSpanCount(spanCount);
            if (getLayoutManager() instanceof StaggeredGridLayoutManager)
                ((StaggeredGridLayoutManager) getLayoutManager()).setSpanCount(spanCount);
        }
    }

//    @Override
//    public boolean canScrollVertically(int direction) {
//        // check if scrolling up
//        if (direction < 1) {
//            boolean original = super.canScrollVertically(direction);
//            return !original && getChildAt(0) != null && getChildAt(0).getTop() < 0 || original;
//        }
//        return super.canScrollVertically(direction);
//
//    }

    public float getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(float columnWidth) {
        this.columnWidth = columnWidth;
    }
}
