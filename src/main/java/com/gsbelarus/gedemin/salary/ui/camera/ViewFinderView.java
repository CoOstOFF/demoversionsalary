package com.gsbelarus.gedemin.salary.ui.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.gsbelarus.gedemin.salary.R;

public class ViewFinderView extends View {

    private Rect framingRect;
    private Rect borderRect;

    private static final int MIN_FRAME_WIDTH = 240;
    private static final int MIN_FRAME_HEIGHT = 240;

    private static final float PORTRAIT_WIDTH_RATIO = 7f / 8;
    private static final float PORTRAIT_HEIGHT_RATIO = 4f / 8;
    private static final int PORTRAIT_MAX_FRAME_WIDTH = (int) (1080 * PORTRAIT_WIDTH_RATIO);
    private static final int PORTRAIT_MAX_FRAME_HEIGHT = (int) (1920 * PORTRAIT_HEIGHT_RATIO);

    int lineLength;
    int paddingLinesRectSmall;
    int paddingLinesRectMedium;

    private Paint maskPaint;
    private Paint borderPaint;
    private int borderColor = ContextCompat.getColor(getContext(), R.color.viewfinder_border);
    private int maskColor = ContextCompat.getColor(getContext(), R.color.viewfinder_mask);

    public ViewFinderView(Context context) {
        super(context);
        init();
    }

    public ViewFinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        //finder mask paint
        maskPaint = new Paint();
        maskPaint.setColor(maskColor);

        //border paint
        borderPaint = new Paint();
        borderPaint.setColor(borderColor);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(dpToPx(3));

        //dimens
        lineLength = dpToPx(25);
        paddingLinesRectSmall = dpToPx(21);
        paddingLinesRectMedium = dpToPx(22.5);

    }

    public void setupViewFinder() {
        updateFramingRect();
        invalidate();
    }

    public Rect getFramingRect() {
        return framingRect;
    }

    public void setBorderColor(int borderColor) {
        borderPaint.setColor(borderColor);
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (framingRect == null) {
            return;
        }

        drawMask(canvas);
        drawBorder(canvas);
    }

    public void drawMask(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        canvas.drawRect(0, 0, width, framingRect.top, maskPaint);
        canvas.drawRect(0, framingRect.top, framingRect.left, framingRect.bottom + 1, maskPaint);
        canvas.drawRect(framingRect.right + 1, framingRect.top, width, framingRect.bottom + 1, maskPaint);
        canvas.drawRect(0, framingRect.bottom + 1, width, height, maskPaint);
    }

    public void drawBorder(Canvas canvas) {

        canvas.drawLine(framingRect.left + paddingLinesRectMedium, framingRect.top + paddingLinesRectSmall, framingRect.left + paddingLinesRectMedium, framingRect.top + paddingLinesRectMedium + lineLength, borderPaint);
        canvas.drawLine(framingRect.left + paddingLinesRectSmall, framingRect.top + paddingLinesRectMedium, framingRect.left + paddingLinesRectMedium + lineLength, framingRect.top + paddingLinesRectMedium, borderPaint);

        canvas.drawLine(framingRect.left + paddingLinesRectMedium, framingRect.bottom - paddingLinesRectSmall, framingRect.left + paddingLinesRectMedium, framingRect.bottom - paddingLinesRectMedium - lineLength, borderPaint);
        canvas.drawLine(framingRect.left + paddingLinesRectSmall, framingRect.bottom - paddingLinesRectMedium, framingRect.left + paddingLinesRectMedium + lineLength, framingRect.bottom - paddingLinesRectMedium, borderPaint);

        canvas.drawLine(framingRect.right - paddingLinesRectMedium, framingRect.top + paddingLinesRectSmall, framingRect.right - paddingLinesRectMedium, framingRect.top + paddingLinesRectMedium + lineLength, borderPaint);
        canvas.drawLine(framingRect.right - paddingLinesRectSmall, framingRect.top + paddingLinesRectMedium, framingRect.right - paddingLinesRectMedium - lineLength, framingRect.top + paddingLinesRectMedium, borderPaint);

        canvas.drawLine(framingRect.right - paddingLinesRectMedium, framingRect.bottom - paddingLinesRectSmall, framingRect.right - paddingLinesRectMedium, framingRect.bottom - paddingLinesRectMedium - lineLength, borderPaint);
        canvas.drawLine(framingRect.right - paddingLinesRectSmall, framingRect.bottom - paddingLinesRectMedium, framingRect.right - paddingLinesRectMedium - lineLength, framingRect.bottom - paddingLinesRectMedium, borderPaint);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        updateFramingRect();
    }

    public synchronized void updateFramingRect() {
        Point viewResolution = new Point(getWidth(), getHeight());
        if (viewResolution == null) {
            return;
        }
        int width;
        int height;

        width = findDesiredDimensionInRange(PORTRAIT_WIDTH_RATIO, viewResolution.x, MIN_FRAME_WIDTH, PORTRAIT_MAX_FRAME_WIDTH);
        height = findDesiredDimensionInRange(PORTRAIT_HEIGHT_RATIO, viewResolution.y, MIN_FRAME_HEIGHT, PORTRAIT_MAX_FRAME_HEIGHT);

        int leftOffSet = (viewResolution.x - width) / 2;
        int topOffSet = (viewResolution.y - height) / 2;
        framingRect = new Rect(leftOffSet, topOffSet, leftOffSet + width, topOffSet + height);
    }

    private static int findDesiredDimensionInRange(float ratio, int resolution, int min, int max) {
        int dimension = (int) (ratio * resolution);
        if (dimension < min) {
            return min;
        }
        if (dimension > max) {
            return max;
        }
        return dimension;
    }

    public int dpToPx(double dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round((int) dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}