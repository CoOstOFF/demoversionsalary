package com.gsbelarus.gedemin.salary.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class WrapSlidingUpLayout extends SlidingUpPanelLayout {

    private int mainContentHeight = -1;

    public WrapSlidingUpLayout(Context context) {
        super(context);
    }

    public WrapSlidingUpLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrapSlidingUpLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mainContentHeight == -1) {
            measureChild(getChildAt(0), widthMeasureSpec, heightMeasureSpec);
            mainContentHeight = getChildAt(0).getMeasuredHeight();
//            setPanelHeight(MeasureSpec.getSize(heightMeasureSpec) - mainContentHeight);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (mainContentHeight != -1) {
            int wrapPanelHeight = MeasureSpec.getSize(heightMeasureSpec) - mainContentHeight;
            if (getPanelHeight() != wrapPanelHeight)
                setPanelHeight(wrapPanelHeight);

            if (getPanelHeight() > getChildAt(1).getMeasuredHeight()) {
                getChildAt(1).getLayoutParams().height = getPanelHeight();
                measureChild(getChildAt(1), widthMeasureSpec, heightMeasureSpec);
            }
        }
    }
}
