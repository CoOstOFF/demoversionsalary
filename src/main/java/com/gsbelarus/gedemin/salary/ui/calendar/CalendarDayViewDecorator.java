package com.gsbelarus.gedemin.salary.ui.calendar;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;

import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;

import io.realm.RealmList;

public abstract class CalendarDayViewDecorator implements DayViewDecorator {

    private RealmHelper realmHelper;
    private Context context;

    public CalendarDayViewDecorator(Context context, RealmHelper realmHelper) {
        this.context = context;
        this.realmHelper = realmHelper;
    }

    @ColorInt
    protected int getDefaultBackgroundColor(RealmList<DayEventModel> dayEvents, DayType dayType) {
        if (dayEvents.size() == 1)
            return getColorARGB(dayEvents.get(0).getColor());
        else
            return getColorARGB(dayType.getColor(dayType));
    }

    @ColorInt
    protected int getColorARGB(@ColorInt int color) {
        return Color.argb(Color.alpha(color) / 2, Color.red(color), Color.green(color), Color.blue(color));
    }

    protected String getHoursString(double hours) {
        if (hours - (int) hours > 0)
            return String.valueOf(hours);
        else
            return String.valueOf((int) hours);
    }

    protected RealmHelper getRealmHelper() {
        return realmHelper;
    }

    protected void setRealmHelper(RealmHelper realmHelper) {
        this.realmHelper = realmHelper;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
