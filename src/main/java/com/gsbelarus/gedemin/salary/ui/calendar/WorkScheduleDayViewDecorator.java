package com.gsbelarus.gedemin.salary.ui.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.WorkScheduleModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmList;

public class WorkScheduleDayViewDecorator extends CalendarDayViewDecorator {

    private SimpleDateFormat dateFormat;

    public WorkScheduleDayViewDecorator(Context context, RealmHelper realmHelper) {
        super(context, realmHelper);
        this.dateFormat = new SimpleDateFormat("H:mm", Locale.getDefault());
    }

    @Override
    public boolean decorate(CalendarDay calendarDay, DayViewFacade dayViewFacade) {
        WorkScheduleModel day = getRealmHelper().getWorkScheduleByDay(calendarDay.getDate());

        if (day != null) {
            dayViewFacade.setGravity(Gravity.START | Gravity.TOP);
            RealmList<DayEventModel> dayEvents = day.getEvents();

            if (dayEvents.size() > 1)
                dayViewFacade.addOnDrawListener(new TopRightTriangleDrawer(getColorARGB(ContextCompat.getColor(getContext(), R.color.calendar_triangle))));

            if (getRealmHelper().containEventType(dayEvents, DayEventModel.EventType.PUBLIC_HOLIDAY))
                dayViewFacade.addOnDrawListener(new BorderDrawer(getColorARGB(DayEventModel.getColor(DayEventModel.EventType.PUBLIC_HOLIDAY))));

            String description = day.getDescription().toUpperCase();
            String duration = "";
            if (day.getDayType() == DayType.WORK) {
                Date begin = getRealmHelper().getTimeBeginEvents(dayEvents);
                if (begin != null)
                    duration = dateFormat.format(begin);
                else
                    duration = getHoursString(day.getHours());
            }

            dayViewFacade.addOnDrawListener(new BottomLabelDrawer(duration, description, dayViewFacade));

            dayViewFacade.setBackgroundColor(getDefaultBackgroundColor(dayEvents, day.getDayType()));

            return true;
        }

        return false;
    }
}
