package com.gsbelarus.gedemin.salary.ui.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.TimesheetModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import io.realm.RealmList;

public class TimesheetDayViewDecorator extends CalendarDayViewDecorator {

    public TimesheetDayViewDecorator(Context context, RealmHelper realmHelper) {
        super(context, realmHelper);
    }

    @Override
    public boolean decorate(CalendarDay calendarDay, DayViewFacade dayViewFacade) {
        TimesheetModel day = getRealmHelper().getTimesheetByDay(calendarDay.getDate());

        if (day != null) {
            dayViewFacade.setGravity(Gravity.START | Gravity.TOP);
            RealmList<DayEventModel> dayEvents = day.getEvents();

            if (dayEvents.size() > 1)
                dayViewFacade.addOnDrawListener(new TopRightTriangleDrawer(getColorARGB(ContextCompat.getColor(getContext(), R.color.calendar_triangle))));

            if (getRealmHelper().containEventType(dayEvents, DayEventModel.EventType.PUBLIC_HOLIDAY))
                dayViewFacade.addOnDrawListener(new BorderDrawer(getColorARGB(DayEventModel.getColor(DayEventModel.EventType.PUBLIC_HOLIDAY))));

            String description = day.getDescription().toUpperCase();
            if (day.getHours() > 0)
                dayViewFacade.addOnDrawListener(new BottomLabelDrawer(getHoursString(day.getHours()), description, dayViewFacade));
            else
                dayViewFacade.addOnDrawListener(new BottomLabelDrawer("", description, dayViewFacade));

            dayViewFacade.setBackgroundColor(getDefaultBackgroundColor(dayEvents, day.getDayType()));

            return true;
        }

        return false;
    }
}
