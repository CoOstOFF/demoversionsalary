package com.gsbelarus.gedemin.salary.ui.calendar;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.util.TypedValue;

import com.prolificinteractive.materialcalendarview.DayCheckedTextView;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

public class BottomLabelDrawer implements DayCheckedTextView.OnDrawListener {

    private Paint paintPriorityLabel = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint paintLabel = new Paint(Paint.ANTI_ALIAS_FLAG);
    private String priorityLabel;
    private String label;
    private DayViewFacade dayViewFacade;
    private float deltaSize;

    public BottomLabelDrawer(String priorityLabel, String label, DayViewFacade dayViewFacade) {
        if (priorityLabel.isEmpty() && label.isEmpty())
            dayViewFacade.removeOnDrawListener(this);

        this.priorityLabel = priorityLabel;
        this.label = label;
        this.dayViewFacade = dayViewFacade;
        this.deltaSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                4, dayViewFacade.getResource().getDisplayMetrics());
    }

    @Override
    public void onDraw(Canvas canvas, int height, int width, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        paintPriorityLabel.setColor(dayViewFacade.getCurrentTextColor());
        paintPriorityLabel.setTextSize(dayViewFacade.getTextSize() - deltaSize);

        paintLabel.setColor(getColorARGB(dayViewFacade.getCurrentTextColor()));
        paintLabel.setTextSize(dayViewFacade.getTextSize() - deltaSize);

        if (label != null && !label.isEmpty() &&
                paintLabel.measureText(label + " " + priorityLabel) + paddingLeft + paddingRight < width)
            canvas.drawText(label, paddingLeft, height - paintLabel.descent() - paddingBottom, paintLabel);

        canvas.drawText(priorityLabel, width - paintPriorityLabel.measureText(priorityLabel) - paddingRight, height - paintPriorityLabel.descent() - paddingBottom, paintPriorityLabel);
    }

    @ColorInt
    private int getColorARGB(@ColorInt int color) {
        return Color.argb(Color.alpha(color) / 2, Color.red(color), Color.green(color), Color.blue(color));
    }
}
