package com.gsbelarus.gedemin.salary.ui.calendar;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.annotation.ColorInt;

import com.prolificinteractive.materialcalendarview.DayCheckedTextView;

public class TopRightTriangleDrawer implements DayCheckedTextView.OnDrawListener {

    private Paint triangle = new Paint(Paint.ANTI_ALIAS_FLAG);

    public TopRightTriangleDrawer(@ColorInt int color) {
        triangle.setColor(color);
        triangle.setStyle(Paint.Style.FILL_AND_STROKE);
        triangle.setStrokeWidth(1);
    }

    @Override
    public void onDraw(Canvas canvas, int height, int width, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        Point point1 = new Point(width - width / 4, 0);
        Point point2 = new Point(width, 0);
        Point point3 = new Point(width, width / 4);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point1.x, point1.y);
        path.close();

        canvas.drawPath(path, triangle);
    }
}
