package com.gsbelarus.gedemin.salary.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;

public class NumberPicker extends FrameLayout {

    private static final String TAG_VALUE = "value";
    private static final String TAG_MIN_VALUE = "min_value";
    private static final String TAG_MAX_VALUE = "max_value";

    private long min = Long.MIN_VALUE;
    private long max = Long.MAX_VALUE;
    private long value;

    private EditText editText;
    private Button plus;
    private Button minus;

    private OnValueChangeListener onValueChangeListener;
    private TextWatcher textWatcher;

    public NumberPicker(Context context) {
        super(context);

        init();
    }

    public NumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public NumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_number_picker, this, true);
        editText = (EditText) findViewById(R.id.counter_et);
        plus = (Button) findViewById(R.id.plus_btn);
        minus = (Button) findViewById(R.id.minus_btn);
        editText.setText(String.valueOf(value));
        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.plus_btn:
                        value++;
                        break;
                    case R.id.minus_btn:
                        value--;
                        break;
                }
                editText.setText(String.valueOf(value));
                refreshButtonsState();
            }
        };
        plus.setOnClickListener(onClickListener);
        minus.setOnClickListener(onClickListener);

        editText.addTextChangedListener(textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    value = 0;
                    setMin(min);
                    setMax(max); // Корректировка значения value
                    if (onValueChangeListener != null) {
                        onValueChangeListener.onValueChange(value);
                    }
                } else {
                    try {
                        long newValue = Long.valueOf(charSequence.toString());
                        if (checkRange(newValue)) {
                            value = newValue;
                            if (onValueChangeListener != null) {
                                onValueChangeListener.onValueChange(value);
                            }
                        } else {
                            setValue(value);
                        }
                        refreshButtonsState();
                    } catch (NumberFormatException e) {
                        LogUtil.d(e);
                        setValue(value);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        LogUtil.d();
        Parcelable parcelable = super.onSaveInstanceState();
        if (parcelable instanceof Bundle) {
            ((Bundle) parcelable).putLong(TAG_VALUE, value);
            ((Bundle) parcelable).putLong(TAG_MIN_VALUE, min);
            ((Bundle) parcelable).putLong(TAG_MAX_VALUE, max);
        }
        return parcelable;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        LogUtil.d();
        if (state instanceof Bundle) {
            setValue(((Bundle) state).getLong(TAG_VALUE));
            setMin(((Bundle) state).getLong(TAG_MIN_VALUE));
            setMax(((Bundle) state).getLong(TAG_MAX_VALUE));
        }
        super.onRestoreInstanceState(state);
    }

    private boolean checkRange(long value) {
        return value >= min && value <= max;
    }

    private void refreshButtonsState() {
        plus.setEnabled(value < max);
        minus.setEnabled(value > min);
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
        if (value > max) {
            value = max;
        }
        refreshButtonsState();
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
        if (value < min) {
            value = min;
        }
        refreshButtonsState();
    }

    public long getValue() {
        return value;
    }

    public boolean setValue(long value) {
        if (checkRange(value)) {
            this.value = value;
            editText.removeTextChangedListener(textWatcher);
            editText.setText(String.valueOf(value));
            editText.addTextChangedListener(textWatcher);
            refreshButtonsState();
            return true;
        }
        return false;
    }

    public OnValueChangeListener getOnValueChangeListener() {
        return onValueChangeListener;
    }

    public void setOnValueChangeListener(OnValueChangeListener onValueChangeListener) {
        this.onValueChangeListener = onValueChangeListener;
    }

    public interface OnValueChangeListener {
        void onValueChange(long value);
    }
}
