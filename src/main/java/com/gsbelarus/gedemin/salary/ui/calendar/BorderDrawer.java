package com.gsbelarus.gedemin.salary.ui.calendar;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.annotation.ColorInt;

import com.prolificinteractive.materialcalendarview.DayCheckedTextView;

public class BorderDrawer implements DayCheckedTextView.OnDrawListener {

    private Paint borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public BorderDrawer(@ColorInt int color) {
        borderPaint.setColor(color);
        borderPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public void onDraw(Canvas canvas, int height, int width, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        borderPaint.setStrokeWidth(width / 14);

        Point point1 = new Point(0, 0);
        Point point2 = new Point(width, 0);
        Point point3 = new Point(width, height);
        Point point4 = new Point(0, height);

        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point4.x, point4.y);
        path.lineTo(point1.x, point1.y);
        path.close();

        canvas.drawPath(path, borderPaint);
    }
}
