package com.gsbelarus.gedemin.salary.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import com.gsbelarus.gedemin.salary.R;


//TODO rename
@CoordinatorLayout.DefaultBehavior(FloatingActionFrameLayout.Behavior.class)
public class FloatingActionFrameLayout extends FrameLayout {

    public FloatingActionFrameLayout(Context context) {
        super(context);
    }

    public FloatingActionFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatingActionFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public static class Behavior extends CoordinatorLayout.Behavior<FloatingActionFrameLayout> {

        private Rect mTmpRect;
        private boolean mIsAnimatingOut;

        @Override
        public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionFrameLayout child, View dependency) {
            if(dependency instanceof AppBarLayout) { // app:layout_anchor  AppBarLayout
                AppBarLayout appBarLayout = (AppBarLayout)dependency;
                if(mTmpRect == null) {
                    mTmpRect = new Rect();
                }

                Rect rect = mTmpRect;
                rect.set(0, 0, dependency.getWidth(), dependency.getHeight());
                offsetDescendantRect(parent, dependency, rect);

                //if(rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                if(rect.bottom <= appBarLayout.getResources().getDimension(R.dimen.floating_action_frame_layout_anim_height)) {
                    if(!mIsAnimatingOut && child.getVisibility() == View.VISIBLE) {
                        animateOut(child);
                    }
                } else if(child.getVisibility() != View.VISIBLE) {
                    animateIn(child);
                }
            }
            return false;
        }

        private void offsetDescendantRect(ViewGroup group, View child, Rect rect) {
            Matrix m = (Matrix) new ThreadLocal().get();
            if (m == null) {
                m = new Matrix();
                new ThreadLocal().set(m);
            } else {
                m.set(new Matrix());
            }

            offsetDescendantMatrix(group, child, m);
            RectF rectF = (RectF) new ThreadLocal().get();
            if (rectF == null) {
                rectF = new RectF();
            }

            rectF.set(rect);
            m.mapRect(rectF);
            rect.set((int) (rectF.left + 0.5F), (int) (rectF.top + 0.5F), (int) (rectF.right + 0.5F), (int) (rectF.bottom + 0.5F));
        }

        private void offsetDescendantMatrix(ViewParent target, View view, Matrix m) {
            ViewParent parent = view.getParent();
            if (parent instanceof View && parent != target) {
                View vp = (View) parent;
                offsetDescendantMatrix(target, vp, m);
                m.preTranslate((float) (-vp.getScrollX()), (float) (-vp.getScrollY()));
            }

            m.preTranslate((float) view.getLeft(), (float) view.getTop());
            if (!view.getMatrix().isIdentity()) {
                m.preConcat(view.getMatrix());
            }
        }

        private void animateIn(FloatingActionFrameLayout button) {
            button.setVisibility(View.VISIBLE);
            if(Build.VERSION.SDK_INT >= 14) {
                ViewCompat.animate(button).scaleX(1.0F).scaleY(1.0F).alpha(1.0F).setInterpolator(new FastOutSlowInInterpolator()).withLayer().setListener(null).start();
            } else {
                @SuppressLint("PrivateResource")
                Animation anim = android.view.animation.AnimationUtils.loadAnimation(button.getContext(), android.support.design.R.anim.abc_fade_in);
                anim.setDuration(200L);
                anim.setInterpolator(new FastOutSlowInInterpolator());
                button.startAnimation(anim);
            }

        }

        private void animateOut(final FloatingActionFrameLayout button) {
            if(Build.VERSION.SDK_INT >= 14) {
                ViewCompat.animate(button).scaleX(0.0F).scaleY(0.0F).alpha(0.0F).setInterpolator(new FastOutSlowInInterpolator()).withLayer().setListener(new ViewPropertyAnimatorListener() {
                    public void onAnimationStart(View view) {
                        Behavior.this.mIsAnimatingOut = true;
                    }

                    public void onAnimationCancel(View view) {
                        Behavior.this.mIsAnimatingOut = false;
                    }

                    public void onAnimationEnd(View view) {
                        Behavior.this.mIsAnimatingOut = false;
                        view.setVisibility(View.GONE);
                    }
                }).start();
            } else {
                @SuppressLint("PrivateResource")
                Animation anim = android.view.animation.AnimationUtils.loadAnimation(button.getContext(), android.support.design.R.anim.abc_fade_out);
                anim.setInterpolator(new FastOutSlowInInterpolator());
                anim.setDuration(200L);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Behavior.this.mIsAnimatingOut = true;
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Behavior.this.mIsAnimatingOut = false;
                        button.setVisibility(View.GONE);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
                button.startAnimation(anim);
            }
        }

    }

}
