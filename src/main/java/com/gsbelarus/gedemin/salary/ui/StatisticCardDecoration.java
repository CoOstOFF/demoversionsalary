package com.gsbelarus.gedemin.salary.ui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gsbelarus.gedemin.salary.adapter.StatisticCardsAdapter;

public class StatisticCardDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public StatisticCardDecoration(Context context, float dp) {
        space = (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public StatisticCardDecoration(Context context, int resource) {
        space = context.getResources().getDimensionPixelSize(resource);
    }

    /**
     * Retrieve any offsets for the given item. Each field of <code>outRect</code> specifies
     * the number of pixels that the item view should be inset by, similar to padding or margin.
     * The default implementation sets the bounds of outRect to 0 and returns.
     * <p/>
     * <p>If this ItemDecoration does not affect the positioning of item views it should set
     * all four fields of <code>outRect</code> (left, top, right, bottom) to zero
     * before returning.</p>
     *
     * @param outRect Rect to receive the output.
     * @param view    The child view to decorate
     * @param parent  RecyclerView this ItemDecoration is decorating
     * @param state   The current state of RecyclerView.
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getAdapter().getItemViewType(parent.getChildAdapterPosition(view)) == StatisticCardsAdapter.ItemTypes.HEADER.ordinal())
            return;

        outRect.top = space;
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
    }
}
