package com.gsbelarus.gedemin.salary.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public abstract class PermissionHelper {

    public static final int REQUEST_CODE = 0;

    public static class ReadPhoneState {

        private static void showPermissionRationale(Activity activity, MaterialDialog.SingleButtonCallback callback) {
            new MaterialDialog.Builder(activity).title("Уведомление")
                    .content("Для безопасной синхронизации необходимы id смартфона и телефонный номер.\nПредоставить права на получение?") ////TODO придумать, что написать
                    .positiveText("Предоставить")
                    .negativeText("Отменить")
                    .onAny(callback)
                    .show();
        }

        public static boolean check(Context context) {
            return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
        }

        public static boolean checkAndRequest(final Activity activity) {
            if (!check(activity.getApplicationContext())) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)) {
                    showPermissionRationale(activity, new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            if (dialogAction == DialogAction.POSITIVE)
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE);
                        }
                    });
                } else
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE);

                return false;
            }
            return true;
        }

        public static boolean checkAndRequest(final Fragment fragment) {
            if (!check(fragment.getContext())) {

                if (fragment.shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                    showPermissionRationale(fragment.getActivity(), new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction dialogAction) {
                                    if (dialogAction == DialogAction.POSITIVE)
                                        fragment.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE);
                                }
                            }
                    );
                } else
                    fragment.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE);
                return false;
            }
            return true;
        }
    }
}
