package com.gsbelarus.gedemin.salary.util;


import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Transliterator {

    private static  String CYRILLIC_SIMILAR_PATTERN = "[АаВСсЕеНКМОоРрТХхвнкмт]";

    public static String transliterateSimilarCyrToLat(String str, boolean isCaseInsensitive){
        Map<Character, String> similarCyrToLatRules = new HashMap<>();
        similarCyrToLatRules.put('А',"A");
        similarCyrToLatRules.put('а',"a");
        similarCyrToLatRules.put('В',"B");
        similarCyrToLatRules.put('С',"C");
        similarCyrToLatRules.put('с',"c");
        similarCyrToLatRules.put('Е',"E");
        similarCyrToLatRules.put('е',"e");
        similarCyrToLatRules.put('Н',"H");
        similarCyrToLatRules.put('К',"K");
        similarCyrToLatRules.put('М',"M");
        similarCyrToLatRules.put('О',"O");
        similarCyrToLatRules.put('о',"o");
        similarCyrToLatRules.put('Р',"P");
        similarCyrToLatRules.put('р',"p");
        similarCyrToLatRules.put('Т',"T");
        similarCyrToLatRules.put('Х',"Х");
        similarCyrToLatRules.put('х',"x");

        if(isCaseInsensitive) {
            similarCyrToLatRules.put('в', "B");
            similarCyrToLatRules.put('н', "H");
            similarCyrToLatRules.put('к', "K");
            similarCyrToLatRules.put('м', "M");
            similarCyrToLatRules.put('т', "T");
        }

        StringBuilder latinStr = new StringBuilder();
        for (char ch :  str.toCharArray())
            latinStr.append(similarCyrToLatRules.containsKey(ch) ? similarCyrToLatRules.get(ch) : String.valueOf(ch));

        return isCaseInsensitive ? latinStr.toString().toUpperCase() : latinStr.toString();
    }

    public static boolean containsSimilarCyr(String str) {
        return Pattern.compile(CYRILLIC_SIMILAR_PATTERN).matcher(str).find();
    }
}
