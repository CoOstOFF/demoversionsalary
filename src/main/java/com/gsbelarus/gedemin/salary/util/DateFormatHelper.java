package com.gsbelarus.gedemin.salary.util;

import android.annotation.SuppressLint;
import android.content.Context;

import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class DateFormatHelper {

    /**
     * @return возвращает дату в формате yyyy-MM-dd
     */
    @SuppressLint("SimpleDateFormat")
    public static String getDateString(Calendar date) {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date.getTimeInMillis());
    }

    /**
     * @return возвращает локализованную дату в формате МЕСЯЦ ГОД
     */
    public static String getMonthString(Context context, Calendar date) {
        return CalendarHelper.getMonthName(context, date.get(Calendar.MONTH)) + " " + date.get(Calendar.YEAR);
    }

    /**
     * Adds the given amount to a {@code Calendar} field.
     *
     * @param field the {@code Calendar} field to modify.
     * @param value the amount to add to the field.
     */
    public static Date getDate(int field, int value) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(field, value);

        return new Date(calendar.getTimeInMillis());
    }

    public static Date[] getMonthPeriodDates(Calendar date) {
        Calendar firstDate = (Calendar) date.clone();
        firstDate.set(Calendar.DAY_OF_MONTH, 1);
        CalendarHelper.initCalendarFields(firstDate, Calendar.DAY_OF_MONTH, 0);

        Calendar lastDate = (Calendar) firstDate.clone();
        lastDate.set(Calendar.DAY_OF_MONTH, date.getActualMaximum(Calendar.DAY_OF_MONTH));
        lastDate.set(Calendar.HOUR_OF_DAY, 23);
        CalendarHelper.initCalendarFields(lastDate, Calendar.HOUR_OF_DAY, 59);

        return new Date[]{firstDate.getTime(), lastDate.getTime()};
    }

    public static Date addCalendarValuesDiff(int yearDiff, int monthDiff, int dayDiff, int hourOfDayDiff, int minuteDiff) {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, yearDiff);
        now.add(Calendar.MONTH, monthDiff);
        now.add(Calendar.DAY_OF_MONTH, dayDiff);
        now.add(Calendar.HOUR_OF_DAY, hourOfDayDiff);
        now.add(Calendar.MINUTE, minuteDiff);

        return now.getTime();
    }

    public static Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

}
