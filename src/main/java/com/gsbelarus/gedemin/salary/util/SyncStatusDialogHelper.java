package com.gsbelarus.gedemin.salary.util;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.RegistrationPager;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;

public abstract class SyncStatusDialogHelper {

    public static void show(final BaseActivity activity, SyncServiceStatus syncServiceStatus, BaseActivity.SyncStatusDialog syncStatusDialog) {
//        if (syncServiceStatus.getStatus().getTypeOfStatus() == SyncStatus.TypeOfStatus.NO_INTERNET_CONNECTION) {
//            View view = activity.getWindow().getDecorView().findViewById(android.R.id.content);
//            if (view != null)
//                Snackbar.make(view, syncServiceStatus.getStatus().getMessage(), Snackbar.LENGTH_SHORT).show();
//
//        } else
        if (syncServiceStatus.getStatus().getMessage().equals(activity.getString(R.string.response_error_need_update_app))) {
            syncStatusDialog.showDialog(new MaterialDialog.Builder(activity)
                    .title(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification)
                    .content(syncServiceStatus.getStatus().getMessage())
                    .positiveText(R.string.action_download)
                    .negativeText(R.string.action_hide)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            SettingsFragment.showMarket(activity.getApplicationContext());
                        }
                    })
                    .build());

        } else if (syncServiceStatus.getStatus().getMessage().equals(activity.getString(R.string.successful_sync_demo_message))) {
            syncStatusDialog.showDialog(new MaterialDialog.Builder(activity)
                    .title(com.gedemin.gsbelarus.lib.R.string.gdmnlib_sync_status_notification)
                    .content(syncServiceStatus.getStatus().getMessage())
                    .positiveText(R.string.action_auth)
                    .negativeText(R.string.action_hide)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            activity.startActivity(new Intent(activity, RegistrationPager.class));
                        }
                    })
                    .build());

        } else
            syncStatusDialog.showAlert();
    }
}
