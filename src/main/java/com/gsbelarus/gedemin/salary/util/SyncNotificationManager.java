package com.gsbelarus.gedemin.salary.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.gsbelarus.gedemin.lib.sync.protocol.SyncNotificationAdapter;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;

import java.util.Map;

public class SyncNotificationManager implements SyncNotificationAdapter {

    private NotificationManager notificationManager;
    private NotificationCompat.Builder foregroundBuilder;
    private Context context;

    public static final int FOREGROUND_NOTIFICATION_ID = 10001;
    public static final int ERROR_NOTIFICATION_ID = 10002;

    public static final int SEND_NOTIFICATION_ID = 10003;
    public static final int NEW_MSG_NOTIFICATION_ID = 10004;
    public static final int UPDATE_AVAILABLE_ID = 10005;
    public static final int NEW_PAYSLIP_ID = 10006;
    public static final int NEW_ADVERT_NOTIFICATION_ID = 10007;

    private String contentTitle;

    public SyncNotificationManager(Context context) {
        this.context = context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        foregroundBuilder = new NotificationCompat.Builder(context);
        contentTitle = context.getString(R.string.app_name);
    }

    @Override
    public int getForegroundId() {
        return FOREGROUND_NOTIFICATION_ID;
    }

    @Override
    public Notification buildStartNotification(boolean isWorkMode) {
        clearForegroundNotification();
        String contentText;
        int iconResource;
        if (isWorkMode) {
            contentText = context.getString(R.string.notification_connect);
            iconResource = android.R.drawable.stat_sys_download;
        } else {
            contentText = context.getString(R.string.notification_create_demo);
            iconResource = android.R.drawable.ic_popup_sync;
        }
        foregroundBuilder.setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(iconResource)
                .setOngoing(true)
                .setProgress(0, 0, true)
                .setWhen(System.currentTimeMillis());
        return foregroundBuilder.build();
    }

    @Override
    public void showUpdatingNotification() {
        foregroundBuilder.setProgress(0, 0, true)
                .setContentText(context.getString(R.string.notification_update));
        notificationManager.notify(FOREGROUND_NOTIFICATION_ID, foregroundBuilder.build());
    }

    @Override
    public void showDownloadingNotification(int percent, int currentBlock, int totalBlocks) {
        foregroundBuilder.setProgress(100, percent, false)
                .setContentText(context.getString(R.string.notification_download) + " " + currentBlock + " " +
                        context.getString(R.string.notification_from) + " " + totalBlocks);
        notificationManager.notify(FOREGROUND_NOTIFICATION_ID, foregroundBuilder.build());
    }

    @Override
    public void showSyncStatusNotification(SyncStatus status) {
        showErrorNotification(status.getMessage());
    }

    @Override
    public void clearForegroundNotification() {
        notificationManager.cancel(FOREGROUND_NOTIFICATION_ID);
    }

    ///////////
    public void showErrorNotification(String errorMessage) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_sync_alert)//R.drawable.ic_sync_problem_24dp)
                .setProgress(0, 0, false)
                .setOngoing(false)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(contentTitle)
                .setContentText(errorMessage)
                .setCategory(NotificationCompat.CATEGORY_ERROR)
                .setLights(Color.RED, 500, 500)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(contentTitle)
                        .bigText(errorMessage))
                .setColor(ContextCompat.getColor(context, R.color.error_notification));

        notificationManager.notify(ERROR_NOTIFICATION_ID, builder.build());
    }

    public Notification getSendNotification(String contentText) { //TODO foregroundBuilder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setOngoing(true)
                .setProgress(0, 0, true)
                .setWhen(System.currentTimeMillis());
        return builder.build();
    }

    public void showNewMsgNotification(Map<Integer, String> notifyTexts, Class<?> activityClass, Class<?> fragmentClass) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_message_text)//R.drawable.ic_chat_24dp)
                .setCategory(NotificationCompat.CATEGORY_EMAIL)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(contentTitle)
                .setLights(Color.GREEN, 500, 500)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setColor(ContextCompat.getColor(context, R.color.new_message_notification));

        Intent intent = new Intent(context, activityClass);
        intent.putExtra("openFragment", fragmentClass.getSimpleName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent resultIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultIntent);
        builder.setAutoCancel(true);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        if (notifyTexts.size() == 1) {
            inboxStyle.setBigContentTitle(context.getString(R.string.notification_new_msg));
            builder.setContentText(context.getString(R.string.notification_new_msg));
        } else {
            inboxStyle.setBigContentTitle(context.getString(R.string.notification_new_msgs, notifyTexts.size()));
            builder.setContentText(context.getString(R.string.notification_new_msgs, notifyTexts.size()));
        }

        for (Map.Entry<Integer, String> entry : notifyTexts.entrySet()) {
            inboxStyle.addLine(entry.getValue());
        }

        builder.setStyle(inboxStyle);
        notificationManager.notify(NEW_MSG_NOTIFICATION_ID, builder.build());
    }

    public void showUpdateAvailableNotification(String contentText) {
        PendingIntent resultIntent = PendingIntent.getActivity(context, 0, SettingsFragment.getMarketIntent(context),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_cloud_download)//R.drawable.ic_system_update_alt_24px)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(contentTitle)
                .addAction(R.mipmap.ic_cloud_download, context.getString(R.string.update), resultIntent)//R.drawable.ic_system_update_alt_24px
                .setContentText(contentText)
                .setContentIntent(resultIntent)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.intro_background));

        notificationManager.notify(UPDATE_AVAILABLE_ID, builder.build());
    }

    public void showNewPaylslipAvailableNotification(String contentText, Class<?> activityClass, Class<?> fragmentClass) {
        Intent intent = new Intent(context, activityClass);
        intent.putExtra("openFragment", fragmentClass.getSimpleName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent resultIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_information_outline)//R.drawable.ic_info_outline_24dp)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setContentIntent(resultIntent)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.intro_background))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(contentTitle)
                        .bigText(contentText));

        notificationManager.notify(NEW_PAYSLIP_ID, builder.build());
    }

    public void showNewBoardPostNotification(Map<Integer, String> notifyTexts, Class<?> activityClass, Class<?> fragmentClass) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_city)//R.drawable.ic_location_city_24dp)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(contentTitle)
                .setColor(ContextCompat.getColor(context, R.color.intro_background));

        Intent intent = new Intent(context, activityClass);
        intent.putExtra("openFragment", fragmentClass.getSimpleName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent resultIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultIntent);
        builder.setAutoCancel(true);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        if (notifyTexts.size() == 1) {
            inboxStyle.setBigContentTitle(context.getString(R.string.notification_new_board_post));
            builder.setContentText(context.getString(R.string.notification_new_board_post));
        } else {
            inboxStyle.setBigContentTitle(context.getString(R.string.notification_new_board_posts, notifyTexts.size()));
            builder.setContentText(context.getString(R.string.notification_new_board_posts, notifyTexts.size()));
        }

        for (Map.Entry<Integer, String> entry : notifyTexts.entrySet()) {
            inboxStyle.addLine(entry.getValue());
        }

        builder.setStyle(inboxStyle);
        notificationManager.notify(NEW_ADVERT_NOTIFICATION_ID, builder.build());
    }
}
