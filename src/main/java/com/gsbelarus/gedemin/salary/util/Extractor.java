package com.gsbelarus.gedemin.salary.util;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgAbsenceModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationModel;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewAbsenceFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewIncomeFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewVacationFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.newmsg.GdMsgNewVacationInfoFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Extractor {

    private static Context context = App.getContext();

    public static class GdMsg {

        @ColorInt
        public static int getRequestStatusColor(GdMsgModel gdMsg) {
            switch (gdMsg.getRequestStatus()) {
                case SUCCESS:
                    return ContextCompat.getColor(context, R.color.messages_request_status_success);
                case ERROR:
                    return ContextCompat.getColor(context, R.color.messages_request_status_error);
                case REJECTED:
                    return ContextCompat.getColor(context, R.color.messages_request_status_rejected);
                case UNKNOWN:
                default:
                    return ContextCompat.getColor(context, R.color.messages_request_status_unknown);
            }
        }

        public static String getRequestStatusName(GdMsgModel gdMsg) {
            switch (gdMsg.getRequestStatus()) {
                case SUCCESS:
                    if (gdMsg.getSubject() == GdMsgModel.Subject.INCOME)
                        return context.getString(R.string.status_success_income) + ". ";
                    else
                        return context.getString(R.string.status_success) + ". ";
                case REJECTED:
                    return context.getString(R.string.status_rejected) + ". ";
                case ERROR:
                    return context.getString(R.string.status_error) + ". ";
                case UNKNOWN:
                    if (gdMsg.getSender() == GdMsgModel.Sender.SERVER &&
                            gdMsg.getSubject() != GdMsgModel.Subject.VACATION_INFO && gdMsg.getSubject() != GdMsgModel.Subject.INFO)
                        return context.getString(R.string.status_unprocessed) + ". ";
                default:
                    return "";
            }
        }

        @Nullable
        public static GdMsgModel getRequestGdMsg(GdMsgModel gdMsg) {
            if (gdMsg != null && gdMsg.getSender() == GdMsgModel.Sender.SERVER) {
                switch (gdMsg.getSubject()) {
                    case ABSENCE:
                        if (gdMsg.getGdMsgAbsenceModel() == null) return null;
                        return gdMsg.getGdMsgAbsenceModel().getRequestGdMsgModel();
                    case VACATION:
                        if (gdMsg.getGdMsgVacationModel() == null) return null;
                        return gdMsg.getGdMsgVacationModel().getRequestGdMsgModel();
                    case INCOME:
                        if (gdMsg.getGdMsgIncomeModel() == null) return null;
                        return gdMsg.getGdMsgIncomeModel().getRequestGdMsgModel();
                    case INFO:
                        if (gdMsg.getGdMsgInfoModel() == null) return null;
                        return gdMsg.getGdMsgInfoModel().getRequestGdMsgModel();
                    case VACATION_INFO:
                        if (gdMsg.getGdMsgVacationInfoModel() == null) return null;
                        return gdMsg.getGdMsgVacationInfoModel().getRequestGdMsgModel();
                }
            }
            return null;
        }

        public static String getSubjectName(GdMsgModel gdMsg) {
            return getSubjectName(gdMsg.getSubject());
        }

        public static String getSubjectName(GdMsgModel.Subject subject) {
            switch (subject) {
                case INCOME:
                    return context.getString(R.string.income_statement);
                case ABSENCE:
                    return context.getString(R.string.absence);
                case VACATION:
                    return context.getString(R.string.request_vacation);
                case INFO:
                    return context.getString(R.string.info);
                case VACATION_INFO:
                    return context.getString(R.string.request_vacation_info);
                case UNKNOWN:
                default:
                    return context.getString(R.string.unknown_data_type);
            }
        }

        /**
         * при добавлении новых типов запросов необходимо прописать для этих типов фрагменты
         */
        @Nullable
        public static Class<? extends BaseFragment> getSubjectFragmentClass(GdMsgModel.Subject subject) {
            switch (subject) {
                case INCOME:
                    return GdMsgNewIncomeFragment.class;
                case ABSENCE:
                    return GdMsgNewAbsenceFragment.class;
                case VACATION:
                    return GdMsgNewVacationFragment.class;
                case VACATION_INFO:
                    return GdMsgNewVacationInfoFragment.class;
                case INFO:
                case UNKNOWN:
                default:
                    return null;
            }
        }

        public static String getData(GdMsgModel gdMsg) {
            String msgData = "";
            String dateBegin;
            String dateEnd;
            SimpleDateFormat dateFormatter = new SimpleDateFormat("d MMMM", Locale.getDefault());
            switch (gdMsg.getSender()) {
                case CLIENT:
                    SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
                    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("d MMMM HH:mm", Locale.getDefault());

                    switch (gdMsg.getSubject()) {
                        case INCOME:
                            if (gdMsg.getGdMsgIncomeModel() != null) {
                                if (gdMsg.getGdMsgIncomeModel().getDateBegin() != null && gdMsg.getGdMsgIncomeModel().getDateEnd() != null) {
                                    dateBegin = monthFormatter.format(gdMsg.getGdMsgIncomeModel().getDateBegin());
                                    dateEnd = monthFormatter.format(gdMsg.getGdMsgIncomeModel().getDateEnd());
                                    msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                                }
                            }
                            break;
                        case ABSENCE:
                            if (gdMsg.getGdMsgAbsenceModel() != null) {
                                if (gdMsg.getGdMsgAbsenceModel().getDateBegin() != null && gdMsg.getGdMsgAbsenceModel().getDateEnd() != null) {
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(gdMsg.getGdMsgAbsenceModel().getDateBegin());
                                    if (calendar.get(Calendar.HOUR) != 0)
                                        dateBegin = dateTimeFormatter.format(gdMsg.getGdMsgAbsenceModel().getDateBegin());
                                    else
                                        dateBegin = dateFormatter.format(gdMsg.getGdMsgAbsenceModel().getDateBegin());

                                    calendar.setTime(gdMsg.getGdMsgAbsenceModel().getDateEnd());
                                    if (calendar.get(Calendar.HOUR) != 0)
                                        dateEnd = dateTimeFormatter.format(gdMsg.getGdMsgAbsenceModel().getDateEnd());
                                    else
                                        dateEnd = dateFormatter.format(gdMsg.getGdMsgAbsenceModel().getDateEnd());
                                    msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                                }
                                String type = GdMsgAbsence.getTypeName(gdMsg.getGdMsgAbsenceModel().getType());
                                if (!type.isEmpty())
                                    msgData += (msgData.isEmpty() ? "" : ". ") + context.getString(R.string.absence_type) + " " + type;
                            }
                            break;
                        case VACATION:
                            if (gdMsg.getGdMsgVacationModel() != null) {
                                if (gdMsg.getGdMsgVacationModel().getDateBegin() != null && gdMsg.getGdMsgVacationModel().getDateEnd() != null) {
                                    dateBegin = dateFormatter.format(gdMsg.getGdMsgVacationModel().getDateBegin().getTime());
                                    dateEnd = dateFormatter.format(gdMsg.getGdMsgVacationModel().getDateEnd().getTime());
                                    msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                                }
                                String type = "";
                                if (gdMsg.getGdMsgVacationModel().getType() == GdMsgVacationModel.Type.REGULAR)
                                    type = context.getString(R.string.request_vacation_type_regular);
                                else if (gdMsg.getGdMsgVacationModel().getType() == GdMsgVacationModel.Type.UNPAID)
                                    type = context.getString(R.string.request_vacation_type_unpaid);

                                if (!type.isEmpty())
                                    msgData += (msgData.isEmpty() ? "" : " ") + type;
                            }
                            break;
                        case VACATION_INFO:
                            msgData = context.getString(R.string.request_vacation_info_message);
                            break;
                    }
                    break;
                case SERVER:
                    if (gdMsg.getRequestStatus() == GdMsgModel.RequestStatus.SUCCESS) {
                        switch (gdMsg.getSubject()) {
                            case VACATION:
                                if (!gdMsg.getMsg().isEmpty())
                                    msgData = context.getString(R.string.vacation_server_msg);
                                break;
                        }
                    } else if (gdMsg.getRequestStatus() == GdMsgModel.RequestStatus.UNKNOWN) {
                        switch (gdMsg.getSubject()) {
                            case VACATION_INFO:
                                if (gdMsg.getGdMsgVacationInfoModel() != null) {
                                    if (gdMsg.getGdMsgVacationInfoModel().getDateBegin() != null && gdMsg.getGdMsgVacationInfoModel().getDateEnd() != null) {
                                        dateBegin = dateFormatter.format(gdMsg.getGdMsgVacationInfoModel().getDateBegin().getTime());
                                        dateEnd = dateFormatter.format(gdMsg.getGdMsgVacationInfoModel().getDateEnd().getTime());
                                        msgData = context.getString(R.string.request_msg_period, dateBegin, dateEnd);
                                    } else {
                                        msgData = context.getString(R.string.vacation_info_empty);
                                    }
                                }
                                break;
                        }
                    }
                    if (!gdMsg.getMsg().isEmpty())
                        msgData += (msgData.isEmpty() ? "" : ". ") + gdMsg.getMsg();
                    break;
            }
            return msgData;
        }

        public static String getSenderName(GdMsgModel gdMsg) {
            switch (gdMsg.getSender()) {
                case CLIENT:
                    return context.getString(R.string.msg_type_outgoing);
                case SERVER:
                    return context.getString(R.string.msg_type_inbox);
                case UNKNOWN:
                default:
                    return context.getString(R.string.unknown_data_type);
            }
        }

        public static @DrawableRes int getSenderIconRes(GdMsgModel gdMsg) {
            switch (gdMsg.getSender()) {
                case CLIENT:
                    return R.drawable.ic_arrow_back_36dp;
                case SERVER:
                    return R.drawable.ic_arrow_forward_36dp;
                case UNKNOWN:
                default:
                    return R.drawable.ic_error_outline_36dp;
            }
        }

        public static String toString(GdMsgModel gdMsg) {
            if (gdMsg != null) {
                String result = getSenderName(gdMsg) +
                        getData(gdMsg) +
                        getSubjectName(gdMsg) +
                        getRequestStatusName(gdMsg) +
                        gdMsg.getComment() +
                        getFormattedTimestamp(gdMsg) +
                        getLongFormattedTimestamp(gdMsg);

                result += toString(getRequestGdMsg(gdMsg));

                return result.toLowerCase();
            } else {
                return "";
            }
        }

        public static String getFormattedTimestamp(GdMsgModel gdMsgModel) {
            Calendar currCalendar = Calendar.getInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(gdMsgModel.getTimestamp());

            SimpleDateFormat dateFormat;
            // current year
            if (calendar.get(Calendar.YEAR) == currCalendar.get(Calendar.YEAR)) {
                // current day
                if (calendar.get(Calendar.DAY_OF_YEAR) == currCalendar.get(Calendar.DAY_OF_YEAR)) {
                    dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                } else {
                    dateFormat = new SimpleDateFormat("d MMM", Locale.getDefault());
                }
            } else {
                dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            }

            return dateFormat.format(calendar.getTime());
        }

        public static String getLongFormattedTimestamp(GdMsgModel gdMsgModel) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy, H:mm", Locale.getDefault());
            return dateFormat.format(gdMsgModel.getTimestamp().getTime());
        }

        public static class GdMsgAbsence {

            public static String getTypeName(GdMsgAbsenceModel.Type type) {
                switch (type) {
                    case SICK:
                        return context.getString(R.string.request_absence_type_sick);
                    case DONOR_DAY:
                        return context.getString(R.string.request_absence_type_donor_day);
                    case MILITARY_COMMISSARIAT:
                        return context.getString(R.string.request_absence_type_military_commissariat);
                    case SUBPOENA:
                        return context.getString(R.string.request_absence_type_subpoena);
                    case OTHER:
                        return context.getString(R.string.request_absence_type_other);
                    case UNKNOWN:
                    default:
                        return context.getString(R.string.unknown_data_type);
                }
            }
        }
    }

    public static class DayTypeCalendar {

        @ColorInt
        public static int getColor(DayType dayType) {
            switch (dayType) {
                case WORK:
                    return ContextCompat.getColor(context, R.color.calendar_work);
                case DAY_OFF:
                    return ContextCompat.getColor(context, R.color.calendar_day_off);
                case UNKNOWN:
                default:
                    return ContextCompat.getColor(context, R.color.calendar_unknown);
            }
        }

        public static String getName(DayType dayType) {
            switch (dayType) {
                case WORK:
                    return context.getString(R.string.day_type_work);
                case DAY_OFF:
                    return context.getString(R.string.day_type_day_off);
                case UNKNOWN:
                default:
                    return context.getString(R.string.unknown_data_type);
            }
        }
    }

    public static class DayEvent {

        public static String getFormatDuration(DayEventModel dayEventModel) {
            if (dayEventModel.getTimeBegin() != null && dayEventModel.getTimeEnd() != null) {

                SimpleDateFormat formatter = new SimpleDateFormat("H:mm", Locale.getDefault());
                return formatter.format(dayEventModel.getTimeBegin()) + " - " + formatter.format(dayEventModel.getTimeEnd());

            } else {
                double hours = dayEventModel.getHours();
                if (hours - (int) hours > 0)
                    return String.valueOf(hours) + context.getResources().getString(R.string.hour_abbr);
                else
                    return String.valueOf((int) hours) + context.getResources().getString(R.string.hour_abbr);
            }
        }

        @ColorInt
        public static int getColor(DayEventModel dayEventModel) {
            return getColor(dayEventModel.getEventType());
        }

        @ColorInt
        public static int getColor(DayEventModel.EventType eventType) {
            switch (eventType) {
                case WORKING:
                    return ContextCompat.getColor(context, R.color.calendar_event_working);
                case NOT_WORKING:
                    return ContextCompat.getColor(context, R.color.calendar_event_not_working);
                case NIGHT:
                    return ContextCompat.getColor(context, R.color.calendar_event_night);
                case VACATION:
                    return ContextCompat.getColor(context, R.color.calendar_event_vacation);
                case PUBLIC_HOLIDAY:
                    return ContextCompat.getColor(context, R.color.calendar_event_public_holiday);
                case SICK:
                    return ContextCompat.getColor(context, R.color.calendar_event_sick);
                case ABSENTEEISM:
                    return ContextCompat.getColor(context, R.color.calendar_event_absenteeism);
                case UNKNOWN:
                default:
                    return ContextCompat.getColor(context, R.color.calendar_event_unknown);
            }
        }

        public static String getName(DayEventModel dayEventModel) {
            switch (dayEventModel.getEventType()) {
                case WORKING:
                    return context.getString(R.string.event_type_working);
                case NOT_WORKING:
                    return context.getString(R.string.event_type_not_working);
                case NIGHT:
                    return context.getString(R.string.event_type_night);
                case VACATION:
                    return context.getString(R.string.event_type_vacation);
                case PUBLIC_HOLIDAY:
                    return context.getString(R.string.event_type_public_holiday);
                case SICK:
                    return context.getString(R.string.event_type_sick);
                case ABSENTEEISM:
                    return context.getString(R.string.event_type_absenteeism);
                case UNKNOWN:
                default:
                    return context.getString(R.string.unknown_data_type);
            }
        }
    }

    public static class ResponseError {

        public static String getMessage(ResponseErrorCode responseErrorCode) {
            switch (responseErrorCode) {
                case INVALID_REQUEST:
                    return context.getString(R.string.response_error_invalid_request);
                case INVALID_DATA:
                    return context.getString(R.string.response_error_invalid_data);
                case NO_PERMISS:
                    return context.getString(R.string.response_error_no_permiss);
                case NO_PERMISS_DATA_WILL_BE_DELETED:
                    return context.getString(R.string.response_error_no_permiss_data_will_be_deleted);
                case UNABLE_GET_INVITE_WAS_SENT:
                    return context.getString(R.string.response_error_unable_get_invite_was_sent);
                case INVALID_INVITE_WAS_ACTIVATED:
                    return context.getString(R.string.response_error_invalid_invite_was_activated);
                case INVALID_INVITE:
                    return context.getString(R.string.response_error_invalid_invite);
                case INVALID_INVITE_EXPIRED:
                    return context.getString(R.string.response_error_invalid_invite_expired);
                case NEED_INVITE:
                    return context.getString(R.string.response_error_need_invite);
                case EMPL_NOT_FOUND:
                    return context.getString(R.string.response_error_empl_not_found);
                case EMPL_NOT_FOUND_INVALID_ID:
                    return context.getString(R.string.response_error_empl_not_found_invalid_id);
                case EMPL_NOT_FOUND_INVALID_NAME:
                    return context.getString(R.string.response_error_empl_not_found_invalid_name);
                case EMPL_NOT_FOUND_INVALID_LN:
                    return context.getString(R.string.response_error_empl_not_found_invalid_ln);
                case NEED_UPDATE_APP:
                    return context.getString(R.string.response_error_need_update_app);
                case SERVER_NOT_FOUND:
                    return context.getString(R.string.response_error_server_not_found);
                case SERVER_UNAVAILABLE:
                    return context.getString(R.string.response_error_server_unavailable);
                case EMPTY_CODE:
                    return context.getString(R.string.response_error_empty_code);

                case UNKNOWN:
                default:
                    return context.getString(R.string.response_error_unknown);
            }
        }

        public static ResponseErrorCode getErrorByCode(String codeStr) {
            try {
                int code = Integer.valueOf(codeStr);
                if (code >= 0 && code < ResponseErrorCode.values().length)
                    return ResponseErrorCode.values()[code];

            } catch (NumberFormatException e) {
                LogUtil.d(e.getMessage());
            }
            return ResponseErrorCode.UNKNOWN;
        }
    }
}
