package com.gsbelarus.gedemin.salary.util;

public abstract class SyncDataHandler {

    public abstract SyncDataHandler dataHandle(String data) throws Exception;

    public abstract SyncDataHandler subscribe(String table, HandlingListener handlingListener);

    public abstract SyncDataHandler unSubscribe(String table);

    public abstract SyncDataHandler unSubscribeAll();

    public interface HandlingListener {

        void onSave(Object object);
    }
}
