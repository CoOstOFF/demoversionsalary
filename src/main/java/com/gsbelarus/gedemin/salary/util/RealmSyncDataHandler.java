package com.gsbelarus.gedemin.salary.util;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.gsbelarus.gedemin.salary.entity.Link;
import com.gsbelarus.gedemin.salary.entity.RealmSyncObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

public class RealmSyncDataHandler extends SyncDataHandler {

    private HashMap<String, HandlingListener> realmClassList = new LinkedHashMap<>();
    private Gson gson;
    private String packageModel;
    private Realm realm;

    public RealmSyncDataHandler(Gson gson, String packageModel, Realm realm) {
        this.gson = gson;
        this.packageModel = packageModel;
        this.realm = realm;
    }

    @SuppressWarnings("unchecked")
    @Override
    public SyncDataHandler dataHandle(String data) throws Exception {
        RealmSyncObject realmSyncObject = gson.fromJson(data, RealmSyncObject.class);
        Class currentClass = Class.forName(getClassName(packageModel, realmSyncObject.getCl()));
        RealmObject currentObject = (RealmObject) gson.fromJson(realmSyncObject.getObj(), currentClass);

        currentObject = realm.copyToRealmOrUpdate(currentObject);

        if (realmSyncObject.isDeleted()) {
            currentObject.deleteFromRealm();

        } else {
            if (realmSyncObject.getLink() != null) {
                for (Link link : realmSyncObject.getLink()) {
                    Class linkClass = Class.forName(getClassName(packageModel, link.getCl()));
                    RealmModel linkObject = realm.where(linkClass).equalTo("uid", link.getUid()).findFirst();
                    Method currentGetter = currentClass.getMethod(getGetterName(link.getField()));
                    if (currentGetter.getReturnType().equals(RealmList.class)) {
                        Object currentFieldObject = currentGetter.invoke(currentObject);
                        if (!((RealmList<RealmModel>) currentFieldObject).contains(linkObject))
                            ((RealmList<RealmModel>) currentFieldObject).add(linkObject);
                    } else {
                        Method currentSetter = currentClass.getMethod(getSetterName(link.getField()), linkClass);
                        currentSetter.invoke(currentObject, linkObject);
                    }
                }
            }
//            RealmModel // FIXME: 15.06.16
            if (realmSyncObject.getMaster() != null) {
                for (Link master : realmSyncObject.getMaster()) {
                    Class masterClass = Class.forName(getClassName(packageModel, master.getCl()));
                    RealmModel masterObject = realm.where(masterClass).equalTo("uid", master.getUid()).findFirst();
                    Method masterObjectGetter = masterClass.getMethod(getGetterName(master.getField()));
                    if (masterObjectGetter.getReturnType().equals(RealmList.class)) {
                        Object toFieldObject = masterObjectGetter.invoke(masterObject);
                        if (!((RealmList<RealmObject>) toFieldObject).contains(currentObject))
                            ((RealmList<RealmObject>) toFieldObject).add(currentObject);
                    } else {
                        Method toObjectSetter = masterClass.getMethod(getSetterName(master.getField()), currentClass);
                        toObjectSetter.invoke(masterObject, currentObject);
                    }
                }
            }

            if (realmClassList.containsKey(currentClass.getSimpleName()))
                realmClassList.get(currentClass.getSimpleName()).onSave(currentObject);
        }
        return this;
    }

    @Override
    public SyncDataHandler subscribe(@NonNull String table, @NonNull HandlingListener handlingListener) {
        realmClassList.put(table, handlingListener);
        return this;
    }

    @Override
    public SyncDataHandler unSubscribe(@NonNull String table) {
        realmClassList.remove(table);
        return this;
    }

    @Override
    public SyncDataHandler unSubscribeAll() {
        realmClassList.clear();
        return this;
    }

    private String getSetterName(String field) {
        return "set" + field.substring(0, 1).toUpperCase() + field.substring(1);
    }

    private String getGetterName(String field) {
        return "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
    }

    private String getClassName(String packageName, String className) {
        return packageName + "." + className;
    }
}
