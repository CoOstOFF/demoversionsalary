package com.gsbelarus.gedemin.salary.util;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.gsbelarus.gedemin.lib.sync.protocol.etc.Parser;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GCMHelper {

    private static final String SENDER_ID = "303114121834";

    private static final ExecutorService executor = Executors.newSingleThreadExecutor();

    private Context context;

    public GCMHelper(Context context) {
        this.context = context;
    }

    public void checkRegistration(RealmHelper realmHelper) {
        SyncMetaDataModel syncMetaData = realmHelper.getSyncMetaData(true);
        if (syncMetaData.getAuthKey().isEmpty() || !checkPlayServices()) return;

        if (getRegistrationId(realmHelper).isEmpty())
            updateRegistration();
    }

    public void deleteRegistration() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                RealmHelper.executeWithClosing(new RealmHelper.RealmHelperExecutor() {

                    @Override
                    public void execute(RealmHelper realmHelper) {
                        try {
                            InstanceID instanceID = InstanceID.getInstance(context);
                            instanceID.deleteToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                            storeRegistrationId(realmHelper, "");
                            LogUtil.i("GCM Registration ID deleted");

                        } catch (Exception ex) {
                            LogUtil.i("Error : " + ex.getMessage());
                        }
                    }
                });
            }
        });
    }

    public void updateRegistration() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                RealmHelper.executeWithClosing(new RealmHelper.RealmHelperExecutor() {

                    @Override
                    public void execute(RealmHelper realmHelper) {
                        try {
                            InstanceID instanceID = InstanceID.getInstance(context);
                            String token = instanceID.getToken(SENDER_ID,
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                            LogUtil.i("GCM Registration ID: " + token);

                            if (!getRegistrationId(realmHelper).equals(token)) {
                                storeRegistrationId(realmHelper, "");
                                if (sendRegistrationIdToBackend(realmHelper, token)) {
                                    storeRegistrationId(realmHelper, token);
                                    LogUtil.i("GCM Registration ID sent to the server");
                                }
                            }

                        } catch (Exception ex) {
                            LogUtil.i("Error : " + ex.getMessage());
                        }
                    }
                });
            }
        });
    }

    private boolean sendRegistrationIdToBackend(RealmHelper realmHelper, String regid) {
        SyncMetaDataModel syncMetaData = realmHelper.getSyncMetaData(true);
        if (regid.isEmpty() || syncMetaData.getAuthKey().isEmpty())
            return false;

        Map<String, String> requestParams = new HashMap<>();
        requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SET_REG_ID);
        requestParams.put(ServerURL.REQUEST_PARAM_ALIAS, syncMetaData.getAlias());
        requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaData.getAuthKey());
        requestParams.put(ServerURL.REQUEST_PARAM_IP_ADDRESS, WebServiceManager.getIpAddress());
        requestParams.put(ServerURL.REQUEST_PARAM_GCM_REG_ID, regid);
        requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

        String urlString = WebServiceManager.buildRequestUrl(syncMetaData.getScheme(), syncMetaData.getAddressServer(), requestParams);
        LogUtil.d(urlString);
        WebServiceManager webServiceManager = new WebServiceManager();
        try {
            webServiceManager.openConnection(urlString);
            webServiceManager.sendGetRequest(requestParams);

            return isSyncStatusOK(webServiceManager.getHttpURLConnection().getInputStream());
        } catch (Exception e) {
            LogUtil.e(e.getMessage());
        } finally {
            webServiceManager.closeConnection();
        }
        return false;
    }

    private boolean isSyncStatusOK(InputStream inputStream) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        List<String> lines = new ArrayList<>();

        String buf;
        while ((buf = reader.readLine()) != null) {
            lines.add(buf);
        }
        HashMap<String, String> headerParams = new Parser().parsingHeader(lines);
        if (headerParams.containsKey(ServerURL.RESPONSE_PARAM_ERROR_CODE)) {
            LogUtil.d(ResponseErrorCode.getErrorByCode(headerParams.get(ServerURL.RESPONSE_PARAM_ERROR_CODE)));
            return false;
        }
        return true;
    }

    private String getRegistrationId(RealmHelper realmHelper) {
        SyncMetaDataModel syncInfo = realmHelper.getSyncMetaData(true);
        String registrationId = syncInfo.getGcmRegistrationId();
        if (registrationId.isEmpty()) {
            LogUtil.i("GCM Registration ID not found.");
            return "";
        }
        return registrationId;
    }

    private void storeRegistrationId(RealmHelper realmHelper, String regId) {
        realmHelper.getRealm().beginTransaction();
        realmHelper.getSyncMetaData(false).setGcmRegistrationId(regId);
        realmHelper.getRealm().commitTransaction();
    }

    public boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(activity, result, 9000).show();
            } else {
                LogUtil.i("This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        return result == ConnectionResult.SUCCESS;
    }
}
