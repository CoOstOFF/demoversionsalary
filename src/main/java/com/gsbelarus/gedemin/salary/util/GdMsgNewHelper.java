package com.gsbelarus.gedemin.salary.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.GdMsgNew;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class GdMsgNewHelper {

    /**
     * отображаются только те пукнты для которых прописаны фрагменты.
     * смотри {@link com.gsbelarus.gedemin.salary.entity.model.GdMsgModel#getSubjectFragmentClass(GdMsgModel.Subject)} и
     * {@link com.gsbelarus.gedemin.salary.entity.model.GdMsgModel#getSubjectName()}
     */
    public static void showGdMsgNewDialog(final FragmentActivity activity, Context context, GdMsgModel.Subject... subjects) {
        showGdMsgNewDialog(activity, null, context, subjects);
    }

    /**
     * отображаются только те пукнты для которых прописаны фрагменты.
     * смотри {@link com.gsbelarus.gedemin.salary.entity.model.GdMsgModel#getSubjectFragmentClass(GdMsgModel.Subject)} и
     * {@link com.gsbelarus.gedemin.salary.entity.model.GdMsgModel#getSubjectName()}
     */
    public static void showGdMsgNewDialog(final FragmentActivity activity, final Calendar day, Context context, GdMsgModel.Subject... subjects) {
        final List<Class<? extends BaseFragment>> fragmentClassList = new ArrayList<>();
        List<String> subjectNameList = new ArrayList<>();
        for (GdMsgModel.Subject subject : subjects) {
            Class<? extends BaseFragment> cl = GdMsgModel.getSubjectFragmentClass(subject);
            if (cl != null) {
                fragmentClassList.add(cl);
                subjectNameList.add(GdMsgModel.getSubjectName(subject, context));
            }
        }
        new MaterialDialog.Builder(activity)
                .title(R.string.new_msg)
                .items(subjectNameList.toArray(new String[subjectNameList.size()]))
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        Intent intent = new Intent(activity, GdMsgNew.class);
                        Bundle bundle = new Bundle();
                        if (day != null)
                            bundle.putSerializable(GdMsgNew.TAG_MSG_DATE, day.getTime());
                        bundle.putSerializable(GdMsgNew.TAG_FRAGMENT_CLASS, fragmentClassList.get(which));
                        intent.putExtras(bundle);
                        activity.startActivity(intent);
                    }
                })
                .show();
    }
}
