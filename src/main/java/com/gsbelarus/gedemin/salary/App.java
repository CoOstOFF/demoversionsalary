package com.gsbelarus.gedemin.salary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.sync.protocol.AlarmReceiver;
import com.gsbelarus.gedemin.lib.ui.BaseApplication;
import com.gsbelarus.gedemin.salary.database.DatabaseMigration;
import com.gsbelarus.gedemin.salary.database.DatabaseRecreation;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.receiver.AutoSyncReceiver;
import com.gsbelarus.gedemin.salary.util.AnalyticsTrackers;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import java.util.HashMap;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends BaseApplication {

    private static App mInstance;
    private static Context context;
    private static boolean recreateDB = false;

    /**
     * Enum used to identify the tracker that needs to be used for tracking [GoogleAnalytics]
     */
    // TODO delete
    public enum TrackerName {
        APP_TRACKER //, GLOBAL_TRACKER
    }

    // TODO delete
    private static HashMap<TrackerName, Tracker> trackers = new HashMap<>();

    // TODO delete
    public static synchronized Tracker getTracker(TrackerName trackerId) {
        if (!trackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(getContext());
            if (trackerId == TrackerName.APP_TRACKER) {
                Tracker tracker = analytics.newTracker(R.xml.analytics_app_tracker);
                trackers.put(trackerId, tracker);
            }
        }
        return trackers.get(trackerId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = this;

        Realm.init(getApplicationContext());
        initRealmDefaultConfiguration(this);
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        changeLocale();
        CurrencyHelper.getInstance(getApplicationContext()).updatesRates(null);
    }

    private static void initRealmDefaultConfiguration(Context context) {
        String dbPassword = "12345678"; //TODO
        byte[] key = new byte[64];
        byte[] passwordBytes = dbPassword.getBytes();
        int keyLength = dbPassword.length() <= 64 ? dbPassword.length() : 64;
        for(int i = 0; i < keyLength; i++) {
            key[i] = passwordBytes[i];
        }

        RealmConfiguration configuration = null;
        RealmConfiguration bufConfig = getConfigurationBuilder()
                .build();
//        try {
            configuration = getConfigurationBuilder()
                    .encryptionKey(key)//new EncryptionManager(context).getKey(bufConfig))
                    .build();
//        } catch (GeneralSecurityException e) {
//            App.getInstance().trackException(e);
//            e.printStackTrace();
//        } catch (IOException e) {
//            App.getInstance().trackException(e);
//            e.printStackTrace();
//        }
        Realm.setDefaultConfiguration(configuration);

        try {

            Realm realm = Realm.getDefaultInstance();
            realm.close();

        } catch (IllegalArgumentException e) {
            new DatabaseRecreation(context).recreate(bufConfig, configuration);
        }

        if (recreateDB) {
            new DatabaseRecreation(context).recreate(configuration, configuration);
            recreateDB = false;
        }
    }

    private static RealmConfiguration.Builder getConfigurationBuilder() {
        return new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(DatabaseMigration.DB_SCHEMA_VERSION)
                .migration(new DatabaseMigration(new DatabaseMigration.OnRecreateListener() {
                    @Override
                    public void setRecreate(boolean flag) {
                        recreateDB = flag;
                    }
                }));
    }

    public void changeLocale() {
        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = getPrefLang();
        if (!config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        changeLocale();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this); // TODO MultiDex disable
    }

    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

    private String getPrefLang() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        return getResources().getStringArray(R.array.language_list_values)
                [pref.getInt(SettingsFragment.LANG_LIST, SettingsFragment.DEFAULT_LANG_PREF_ITEM)];
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public AlarmReceiver getAlarmReceiver() {
        return new AutoSyncReceiver();
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }
}
