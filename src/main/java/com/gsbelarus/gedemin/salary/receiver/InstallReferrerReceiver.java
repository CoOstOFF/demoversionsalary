package com.gsbelarus.gedemin.salary.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gsbelarus.gedemin.salary.analytics.InstallTracker;

public class InstallReferrerReceiver extends BroadcastReceiver {

    static final String INSTALL_ACTION = "com.android.vending.INSTALL_REFERRER";
    static final String REFERRER_KEY = "referrer";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null && intent.getAction().equals(INSTALL_ACTION)) {
            final String referrer = intent.getStringExtra(REFERRER_KEY);

            if (referrer != null && referrer.length() != 0) {
                InstallTracker.getInstance(context).setReferrer(referrer);
            }
        }
    }
}