package com.gsbelarus.gedemin.salary.receiver;

import android.content.Intent;

import com.gsbelarus.gedemin.lib.sync.protocol.AlarmReceiver;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;

import java.util.Calendar;

import io.realm.Realm;

public class AutoSyncReceiver extends AlarmReceiver {

    /**
     * Обычно вызывается после вызова метода  startSync()
     * Может быть вызван без вызова метода startSync()
     * Внутри метода можно вызвать  setIntervalUpdateInMinutes(int)
     *
     * @param oldDate последняя дата на которой была синхронизация
     * @return дата седующей синхронизации
     */
    @Override
    public Calendar getNextSyncDate(Calendar oldDate) {
        oldDate.add(Calendar.DATE, 1);
        return oldDate;
    }

    /**
     * Внутри следуе начать синхронизацию
     * Вызывается перед вызовом метода  getNextSyncDate(Calendar)
     */
    @Override
    public void startSync() {
        getContext().startService(SyncServiceModel.getIntentWithTask(new Intent(getContext(), SyncDataService.class),
                SyncServiceTask.TypeOfTask.SERIES_SYNC,
                new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_REQUEST),
                true));
    }

    /**
     * @return в демо режиме автосинхронизация не будет срабатывать
     */
    @Override
    public boolean isWorkMode() {
        boolean isWorkMode = SettingsFragment.getPrefMode(getContext()) == SettingsFragment.PrefMode.WORK_MODE;
        RealmHelper realmHelper = new RealmHelper(Realm.getDefaultInstance());
        isWorkMode = isWorkMode && realmHelper.getSyncMetaData(true).isPermits();
        realmHelper.getRealm().close();
        return isWorkMode;
    }
}
