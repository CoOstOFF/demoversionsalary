package com.gsbelarus.gedemin.salary.analytics;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.NoSuchPropertyException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class InstallTracker {

    /**
     * https://play.google.com/store/apps/details?id=com.gsbelarus.gedemin.salary&referrer=server=address$empl=employeeModel{}
     */


    private static final String PREFERENCES_FILE = "google_play_install_receiver";
    private static final String PROPERTY_INSTALL_REFERRER = "install_referrer";

    private static final String ERROR_NO_REFERRER   = "No install referrer found";
    private static final String ERROR_NO_SUCH_PARAM = "No such parameter found";

    private static InstallTracker instance;

    private Map<String, String> referralParams;
    private String referrer;
    private Context context;


    InstallTracker(Context context) {
        this.context = context;

        referrer = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).getString(PROPERTY_INSTALL_REFERRER, null);
        if (referrer != null && referrer.length() != 0) {
            referralParams = extractUrlParams(referrer);
        }
        //TODO get from db
    }

    public static InstallTracker getInstance(Context context) {
        if (instance == null) {
            instance = new InstallTracker(context);
        }

        return instance;
    }

    public void setReferrer(final String ref) {
        referrer = ref;
        referralParams = extractUrlParams(referrer);

        SharedPreferences.Editor sharedPrefEditor = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit();
        sharedPrefEditor.putString(PROPERTY_INSTALL_REFERRER, referrer);
        sharedPrefEditor.apply();

        //TODO save in db
    }

    public static Map<String, String> extractUrlParams(@NonNull String url) {
        Map<String, String> paramsMap = new HashMap<>();

        try {

            /// ERROR paramsMap = Splitter.on("\\$").withKeyValueSeparator("%3D").split(URLDecoder.decode(url, "UTF-8"));//

            String[] params = URLDecoder.decode(url, "UTF-8").split("\\$");

            for (String param : params) {
                String[] pair = param.split("=");

                if (pair.length == 2) {
                    paramsMap.put(pair[0], pair[1]);
                }
            }

        } catch (UnsupportedEncodingException err) {
            paramsMap = null;
        }

        return paramsMap;
    }

    public String getReferrer() throws NoSuchPropertyException {
        if (referrer == null) {
            throw new NoSuchPropertyException(ERROR_NO_REFERRER);
        } else {
            return referrer;
        }
    }

    public String getRefferalParam(String param) throws NoSuchPropertyException {
        if (referralParams == null) {
            throw new NoSuchPropertyException(ERROR_NO_REFERRER);
        }

        if (referralParams.containsKey(param)) {
            return referralParams.get(param);
        } else {
            throw new NoSuchPropertyException(ERROR_NO_SUCH_PARAM + ": " + param);
        }
    }

}
