package com.gsbelarus.gedemin.salary.adapter.payslip;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.PayslipDetailCard;
import com.gsbelarus.gedemin.salary.entity.PayslipSubItem;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PayslipDetailAdapter extends RecyclerView.Adapter<PayslipDetailAdapter.ItemViewHolder> {

    private List<PayslipDetailCard> cardsList;
    private Calendar date;

    private Context context;

    private InternalClickListener internalClickListener;

    private CurrencyHelper currencyHelper;
    private RealmHelper realmHelper;

    public PayslipDetailAdapter(Context context, RealmHelper realmHelper) {
        this.context = context;
        this.realmHelper = realmHelper;
        cardsList = new ArrayList<>();
        currencyHelper = CurrencyHelper.getInstance(context);
    }

    @Override
    public PayslipDetailAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payslip_detail_cards_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PayslipDetailAdapter.ItemViewHolder holder, int position) {

        final PayslipDetailCard item = cardsList.get(position);

        if (item.getInfo().isEmpty())
            holder.cardInfoButton.setVisibility(View.GONE);
        else
            holder.cardInfoButton.setVisibility(View.VISIBLE);
        holder.cardHeaderText.setText(item.getName());
//        holder.cardHeaderText.setTextColor(ContextCompat.getColor(context, item.getTitleColor()));
        holder.cardHeaderSum.setText(currencyHelper.getConvertedSumString(realmHelper, date, item.getSum()));
        if (currencyHelper.getLastChoice() != CurrencyHelper.Kind.BYN) {
            holder.cardHeaderCurrencySymbol.setText(currencyHelper.getSymbolCurrencyText(""));
            holder.cardHeaderCurrencySymbol.setVisibility(View.VISIBLE);
        } else
            holder.cardHeaderCurrencySymbol.setVisibility(View.GONE);

        holder.cardContent.removeAllViews();
        RelativeLayout contentItem;
        for (PayslipSubItem entry : item.getPayslipSubItems()) {
            contentItem = (RelativeLayout) LayoutInflater.from(holder.cardContent.getContext())
                    .inflate(R.layout.payslip_detail_cards_item_content, holder.cardContent, false);

            TextView contentText = (TextView) contentItem.findViewById(R.id.payslip_detail_cards_content_text);
            String textSecondary = !entry.getFormattedDate().isEmpty() ? " (" + entry.getFormattedDate() + ")" : "";
            contentText.setText(entry.getName() + textSecondary);

            TextView contentSum = (TextView) contentItem.findViewById(R.id.payslip_detail_cards_content_sum);
            contentSum.setText(currencyHelper.getConvertedSumString(realmHelper, date, entry.getSum()));

            holder.cardContent.addView(contentItem);
        }
    }

    @Override
    public int getItemCount() {
        return cardsList.size();
    }

    public void setList(Calendar date, List<PayslipDetailCard> cardsList) {
        this.date = date;
        this.cardsList = cardsList;
        notifyDataSetChanged();
    }

    public List<PayslipDetailCard> getList() {
        return cardsList;
    }

    public void clear() {
        cardsList.clear();
        notifyDataSetChanged();
    }


    public void setOnClickInfoButtons(InternalClickListener internalClickListener) {
        this.internalClickListener = internalClickListener;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView cardHeaderText;
        private TextView cardHeaderSum;
        private TextView cardHeaderCurrencySymbol;
        private ImageView cardInfoButton;
        private LinearLayout cardContent;

        public ItemViewHolder(View v) {
            super(v);
            cardHeaderText = (TextView) v.findViewById(R.id.payslip_detail_cards_header_text);
            cardHeaderSum = (TextView) v.findViewById(R.id.payslip_detail_cards_header_sum);
            cardHeaderCurrencySymbol = (TextView) v.findViewById(R.id.payslip_detail_cards_header_item_sum_currency_symbol);
            cardInfoButton = (ImageView) v.findViewById(R.id.payslip_detail_cards_header_info_button);
            cardContent = (LinearLayout) v.findViewById(R.id.payslip_detail_cards_content);

            cardInfoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (internalClickListener != null) {
                        internalClickListener.onInternalViewClick(itemView, view, getAdapterPosition());
                    }
                }
            });
        }
    }
}
