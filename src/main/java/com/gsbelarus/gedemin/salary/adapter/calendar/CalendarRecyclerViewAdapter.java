package com.gsbelarus.gedemin.salary.adapter.calendar;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;

import io.realm.RealmList;

public class CalendarRecyclerViewAdapter extends RecyclerView.Adapter<CalendarRecyclerViewAdapter.RecyclerItemViewHolder> {

    private RealmList<DayEventModel> dayEventDataSet;

    private Context context;

    public final static class RecyclerItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvEventTitle;
        TextView tvEventSummary;
        ImageView ivOval;

        public RecyclerItemViewHolder(View itemView) {
            super(itemView);
            ivOval = (ImageView) itemView.findViewById(R.id.iv_oval);
            tvEventTitle = (TextView) itemView.findViewById(R.id.tv_event_title);
            tvEventSummary = (TextView) itemView.findViewById(R.id.tv_event_summary);
        }
    }

    @Override
    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_recycler_item, viewGroup, false);
        context = viewGroup.getContext();
        return new RecyclerItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerItemViewHolder viewHolder, int position) {
        DayEventModel dayEvent = dayEventDataSet.get(position);

        String dayEventTitle;
        String dayEventSummary;
        if (dayEvent.getEventType() == DayEventModel.EventType.PUBLIC_HOLIDAY) {
            dayEventTitle = dayEvent.getName();
            dayEventSummary = getDescription(dayEvent);
        } else {
            dayEventTitle = getTitle(dayEvent);
            dayEventSummary = getDuration(dayEvent);
        }

        if (!dayEventSummary.isEmpty()) {
            viewHolder.tvEventSummary.setVisibility(View.VISIBLE);
            viewHolder.tvEventSummary.setText(dayEventSummary);
        } else
            viewHolder.tvEventSummary.setVisibility(View.GONE);

        viewHolder.tvEventTitle.setText(dayEventTitle);
        ((GradientDrawable) viewHolder.ivOval.getBackground()).setColor(dayEvent.getColor());
    }

    private String getTitle(DayEventModel dayEvent) {
        return getDescription(dayEvent).isEmpty() ? dayEvent.getName() : getDescription(dayEvent);
    }

    private String getDuration(DayEventModel dayEvent) {
        String dayEventDuration;
        if (dayEvent.isAllDay()) {
//            dayEventDuration = context.getResources().getString(R.string.event_duration_title) + ": " + context.getResources().getString(R.string.event_duration_all_day) + ".";
            dayEventDuration = "";

        } else {
            String duration = dayEvent.getFormatDuration();
            if (duration.contains(":"))
                dayEventDuration = duration;
            else
                dayEventDuration = context.getResources().getString(R.string.event_duration_title) + ": " + duration + ".";
        }

        return dayEventDuration;
    }

    private String getDescription(DayEventModel dayEvent) {
        if (!dayEvent.getDescription().isEmpty())
            return dayEvent.getDescription().substring(0, 1).toUpperCase() + dayEvent.getDescription().substring(1);
        return "";
    }

    @Override
    public int getItemCount() {
        return dayEventDataSet == null ? 0 : dayEventDataSet.size();
    }

    public RealmList<DayEventModel> getItems() {
        return dayEventDataSet;
    }

    public void setItems(RealmList<DayEventModel> dayEventDataSet) {
        this.dayEventDataSet = dayEventDataSet;
        notifyDataSetChanged();
    }

    public DayEventModel getItem(int position) {
        return dayEventDataSet.get(position);
    }
}