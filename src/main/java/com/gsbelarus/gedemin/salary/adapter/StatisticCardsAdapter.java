package com.gsbelarus.gedemin.salary.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.StatisticPayslip;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class StatisticCardsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private Context context;
    private List<StatisticPayslip> list;
    private LineData chartData;
    private CurrencyHelper currencyHelper;
    private RealmHelper realmHelper;
    private Double startSum;

    private int headerCount = 0;

    private OnItemClickListener onItemClickListener;
    private OnHeaderClickListener onHeaderClickListener;

    private int posDifColor;
    private int negDifColor;
    private int zeroDifColor;

    public enum ItemTypes {
        HEADER, CARD_ITEM
    }

    public StatisticCardsAdapter(Activity activity, Context context, RealmHelper realmHelper) {
        this.activity = activity;
        this.context = context;
        this.realmHelper = realmHelper;
        list = new ArrayList<>();
        currencyHelper = CurrencyHelper.getInstance(context);

        com.github.mikephil.charting.utils.Utils.init(context);

        posDifColor = ContextCompat.getColor(context, R.color.statistic_positive_dynamics);
        negDifColor = ContextCompat.getColor(context, R.color.statistic_negative_dynamics);
        zeroDifColor = ContextCompat.getColor(context, R.color.statistic_stability);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (ItemTypes.values()[viewType]) {
            case HEADER:
                viewHolder = new HeaderVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_cards_header, parent, false));
                break;
            case CARD_ITEM:
                viewHolder = new CardItemVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_cards_item, parent, false));
                break;
        }
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (ItemTypes.values()[getItemViewType(position)]) {
            case HEADER:
                HeaderVH headerHolder = (HeaderVH) holder;

                headerHolder.chart.setData(chartData);
                headerHolder.chart.animateY(300);
                headerHolder.chart.invalidate();
                break;
            case CARD_ITEM:
                StatisticPayslip cardRow = list.get(position - headerCount);
                Calendar date = cardRow.getDate();
                CardItemVH cardHolder = ((CardItemVH) holder);

                cardHolder.date.setText(CalendarHelper.getMonthName(context, date.get(Calendar.MONTH)) + " " +
                        String.valueOf(date.get(Calendar.YEAR)));

                cardHolder.totalSum.setText(
                        currencyHelper.getConvertedSumString(realmHelper, date, cardRow.getTotalSum()));
                cardHolder.totalSumCurrencySymbol.setText(currencyHelper.getSymbolCurrencyText(""));
                cardHolder.totalSumCurrencySymbol.setVisibility(
                        currencyHelper.getLastChoice() == CurrencyHelper.Kind.BYN ? View.GONE : View.VISIBLE);

                Drawable cardDrawable = cardHolder.differenceImage.getDrawable();

                if (cardRow.getDifference() > 0) {
                    cardHolder.difference.setTextColor(posDifColor);
                    cardHolder.differenceCurrencySymbol.setTextColor(posDifColor);
                    cardDrawable.setColorFilter(posDifColor, PorterDuff.Mode.SRC_ATOP);
                    cardHolder.differenceImage.setRotation(0);

                } else if (cardRow.getDifference() < 0) {
                    cardHolder.difference.setTextColor(negDifColor);
                    cardHolder.differenceCurrencySymbol.setTextColor(negDifColor);
                    cardDrawable.setColorFilter(negDifColor, PorterDuff.Mode.SRC_ATOP);
                    cardHolder.differenceImage.setRotation(90);

                } else {
                    cardHolder.difference.setTextColor(zeroDifColor);
                    cardHolder.differenceCurrencySymbol.setTextColor(zeroDifColor);
                    cardDrawable.setColorFilter(zeroDifColor, PorterDuff.Mode.SRC_ATOP);
                    cardHolder.differenceImage.setRotation(45);
                }

                cardHolder.difference.setText(currencyHelper.getFormattedSum(Math.abs(cardRow.getDifference())));
                cardHolder.differenceCurrencySymbol.setText(currencyHelper.getSymbolCurrencyText(""));
                cardHolder.differenceCurrencySymbol.setVisibility(
                        currencyHelper.getLastChoice() == CurrencyHelper.Kind.BYN ? View.GONE : View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + headerCount;
    }

    @Override
    public int getItemViewType(int position) {
        return (headerCount != 0 && position == 0) ? ItemTypes.HEADER.ordinal() : ItemTypes.CARD_ITEM.ordinal();
    }

    private void createHeaderData() {
        if (list.isEmpty() && chartData != null)
            chartData.removeDataSet(0);
        ArrayList<Entry> yList = new ArrayList<>();
        ArrayList<String> xList = new ArrayList<>();
        int i = list.size() - 1;
        for (StatisticPayslip item : list) {

            boolean beforeDenomination = item.getDate().before(new GregorianCalendar(2016, 6, 1, 0, 0, 0));

            yList.add(new Entry((float) currencyHelper.getConvertedSum(realmHelper, item.getDate(),
                    (beforeDenomination && currencyHelper.getLastChoice() == CurrencyHelper.Kind.BYN)
                            ? item.getTotalSum() / 10000 : item.getTotalSum()), i));
            xList.add(new SimpleDateFormat("MM/yy", Locale.getDefault()).format(item.getDate().getTimeInMillis()));
            i--;
        }
        Collections.reverse(yList);
        Collections.reverse(xList);

        LineDataSet set = new LineDataSet(yList, "DataSet");
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(ColorTemplate.getHoloBlue());
        set.setLineWidth(2f);
        set.setCircleSize(3f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setCircleColorHole(ContextCompat.getColor(context, android.R.color.transparent));
        set.setDrawFilled(true);
        set.setDrawValues(false);

        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set);

        chartData = new LineData(xList, dataSets);
    }

    private void createDifferenceSum() {
        if (list.size() > 1) {
            for (int i = 0; i < list.size() - 1; i++) {
                StatisticPayslip previousItem = list.get(i + 1);
                StatisticPayslip currentItem = list.get(i);

                Double currentSum = currencyHelper.getConvertedSum(realmHelper, currentItem.getDate(), currentItem.getTotalSum());
                Double previousSum = currencyHelper.getConvertedSum(realmHelper, previousItem.getDate(),
                        (previousItem.getDate().get(Calendar.YEAR) == 2016 &&
                                previousItem.getDate().get(Calendar.MONTH) == Calendar.JUNE &&
                                currencyHelper.getLastChoice() == CurrencyHelper.Kind.BYN) ? previousItem.getTotalSum() / 10000 : previousItem.getTotalSum());

                currentItem.setDifference(currentSum - previousSum);
            }
        }
        if (!list.isEmpty() && startSum != null) {
            StatisticPayslip item = list.get(list.size() - 1);
            Calendar before = (Calendar) item.getDate().clone();
            before.add(Calendar.MONTH, -1);

            Double currentSum = currencyHelper.getConvertedSum(realmHelper, item.getDate(), item.getTotalSum());
            Double previousSum = currencyHelper.getConvertedSum(realmHelper, before,
                    (item.getDate().get(Calendar.YEAR) == 2016 &&
                            item.getDate().get(Calendar.MONTH) == Calendar.JULY &&
                            currencyHelper.getLastChoice() == CurrencyHelper.Kind.BYN) ? startSum / 10000 : startSum);

            item.setDifference(currentSum - previousSum);
        }
    }

    public void setList(List<StatisticPayslip> list) {
        this.list = list;
        createDifferenceSum();
        createHeaderData();
        notifyDataSetChanged();
    }

    public void setStartSum(Double startSum) {
        this.startSum = startSum;
    }

    public void clear() {
        this.list.clear();
        createDifferenceSum();
        createHeaderData();
        notifyDataSetChanged();
    }

    public void showHeader(OnHeaderClickListener onHeaderClickListener) {
        this.headerCount = 1;
        this.onHeaderClickListener = onHeaderClickListener;
        notifyItemInserted(0);
    }

    public void hideHeader() {
        headerCount = 0;
        notifyItemRemoved(0);
    }

    public void notifyDataSetChangedList() {
        createDifferenceSum();
        createHeaderData();
        notifyItemRangeChanged(0, getItemCount());
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OnHeaderClickListener getOnHeaderClickListener() {
        return onHeaderClickListener;
    }

    public void setOnHeaderClickListener(OnHeaderClickListener onHeaderClickListener) {
        this.onHeaderClickListener = onHeaderClickListener;
    }

    class CardItemVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView totalSum;
        private TextView totalSumCurrencySymbol;
        private TextView difference;
        private TextView differenceCurrencySymbol;
        private ImageView differenceImage;

        CardItemVH(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.statistic_card_date);
            totalSum = (TextView) itemView.findViewById(R.id.statistic_card_total_sum);
            totalSumCurrencySymbol = (TextView) itemView.findViewById(R.id.statistic_card_total_sum_currency_symbol);
            difference = (TextView) itemView.findViewById(R.id.statistic_card_difference_sum);
            differenceCurrencySymbol = (TextView) itemView.findViewById(R.id.statistic_card_difference_sum_currency_symbol);
            differenceImage = (ImageView) itemView.findViewById(R.id.statistic_card_difference_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null)
                onItemClickListener.onClick(v, list.get(getAdapterPosition() - headerCount));
        }
    }

    public interface OnItemClickListener {
        void onClick(View view, StatisticPayslip item);
    }

    class HeaderVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LineChart chart;

        HeaderVH(View itemView) {
            super(itemView);
            chart = (LineChart) itemView.findViewById(R.id.chart);

            int[] attrs = {android.R.attr.textColorPrimary};
            TypedArray ta = activity.obtainStyledAttributes(attrs);

            int axisXLabelTextColor = ta.getColor(0, Color.BLACK);

            chart.setDrawGridBackground(false);
            chart.getLegend().setEnabled(false);
            chart.getXAxis().setDrawAxisLine(false);
            chart.getXAxis().setAvoidFirstLastClipping(true);
            chart.getXAxis().setTextColor(axisXLabelTextColor);
            chart.getAxisLeft().setStartAtZero(false);
            chart.getAxisLeft().setEnabled(false);
            chart.getAxisRight().setStartAtZero(false);
            chart.getAxisRight().setEnabled(false);
            chart.setDescription("");
            chart.setDoubleTapToZoomEnabled(false);
            chart.setTouchEnabled(false);
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2)
                chart.setHardwareAccelerationEnabled(false);
            itemView.setOnClickListener(this);
            ta.recycle();
        }

        @Override
        public void onClick(View v) {
            if (onHeaderClickListener != null)
                onHeaderClickListener.onClick(v);
        }
    }

    public interface OnHeaderClickListener {
        void onClick(View view);
    }
}
