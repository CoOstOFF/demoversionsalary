package com.gsbelarus.gedemin.salary.adapter.payslip;

import android.view.View;

public interface InternalClickListener {
    void onInternalViewClick(View view, View internalView, int position);
}
