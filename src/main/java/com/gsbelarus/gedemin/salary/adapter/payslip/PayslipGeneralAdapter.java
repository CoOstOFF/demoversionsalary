package com.gsbelarus.gedemin.salary.adapter.payslip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.HorizontalBarData;
import com.gsbelarus.gedemin.salary.entity.PayslipGeneralItem;
import com.gsbelarus.gedemin.salary.entity.PayslipSubItem;
import com.gsbelarus.gedemin.salary.ui.HorizontalBarChart;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;
import com.gsbelarus.gedemin.salary.util.DateFormatHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PayslipGeneralAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    private List<PayslipGeneralItem> listItems;
    private List<HorizontalBarData> chartData;
    private Calendar date;

    private InternalClickListener internalClickListener;

    private CurrencyHelper currencyHelper;
    private RealmHelper realmHelper;

    private int headerCount = 0;

    private String headerTitle;
    private double headerSum;

    public enum ItemType {
        HEADER, ITEM, SIMPLE_CARD_ITEM
    }

    public enum SubItemType {
        WITH_SUM, WITH_CONVERTED_SUM, WITHOUT_SUM
    }

    public PayslipGeneralAdapter(Context context, RealmHelper realmHelper) {
        this.context = context;
        this.realmHelper = realmHelper;
        listItems = new ArrayList<>();
        currencyHelper = CurrencyHelper.getInstance(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (ItemType.values()[viewType]) {
            case HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payslip_general_list_header, parent, false));
            case ITEM:
                return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payslip_general_cards_item, parent, false));
            case SIMPLE_CARD_ITEM:
                return new SimpleCardItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payslip_general_cards_simple_item, parent, false));
            default:
                return null;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int i = position - headerCount;
        switch (ItemType.values()[getItemViewType(position)]) {
            case HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.chart.setType(HorizontalBarChart.Type.SERIES_BAR_CHART);
                headerViewHolder.chart.setData(chartData);
                headerViewHolder.chart.animate(300);
                headerViewHolder.date.setText(DateFormatHelper.getMonthString(context, date));
                headerViewHolder.name.setText(headerTitle);
                headerViewHolder.sum.setText(currencyHelper.getConvertedSumString(realmHelper, date, headerSum));
                if (currencyHelper.getLastChoice() != CurrencyHelper.Kind.BYN) {
                    headerViewHolder.currencySymbol.setText(currencyHelper.getSymbolCurrencyText(""));
                    headerViewHolder.currencySymbol.setVisibility(View.VISIBLE);
                } else
                    headerViewHolder.currencySymbol.setVisibility(View.GONE);
                break;
            case ITEM:
                PayslipGeneralItem item = listItems.get(i);
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                if (item.getInfo().isEmpty())
                    itemViewHolder.cardInfoButton.setVisibility(View.GONE);
                else
                    itemViewHolder.cardInfoButton.setVisibility(View.VISIBLE);

                itemViewHolder.name.setText(item.getName());
                if (item.getSubName() == null || item.getSubName().isEmpty())
                    itemViewHolder.subName.setVisibility(View.GONE);
                else {
                    itemViewHolder.subName.setVisibility(View.VISIBLE);
                    itemViewHolder.subName.setText(item.getSubName());
                }
                itemViewHolder.sum.setText(currencyHelper.getConvertedSumString(realmHelper, date, item.getSum()));
                if (currencyHelper.getLastChoice() != CurrencyHelper.Kind.BYN) {
                    itemViewHolder.currencySymbol.setText(currencyHelper.getSymbolCurrencyText(""));
                    itemViewHolder.currencySymbol.setVisibility(View.VISIBLE);
                } else
                    itemViewHolder.currencySymbol.setVisibility(View.GONE);
                itemViewHolder.cardContent.removeAllViews();
                for (PayslipSubItem entry : item.getPayslipGeneralItemSubItems()) {
                    RelativeLayout contentItem;
                    contentItem = (RelativeLayout) LayoutInflater.from(itemViewHolder.cardContent.getContext())
                            .inflate(R.layout.payslip_general_cards_item_content, itemViewHolder.cardContent, false);
                    TextView contentItemName = (TextView) contentItem.findViewById(R.id.payslip_general_cards_content_name);
                    TextView contentItemSum = (TextView) contentItem.findViewById(R.id.payslip_general_cards_content_sum);
                    TextView contentItemDate = (TextView) contentItem.findViewById(R.id.payslip_general_cards_content_date);
                    View contentItemColor = contentItem.findViewById(R.id.payslip_general_cards_content_color);
                    contentItemColor.setBackgroundColor(entry.getColor());
                    contentItemName.setText(entry.getName());
                    contentItemSum.setText(
                            currencyHelper.getConvertedSumString(realmHelper, date, entry.getSum()));
//                    if (entry.getFormattedDate().isEmpty())
                        contentItemDate.setVisibility(View.GONE);
//                    else {
//                        contentItemDate.setVisibility(View.VISIBLE);
//                        contentItemDate.setText("(" + entry.getFormattedDate() + ")");
//                    }
                    if (entry.getSum() != 0)
                        ((ItemViewHolder) holder).cardContent.addView(contentItem);
                }
                break;
            case SIMPLE_CARD_ITEM:
                PayslipGeneralItem simpleItem = listItems.get(i);
                SimpleCardItemViewHolder simpleCardItemViewHolder = (SimpleCardItemViewHolder) holder;
                simpleCardItemViewHolder.title.setText(simpleItem.getTitle());

//                simpleCardItemViewHolder.title.setTextColor(ContextCompat.getColor(context, simpleItem.getTitleColor()));
                simpleCardItemViewHolder.cardContent.removeAllViews();
                RelativeLayout contentSimpleItem;
                for (PayslipSubItem entry : simpleItem.getPayslipGeneralItemSubItems()) {
                    contentSimpleItem = (RelativeLayout) LayoutInflater.from(simpleCardItemViewHolder.cardContent.getContext())
                            .inflate(R.layout.payslip_general_cards_item_content, simpleCardItemViewHolder.cardContent, false);
                    TextView contentSimpleCardName = (TextView) contentSimpleItem.findViewById(R.id.payslip_general_cards_content_name);
                    TextView contentSimpleCardSum = (TextView) contentSimpleItem.findViewById(R.id.payslip_general_cards_content_sum);
                    TextView contentSimpleCardDate = (TextView) contentSimpleItem.findViewById(R.id.payslip_general_cards_content_date);
                    View contentSimpleCardColor = contentSimpleItem.findViewById(R.id.payslip_general_cards_content_color);
                    contentSimpleCardColor.setVisibility(View.GONE);
                    contentSimpleCardDate.setVisibility(View.GONE);
                    contentSimpleCardName.setText(entry.getName());

                    switch (entry.getId()) {
                        case WITHOUT_SUM:
                            contentSimpleCardSum.setVisibility(View.INVISIBLE);
                            ((SimpleCardItemViewHolder) holder).cardContent.addView(contentSimpleItem);
                            break;
                        case WITH_SUM:
                            if (entry.getSum() != 0) {
                                contentSimpleCardSum.setText(currencyHelper.getFormattedSum(entry.getSum()));
                                ((SimpleCardItemViewHolder) holder).cardContent.addView(contentSimpleItem);
                            }
                            break;
                        case WITH_CONVERTED_SUM:
                            if (entry.getSum() != 0) {
                                contentSimpleCardSum.setText(currencyHelper.getConvertedSumString(realmHelper, date, entry.getSum()));
                                ((SimpleCardItemViewHolder) holder).cardContent.addView(contentSimpleItem);
                            }
                            break;
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size() + headerCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (headerCount > 0 && position == 0)
            return ItemType.HEADER.ordinal();
        return (listItems.get(position - headerCount).getId() == ItemType.SIMPLE_CARD_ITEM) ?
                ItemType.SIMPLE_CARD_ITEM.ordinal() : ItemType.ITEM.ordinal();
    }

    public void showHeader(String headerTitle, double headerSum, List<HorizontalBarData> chartData) {
        this.chartData = chartData;
        this.headerTitle = headerTitle;
        this.headerSum = headerSum;
        this.headerCount = 1;
        notifyItemInserted(0);
    }

    public void hideHeader() {
        chartData = null;
        headerCount = 0;
        headerSum = 0;
        headerTitle = "";
        notifyItemRemoved(0);
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setList(List<PayslipGeneralItem> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    public void addItem(PayslipGeneralItem item) {
        int position = listItems.size();
        listItems.add(position, item);
        notifyItemInserted(position + headerCount);
    }

    public PayslipGeneralAdapter addItems(List<PayslipGeneralItem> items) {
        int position = listItems.size();
        listItems.addAll(items);
        notifyItemRangeInserted(position + headerCount, items.size());
        return this;
    }

    public void clear() {
        hideHeader();
        listItems.clear();
        notifyDataSetChanged();
    }

    public List<PayslipGeneralItem> getList() {
        return listItems;
    }

    public void setOnClickButtons(InternalClickListener internalClickListener) {
        this.internalClickListener = internalClickListener;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        private HorizontalBarChart chart;
        private TextView sum;
        private TextView currencySymbol;
        private TextView name;
        private TextView date;
        private ImageView syncButton;
        private ImageView statisticsButton;

        HeaderViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.payslip_general_header_item_name);
            sum = (TextView) v.findViewById(R.id.payslip_general_header_item_sum);
            currencySymbol = (TextView) v.findViewById(R.id.payslip_general_header_item_sum_currency_symbol);
            date = (TextView) v.findViewById(R.id.payslip_general_header_item_date);
            chart = (HorizontalBarChart) v.findViewById(R.id.horizontal_chart);
            syncButton = (ImageView) v.findViewById(R.id.payslip_general_sync_button);
            syncButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (internalClickListener != null)
                        internalClickListener.onInternalViewClick(itemView, view, getAdapterPosition());
                }
            });
            statisticsButton = (ImageView) v.findViewById(R.id.payslip_general_statistics_button);
            statisticsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (internalClickListener != null)
                        internalClickListener.onInternalViewClick(itemView, view, getAdapterPosition());
                }
            });
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView subName;
        private TextView sum;
        private TextView currencySymbol;
        private ImageView cardInfoButton;
        private LinearLayout cardContent;

        public ItemViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.payslip_general_cards_header_name);
            subName = (TextView) v.findViewById(R.id.payslip_general_cards_header_subname);
            sum = (TextView) v.findViewById(R.id.payslip_general_cards_header_sum);
            currencySymbol = (TextView) v.findViewById(R.id.payslip_general_cards_header_item_sum_currency_symbol);
            cardInfoButton = (ImageView) v.findViewById(R.id.payslip_general_cards_header_info_button);
            cardContent = (LinearLayout) v.findViewById(R.id.payslip_general_cards_content);
            cardInfoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (internalClickListener != null)
                        internalClickListener.onInternalViewClick(itemView, view, getAdapterPosition() - headerCount);
                }
            });
        }
    }

    class SimpleCardItemViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private LinearLayout cardContent;

        public SimpleCardItemViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.payslip_general_list_simple_card_item_title);
            cardContent = (LinearLayout) v.findViewById(R.id.payslip_general_cards_simple_card_content);
        }
    }
}
