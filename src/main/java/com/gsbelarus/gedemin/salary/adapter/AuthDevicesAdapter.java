package com.gsbelarus.gedemin.salary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;

import io.realm.RealmResults;

public class AuthDevicesAdapter extends RecyclerView.Adapter<AuthDevicesAdapter.ItemViewHolder> {

    public interface OnLockDeviceClickListener {
        void onItemClick(View view, final DeviceInfoModel deviceInfoModel);
    }

    private Context context;
    private RealmResults<DeviceInfoModel> deviceDataSet;
    private OnLockDeviceClickListener lockDeviceClickListener;

    public AuthDevicesAdapter(Context context, RealmResults<DeviceInfoModel> deviceDataSet, OnLockDeviceClickListener lockDeviceClickListener) {
        this.context = context;
        this.deviceDataSet = deviceDataSet;
        this.lockDeviceClickListener = lockDeviceClickListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.auth_devices_recycler_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        DeviceInfoModel deviceInfoModel = deviceDataSet.get(position);
        holder.bind(deviceInfoModel, lockDeviceClickListener);
    }

    @Override
    public int getItemCount() {
        return deviceDataSet.size();
    }

    public DeviceInfoModel getItem(int position) {
        return deviceDataSet.get(position);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvSummary;
        private ImageView imgLockIcon;

        public ItemViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.title_text);
            tvSummary = (TextView) itemView.findViewById(R.id.summary_text);
            imgLockIcon = (ImageView) itemView.findViewById(R.id.icon_lock);
        }

        public void bind(final DeviceInfoModel deviceInfoModel, final OnLockDeviceClickListener lockDeviceClickListener) {
            tvTitle.setText(deviceInfoModel.getDeviceModel());
            if (deviceInfoModel.getUid() == DeviceInfoModel.CURRENT_DEVICE_UID) {
                tvSummary.setText(context.getString(R.string.current_device));
                imgLockIcon.setVisibility(View.GONE);

            } else {
                StringBuilder summary = new StringBuilder();
                if (!deviceInfoModel.getPhoneNumber().isEmpty()) {
                    summary.append(context.getString(R.string.auth_devices_mobile_number));
                    summary.append(": ");
                    summary.append(deviceInfoModel.getPhoneNumber());

                } else if (!deviceInfoModel.getDeviceId().isEmpty()) {
                    summary.append(deviceInfoModel.getDeviceId());
                }
                tvSummary.setText(summary.toString());

                imgLockIcon.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        if (lockDeviceClickListener != null)
                            lockDeviceClickListener.onItemClick(v, deviceInfoModel);
                    }
                });
            }
        }
    }
}
