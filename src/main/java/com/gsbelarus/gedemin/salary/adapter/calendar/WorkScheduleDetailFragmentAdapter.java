package com.gsbelarus.gedemin.salary.adapter.calendar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.WorkScheduleModel;
import com.gsbelarus.gedemin.salary.fragment.calendar.CalendarDetailFragment;
import com.gsbelarus.gedemin.salary.fragment.calendar.WorkScheduleDetailFragment;

import java.util.Calendar;

import io.realm.RealmResults;
import io.realm.Sort;

public class WorkScheduleDetailFragmentAdapter extends CalendarDetailFragmentAdapter<WorkScheduleModel> {

    public WorkScheduleDetailFragmentAdapter(FragmentManager fm, RealmHelper realmHelper) {
        super(fm, realmHelper);
    }

    @Override
    protected RealmResults<WorkScheduleModel> onUpdateList() {
        return getRealmHelper().getAllWorkSchedule(Sort.ASCENDING);
    }

    @Override
    public int getPositionByDate(Calendar date) {
        Calendar calendar = Calendar.getInstance();

        if (getList() != null)
            for (int i = 0; i < getList().size(); i++) {
                calendar.setTime(getList().get(i).getDate());

                if (calendar.get(Calendar.YEAR) == date.get(Calendar.YEAR) &&
                        calendar.get(Calendar.MONTH) == date.get(Calendar.MONTH) &&
                        calendar.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH)) {

                    return i;
                }
            }
        return UNDEFINED_INDEX;
    }

    @Override
    public Calendar getCalendarByPosition(int index) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getList().get(index).getDate());

            return calendar;
        } catch (IndexOutOfBoundsException e) {
            LogUtil.d(e.toString());
        }
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        return CalendarDetailFragment.newInstance(WorkScheduleDetailFragment.class, getList().get(position).getUid());
    }

    @Override
    public MonthInfo getMonthInfo(Calendar date) {
        return new MonthInfo(
                getRealmHelper().getWorkScheduleHours(date),
                getRealmHelper().getWorkScheduleWorkingDaysCount(date),
                R.string.planned_time
        );
    }
}
