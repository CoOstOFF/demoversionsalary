package com.gsbelarus.gedemin.salary.adapter;

import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;

public class GdMsgRecyclerViewAdapter extends BaseSimpleAdapter<GdMsgModel, GdMsgRecyclerViewAdapter.RecyclerItemViewHolder> {

    private Activity activity;
    private RealmResults<GdMsgModel> gdMsgDataSet;
    private SparseBooleanArray activatedUids;

    private int selectedUid = -1;
    private boolean selectionVisible;

    private String textFilter;
    private GdMsgModel.Sender senderFilter;

    public GdMsgRecyclerViewAdapter(Activity activity, RealmResults<GdMsgModel> gdMsgDataSet) {
        this.activity = activity;
        this.gdMsgDataSet = gdMsgDataSet;
        activatedUids = new SparseBooleanArray();
        setItems(gdMsgDataSet);
    }

    @Override
    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerItemViewHolder(inflateView(R.layout.gdmsg_recycler_item, parent));
    }

    @Override
    public void onBindViewHolder(RecyclerItemViewHolder viewHolder, int position) {
        GdMsgModel gdMsg = getItem(position);

        String status = gdMsg.getRequestStatusName();
        String subject = gdMsg.getSubjectName();

        String data = gdMsg.getData();
        if (!data.isEmpty())
            data += ". ";

        viewHolder.subjectTextView.setText(Html.fromHtml(subject));
        viewHolder.commentTextView.setText(Html.fromHtml(status + data + gdMsg.getComment()));

        viewHolder.itemView.setActivated(activatedUids.get(gdMsg.getUid(), false));

        viewHolder.itemView.setSelected(selectionVisible && selectedUid == gdMsg.getUid());

        int[] attrs = {R.attr.colorMessagesItemImageSelected, R.attr.colorMessagesItemDateRead, R.attr.colorMessagesItemDateUnread};

        TypedArray ta = activity.obtainStyledAttributes(attrs);

        // FIXME: 25.03.2016 styleable
        int selectedImageColor = ta.getColor(0, Color.GRAY);
        @SuppressWarnings("ResourceType") int readDateColor = ta.getColor(1, Color.DKGRAY);
        @SuppressWarnings("ResourceType") int unreadDateColor = ta.getColor(2, Color.BLUE);

        if (viewHolder.itemView.isActivated()) {
            viewHolder.avatarImageView.setImageResource(R.drawable.ic_done_24dp);
            ((GradientDrawable) viewHolder.avatarImageView.getBackground()).setColor(selectedImageColor);
        } else {
            viewHolder.avatarImageView.setImageResource(gdMsg.getSenderIconRes());
            ((GradientDrawable) viewHolder.avatarImageView.getBackground()).setColor(gdMsg.getRequestStatusColor());
        }

        if (!gdMsg.isUnread()) {
            viewHolder.subjectTextView.setTypeface(null, Typeface.NORMAL);
            viewHolder.dateTextView.setTypeface(null, Typeface.NORMAL);
            viewHolder.dateTextView.setTextColor(readDateColor);
        } else {
            viewHolder.subjectTextView.setTypeface(null, Typeface.BOLD);
            viewHolder.dateTextView.setTypeface(null, Typeface.BOLD);
            viewHolder.dateTextView.setTextColor(unreadDateColor);
        }

        viewHolder.dateTextView.setText(gdMsg.getFormattedTimestamp());
        ta.recycle();
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getUid();
    }

    public GdMsgModel getItemByUid(long uid) {
        for (GdMsgModel gdMsgModel : getItems()) {
            if (gdMsgModel.getUid() == uid) {
                return gdMsgModel;
            }
        }
        return null;
    }

    public int getPositionByUid(long uid) {
        return getItems().indexOf(getItemByUid(uid));
    }

    public int getActivatedItemCount() {
        return activatedUids.size();
    }

    public List<GdMsgModel> getActivatedItems() {
        List<GdMsgModel> selectedItems = new ArrayList<>();
        for (int i = 0; i < activatedUids.size(); i++) {
            GdMsgModel gdMsg = getItemByUid(activatedUids.keyAt(i));
            if (gdMsg != null) {
                selectedItems.add(gdMsg);
            } else {
                activatedUids.delete(activatedUids.keyAt(i));
                notifyDataSetChanged();
            }
        }
        return selectedItems;
    }

    public SparseBooleanArray getActivatedUids() {
        return activatedUids;
    }

    public void setActivatedUids(SparseBooleanArray activatedUids) {
        this.activatedUids = activatedUids;
        notifyDataSetChanged();
    }

    public void toggleActivation(int pos) {
        int uid = getItem(pos).getUid();
        if (activatedUids.get(uid, false)) {
            activatedUids.delete(uid);
        } else {
            activatedUids.put(uid, true);
        }
        notifyItemChanged(pos);
    }

    public void setActivationAll() {
        for (GdMsgModel gdMsg : getItems()) {
            activatedUids.put(gdMsg.getUid(), true);
        }
        notifyDataSetChanged();
    }

    public void clearActivations() {
        activatedUids.clear();
        notifyDataSetChanged();
    }

    public boolean isSelectionVisible() {
        return selectionVisible;
    }

    public void setSelectionVisible(boolean selectionVisible) {
        this.selectionVisible = selectionVisible;
    }

    public int getSelectedUid() {
        if (getItemByUid(selectedUid) == null) {
            if (getItemCount() == 0) {
                selectedUid = -1;
            } else {
                selectedUid = getItem(0).getUid();
            }
        }
        return selectedUid;
    }

    public void setSelectedUid(int selectedUid) {
        this.selectedUid = selectedUid;
        if (selectionVisible)
            notifyDataSetChanged();
    }

    public void setSelectedPosition(int position) {
        setSelectedUid((int) getItemId(position));
    }

    public void notifyDataSetChangedWithFilters() {
        if (textFilter != null && !textFilter.isEmpty())
            setItems(gdMsgDataSet.where().contains("gdMsgString", textFilter.toLowerCase(), Case.INSENSITIVE).findAllSorted("timestamp", Sort.DESCENDING));
        else
            setItems(gdMsgDataSet);

        if (senderFilter != null)
            setItems(((RealmResults<GdMsgModel>) getItems()).where().equalTo("senderIndex", senderFilter.ordinal()).findAllSorted("timestamp", Sort.DESCENDING));
        notifyDataSetChanged();
    }

    public RealmResults<GdMsgModel> getGdMsgDataSet() {
        return gdMsgDataSet;
    }

    public String getTextFilter() {
        return textFilter;
    }

    public void setTextFilter(String textFilter) {
        this.textFilter = textFilter;
    }

    public GdMsgModel.Sender getSenderFilter() {
        return senderFilter;
    }

    public void setSenderFilter(GdMsgModel.Sender senderFilter) {
        this.senderFilter = senderFilter;
    }

    public class RecyclerItemViewHolder extends BaseSimpleAdapter.ItemViewHolder {

        public ImageView avatarImageView;
        TextView subjectTextView;
        TextView dateTextView;
        TextView commentTextView;

        RecyclerItemViewHolder(View itemView) {
            super(itemView);
            subjectTextView = (TextView) itemView.findViewById(R.id.txt_subject);
            dateTextView = (TextView) itemView.findViewById(R.id.txt_date);
            commentTextView = (TextView) itemView.findViewById(R.id.txt_comment);
            avatarImageView = (ImageView) itemView.findViewById(R.id.iv_avatar);
        }
    }
}