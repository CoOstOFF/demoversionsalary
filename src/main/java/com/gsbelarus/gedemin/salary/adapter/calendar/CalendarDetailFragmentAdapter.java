package com.gsbelarus.gedemin.salary.adapter.calendar;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.gsbelarus.gedemin.salary.database.RealmHelper;

import java.util.Calendar;

import io.realm.RealmObject;
import io.realm.RealmResults;

public abstract class CalendarDetailFragmentAdapter<T extends RealmObject> extends FragmentStatePagerAdapter {

    public static final int UNDEFINED_INDEX = -1;
    private RealmResults<T> list;
    private RealmHelper realmHelper;

    private boolean showEmpty;

    public CalendarDetailFragmentAdapter(FragmentManager fm, RealmHelper realmHelper) {
        super(fm);
        this.realmHelper = realmHelper;
        list = onUpdateList();
    }

    @Override
    public int getCount() {
        if (showEmpty) return 0;
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public boolean isShowEmpty() {
        return showEmpty;
    }

    public void setShowEmpty(boolean showEmpty) {
        this.showEmpty = showEmpty;
    }

    protected RealmResults<T> getList() {
        return list;
    }

    protected RealmHelper getRealmHelper() {
        return realmHelper;
    }

    protected abstract RealmResults<T> onUpdateList();

    public abstract int getPositionByDate(Calendar date);

    @Nullable
    public abstract Calendar getCalendarByPosition(int index);

    public abstract Fragment getItem(int position);

    public abstract MonthInfo getMonthInfo(Calendar date);

    public class MonthInfo {

        private double workingHours;
        private int workingDaysCount;
        @StringRes
        private int description;

        public MonthInfo(double workingHours, int workingDaysCount, @StringRes int description) {
            this.workingHours = workingHours;
            this.workingDaysCount = workingDaysCount;
            this.description = description;
        }

        public double getWorkingHours() {
            return workingHours;
        }

        public void setWorkingHours(double workingHours) {
            this.workingHours = workingHours;
        }

        public int getWorkingDaysCount() {
            return workingDaysCount;
        }

        public void setWorkingDaysCount(int workingDaysCount) {
            this.workingDaysCount = workingDaysCount;
        }

        @StringRes
        public int getDescription() {
            return description;
        }

        public void setDescription(@StringRes int description) {
            this.description = description;
        }
    }
}
