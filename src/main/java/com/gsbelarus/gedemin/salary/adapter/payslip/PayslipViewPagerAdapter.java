package com.gsbelarus.gedemin.salary.adapter.payslip;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipFragment;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipTabDetailFragment;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipTabGeneralFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PayslipViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Class<? extends BaseFragment>> fragmentsList;
    private List<String> fragmentsTitles;
    private Calendar date;

    public PayslipViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        fragmentsList = new ArrayList<>();
        fragmentsList.add(PayslipTabGeneralFragment.class);
        fragmentsList.add(PayslipTabDetailFragment.class);

        fragmentsTitles = new ArrayList<>();
        fragmentsTitles.add(context.getString(R.string.payslip_general));
        fragmentsTitles.add(context.getString(R.string.payslip_detail));
    }

    @Override
    public Fragment getItem(int position) {
        try {
            BaseFragment fragment = fragmentsList.get(position).newInstance();
            if (date != null)
                fragment.setArguments(new BundleHelper.Builder()
                        .putSerializable(PayslipFragment.TAG_DATE, date)
                        .build());
            return fragment;
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentsTitles.get(position);
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public int getItemPosition(Object object) {
        ((BaseFragment) object).reloadFragmentData();
        return POSITION_UNCHANGED;
    }
}