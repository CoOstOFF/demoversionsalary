package com.gsbelarus.gedemin.salary.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.DrawerPersonInfoItem;

import java.util.ArrayList;
import java.util.List;

public class LinearLayoutAdapter implements View.OnLongClickListener {

    private LinearLayout content;
    private List<DrawerPersonInfoItem> list;
    private OnItemLongClickListener onItemLongClickListener;

    public LinearLayoutAdapter(LinearLayout content) {
        this.content = content;
        list = new ArrayList<>();
    }

    public void notifyDateChange(){
        content.removeAllViews();
        for(DrawerPersonInfoItem item: list){

            View view = LayoutInflater.from(content.getContext()).inflate(R.layout.about_employee_list_item, content, false);

            LinearLayout linearLayout =(LinearLayout) view.findViewById(R.id.about_employee_line_above_item);

            TextView textView = (TextView) view.findViewById(R.id.person_info_text);
            TextView textViewItem = (TextView) view.findViewById(R.id.person_info_item);
            ImageView imageView = (ImageView) view.findViewById(R.id.person_info_image);

            textView.setText(item.getPersonInfoText());
            textViewItem.setText(item.getPersonInfoItem());
            imageView.setImageResource(item.getPersonInfoImage());

            if(content.getChildCount() == (list.size() - 1)){
                linearLayout.setVisibility(View.INVISIBLE);
            }

            content.addView(view);

            view.setOnLongClickListener(this);
        }
    }

    public LinearLayoutAdapter add(DrawerPersonInfoItem item) {
        int position = list.size();
        list.add(position, item);
        return this;
    }

    public void clear() {
        list.clear();
    }

    public DrawerPersonInfoItem getItem(int position) {
        return list.get(position);
    }


    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    @Override
    public boolean onLongClick(View v) {

        for( int i =0; i < content.getChildCount(); i++){
            if(v == content.getChildAt(i))
                return onItemLongClickListener == null || onItemLongClickListener.onItemLongClick(v, i);
        }

        return false;
    }

    public interface OnItemLongClickListener {

        boolean onItemLongClick(View view, int position);
    }

}
