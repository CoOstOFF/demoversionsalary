package com.gsbelarus.gedemin.salary.database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;

import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.entity.StatisticPayslip;
import com.gsbelarus.gedemin.salary.entity.model.ContactModel;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.ExRatesMonthlyModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgAbsenceModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgIncomeModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipBenefitModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipDeductionModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipItemModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncGdMsgStateModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.entity.model.TimesheetModel;
import com.gsbelarus.gedemin.salary.entity.model.WorkScheduleModel;
import com.gsbelarus.gedemin.salary.util.DateFormatHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.exceptions.RealmException;

public class RealmHelper {

    private Realm realm;

    public interface RealmHelperExecutor {
        void execute(RealmHelper realmHelper);
    }

    public static void executeWithClosing(RealmHelperExecutor realmHelperExecutor) {
        if (realmHelperExecutor != null) {
            RealmHelper realmHelper = new RealmHelper(Realm.getDefaultInstance());
            realmHelperExecutor.execute(realmHelper);
            realmHelper.getRealm().close();
        }
    }

    public static void executeTransactionWithClosing(final RealmHelperExecutor realmHelperExecutor) {
        if (realmHelperExecutor != null) {
            final RealmHelper realmHelper = new RealmHelper(Realm.getDefaultInstance());
            realmHelper.getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realmHelperExecutor.execute(realmHelper);
                }
            });
            realmHelper.getRealm().close();
        }
    }

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    public Realm getRealm() {
        return realm;
    }

    public void clearDB(boolean withTransaction) {
        if (withTransaction)
            realm.beginTransaction();

        realm.deleteAll();

        if (withTransaction)
            realm.commitTransaction();
    }

    public long randLong(long min, long max) {
        long result;
        do {
            result = (new Random().nextLong() % (max - min)) + min;
        } while (result <= 0);

        return result;
    }

    public int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public double randDouble(double min, double max) {
        return (min + (new Random().nextDouble() * (max - min) ) * 0.1);
    }

    public boolean randBoolean(int trueChance) {
        return new Random().nextInt(100) < trueChance;
    }

    public <T extends RealmObject> boolean existOnUid(Class<T> item, int uid) {
        return realm.where(item).equalTo("uid", uid).count() != 0;
    }

    public <T extends RealmObject> T save(T item, boolean withTransaction) {
        T realmItem;
        try {
            if (withTransaction)
                realm.beginTransaction();
            realmItem = realm.copyToRealmOrUpdate(item);

            if (withTransaction)
                realm.commitTransaction();
        } catch (RealmException ex) {
            if (withTransaction)
                realm.cancelTransaction();
            return null;
        }

        return realmItem;
    }

    public void saveObjectList(List<? extends RealmObject> realmObjects, boolean withTransaction) {
        if (withTransaction)
            realm.beginTransaction();

        for (RealmObject realmObject : realmObjects)
            if (realmObject != null)
                realm.copyToRealmOrUpdate(realmObject);

        if (withTransaction)
            realm.commitTransaction();
    }

    public void deleteObject(RealmResults<? extends RealmObject> list, int position, boolean withTransaction) {
        if (withTransaction)
            realm.beginTransaction();

        list.deleteFromRealm(position);

        if (withTransaction)
            realm.commitTransaction();
    }

    public void deleteObject(boolean withTransaction, RealmObject... realmObjects) {
        try {
            if (withTransaction)
                realm.beginTransaction();

            for (RealmObject realmObject : realmObjects)
                if (realmObject != null)
                    realmObject.deleteFromRealm();

            if (withTransaction)
                realm.commitTransaction();
        } catch (Exception ex) {
            LogUtil.e(ex.getMessage());
            if (withTransaction)
                realm.cancelTransaction();
        }
    }

    public void deleteObjectList(List<? extends RealmObject> realmObjects, boolean withTransaction) {
        try {
            if (withTransaction)
                realm.beginTransaction();

            ///for (RealmObject realmObject : realmObjects)
            ///    if (realmObject != null)
            ///        realmObject.removeFromRealm();
            for (int i = realmObjects.size() - 1; i >= 0; i--)
                if (realmObjects.get(i) != null)
                    realmObjects.get(i).deleteFromRealm();

            if (withTransaction)
                realm.commitTransaction();
        } catch (Exception ex) {
            LogUtil.e(ex.getMessage());
            if (withTransaction)
                realm.cancelTransaction();
        }
    }

    public <T extends RealmObject> void deleteAll(Class<T> clazz, boolean withTransaction) {
        if (withTransaction)
            realm.beginTransaction();

        realm.delete(clazz);

        if (withTransaction)
            realm.commitTransaction();
    }


    //getters

    public <T extends RealmObject> RealmResults<T> getAll(Class<T> clazz) {
        return realm.where(clazz).findAll();
    }

    @Nullable
    public <T extends RealmObject> T getObjectByUid(Class<T> objectClass, Integer key) {
        try {
            return realm.where(objectClass)
                    .equalTo("uid", key).findFirst();
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return null;
    }

    @Nullable
    public EmployeeModel getLastEmployee() {
        EmployeeModel employeeModel;
        PayslipModel payslipModel = getLastPayslip();  //TODO demo
        if (payslipModel == null) {
            RealmQuery<EmployeeModel> employeeQuery = realm.where(EmployeeModel.class);
            if (employeeQuery.count() > 1) {
                employeeModel = employeeQuery.findAll().last();
            } else {
                employeeModel = employeeQuery.findFirst();
            }
        } else
            employeeModel = payslipModel.getEmployee();
        return employeeModel;
    }

    public EmployeeModel getLastEmployee(boolean withTransaction) {
        EmployeeModel employeeModel = getLastEmployee();
        if (employeeModel == null) {
            EmployeeModel empl = new EmployeeModel();
            empl.setUid(-1);
            employeeModel = save(ModelFactory.newInstanceEmployeeModel(empl, "", "", "", "", ""), withTransaction);
        }
        return employeeModel;
    }

    public RealmResults<WorkScheduleModel> getWorkScheduleWorkingDays(RealmResults<WorkScheduleModel> workScheduleModels) {
        return workScheduleModels.where().equalTo("dayTypeIndex", DayType.WORK.ordinal()).findAll();
    }

    public int getWorkScheduleWorkingDaysCount(Calendar date) {
        RealmResults<WorkScheduleModel> list = getWorkScheduleWorkingDays(getMonthWorkSchedule(date));
        return list.size();
    }

    public double getWorkScheduleHours(Calendar date) {
        RealmResults<WorkScheduleModel> list = getWorkScheduleWorkingDays(getMonthWorkSchedule(date));
        double plannedHours = 0;
        for (WorkScheduleModel workScheduleModel : list) {
            plannedHours += workScheduleModel.getHours();
        }
        return plannedHours;
    }

    public RealmResults<TimesheetModel> getTimesheetWorkingDays(RealmResults<TimesheetModel> timesheetModels) {
        return timesheetModels.where().equalTo("dayTypeIndex", DayType.WORK.ordinal()).findAll();
    }

    public int getTimesheetWorkingDaysCount(Calendar date) {
        RealmResults<TimesheetModel> list = getTimesheetWorkingDays(getMonthTimesheet(date));
        return list.size();
    }

    public double getTimesheetHours(Calendar date) {
        RealmResults<TimesheetModel> list = getTimesheetWorkingDays(getMonthTimesheet(date));
        double workedHours = 0;
        for (TimesheetModel timesheetModel : list) {
            workedHours += timesheetModel.getHours();
        }
        return workedHours;
    }

    // рабочий график на месяц
    public RealmResults<WorkScheduleModel> getMonthWorkSchedule(Calendar date) {
        Date[] monthPeriod = DateFormatHelper.getMonthPeriodDates(date);
        return getAllWorkSchedule(Sort.ASCENDING).where().between("date", monthPeriod[0], monthPeriod[1]).findAll();
    }

    // табель на месяц
    public RealmResults<TimesheetModel> getMonthTimesheet(Calendar date) {
        Date[] monthPeriod = DateFormatHelper.getMonthPeriodDates(date);
        return getAllTimesheet(Sort.ASCENDING).where().between("date", monthPeriod[0], monthPeriod[1]).findAll();
    }

    public RealmResults<WorkScheduleModel> getAllWorkSchedule(Sort sort) {
        return realm.where(WorkScheduleModel.class).findAllSorted("date", sort);
    }

    public RealmResults<TimesheetModel> getAllTimesheet(Sort sort) {
        return realm.where(TimesheetModel.class).findAllSorted("date", sort);
    }

    @Nullable
    public Date getTimeBeginEvents(RealmList<DayEventModel> dayEvents) {
        if (dayEvents.isEmpty()) return null;
        return dayEvents.where().equalTo("isAllDay", false).isNotNull("timeBegin").minimumDate("timeBegin");
    }

    @Nullable
    public Date getTimeEndEvents(RealmList<DayEventModel> dayEvents) {
        if (dayEvents.isEmpty()) return null;
        return dayEvents.where().equalTo("isAllDay", false).isNotNull("timeEnd").maximumDate("timeEnd");
    }

    public boolean containEventType(RealmList<DayEventModel> dayEvents, DayEventModel.EventType... eventTypes) {
        if (!dayEvents.isEmpty() || eventTypes.length == 0) {
            RealmQuery<DayEventModel> query = dayEvents.where();
            for (int i = 0; i < eventTypes.length - 1; i++)
                query = query.equalTo("eventTypeIndex", eventTypes[i].ordinal()).or();
            return query.equalTo("eventTypeIndex", eventTypes[eventTypes.length - 1].ordinal()).findFirst() != null;
        }
        return false;
    }

    @Nullable
    public PayslipModel getLastPayslip() {
        try {
            RealmResults<PayslipModel> payslips = realm.where(PayslipModel.class)
                    .findAllSorted("payDate", Sort.DESCENDING);

            for (PayslipModel payslip : payslips)
                if (payslip.getItems().where().sum("debit").doubleValue() > 0)
                    return payslip;
        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return null;
    }

    @Nullable
    public TimesheetModel getTimesheetByDay(Date date) {
        try {
            Calendar begin = Calendar.getInstance();
            begin.setTime(date);
            Calendar end = (Calendar) begin.clone();
            begin.set(Calendar.HOUR_OF_DAY, 0);
            end.set(Calendar.HOUR_OF_DAY, 23);

            begin.set(Calendar.MINUTE, 0);
            end.set(Calendar.MINUTE, 59);

            begin.set(Calendar.SECOND, 0);
            end.set(Calendar.SECOND, 59);
            return realm.where(TimesheetModel.class)
                    .between("date", begin.getTime(), end.getTime()).findFirst();
        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return null;
    }

    @Nullable
    public WorkScheduleModel getWorkScheduleByDay(Date date) {
        try {
            Calendar begin = Calendar.getInstance();
            begin.setTime(date);
            Calendar end = (Calendar) begin.clone();
            begin.set(Calendar.HOUR_OF_DAY, 0);
            end.set(Calendar.HOUR_OF_DAY, 23);

            begin.set(Calendar.MINUTE, 0);
            end.set(Calendar.MINUTE, 59);

            begin.set(Calendar.SECOND, 0);
            end.set(Calendar.SECOND, 59);
            return realm.where(WorkScheduleModel.class)
                    .between("date", begin.getTime(), end.getTime()).findFirst();
        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return null;
    }

    /**
     * Курс валют на месяц
     */
    @Nullable
    public ExRatesMonthlyModel getExRatesMonthly(Calendar date) {
        Date[] monthPeriod = DateFormatHelper.getMonthPeriodDates(date);

        try {
            return realm.where(ExRatesMonthlyModel.class)
                    .between("onDate", monthPeriod[0], monthPeriod[1])
                    .findFirst();
        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return null;
    }

    /**
     * Даты присутствующие в листках
     */
    public List<Calendar> getPayslipPossibleDatesList() {
        //TODO обработать когда несколько листков на один месяц
        RealmResults<PayslipModel> payslips = realm.where(PayslipModel.class)
                .findAllSorted("payDate", Sort.ASCENDING);

        List<Calendar> list = new ArrayList<>();
        for (PayslipModel payslip : payslips) {
            if (payslip.getItems().where().sum("debit").doubleValue() > 0) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(payslip.getPayDate());
                list.add(cal);
            }
        }
        if (list.isEmpty())
            list.add(Calendar.getInstance());

        return list;
    }

    /**
     * Листок за месяц
     */
    @Nullable
    public PayslipModel getPayslip(Calendar date) {
        Date[] monthPeriod = DateFormatHelper.getMonthPeriodDates(date);

        try {
            PayslipModel payslip =
                    realm.where(PayslipModel.class)
                            .equalTo("employee.uid", getLastEmployee().getUid())
                            .between("payDate", monthPeriod[0], monthPeriod[1])
                            .findAllSorted("payDate", Sort.DESCENDING)
                            .first();
            if (payslip.getItems().where().sum("debit").doubleValue() > 0)
                return payslip;
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return null;
    }

    public List<StatisticPayslip> getPayslipStatistics(Calendar firstDate, Calendar secondDate, boolean sortAscending) {   //TODO  return RealmResult
        List<StatisticPayslip> payslips = new ArrayList<>();
        List<PayslipModel> payslipModelList = new ArrayList<>();

        Calendar beginPeriodDate = Calendar.getInstance();
        Calendar endPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(DateFormatHelper.getMonthPeriodDates(firstDate)[0]);
        endPeriodDate.setTime(DateFormatHelper.getMonthPeriodDates(secondDate)[1]);

        try {
            while (!beginPeriodDate.after(endPeriodDate)) {
                PayslipModel payslipModel = getPayslip(beginPeriodDate);
                if (payslipModel != null)
                    payslipModelList.add(payslipModel);
                beginPeriodDate.add(Calendar.MONTH, 1);
            }
            if (!sortAscending)
                Collections.reverse(payslipModelList);
            for (PayslipModel item : payslipModelList) {

                double debit = item.getItems().where().sum("debit").doubleValue();
                if (debit > 0) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(item.getPayDate());
                    payslips.add(new StatisticPayslip(
                            cal,
                            debit));
                }
            }
        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return payslips;
    }

    @Nullable
    public Date getPrepaymentDate(PayslipModel payslipModel) {
        try {
            return payslipModel.getItems().where()
                    .equalTo("categoryIndex", PayslipItemModel.Category.PREPAYMENT.ordinal())
                    .findAllSorted("date")
                    .first().getDate();
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return null;
    }

    public double getPrepaymentSum(PayslipModel payslipModel) {
        try {
            return payslipModel.getItems().where()
                    .equalTo("categoryIndex", PayslipItemModel.Category.PREPAYMENT.ordinal())
                    // .equalTo("debit", 0.0)
                    .sum("credit").doubleValue();

        } catch (RuntimeException e) {
            //Crashlytics.getInstance().core.logException(e);
            LogUtil.d(e.toString());
        }
        return 0;
    }

    /**
     * Cписок налогов для листка
     */
    public RealmResults<PayslipItemModel> getTaxesList(PayslipModel payslipModel) { //TODO group by CODE
        return payslipModel.getItems().where()
                .equalTo("categoryIndex", PayslipItemModel.Category.TAX.ordinal())
                .greaterThan("credit", 0.0)
                // .equalTo("debit", 0.0)
                .findAllSorted("credit", Sort.DESCENDING);
    }

    public double getTaxesSum(PayslipModel payslipModel) {
        return getTaxesList(payslipModel).sum("credit").doubleValue();
    }

    public double getTotalTaxesSum(Calendar firstDate, Calendar secondDate) {

        double totalTaxes = 0;
        Calendar dateBegin = Calendar.getInstance();
        dateBegin.setTime(DateFormatHelper.getMonthPeriodDates(firstDate)[0]);
        Calendar dateEnd = Calendar.getInstance();
        dateEnd.setTime(DateFormatHelper.getMonthPeriodDates(secondDate)[1]);

        Calendar denominationDate = new GregorianCalendar(2016, 6, 1, 0, 0, 1);

        boolean afterDenomination = dateEnd.after(new GregorianCalendar(2016, 6, 1, 0, 0, 0));
        try {
            while (!dateBegin.after(dateEnd)) {
                PayslipModel payslipModel = getPayslip(dateBegin);
                if (payslipModel != null) {
                    if (afterDenomination && dateBegin.before(denominationDate)) {
                    totalTaxes += payslipModel.getItems().where()
                            .equalTo("categoryIndex", PayslipItemModel.Category.TAX.ordinal())
                            .sum("credit").doubleValue() / 10000;
                    } else {
                        totalTaxes += payslipModel.getItems().where()
                                .equalTo("categoryIndex", PayslipItemModel.Category.TAX.ordinal())
                                .sum("credit").doubleValue();
                    }
                }
                dateBegin.add(Calendar.MONTH, 1);
            }
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return totalTaxes;
    }

    public RealmResults<PayslipItemModel> getPrepaymentList(PayslipModel payslipModel) { //TODO group by CODE
        return payslipModel.getItems().where()
                .equalTo("categoryIndex", PayslipItemModel.Category.PREPAYMENT.ordinal())
                .greaterThan("credit", 0.0)
                //  .equalTo("debit", 0.0)
                .findAllSorted("credit", Sort.DESCENDING);
    }

    /**
     * Список yдержаний для листка  (кроме налогов и аванса).
     */
    public RealmResults<PayslipItemModel> getCreditList(PayslipModel payslipModel) { //TODO group by CODE
        return payslipModel.getItems().where()
                .equalTo("categoryIndex", PayslipItemModel.Category.CREDIT.ordinal())
                .greaterThan("credit", 0.0)
                //  .equalTo("debit", 0.0)
                .findAllSorted("credit", Sort.DESCENDING);
    }

    public double getCreditSum(PayslipModel payslipModel) {
        return getCreditList(payslipModel).sum("credit").doubleValue();
    }

    //Список начислений для листка
    public RealmResults<PayslipItemModel> getDebitList(PayslipModel payslipModel) { //TODO group by CODE
        return payslipModel.getItems().where()
                .equalTo("categoryIndex", PayslipItemModel.Category.DEBIT.ordinal())
                .greaterThan("debit", 0.0)
                // .equalTo("credit", 0.0)
                .findAllSorted("debit", Sort.DESCENDING);
    }

    public double getDebitSum(PayslipModel payslipModel) {
        return getDebitList(payslipModel).sum("debit").doubleValue();
    }

    public double getTotalDebitSum(Calendar firstDate, Calendar secondDate) {

        double totalDebit = 0;
        Calendar dateBegin = Calendar.getInstance();
        dateBegin.setTime(DateFormatHelper.getMonthPeriodDates(firstDate)[0]);
        Calendar dateEnd = Calendar.getInstance();
        dateEnd.setTime(DateFormatHelper.getMonthPeriodDates(secondDate)[1]);

        Calendar denominationDate = new GregorianCalendar(2016, 6, 1, 0, 0, 1);

        boolean afterDenomination = dateEnd.after(new GregorianCalendar(2016, 6, 1, 0, 0, 0));
        try {
            while (!dateBegin.after(dateEnd)) {
                PayslipModel payslipModel = getPayslip(dateBegin);
                if (payslipModel != null) {
                    if (afterDenomination && dateBegin.before(denominationDate)) {
                        totalDebit += payslipModel.getItems().where().sum("debit").doubleValue() / 10000;
                    } else {
                        totalDebit += payslipModel.getItems().where().sum("debit").doubleValue();
                    }
                }
                dateBegin.add(Calendar.MONTH, 1);
            }
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return totalDebit;
    }


    // кол-во непрочитанных сообщений
    public RealmResults<GdMsgModel> getUnreadGdMsgs() {
        return realm.where(GdMsgModel.class).equalTo("isUnread", true).findAll();
    }

    // все сообщения //TODO limit
    public RealmResults<GdMsgModel> getAllGdMsgs() {
        return realm.where(GdMsgModel.class).findAllSorted("timestamp", Sort.DESCENDING);
    }



    // сообщение прочитано
    public void setGdMsgViewedWithSync(GdMsgModel gdMsgModel) {
        try {
            realm.beginTransaction();
            gdMsgModel.setIsUnread(false);
            ModelFactory.newInstanceSyncGdMsgStatusModel(realm.createObject(SyncGdMsgStateModel.class, UUID.randomUUID().toString()), SyncGdMsgStateModel.UpdatedStatus.READED, gdMsgModel.getUid());
            realm.commitTransaction();

        } catch (IllegalStateException e) {
            gdMsgModel.setIsUnread(false);
            ModelFactory.newInstanceSyncGdMsgStatusModel(realm.createObject(SyncGdMsgStateModel.class, UUID.randomUUID().toString()), SyncGdMsgStateModel.UpdatedStatus.READED, gdMsgModel.getUid());
        }
    }

    public void deleteGdMsgWithSync(RealmResults<GdMsgModel> gdMsgs, int position, boolean withTransaction) {
        SyncGdMsgStateModel syncGdMsgStateModel = new SyncGdMsgStateModel();
        syncGdMsgStateModel.setUid(UUID.randomUUID().toString());
        save(ModelFactory.newInstanceSyncGdMsgStatusModel(syncGdMsgStateModel, SyncGdMsgStateModel.UpdatedStatus.DELETED, gdMsgs.get(position).getUid()), withTransaction);
    }

    public void deleteGdMsgWithSync(GdMsgModel gdMsgModel, boolean withTransaction) {
        //TODO assertNotNull(gdMsgModel);
        SyncGdMsgStateModel syncGdMsgStateModel = new SyncGdMsgStateModel();
        syncGdMsgStateModel.setUid(UUID.randomUUID().toString());
        save(ModelFactory.newInstanceSyncGdMsgStatusModel(syncGdMsgStateModel, SyncGdMsgStateModel.UpdatedStatus.DELETED, gdMsgModel.getUid()), withTransaction);
        deleteObject(withTransaction, gdMsgModel);
    }

    public SyncMetaDataModel getSyncMetaData(boolean withTransaction) {
        SyncMetaDataModel syncMetaDataModel = realm.where(SyncMetaDataModel.class).findFirst();
        if (syncMetaDataModel == null)
            syncMetaDataModel = save(ModelFactory.newInstanceSyncMetaDataModel(new SyncMetaDataModel()), withTransaction);
        return syncMetaDataModel;
    }

    public DeviceInfoModel getDeviceInfo(boolean withTransaction) {
        DeviceInfoModel deviceInfoModel = realm.where(DeviceInfoModel.class).findFirst();
        if (deviceInfoModel == null) {
            String deviceId = getDeviceId();
            String phoneNumber = getDevicePhoneNumber();
            String modelName = getDeviceModelName();

            deviceInfoModel = save(ModelFactory.newInstanceDeviceInfoModel(new DeviceInfoModel(), deviceId, phoneNumber, modelName),
                    withTransaction);
        }
        return deviceInfoModel;
    }

    public static String getVersionName() {
        String versionName = "";
        try {
            versionName = App.getContext().getPackageManager().getPackageInfo(App.getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            LogUtil.d(e.toString());
        }
        return versionName;
    }//TODO вынести куда-то

    public static int getVersionCode() {
        int versionCode = 0;
        try {
            versionCode = App.getContext().getPackageManager().getPackageInfo(App.getContext().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            LogUtil.d(e.toString());
        }
        return versionCode;
    }//TODO вынести куда-то

    /**
     * @return IMEI for GSM, MEI for GSM, MEID/ESN for CDMA phones, else: android_id
     */
    private String getDeviceId() {
        String deviceId = "";
        try {
            final TelephonyManager mTelephony = (TelephonyManager) App.getContext().getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = mTelephony.getDeviceId();

            deviceId = (deviceId != null ? "Device ID " + deviceId :
                    "Android ID " + Settings.Secure.getString(App.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

        } catch (SecurityException e) {
            LogUtil.d(e.toString());
        }
        return deviceId;
    } //TODO вынести куда-то

    /**
     * @return the number from the SIM's MSISDN header
     */
    @Nullable
    private String getDevicePhoneNumber() { //TODO номер -> аккаунт гугл
        String devicePhoneNumber = "";
        try {
            devicePhoneNumber = ((TelephonyManager) App.getContext().getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
        } catch (SecurityException e) {
            LogUtil.d(e.toString());
        }
        return devicePhoneNumber == null ? "" : devicePhoneNumber;
    }//TODO вынести куда-то

    private String getDeviceModelName() {
        return Build.MODEL;
    }//TODO вынести куда-то

    public int generateUID(Class cl) {
        try {
            return realm.where(cl).findAll().max("uid").intValue() + 1;
        } catch (RuntimeException e) {
            LogUtil.d(e.toString());
        }
        return 1;
    }

    public void applyGdMsgState(SyncGdMsgStateModel state, boolean withTransaction) {
        if (withTransaction)
            realm.beginTransaction();

        GdMsgModel gdMsg = getObjectByUid(GdMsgModel.class, state.getGdMsgKey());
        if (gdMsg != null) {
            switch (state.getStatus()) {
                case READED:
                    gdMsg.setIsUnread(false);
                    break;
                case DELETED:
                    gdMsg.deleteFromRealm();
                    break;
            }
        }

        RealmResults<SyncGdMsgStateModel> gdMsgStates = realm.where(SyncGdMsgStateModel.class)
                .equalTo("gdMsgKey", state.getGdMsgKey())
                .equalTo("statusIndex", state.getStatusIndex()).findAll();

        deleteObjectList(gdMsgStates, false);

        if (withTransaction)
            realm.commitTransaction();
    }

    @SuppressLint("SimpleDateFormat")
    public void initializeDemoData() {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

        // Пользователь
        realm.beginTransaction();

        EmployeeModel employeeModel = ModelFactory.newInstanceEmployeeModel(
                realm.createObject(EmployeeModel.class, -1),
                "Иван", "Иванович", "Иванов", "7340789K037PB2", "2902");

        ModelFactory.newInstanceContactModel(
                realm.createObject(ContactModel.class, employeeModel.getUid()),
                employeeModel.getFirstname(), employeeModel.getSurname(), employeeModel.getMiddlename(), "");

        realm.commitTransaction();

        // Рабочий график
        realm.beginTransaction();

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        HashMap<Calendar, String> holidaysNames = new HashMap<>();
        HashMap<Calendar, String> holidaysDesc = new HashMap<>();
        List<Calendar> loungeDates = new ArrayList<>();
        List<Calendar> businessTripDates = new ArrayList<>();

        for (int i = currentYear - 1; i < currentYear + 1; i++) {
            holidaysNames.put(new GregorianCalendar(i, 0, 1), "Новый Год");
            holidaysNames.put(new GregorianCalendar(i, 0, 7), "Православное Рождество");
            holidaysNames.put(new GregorianCalendar(i, 1, 23), "День Защитника Отечества");
            holidaysNames.put(new GregorianCalendar(i, 2, 8), "Международный Женский День");
            holidaysNames.put(new GregorianCalendar(i, 2, 15), "День Конституции");
            holidaysNames.put(new GregorianCalendar(i, 3, 26), "Радоница");
            holidaysNames.put(new GregorianCalendar(i, 3, 21), "День Памяти Чернобыльской Трагедии");
            holidaysNames.put(new GregorianCalendar(i, 4, 1), "День Труда");
            holidaysNames.put(new GregorianCalendar(i, 4, 9), "День Победы");
            holidaysNames.put(new GregorianCalendar(i, 4, 10), "День Национальной Символики");
            holidaysNames.put(new GregorianCalendar(i, 6, 3), "День Независимости Республики Беларусь");
            holidaysNames.put(new GregorianCalendar(i, 10, 7), "День Октябрьской Революции");
            holidaysNames.put(new GregorianCalendar(i, 11, 25), "Католическое Рождество");

            holidaysDesc.put(new GregorianCalendar(i, 0, 1), "НГ");
            holidaysDesc.put(new GregorianCalendar(i, 0, 7), "ПР");
            holidaysDesc.put(new GregorianCalendar(i, 1, 23), "ДЗО");
            holidaysDesc.put(new GregorianCalendar(i, 2, 8), "МЖД");
            holidaysDesc.put(new GregorianCalendar(i, 2, 15), "ДК");
            holidaysDesc.put(new GregorianCalendar(i, 3, 26), "Р");
            holidaysDesc.put(new GregorianCalendar(i, 3, 21), "ДПЧТ");
            holidaysDesc.put(new GregorianCalendar(i, 4, 1), "ДТ");
            holidaysDesc.put(new GregorianCalendar(i, 4, 9), "ДП");
            holidaysDesc.put(new GregorianCalendar(i, 4, 10), "ДНС");
            holidaysDesc.put(new GregorianCalendar(i, 6, 3), "ДНРБ");
            holidaysDesc.put(new GregorianCalendar(i, 10, 7), "ДОР");
            holidaysDesc.put(new GregorianCalendar(i, 11, 25), "КР");

            for (int j = 0; j < 12; j++) {
                loungeDates.add(new GregorianCalendar(i, j, randInt(1, 28)));
            }

            for (int j = 0; j < 12; j++) {
                businessTripDates.add(new GregorianCalendar(i, j, randInt(1, 28)));
            }
        }

        generateInstanceWorkSchedules(new GregorianCalendar(currentYear - 1, 0, 1), new GregorianCalendar(currentYear, 11, 31),
                employeeModel, holidaysNames, holidaysDesc, businessTripDates);

        generateInstanceTimesheet(new GregorianCalendar(currentYear - 1, 0, 1), Calendar.getInstance(),
                employeeModel, holidaysNames, holidaysDesc, loungeDates, businessTripDates);

        realm.commitTransaction();

        // Курсы валют
        realm.beginTransaction();

        try {
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("06.04.2015"), 14620, 15910);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.03.2015"), 15060, 15950);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.02.2015"), 15090, 17240);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.01.2015"), 15010, 17710);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.12.2014"), 10860, 13450);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.11.2014"), 10760, 13390);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.10.2014"), 10650, 13520);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.09.2014"), 10490, 13560);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.08.2014"), 10370, 13860);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.07.2014"), 10250, 13970);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.06.2014"), 10150, 13770);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.05.2014"), 10020, 13740);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.04.2014"), 9930, 13760);
            generateInstanceExRatesMonthlyModel(dateFormatter.parse("15.03.2014"), 9820, 13610);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        realm.commitTransaction();

        // Расчетные листки
        realm.beginTransaction();

        Calendar calMonth = Calendar.getInstance();
        CalendarHelper.initCalendarFields(calMonth, Calendar.MONTH, 1);

        for (int i = 0; i < 12; i++) {
            calMonth.add(Calendar.MONTH, -1);
            Calendar calDate = (Calendar) calMonth.clone();
            Date date = new Date(calDate.getTimeInMillis());

            boolean afterDenomination = date.after(new GregorianCalendar(2016, 6, 1, 0, 0, 0).getTime());

            PayslipModel payslipModel = ModelFactory.newInstancePayslipModel(
                    realm.createObject(PayslipModel.class, generateUID(PayslipModel.class)),
                    date,
                    employeeModel,
                    "Электроучасток",
                    "Электромантер",
                    0,
                    afterDenomination ? 3 : 5312,
                    1,
                    true);

            // начисления
            List<String> debitNames = new ArrayList<>();
            debitNames.add("Доплата за совмещение профессий (6 дн.)");
            debitNames.add("За работу в выходные");
            debitNames.add("Премия");
            debitNames.add("Доплата за выслугу лет");
            debitNames.add("Материальная помощь");
            debitNames.add("За работу в ночь (56 ч)");
            debitNames.add("Повременно (13 дн. 104 ч.)");
            debitNames.add("Питание");

            List<Double> debitSums = new ArrayList<>();
            debitSums.add(afterDenomination ? randDouble(20, 30) : (double) randInt(200000, 300000));
            debitSums.add(afterDenomination ? randDouble(30, 40) : (double) randInt(300000, 400000));
            debitSums.add(afterDenomination ? randDouble(40, 60) : (double) randInt(400000, 600000));
            debitSums.add(afterDenomination ? randDouble(20, 30) : (double) randInt(200000, 300000));
            debitSums.add(afterDenomination ? randDouble(50, 70) : (double) randInt(500000, 700000));
            debitSums.add(afterDenomination ? randDouble(270, 350) : (double) randInt(2700000, 3500000));
            debitSums.add(afterDenomination ? randDouble(300, 400) : (double) randInt(3000000, 4000000));
            debitSums.add(afterDenomination ? randDouble(15, 30) : (double) randInt(150000, 300000));

            double totalDebitSum = 0.0;
            double totalCreditSum = 0.0;
            double prepaymentSum = 0.0;
            double totalTaxesSum = 0.0;

            for (int j = 0; j < debitNames.size(); j++) {
                totalDebitSum += debitSums.get(j);
                payslipModel.getItems().add(ModelFactory.newInstancePayslipItemModel(
                        realm.createObject(PayslipItemModel.class, generateUID(PayslipItemModel.class)),
                        PayslipItemModel.Category.DEBIT,
                        date,
                        debitNames.get(j),
                        100 + j,
                        debitSums.get(j),
                        0));
            }

            // удержания
            List<String> creditNames = new ArrayList<>();
            creditNames.add("Долг за сотрудником");
            creditNames.add("БАНК");
            creditNames.add("Удержание за мобильную связь");
            creditNames.add("Субботник");

            List<Double> creditSums = new ArrayList<>();
            creditSums.add(afterDenomination ? randDouble(10, 16) : (double) randInt(100000, 160000));
            creditSums.add(afterDenomination ? randDouble(40, 80) : (double) randInt(400000, 800000));
            creditSums.add(afterDenomination ? randDouble(1.5, 3) : (double) randInt(15000, 30000));
            creditSums.add(afterDenomination ? randDouble(6, 9) : (double) randInt(60000, 90000));

            for (int j = 0; j < creditNames.size(); j++) {
                totalCreditSum += creditSums.get(j);
                payslipModel.getItems().add(ModelFactory.newInstancePayslipItemModel(
                        realm.createObject(PayslipItemModel.class, generateUID(PayslipItemModel.class)),
                        PayslipItemModel.Category.CREDIT,
                        date,
                        creditNames.get(j),
                        200 + j,
                        0,
                        creditSums.get(j)));
            }

            prepaymentSum = afterDenomination ? randDouble(120, 150) : (double) randInt(1200000, 1500000);
            payslipModel.getItems().add(ModelFactory.newInstancePayslipItemModel(
                    realm.createObject(PayslipItemModel.class, generateUID(PayslipItemModel.class)),
                    PayslipItemModel.Category.PREPAYMENT,
                    date,
                    "Аванс",
                    204,
                    0,
                    prepaymentSum));

            // налоги
            List<String> taxesNames = new ArrayList<>();
            taxesNames.add("Подоходный");
            taxesNames.add("Профсоюзный");
            taxesNames.add("Пенсионный");

            List<Double> taxesSums = new ArrayList<>();
            taxesSums.add(afterDenomination ? randDouble(50, 75) : (double) randInt(500000, 750000));
            taxesSums.add(afterDenomination ? randDouble(6, 9) : (double) randInt(60000, 90000));
            taxesSums.add(afterDenomination ? randDouble(5, 7.5) : (double) randInt(50000, 75000));

            for (int j = 0; j < taxesNames.size(); j++) {
                totalTaxesSum += taxesSums.get(j);
                payslipModel.getItems().add(ModelFactory.newInstancePayslipItemModel(
                        realm.createObject(PayslipItemModel.class, generateUID(PayslipItemModel.class)),
                        PayslipItemModel.Category.TAX,
                        date,
                        taxesNames.get(j),
                        205 + j,
                        0,
                        taxesSums.get(j)));
            }

            // льготы
            List<String> benefitsNames = new ArrayList<>();
            benefitsNames.add("Вознаграждение");
            benefitsNames.add("Стоимость путевок");

            List<Double> benefitsSums = new ArrayList<>();
            benefitsSums.add(afterDenomination ? randDouble(20, 40) : (double) randInt(200000, 400000));
            benefitsSums.add(afterDenomination ? randDouble(30, 50) : (double) randInt(300000, 500000));

            for (int j = 0; j < benefitsNames.size(); j++) {
                payslipModel.getBenefits().add(ModelFactory.newInstancePayslipBenefitModel(
                        realm.createObject(PayslipBenefitModel.class, generateUID(PayslipBenefitModel.class)),
                        date,
                        benefitsNames.get(j),
                        benefitsSums.get(j)));
            }

            // вычеты
            List<String> deductionsNames = new ArrayList<>();
            deductionsNames.add("На строительство жилья");
            deductionsNames.add("Сумма, уплаченная за обучение");
            deductionsNames.add("На ребенка");

            List<Double> deductionsSums = new ArrayList<>();
            deductionsSums.add(afterDenomination ? randDouble(25, 35) : (double) randInt(250000, 350000));
            deductionsSums.add(afterDenomination ? randDouble(17, 25) : (double) randInt(170000, 250000));
            deductionsSums.add(afterDenomination ? randDouble(30, 40) : (double) randInt(300000, 400000));

            for (int j = 0; j < deductionsNames.size(); j++) {
                payslipModel.getDeductions().add(ModelFactory.newInstancePayslipDeductionModel(
                        realm.createObject(PayslipDeductionModel.class, generateUID(PayslipDeductionModel.class)),
                        date,
                        deductionsNames.get(j),
                        "Стандартный вычет",
                        deductionsSums.get(j)));
            }

            payslipModel.setEndSaldo(totalDebitSum - totalCreditSum - prepaymentSum - totalTaxesSum);
        }
        realm.commitTransaction();

        //Cообщения
        realm.beginTransaction();

        ModelFactory.newInstanceGdMsgInfoModel(
                realm.createObject(GdMsgInfoModel.class, generateUID(GdMsgInfoModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "Добро пожаловать",
                "Программа работает с демонстрационными данными",
                new Date(),
                GdMsgModel.RequestStatus.UNKNOWN);

        //14
        GdMsgModel gdMsgAbsenceModel = ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                DateFormatHelper.addCalendarValuesDiff(-1, -2, 1, 2, 5),
                DateFormatHelper.addCalendarValuesDiff(-1, -2, 3, 2, 15),
                DateFormatHelper.addCalendarValuesDiff(-1, -2, 4, 2, 15),
                GdMsgAbsenceModel.Type.MILITARY_COMMISSARIAT);

        ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "",
                DateFormatHelper.addCalendarValuesDiff(-1, -2, 2, 6, 2),
                GdMsgModel.RequestStatus.REJECTED,
                gdMsgAbsenceModel
        );
        //16
        GdMsgModel gdMsgIncomeModel = ModelFactory.newInstanceGdMsgIncomeModel(
                realm.createObject(GdMsgIncomeModel.class, generateUID(GdMsgIncomeModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "Желательно к среде",
                DateFormatHelper.addCalendarValuesDiff(0, -2, -3, 1, 5),
                DateFormatHelper.addCalendarValuesDiff(0, -9, 0, 0, 0),
                DateFormatHelper.addCalendarValuesDiff(0, -3, 0, 0, 0));

        ModelFactory.newInstanceGdMsgIncomeModel(
                realm.createObject(GdMsgIncomeModel.class, generateUID(GdMsgIncomeModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "Можете забрать после 12:00",
                DateFormatHelper.addCalendarValuesDiff(0, -2, -2, 4, 6),
                GdMsgModel.RequestStatus.SUCCESS,
                gdMsgIncomeModel);

        GdMsgModel gdMsgVacationInfoModel = ModelFactory.newInstanceGdMsgVacationInfoModel(
                realm.createObject(GdMsgVacationInfoModel.class, generateUID(GdMsgVacationInfoModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -3, -4, -1, -6));

        ModelFactory.newInstanceGdMsgVacationInfoModel(
                realm.createObject(GdMsgVacationInfoModel.class, generateUID(GdMsgVacationInfoModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -2, -1, 5, 7),
                DateFormatHelper.addCalendarValuesDiff(0, -1, -1, 4, 8),
                DateFormatHelper.addCalendarValuesDiff(0, 0, -6, 2, 7),
                gdMsgVacationInfoModel);

        //18
        GdMsgModel gdMsgAbsenceModel2 = ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -4, -3, 6, 8),
                DateFormatHelper.addCalendarValuesDiff(0, -3, -6, 7, 2),
                DateFormatHelper.addCalendarValuesDiff(0, -3, -1, 4, 7),
                GdMsgAbsenceModel.Type.SICK);

        ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -4, -2, 2, 3),
                GdMsgModel.RequestStatus.SUCCESS,
                gdMsgAbsenceModel2);

        GdMsgModel gdMsgAbsenceModel3 = ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                DateFormatHelper.addCalendarValuesDiff(-2, -4, 5, 6, 2),
                DateFormatHelper.addCalendarValuesDiff(-2, -3, 6, 4, 6),
                DateFormatHelper.addCalendarValuesDiff(-2, -3, 7, 4, 6),
                GdMsgAbsenceModel.Type.DONOR_DAY);

        ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "",
                DateFormatHelper.addCalendarValuesDiff(-2, -4, 6, 2, 6),
                GdMsgModel.RequestStatus.SUCCESS,
                gdMsgAbsenceModel3
        );

        GdMsgModel gdMsgVacationModel = ModelFactory.newInstanceGdMsgVacationModel(
                realm.createObject(GdMsgVacationModel.class, generateUID(GdMsgVacationModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -8, 5, 6, 2),
                DateFormatHelper.addCalendarValuesDiff(0, -6, 1, 4, 6),
                DateFormatHelper.addCalendarValuesDiff(0, -5, 1, 4, 6),
                GdMsgVacationModel.Type.UNPAID);

        ModelFactory.newInstanceGdMsgAbsenceModel(
                realm.createObject(GdMsgAbsenceModel.class, generateUID(GdMsgAbsenceModel.class)),
                realm.createObject(GdMsgModel.class, generateUID(GdMsgModel.class)),
                "",
                "",
                DateFormatHelper.addCalendarValuesDiff(0, -8, 6, 2, 6),
                GdMsgModel.RequestStatus.SUCCESS,
                gdMsgVacationModel);

        realm.commitTransaction();
    }

    private void generateInstanceWorkSchedules(Calendar dateBegin, Calendar dateEnd, EmployeeModel employeeModel,
                                               HashMap<Calendar, String> holidaysNames, HashMap<Calendar, String> holidaysDesc,
                                               List<Calendar> businessTripDates) { //TODO final  params
        boolean isDayShift = true;
        while (!dateBegin.after(dateEnd)) {
            Date day = dateBegin.getTime();
            if (dateBegin.get(Calendar.MONTH) == Calendar.JANUARY) {
                WorkScheduleModel workScheduleModel = ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        "о",
                        0);
                workScheduleModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.VACATION,
                        "отпуск",
                        true));

            } else if (holidaysNames.containsKey(dateBegin)) { //TODO locale
                WorkScheduleModel daySchedule = ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        holidaysDesc.get(dateBegin),
                        0);

                daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.PUBLIC_HOLIDAY,
                        holidaysNames.get(dateBegin),
                        true));

            } else if ((dateBegin.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && !holidaysNames.containsKey(dateBegin)) ||
                    (dateBegin.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && !holidaysNames.containsKey(dateBegin))) {
                ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        "",
                        0);

            } else if ((dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && dateBegin.get(Calendar.MONTH) == Calendar.MAY
                    && !holidaysNames.containsKey(dateBegin) && !businessTripDates.contains(dateBegin))) {
                WorkScheduleModel daySchedule = ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.WORK,
                        employeeModel,
                        day,
                        "",
                        isDayShift ? 8 : 7);

                daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        isDayShift ? DayEventModel.EventType.WORKING : DayEventModel.EventType.NIGHT,
                        "",
                        isDayShift ? 8 : 7));

                isDayShift = !isDayShift;

            } else if (dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && !holidaysNames.containsKey(dateBegin) && businessTripDates.contains(dateBegin)) {
                WorkScheduleModel daySchedule = ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.WORK,
                        employeeModel,
                        day,
                        "к",
                        isDayShift ? 8 : 7);
                daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.WORKING,
                        "командировка",
                        isDayShift ? 8 : 7));

                isDayShift = !isDayShift;

            } else if (dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && dateBegin.get(Calendar.MONTH) != Calendar.MAY
                    && !holidaysNames.containsKey(dateBegin) && !businessTripDates.contains(dateBegin)) {
                WorkScheduleModel daySchedule = ModelFactory.newInstanceWorkScheduleModel(
                        realm.createObject(WorkScheduleModel.class, generateUID(WorkScheduleModel.class)),
                        DayType.WORK,
                        employeeModel,
                        day,
                        "",
                        isDayShift ? 8 : 7);
                Calendar tempDate = (Calendar) dateBegin.clone();
                if (isDayShift) {
                    tempDate.add(Calendar.HOUR_OF_DAY, 9);
                    Date timeWorkBegin = tempDate.getTime();
                    tempDate.add(Calendar.HOUR_OF_DAY, 8);
                    Date timeWorkEnd = tempDate.getTime();

                    daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.WORKING,
                            timeWorkBegin,
                            timeWorkEnd,
                            ""));
                } else {
                    tempDate.add(Calendar.HOUR_OF_DAY, 17);
                    Date timeNightBegin = tempDate.getTime();
                    tempDate.add(Calendar.HOUR_OF_DAY, 7);
                    Date timeNightEnd = tempDate.getTime();
                    daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.NIGHT,
                            timeNightBegin,
                            timeNightEnd,
                            ""));
                }

                isDayShift = !isDayShift;
            }

            dateBegin.add(Calendar.DATE, 1);
        }

    }

    private void generateInstanceTimesheet(Calendar dateBegin, Calendar dateEnd, EmployeeModel employeeModel,
                                           HashMap<Calendar, String> holidaysNames, HashMap<Calendar, String> holidaysDesc,
                                           List<Calendar> loungeDates, List<Calendar> businessTripDates) {
        boolean isDayShift = true;
        while (!dateBegin.after(dateEnd)) {
            Date day = dateBegin.getTime();
            if (dateBegin.get(Calendar.MONTH) == Calendar.JANUARY) {
                TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        "о",
                        0);
                timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.VACATION,
                        "",
                        true));

            } else if (holidaysNames.containsKey(dateBegin)) {
                TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        holidaysDesc.get(dateBegin),
                        0);
                timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.PUBLIC_HOLIDAY,
                        holidaysNames.get(dateBegin),
                        true));

            } else if ((dateBegin.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && !holidaysNames.containsKey(dateBegin)) ||
                    (dateBegin.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && !holidaysNames.containsKey(dateBegin))) {
                TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        "в",
                        0);
                timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.NOT_WORKING,
                        "выходной",
                        true));

            } else if (dateBegin.get(Calendar.WEEK_OF_YEAR) == 25
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && !holidaysNames.containsKey(dateBegin)) {
                TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.DAY_OFF,
                        employeeModel,
                        day,
                        "б",
                        0);
                timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.SICK,
                        "",
                        true));

                isDayShift = !isDayShift;

            } else if (loungeDates.contains(dateBegin) && dateBegin.get(Calendar.WEEK_OF_YEAR) != 25) {
                if (randBoolean(40)) {
                    TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                            realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                            DayType.DAY_OFF,
                            employeeModel,
                            day,
                            "п",
                            0);
                    timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.ABSENTEEISM,
                            "",
                            isDayShift ? 8 : 7));
                } else {
                    TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                            realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                            DayType.WORK,
                            employeeModel,
                            day,
                            "",
                            4);

                    String randDesk = (new Random().nextInt() < 0) ?
                            "отгул" : "прогул";

                    timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.WORKING,
                            "",
                            4));

                    timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            randDesk.equals("отгул") ? DayEventModel.EventType.NOT_WORKING : DayEventModel.EventType.ABSENTEEISM,
                            randDesk,
                            isDayShift ? 4 : 3));
                }

                isDayShift = !isDayShift;

            } else if (dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && dateBegin.get(Calendar.WEEK_OF_YEAR) != 25
                    && !holidaysNames.containsKey(dateBegin) && !loungeDates.contains(dateBegin)
                    && businessTripDates.contains(dateBegin)) {
                TimesheetModel daySchedule = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.WORK,
                        employeeModel,
                        day,
                        "к",
                        isDayShift ? 8 : 7);

                daySchedule.getEvents().add(ModelFactory.newInstanceDayEventModel(
                        realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                        DayEventModel.EventType.WORKING,
                        "командировка",
                        isDayShift ? 8 : 7));

                isDayShift = !isDayShift;

            } else if (dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && dateBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && dateBegin.get(Calendar.WEEK_OF_YEAR) != 25
                    && !holidaysNames.containsKey(dateBegin) && !loungeDates.contains(dateBegin)
                    && !businessTripDates.contains(dateBegin)) {
                TimesheetModel timesheetModel = ModelFactory.newInstanceTimesheetModel(
                        realm.createObject(TimesheetModel.class, generateUID(TimesheetModel.class)),
                        DayType.WORK,
                        employeeModel,
                        day,
                        "",
                        isDayShift ? 8 : 7);

                if (isDayShift) {
                    timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.WORKING,
                            "",
                            8));
                } else {
                    timesheetModel.getEvents().add(ModelFactory.newInstanceDayEventModel(
                            realm.createObject(DayEventModel.class, generateUID(DayEventModel.class)),
                            DayEventModel.EventType.NIGHT,
                            "",
                            7));
                }

                isDayShift = !isDayShift;
            }
            dateBegin.add(Calendar.DATE, 1);
        }
    }

    public ExRatesMonthlyModel generateInstanceExRatesMonthlyModel(Date onDate, double usdRate, double eurRate) {
        return ModelFactory.newInstanceExRatesMonthlyModel(
                realm.createObject(ExRatesMonthlyModel.class, generateUID(ExRatesMonthlyModel.class)),
                onDate,
                usdRate,
                eurRate);
    }

}