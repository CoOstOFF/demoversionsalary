package com.gsbelarus.gedemin.salary.database;


import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

public abstract class ModelManager<T extends RealmObject> {
    protected Realm realm;

    final Class<T> typeParameterClass;

    public ModelManager(Class<T> typeParameterClass) {
        realm = Realm.getDefaultInstance();
        this.typeParameterClass = typeParameterClass;
    }

    public T save(T item) {
        realm.beginTransaction();
        T realmItem = realm.copyToRealm(item);
        realm.commitTransaction();
        return realmItem;
    }

    public List<T> getAll() {
        return realm.where(typeParameterClass).findAll();
    }
}