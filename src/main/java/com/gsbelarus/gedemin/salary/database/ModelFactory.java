package com.gsbelarus.gedemin.salary.database;

import com.gsbelarus.gedemin.salary.entity.model.ContactModel;
import com.gsbelarus.gedemin.salary.entity.model.DayEventModel;
import com.gsbelarus.gedemin.salary.entity.model.DayType;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.ExRatesMonthlyModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgAbsenceModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgIncomeModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgVacationModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipBenefitModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipDeductionModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipItemModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncGdMsgStateModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.entity.model.TimesheetModel;
import com.gsbelarus.gedemin.salary.entity.model.WorkScheduleModel;

import java.util.Date;

public class ModelFactory {

    public static SyncGdMsgStateModel newInstanceSyncGdMsgStatusModel(SyncGdMsgStateModel model, SyncGdMsgStateModel.UpdatedStatus updatedStatus, Integer gdMsgKey) {
        model.setStatus(updatedStatus);
        model.setGdMsgKey(gdMsgKey);
        return model;
    }

    public static EmployeeModel newInstanceEmployeeModel(EmployeeModel model, String firstname, String surname, String middlename, String passportId, String listNumber) {
        model.setFirstname(firstname);
        model.setSurname(surname);
        model.setMiddlename(middlename);
        model.setPassportId(passportId);
        model.setListNumber(listNumber);
        return model;
    }

    public static ExRatesMonthlyModel newInstanceExRatesMonthlyModel(ExRatesMonthlyModel model, Date onDate, double usdRate, double eurRate) {
        model.setOnDate(onDate);
        model.setUsdRate(usdRate);
        model.setEurRate(eurRate);
        return model;
    }

    public static PayslipItemModel newInstancePayslipItemModel(PayslipItemModel model, PayslipItemModel.Category category, Date date, String name, int code, double debit, double credit) {
        model.setCategory(category);
        model.setDate(date);
        model.setName(name);
        model.setCode(code);
        model.setDebit(debit);
        model.setCredit(credit);
        return model;
    }

    public static PayslipBenefitModel newInstancePayslipBenefitModel(PayslipBenefitModel model, Date date, String name, double sum) {
        model.setDate(date);
        model.setName(name);
        model.setSum(sum);
        return model;
    }

    public static PayslipDeductionModel newInstancePayslipDeductionModel(PayslipDeductionModel model, Date date, String name, String deductionKind, double sum) {
        model.setDate(date);
        model.setName(name);
        model.setDeductionKind(deductionKind);
        model.setSum(sum);
        return model;
    }

    /// без списков
    public static PayslipModel newInstancePayslipModel(PayslipModel model, Date payDate, EmployeeModel employee,
                                                       String department, String position, double salary,
                                                       double hourRate, int dependents, boolean isFinal) {
        model.setPayDate(payDate);
        model.setEmployee(employee);
        model.setDepartment(department);
        model.setPosition(position);
        model.setSalary(salary);
        model.setHourRate(hourRate);
        model.setDependents(dependents);
        model.setFinal(isFinal);
        return model;
    }

    public static DayEventModel newInstanceDayEventModel(DayEventModel model, DayEventModel.EventType eventType, String description, double hours) {
        model.setEventType(eventType);
        model.setDescription(description);
        model.setHours(hours);
        return model;
    }

    public static DayEventModel newInstanceDayEventModel(DayEventModel model, DayEventModel.EventType eventType, String description, boolean isAllDay) {
        model.setEventType(eventType);
        model.setDescription(description);
        model.setAllDay(isAllDay);
        return model;
    }

    public static DayEventModel newInstanceDayEventModel(DayEventModel model, DayEventModel.EventType eventType, Date timeBegin, Date timeEnd, String description) {
        model.setEventType(eventType);
        model.setTimeBegin(timeBegin);
        model.setTimeEnd(timeEnd);
        model.setDescription(description);
        return model;
    }

    /// без списков
    public static WorkScheduleModel newInstanceWorkScheduleModel(WorkScheduleModel model, DayType dayType,
                                                                 EmployeeModel employee, Date date, String description, double hours) {
        model.setDayType(dayType);
        model.setEmployee(employee);
        model.setDate(date);
        model.setDescription(description);
        model.setHours(hours);
        return model;
    }

    /// без списков
    public static TimesheetModel newInstanceTimesheetModel(TimesheetModel model, DayType dayType, EmployeeModel employee,
                                                           Date date, String description, double hours) {
        model.setDayType(dayType);
        model.setEmployee(employee);
        model.setDate(date);
        model.setHours(hours);
        model.setDescription(description);
        return model;
    }


    /// без subModel
    // create client message
    public static GdMsgModel newInstanceGdMsgModel(GdMsgModel model, GdMsgModel.Subject subject, String comment, Date timestamp) {
        model.setSubject(subject);
        model.setMsg("");
        model.setComment(comment);
        model.setTimestamp(timestamp);

        model.setRequestStatus(GdMsgModel.RequestStatus.UNKNOWN);
        model.setIsUnread(false);
        model.setSender(GdMsgModel.Sender.CLIENT);
        return model;
    }

    /// без subModel
    // create server message
    public static GdMsgModel newInstanceGdMsgModel(GdMsgModel model, GdMsgModel.Subject subject, String msg, String comment, Date timestamp,
                                                   GdMsgModel.RequestStatus requestStatus) {
        model.setSubject(subject);
        model.setMsg(msg);
        model.setComment(comment);
        model.setTimestamp(timestamp);
        model.setRequestStatus(requestStatus);

        model.setIsUnread(true);
        model.setSender(GdMsgModel.Sender.SERVER);
        return model;
    }


    // create client message
    public static GdMsgModel newInstanceGdMsgAbsenceModel(GdMsgAbsenceModel subModel, GdMsgModel model, String comment, Date timestamp,
                                                          Date dateBegin, Date dateEnd, GdMsgAbsenceModel.Type type) {
        subModel.setDateBegin(dateBegin);
        subModel.setDateEnd(dateEnd);
        subModel.setType(type);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.ABSENCE, comment, timestamp);
        model.setGdMsgAbsenceModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create server message
    public static GdMsgModel newInstanceGdMsgAbsenceModel(GdMsgAbsenceModel subModel, GdMsgModel model, String msg, String comment, Date timestamp,
                                                          GdMsgModel.RequestStatus requestStatus, GdMsgModel requestGdMsg) {
        subModel.setRequestGdMsgModel(requestGdMsg);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.ABSENCE, msg, comment, timestamp, requestStatus);
        model.setGdMsgAbsenceModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }


    // create client message
    public static GdMsgModel newInstanceGdMsgIncomeModel(GdMsgIncomeModel subModel, GdMsgModel model, String comment, Date timestamp,
                                                         Date dateBegin, Date dateEnd) {
        subModel.setDateBegin(dateBegin);
        subModel.setDateEnd(dateEnd);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.INCOME, comment, timestamp);
        model.setGdMsgIncomeModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create server message
    public static GdMsgModel newInstanceGdMsgIncomeModel(GdMsgIncomeModel subModel, GdMsgModel model, String msg, String comment, Date timestamp,
                                                         GdMsgModel.RequestStatus requestStatus, GdMsgModel requestGdMsg) {
        subModel.setRequestGdMsgModel(requestGdMsg);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.INCOME, msg, comment, timestamp, requestStatus);
        model.setGdMsgIncomeModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create client message
    public static GdMsgModel newInstanceGdMsgVacationInfoModel(GdMsgVacationInfoModel subModel,
                                                               GdMsgModel model, String comment, Date timestamp) {
        newInstanceGdMsgModel(model, GdMsgModel.Subject.VACATION_INFO, comment, timestamp);
        model.setGdMsgVacationInfoModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create server message
    public static GdMsgModel newInstanceGdMsgVacationInfoModel(GdMsgVacationInfoModel subModel, GdMsgModel model,
                                                               String msg, String comment, Date timestamp, Date dateBegin, Date dateEnd,
                                                               GdMsgModel requestGdMsg) {
        subModel.setRequestGdMsgModel(requestGdMsg);
        subModel.setDateBegin(dateBegin);
        subModel.setDateEnd(dateEnd);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.VACATION_INFO, msg, comment, timestamp, GdMsgModel.RequestStatus.UNKNOWN);
        model.setGdMsgVacationInfoModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create client message
    public static GdMsgModel newInstanceGdMsgVacationModel(GdMsgVacationModel subModel, GdMsgModel model, String comment, Date timestamp,
                                                           Date dateBegin, Date dateEnd, GdMsgVacationModel.Type type) {
        subModel.setDateBegin(dateBegin);
        subModel.setDateEnd(dateEnd);
        subModel.setType(type);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.VACATION, comment, timestamp);
        model.setGdMsgVacationModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create server message
    public static GdMsgModel newInstanceGdMsgVacationModel(GdMsgVacationModel subModel, GdMsgModel model, String msg, String comment, Date timestamp,
                                                           GdMsgModel.RequestStatus requestStatus, GdMsgModel requestGdMsg) {
        subModel.setRequestGdMsgModel(requestGdMsg);

        newInstanceGdMsgModel(model, GdMsgModel.Subject.VACATION, msg, comment, timestamp, requestStatus);
        model.setGdMsgVacationModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    // create server message
    public static GdMsgModel newInstanceGdMsgInfoModel(GdMsgInfoModel subModel, GdMsgModel model, String msg, String comment, Date timestamp,
                                                       GdMsgModel.RequestStatus requestStatus) {
        newInstanceGdMsgModel(model, GdMsgModel.Subject.INFO, msg, comment, timestamp, requestStatus);
        model.setGdMsgInfoModel(subModel);
        model.setGdMsgString(GdMsgModel.toString(model));
        return model;
    }

    public static DeviceInfoModel newInstanceDeviceInfoModel(DeviceInfoModel model, String deviceId, String phoneNumber, String modelName) {
        model.setUid(DeviceInfoModel.CURRENT_DEVICE_UID);
        model.setDeviceId(deviceId);
        model.setPhoneNumber(phoneNumber);
        model.setDeviceModel(modelName);
        return model;
    }

    public static SyncMetaDataModel newInstanceSyncMetaDataModel(SyncMetaDataModel model) {
        model.setUid(0);
        model.setLastSyncDate(SyncMetaDataModel.DEFAULT_SYNC_DATE);
        model.setLastSyncMsgDate(SyncMetaDataModel.DEFAULT_SYNC_DATE);
        model.setLastSyncPostsDate(SyncMetaDataModel.DEFAULT_SYNC_DATE);
        model.setAuthKey("");
        model.setGcmRegistrationId("");
        model.setInviteCode(null);
        return model;
    }

    public static ContactModel newInstanceContactModel(ContactModel model, String firstname, String surname, String middlename, String workPosition) {
        model.setFirstname(firstname);
        model.setSurname(surname);
        model.setMiddlename(middlename);
        model.setWorkPosition(workPosition);

        return model;
    }
}
