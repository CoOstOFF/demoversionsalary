package com.gsbelarus.gedemin.salary.database;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;

import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class DatabaseMigration implements RealmMigration {

    public static final int DB_SCHEMA_VERSION = 21;
    //v1.0 build 39 - DB_SCHEMA_VERSION = 10
    //v1.1 build 43 - DB_SCHEMA_VERSION = 13
    //v1.2 build 44 - DB_SCHEMA_VERSION = 14
    //v1.3 build 47 - DB_SCHEMA_VERSION = 16
    //v1.4 build 48 - DB_SCHEMA_VERSION = 17
    //v1.5 build 51 - DB_SCHEMA_VERSION = 18
    //v1.5 build 52 - DB_SCHEMA_VERSION = 19
    //v1.5 build 55 - DB_SCHEMA_VERSION = 20
    //v1.5 build 58 - DB_SCHEMA_VERSION = 21

    private OnRecreateListener onRecreateListener;

    public DatabaseMigration(OnRecreateListener onRecreateListener) {
        this.onRecreateListener = onRecreateListener;
    }

    @Override
    public void migrate(DynamicRealm dynamicRealm, long oldVersion, long newVersion) {
        LogUtil.i("migrate from " + oldVersion + " to " + newVersion);

        RealmSchema schema = dynamicRealm.getSchema();

        if (oldVersion == 10) {
            schema.get("SyncMetaDataModel")
                    .removeField("needInvite")
                    .setNullable("inviteCode", true);

            oldVersion++;
        }
        if (oldVersion == 11) {
            RealmObjectSchema vacationInfo = schema.create("GdMsgVacationInfoModel")
                    .addField("uid", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("dateBegin", Date.class)
                    .addField("dateEnd", Date.class)
                    .addRealmObjectField("requestGdMsgModel", schema.get("GdMsgModel"));

            schema.get("GdMsgModel").addRealmObjectField("gdMsgVacationInfoModel", vacationInfo);

            oldVersion++;
        }
        if (oldVersion == 13) {
            schema.get("SyncMetaDataModel").addField("permits", boolean.class);

            oldVersion++;
        }
        if (oldVersion == 14) {
            schema.get("PayslipModel")
                    .addField("isFinal", boolean.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.setBoolean("isFinal", true);
                        }
                    });

            oldVersion++;
        }
        if (oldVersion == 15) {
            changeUidType(schema.get("PayslipModel")).removeField("employeeKey");
            changeUidType(schema.get("PayslipItemModel")).removeField("payslipKey");
            changeUidType(schema.get("PayslipBenefitModel")).removeField("payslipKey");
            changeUidType(schema.get("PayslipDeductionModel")).removeField("payslipKey");
            changeUidType(schema.get("DayEventModel")).removeField("workScheduleKey").removeField("timesheetKey");
            changeUidType(schema.get("EmployeeModel"));
            changeUidType(schema.get("ExRatesMonthlyModel"));
            changeUidType(schema.get("TimesheetModel"));
            changeUidType(schema.get("WorkScheduleModel"));

            oldVersion++;
        }
        if (oldVersion == 16) {
            schema.get("SyncMetaDataModel")
                    .addField("lostMode", boolean.class)
                    .addField("alias", String.class, FieldAttribute.REQUIRED)
                    .addField("lastSyncPostsDate", String.class, FieldAttribute.REQUIRED);

            oldVersion++;
        }
        if (oldVersion == 17) {
            schema.create("DepartmentModel")
                    .addField("uid", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String.class)
                    .addField("address", String.class)
                    .addField("openingTime", Date.class)
                    .addField("closingTime", Date.class);

            schema.create("ContactModel")
                    .addField("uid", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("firstname", String.class)
                    .addField("surname", String.class)
                    .addField("middlename", String.class)
                    .addField("workPosition", String.class);

            oldVersion++;
        }
        if (oldVersion == 18) {
            schema.get("PayslipModel")
                    .addField("endSaldo", double.class);

            oldVersion++;
        }
        if (oldVersion == 19) {
            onRecreateListener.setRecreate(true);

            oldVersion++;
        }
        if (oldVersion == 20) {
            schema.get("SyncMetaDataModel")
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            if (obj.getString("addressServer").equals("46.56.80.10:21512")) {
                                obj.setString("addressServer", "46.56.85.8:21512");
                            }
                        }
                    });
        }
    }

    private RealmObjectSchema changeUidType(final RealmObjectSchema objectSchema) {
        return objectSchema
                .addField("uidNew", int.class)
                .transform(new RealmObjectSchema.Function() {
                    int id = 0;

                    @Override
                    public void apply(DynamicRealmObject dynamicRealmObject) {
                        try {
                            dynamicRealmObject.setInt("uidNew", Integer.valueOf(dynamicRealmObject.getString("uid")));
                        } catch (NumberFormatException e) {
                            dynamicRealmObject.setInt("uidNew", id);
                        }
                        id++;
                    }
                })
                .removePrimaryKey()
                .removeIndex("uid")
                .removeField("uid")
                .renameField("uidNew", "uid")
                .addIndex("uid")
                .addPrimaryKey("uid");
    }

    public interface OnRecreateListener {
        void setRecreate(boolean flag);
    }
}
