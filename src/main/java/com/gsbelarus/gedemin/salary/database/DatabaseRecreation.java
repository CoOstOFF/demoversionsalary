package com.gsbelarus.gedemin.salary.database;

import android.content.Context;
import android.content.Intent;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DatabaseRecreation {

    private Context context;

    public DatabaseRecreation(Context context) {
        this.context = context;
    }

    public void recreate(RealmConfiguration oldConfig, RealmConfiguration newConfig) {
        switch (SettingsFragment.getPrefMode(context)) {
            case UNDEFINED_MODE:
                Realm.deleteRealm(oldConfig);
                break;
            case DEMO_MODE:
                Realm.deleteRealm(oldConfig);

                context.startService(SyncDataService.getIntentWithTask(
                        new Intent(context, SyncDataService.class),
                        SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_REQUEST)));
                break;
            case WORK_MODE:
                RealmHelper realmHelper = new RealmHelper(Realm.getInstance(oldConfig));
                EmployeeModel employee = realmHelper.getRealm().copyFromRealm(realmHelper.getLastEmployee(true));
                SyncMetaDataModel syncMetaData = realmHelper.getRealm().copyFromRealm(realmHelper.getSyncMetaData(true));
                syncMetaData = ModelFactory.newInstanceSyncMetaDataModel(syncMetaData);
                realmHelper.getRealm().close();

                Realm.deleteRealm(oldConfig);

                realmHelper = new RealmHelper(Realm.getInstance(newConfig));
                realmHelper.getDeviceInfo(true);
                realmHelper.save(employee, true);
                realmHelper.save(syncMetaData, true);
                realmHelper.getRealm().close();

                context.startService(SyncDataService.getIntentWithTask(
                        new Intent(context, SyncDataService.class),
                        SyncServiceTask.TypeOfTask.SERIES_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.AUTH_REQUEST)));
                context.startService(SyncDataService.getIntentWithTask(
                        new Intent(context, SyncDataService.class),
                        SyncServiceTask.TypeOfTask.SERIES_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));
                break;
        }
    }
}
