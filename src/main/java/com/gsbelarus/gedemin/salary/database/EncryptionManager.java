package com.gsbelarus.gedemin.salary.database;


import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import io.realm.Realm;
import io.realm.RealmConfiguration;

//TODO test Android M
public class EncryptionManager {

    private static final String KEYSTORE_PRIVATEKEY_ALIAS = "keystores_privatekey_alias"; //TODO name
    private static final String KEYSTORE_PROVIDER_NAME = "AndroidKeyStore";

    private Context context;

    public EncryptionManager(Context context) {
        this.context = context;
    }

    // application's 256-bit AES key
    public byte[] getKey(RealmConfiguration oldConfig) throws GeneralSecurityException, IOException, IOException {
        // As of 4.3, Android has a secure per-application key store, but it can't store symmetric keys
        // As a result, we use it to store a public/private key-pair which is used to encrypt the
        // symmetric key which is stored in a file in the application context
        byte[] keyData;
        try {
            File file = new File(context.getFilesDir(), Realm.DEFAULT_REALM_NAME + ".key");
            keyData = new byte[256];
            FileInputStream stream = new FileInputStream(file);
            try {
                int read = stream.read(keyData);
                if (read != keyData.length) {
                    keyData = null;
                }
            } finally {
                stream.close();
            }
        } catch (IOException e) {
            // generate a new key
            keyData = null;
        }

        KeyPair keyPair = getKeyPair();
        final Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");

        if (keyData != null) { // We have an existing secret key -> decrypt and return it
            cipher.init(Cipher.UNWRAP_MODE, keyPair.getPrivate());

            return cipher.unwrap(keyData, "AES", Cipher.SECRET_KEY).getEncoded();
        } else { // generate a new secret key

            keyData = new byte[64];
            new SecureRandom().nextBytes(keyData);

            cipher.init(Cipher.WRAP_MODE, keyPair.getPublic());

            // save to the file
            File file = new File(context.getFilesDir(), Realm.DEFAULT_REALM_NAME + ".key");
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(cipher.wrap(new SecretKeySpec(keyData, "AES")));
            } finally {
                stream.close();
            }

            ////new DatabaseRecreation(context).recreate(oldConfig, Realm.getInstance(context).getConfiguration()); //TODO new conf

            return keyData;
        }
    }

    private KeyPair getKeyPair() throws GeneralSecurityException, IOException {
        final KeyStore keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER_NAME);
        keyStore.load(null);
        if (!keyStore.containsAlias(KEYSTORE_PRIVATEKEY_ALIAS)) {
            generateKeyPair(KEYSTORE_PRIVATEKEY_ALIAS);
        }
        final KeyStore.PrivateKeyEntry entry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEYSTORE_PRIVATEKEY_ALIAS, null);

        return new KeyPair(entry.getCertificate().getPublicKey(), entry.getPrivateKey());
    }

    //TODO 17 --> 14
    private void generateKeyPair(String alias) throws GeneralSecurityException {
        final Calendar start = new GregorianCalendar();
        final Calendar end = new GregorianCalendar();
        end.add(Calendar.YEAR, 100);
        final KeyPairGeneratorSpec spec;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {  //TODO tmp
            spec = new KeyPairGeneratorSpec.Builder(context)
                    .setAlias(alias)
                    .setSubject(new X500Principal("CN=" + alias))
                    .setSerialNumber(BigInteger.ONE)
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                    .build();


//        final KeyGenParameterSpec keySpec = new KeyGenParameterSpec.Builder(alias, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT) //TODO PURPOSE_??
//                .setKeySize(256) ////I know realm uses 64 bit but the lowest is 128. This doesn't make a difference anyway. Still returns null at 128
////                .setAlgorithmParameterSpec()
////                .setCertificateSubject(new X500Principal("CN=" + alias))
////                .setCertificateSerialNumber(BigInteger.ONE)
////                .setKeyValidityStart(start.getTime())
////                .setKeyValidityEnd(end.getTime())
//                .build();
////
////        KeyProperties.KEY_ALGORITHM_RSA
//
//        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM, PROVIDER_NAME);
//        keyGenerator.init(keySpec);
//        keyGenerator.generateKey();


        final KeyPairGenerator gen = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEYSTORE_PROVIDER_NAME);
        gen.initialize(spec);
        gen.generateKeyPair();

        }
    }

//    public boolean writeEncryptedCopy() throws Exception {
//        File destination = new File(context.getFilesDir(), ENCRYPTED_REALM_FILE_NAME);
//        byte[] key = getKey();
//        new Random(42).nextBytes(key);
//        try {
//            realm.writeEncryptedCopyTo(destination, key);
//        } catch(Exception e) {
//            return false;
//        }
//        Realm encryptedRealm = Realm.getInstance(context, ENCRYPTED_REALM_FILE_NAME, key);
//        encryptedRealm.close();
//
//        return true;
//    }
//    private KeyPairGenerator keyPairGenerator;
//    private KeyPair keyPair;

//    public KeyPair getKeyPair() throws NoSuchAlgorithmException {
//        if (keyPairGenerator == null) {
//            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
//            keyPairGenerator.initialize(2048);
//        }
//        if (keyPair == null) {
//            keyPair = keyPairGenerator.generateKeyPair();
//        }
//
//        return keyPair;
//    }

//    public String decrypt(byte[] message) throws GeneralSecurityException, UnsupportedEncodingException {
//        byte[] encodedPrivateKey = getKeyPair().getPrivate().getEncoded();
//
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(encodedPrivateKey);
//        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//        PrivateKey privateKey = keyFactory.generatePrivate(spec);
//        cipher.init(Cipher.DECRYPT_MODE, privateKey);
//        byte[] decrypted = cipher.doFinal(message);
//
//        return new String(decrypted, "UTF8");
//    }
//
//    public byte[] encrypt(String message) throws GeneralSecurityException, UnsupportedEncodingException {
//        byte[] encodedPublicKey = getKeyPair().getPublic().getEncoded();
//
//        X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        PublicKey publicKey = keyFactory.generatePublic(spec);
//        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//
//        return cipher.doFinal(message.getBytes("UTF8"));
//    }
}
