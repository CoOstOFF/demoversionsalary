package com.gsbelarus.gedemin.salary.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncGdMsgStateModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.util.SyncNotificationManager;
import com.gsbelarus.gedemin.salary.util.WebServiceManager;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

public class SendDataService extends IntentService implements ExclusionStrategy {

    public static final String SEND_GD_MSG_STATES_ACTION    = "com.gsbelarus.gedemin.salary.service.send_gd_msg_states_action";

    public static final String SEND_GD_MSG_ACTION           = "com.gsbelarus.gedemin.salary.service.send_gd_msg_action";
    public static final String EXTRA_KEY_GD_MSG             = "gd_msg";
    public static final String SEND_BOARD_ITEM_ACTION       = "com.gsbelarus.gedemin.salary.service.send_board_item_action";
    public static final String EXTRA_KEY_BOARD_ITEM         = "board_item";

    public static final String SEND_GD_MSG_BROADCAST_ACTION     = "com.gsbelarus.gedemin.salary.service.send_gd_msg_broadcast_action";
    public static final String EXTRA_KEY_GD_MSG_WAS_SENT        = "gd_msg_was_sent";
    public static final String SEND_BOARD_ITEM_BROADCAST_ACTION = "com.gsbelarus.gedemin.salary.service.send_board_item_broadcast_action";
    public static final String EXTRA_KEY_BOARD_ITEM_WAS_SENT    = "board_item_was_sent";


    private SyncNotificationManager syncNotificationManager;
    private Context context;

    interface CallableFunc<TArg1, TArg2> {
        void onResponse(TArg1 val1, TArg2 val2) throws Exception;
    }

    public SendDataService() {
        super(SendDataService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        context = getApplicationContext();
        syncNotificationManager = new SyncNotificationManager(context);

        startForeground(SyncNotificationManager.SEND_NOTIFICATION_ID, syncNotificationManager.getSendNotification(context.getString(R.string.notification_sending)));

        RealmHelper realmHelper = new RealmHelper(Realm.getDefaultInstance());
        SyncMetaDataModel syncMetaDataModel = realmHelper.getSyncMetaData(true);

        if (intent.getAction().equalsIgnoreCase(SEND_GD_MSG_ACTION)) {

            final GdMsgModel gdMsgModel = (GdMsgModel) intent.getExtras().getSerializable(EXTRA_KEY_GD_MSG);
            assert gdMsgModel != null;

            if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.WORK_MODE) {
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat(ServerURL.SERVER_DATE_TIME_FORMAT);
                Gson gson = builder.excludeFieldsWithoutExposeAnnotation().setExclusionStrategies(this).create();
                String jsonMsg = gson.toJson(gdMsgModel);

                Map<String, String> requestParams = new HashMap<>();
                requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SEND_MSG);
                requestParams.put(ServerURL.REQUEST_PARAM_ALIAS, syncMetaDataModel.getAlias());
                requestParams.put(ServerURL.REQUEST_PARAM_IP_ADDRESS, WebServiceManager.getIpAddress());
                requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                if (!syncMetaDataModel.getGcmRegistrationId().isEmpty())
                    requestParams.put(ServerURL.REQUEST_PARAM_GCM_REG_ID, syncMetaDataModel.getGcmRegistrationId());
                requestParams.put(ServerURL.REQUEST_PARAM_GD_MSG, jsonMsg);
                requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                Map<String, String> headers = new HashMap<>();
                ///headers.put("Connection", "close");  // disable keep-alive
                headers.put("Content-Type", "application/json; charset=utf-8");

                callWebService(
                        new CallableFunc<RealmHelper, WebServiceManager>() {

                            @Override
                            public void onResponse(RealmHelper realmHelper, WebServiceManager webServiceManager) throws Exception {
                                if (webServiceManager.hasResponseStatusOK()) {
                                    HashMap<String, String> response = webServiceManager.parseResponse();
                                    if (response.containsKey(ServerURL.RESPONSE_PARAM_ERROR_CODE)) {
                                        Exception exception = new Exception("SendDataService sending GD_MSG: " +
                                                ResponseErrorCode.getErrorByCode(response.get(ServerURL.RESPONSE_PARAM_ERROR_CODE)));
                                        LogUtil.e(exception.getMessage());
                                        //Crashlytics.getInstance().core.logException(new Exception("SendDataService sending GD_MSG: " + response.get(SyncDataService.RESPONSE_PARAM_ERROR_MSG)));
                                        throw exception;
                                    }

                                    if (response.containsKey(ServerURL.RESPONSE_PARAM_GDMSG_ID) && response.containsKey(ServerURL.RESPONSE_PARAM_GDMSG_TIMESTAMP)) {
                                        gdMsgModel.setUid(Integer.valueOf(response.get(ServerURL.RESPONSE_PARAM_GDMSG_ID)));
                                        gdMsgModel.setTimestamp(new SimpleDateFormat(ServerURL.SERVER_DATE_TIME_FORMAT, Locale.getDefault()).parse(response.get(ServerURL.RESPONSE_PARAM_GDMSG_TIMESTAMP)));
                                    } else {
                                        Exception exception = new Exception("gdmsg_id или gdmsg_timestamp не найдены");
                                        LogUtil.e(exception.getMessage());
                                        //Crashlytics.getInstance().core.logException(new Exception("gdmsg_id или gdmsg_timestamp не найдены"));
                                        throw exception;
                                    }

                                    realmHelper.save(gdMsgModel, true);
                                    Intent broadcastIntent = new Intent(SEND_GD_MSG_BROADCAST_ACTION);
                                    broadcastIntent.putExtra(EXTRA_KEY_GD_MSG_WAS_SENT, true);
                                    sendBroadcast(broadcastIntent);
                                }
                            }
                        },
                        WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams),
                        headers,
                        realmHelper
                );
            } else {
                if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.DEMO_MODE) {
                    realmHelper.save(gdMsgModel, true);
                    Intent broadcastIntent = new Intent(SEND_GD_MSG_BROADCAST_ACTION);
                    broadcastIntent.putExtra(EXTRA_KEY_GD_MSG_WAS_SENT, true);
                    sendBroadcast(broadcastIntent);
                }
            }
        } else if (intent.getAction().equalsIgnoreCase(SEND_GD_MSG_STATES_ACTION)) { ///TODO

            if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.WORK_MODE) {
                RealmResults<SyncGdMsgStateModel> states = realmHelper.getAll(SyncGdMsgStateModel.class);

                if (states.size() > 0) {
                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat(ServerURL.SERVER_DATE_TIME_FORMAT);
                    Gson gson = builder.excludeFieldsWithoutExposeAnnotation().setExclusionStrategies(this).create();
                    String jsonData = "";

                    for (final SyncGdMsgStateModel state : states) {
                        jsonData += gson.toJson(state);
                    }

                    Map<String, String> requestParams = new HashMap<>();
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SEND_GD_MSG_STATES);
                    requestParams.put(ServerURL.REQUEST_PARAM_ALIAS, syncMetaDataModel.getAlias());
                    requestParams.put(ServerURL.REQUEST_PARAM_IP_ADDRESS, WebServiceManager.getIpAddress());
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    if (!syncMetaDataModel.getGcmRegistrationId().isEmpty())
                        requestParams.put(ServerURL.REQUEST_PARAM_GCM_REG_ID, syncMetaDataModel.getGcmRegistrationId());
                    requestParams.put(ServerURL.REQUEST_PARAM_GD_MSG_STATE_ARRAY, Uri.encode(jsonData, "utf-8"));
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                    Map<String, String> headers = new HashMap<>();
                    ///headers.put("Connection", "close");  // disable keep-alive
                    headers.put("Content-Type", "application/json; charset=utf-8");

                    callWebService(
                            new CallableFunc<RealmHelper, WebServiceManager>() {
                                @Override
                                public void onResponse(RealmHelper realmHelper, WebServiceManager webServiceManager) throws Exception {
                                    if (webServiceManager.hasResponseStatusOK()) {
                                        HashMap<String, String> response = webServiceManager.parseResponse();
                                        if (response.containsKey(ServerURL.RESPONSE_PARAM_ERROR_CODE)) {
                                            Exception exception = new Exception("SendDataService sending GD_MSG_STATES: " +
                                                    ResponseErrorCode.getErrorByCode(response.get(ServerURL.RESPONSE_PARAM_ERROR_CODE)));
                                            LogUtil.e(exception.getMessage());
                                            //Crashlytics.getInstance().core.logException(exception);
                                            throw exception;
                                        } else {
                                            realmHelper.deleteAll(SyncGdMsgStateModel.class, true);
                                        }
                                    }
                                }
                            },
                            WebServiceManager.buildRequestUrl("http", syncMetaDataModel.getAddressServer(), requestParams),
                            headers,
                            realmHelper
                    );
                }
            }
        } else if(intent.getAction().equalsIgnoreCase(SEND_BOARD_ITEM_ACTION)) {

            if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.WORK_MODE) {
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat(ServerURL.SERVER_DATE_TIME_FORMAT);
                Gson gson = builder.excludeFieldsWithoutExposeAnnotation().setExclusionStrategies(this).create();

                Map<String, String> requestParams = new HashMap<>();
                requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SEND_BOARD_ITEM);
                requestParams.put(ServerURL.REQUEST_PARAM_ALIAS, syncMetaDataModel.getAlias());
                requestParams.put(ServerURL.REQUEST_PARAM_IP_ADDRESS, WebServiceManager.getIpAddress());
                requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                if (!syncMetaDataModel.getGcmRegistrationId().isEmpty())
                    requestParams.put(ServerURL.REQUEST_PARAM_GCM_REG_ID, syncMetaDataModel.getGcmRegistrationId());
                requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                Map<String, String> headers = new HashMap<>();
                ///headers.put("Connection", "close");  // disable keep-alive
                headers.put("Content-Type", "application/json; charset=utf-8");

                callWebService(
                        new CallableFunc<RealmHelper, WebServiceManager>() {

                            @Override
                            public void onResponse(RealmHelper realmHelper, WebServiceManager webServiceManager) throws Exception {
                                if (webServiceManager.hasResponseStatusOK()) {
                                    HashMap<String, String> response = webServiceManager.parseResponse();
                                    if (response.containsKey(ServerURL.RESPONSE_PARAM_ERROR_CODE)) {
                                        Exception exception = new Exception("SendDataService sending BOARD_ITEM: " +
                                                ResponseErrorCode.getErrorByCode(response.get(ServerURL.RESPONSE_PARAM_ERROR_CODE)));
                                        LogUtil.e(exception.getMessage());
                                        throw exception;
                                    }

                                    Intent broadcastIntent = new Intent(SEND_BOARD_ITEM_BROADCAST_ACTION);
                                    broadcastIntent.putExtra(EXTRA_KEY_BOARD_ITEM_WAS_SENT, true);
                                    sendBroadcast(broadcastIntent);
                                }
                            }
                        },
                        WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams),
                        headers,
                        realmHelper
                );
            } else {
                if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.DEMO_MODE) {
                    Intent broadcastIntent = new Intent(SEND_BOARD_ITEM_BROADCAST_ACTION);
                    broadcastIntent.putExtra(EXTRA_KEY_BOARD_ITEM_WAS_SENT, true);
                    sendBroadcast(broadcastIntent);
                }
            }
        }
        stopForeground(true);
    }

    public void callWebService(CallableFunc<RealmHelper, WebServiceManager> c, String uri, Map<String, String> headers, RealmHelper realmHelper) {
        LogUtil.d(uri);
        WebServiceManager webServiceManager = new WebServiceManager();
        try {
            webServiceManager.openConnection(uri);

            webServiceManager.sendGetRequest(headers); //TODO POST

            c.onResponse(realmHelper, webServiceManager);

        } catch (Exception e) {
            LogUtil.e(e.getMessage());
            //Crashlytics.getInstance().core.logException(e);
            stopForeground(true);
            showFinishError();
        } finally {
            webServiceManager.closeConnection();
            realmHelper.getRealm().close();
        }
    }

    public void showFinishError() {
        syncNotificationManager.showErrorNotification(context.getString(R.string.notification_sending_error));
    }

    /**
     * @param f the field object that is under test
     * @return true if the field should be ignored; otherwise false
     */
    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return false;
    }

    /**
     * @param clazz the class object that is under test
     * @return true if the class should be ignored; otherwise false
     */
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
