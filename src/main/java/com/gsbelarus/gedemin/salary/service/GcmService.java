package com.gsbelarus.gedemin.salary.service;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.sync.protocol.etc.Parser;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class GcmService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String msg = data.getString("message");
        LogUtil.d(msg);

        if (SettingsFragment.getPrefMode(getApplicationContext()) != SettingsFragment.PrefMode.WORK_MODE) {
            LogUtil.d("current mode", SettingsFragment.getPrefMode(getApplicationContext()));
            return;
        }

        if (msg != null) {
            try {
                HashMap<String, String> headerMap = new Parser().parsingHeader(Arrays.asList(
                        msg.split("\\n")));

                if (headerMap.containsKey(ServerURL.GCM_RESPONSE_PARAM_ACTION)) {
                    String action = headerMap.get(ServerURL.GCM_RESPONSE_PARAM_ACTION);

                    switch (action) {
                        case ServerURL.GCM_RESPONSE_ACTION_REMOVE_PERMITS:
                            RealmHelper.executeTransactionWithClosing(new RealmHelper.RealmHelperExecutor() {
                                @Override
                                public void execute(RealmHelper realmHelper) {
                                    realmHelper.getSyncMetaData(false).setPermits(false);
                                }
                            });
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_ADD_PERMITS:
                            RealmHelper.executeTransactionWithClosing(new RealmHelper.RealmHelperExecutor() {
                                @Override
                                public void execute(RealmHelper realmHelper) {
                                    realmHelper.getSyncMetaData(false).setPermits(true);
                                }
                            });
                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST),
                                    true));
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_CLEAN_DATA:
                            ResponseErrorCode responseErrorCode = ResponseErrorCode.getErrorByCode(headerMap.get(ServerURL.RESPONSE_PARAM_ERROR_CODE));

                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.CLEAN_DATA),
                                    responseErrorCode.getMessage()));
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_LOST_CLEAN_DATA:
                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.LOST_CLEAN_DATA)));
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_SYNC_MSGS:
                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.MSG_REQUEST),
                                    true));
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_SYNC_BOARD_ITEMS:
                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.BOARD_ITEM_REQUEST),
                                    true));
                            break;

                        case ServerURL.GCM_RESPONSE_ACTION_SYNC:
                            getApplicationContext().startService(SyncServiceModel.getIntentWithTask(
                                    new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SERIES_SYNC,
                                    new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_REQUEST),
                                    true));
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
