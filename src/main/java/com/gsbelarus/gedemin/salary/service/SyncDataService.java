package com.gsbelarus.gedemin.salary.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncNotificationAdapter;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncProperty;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.exception.CustomSyncException;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.activity.Main;
import com.gsbelarus.gedemin.salary.config.ResponseErrorCode;
import com.gsbelarus.gedemin.salary.config.ServerURL;
import com.gsbelarus.gedemin.salary.database.ModelFactory;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncGdMsgStateModelSerializer;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.ContactModel;
import com.gsbelarus.gedemin.salary.entity.model.DeviceInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgInfoModel;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.entity.model.PayslipModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncGdMsgStateModel;
import com.gsbelarus.gedemin.salary.entity.model.SyncMetaDataModel;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.GdMsgsListFragment;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipFragment;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.util.CurrencyHelper;
import com.gsbelarus.gedemin.salary.util.DateFormatHelper;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.RealmSyncDataHandler;
import com.gsbelarus.gedemin.salary.util.SyncDataHandler;
import com.gsbelarus.gedemin.salary.util.SyncNotificationManager;
import com.gsbelarus.gedemin.salary.util.WebServiceManager;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import me.leolin.shortcutbadger.ShortcutBadger;

public class SyncDataService extends SyncServiceModel<SyncRequestType> {

    private static final String[] SERVER_DATE_FORMATS = new String[]{ServerURL.SERVER_DATE_FORMAT, ServerURL.SERVER_DATE_TIME_FORMAT};

    private RealmHelper realmHelperSyncThread;
    private RealmHelper realmHelperUIThread;
    private Gson gsonParser;
    private CurrencyHelper currencyHelper;

    private Map<Integer, String> inboxMsgNotificationTexts;
    private String newPayslipAvailableText;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        currencyHelper = CurrencyHelper.getInstance(context);
        realmHelperUIThread = new RealmHelper(Realm.getDefaultInstance());

        inboxMsgNotificationTexts = new HashMap<>();

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                for (String format : SERVER_DATE_FORMATS) {
                    try {
                        return new SimpleDateFormat(format, Locale.getDefault()).parse(json.getAsString());
                    } catch (ParseException ignore) {
                    }
                }
                throw new JsonParseException("Unparseable date: \"" + json.getAsString()
                        + "\". Supported formats: " + Arrays.toString(SERVER_DATE_FORMATS));
            }
        });
        gsonParser = builder.excludeFieldsWithoutExposeAnnotation().create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realmHelperUIThread.getRealm().close();
    }

    /**
     * Вызывается если пришел кастомный Task
     *
     * @param syncServiceTask содержит кастомный Task
     * @return если true - выполнение синхронизации продолжится с полученым Task-ом
     * если false - выполнение синхронизации с полученным Task-ом прекратится
     */
    @Override
    protected boolean onStartSubTask(SyncServiceTask<SyncRequestType> syncServiceTask) {
        boolean continueTask = true;
        if (isWorkMode()) {
            switch (syncServiceTask.getSubTask().getTypeOfRequest()) {
                case CLEAN_DATA:
                    realmHelperSyncThread.clearDB(true);
                    SettingsFragment.putPrefMode(context, SettingsFragment.PrefMode.UNDEFINED_MODE);
                    break;
                case LOST_CLEAN_DATA:
                    realmHelperUIThread.clearDB(true);
                    SettingsFragment.putPrefMode(context, SettingsFragment.PrefMode.UNDEFINED_MODE);

                    RealmHelper.executeWithClosing(new RealmHelper.RealmHelperExecutor() {
                        @Override
                        public void execute(RealmHelper realmHelper) {
                            GdMsgInfoModel gdMsgInfoModel = new GdMsgInfoModel();
                            gdMsgInfoModel.setUid(realmHelper.generateUID(GdMsgInfoModel.class));
                            GdMsgModel gdMsgModel = new GdMsgModel();
                            gdMsgModel.setUid(realmHelper.generateUID(GdMsgModel.class));
                            GdMsgModel gdMsg = ModelFactory.newInstanceGdMsgInfoModel(
                                    gdMsgInfoModel,
                                    gdMsgModel,
                                    getString(R.string.gdmsg_lock_device_subject),
                                    getString(R.string.gdmsg_lock_device),
                                    new Date(),
                                    GdMsgModel.RequestStatus.UNKNOWN);

                            Map<Integer, String> notifyTexts = new HashMap<>();
                            notifyTexts.put(gdMsg.getUid(), gdMsg.getSubjectName() + ". ");
                            new SyncNotificationManager(context).showNewMsgNotification(notifyTexts, Main.class, GdMsgsListFragment.class);

                            realmHelper.save(gdMsg, true);
                        }
                    });

                    break;
                case DATA_MSG_BOARDITEM_REQUEST:
                    startTask(new SyncServiceTask<SyncRequestType>(SyncServiceTask.TypeOfTask.SERIES_SYNC)
                            .setSubTask(new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_REQUEST)));

                    startTask(new SyncServiceTask<SyncRequestType>(SyncServiceTask.TypeOfTask.SERIES_SYNC)
                            .setSubTask(new SyncRequestType(SyncRequestType.TypeOfRequest.MSG_REQUEST)));

                    startTask(new SyncServiceTask<SyncRequestType>(SyncServiceTask.TypeOfTask.SERIES_SYNC)
                            .setSubTask(new SyncRequestType(SyncRequestType.TypeOfRequest.BOARD_ITEM_REQUEST)));

                    continueTask = false;
                    break;
            }
        }
        return continueTask;
    }

    /**
     * Выполняется во время подготовки к созданию нового потока загрузки, имеет доступ к UI потоку
     */
    @Override
    public void onPreExecute() {
        super.onPreExecute();
        currencyHelper.onStartUpdateDB();
    }

    /**
     * Открыть транзакцию, выполняется в потоке загрузки
     */
    @Override
    public void onBeginTransaction() {
        LogUtil.d();
        realmHelperSyncThread = new RealmHelper(Realm.getDefaultInstance());
        realmHelperSyncThread.getRealm().beginTransaction();
    }

    /**
     * Вызывается после получения ответа, в потоке загрузки.
     * Для вывода ошибки необходимо сгененировать исключение CustomSyncException("локализованное название ошибки")
     *
     * @param headerMap массив праметров из заголовка.
     * @throws com.gsbelarus.gedemin.lib.sync.protocol.exception.CustomSyncException прерывает загрузку данных, отображает диолог об ошибке
     */
    @Override
    public void onHeaderLoaded(HashMap<String, String> headerMap) throws CustomSyncException {
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            LogUtil.d(key, value);
        }

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_SERVER_VERSION) &&
                !isCompatibleVersions(headerMap.get(ServerURL.RESPONSE_PARAM_SERVER_VERSION))) {
            throw new CustomSyncException(getString(R.string.incompatible_server_version));
        }

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_LATEST_CLIENT_VERSION) &&
                isNeedShowUpdateNotification(headerMap.get(ServerURL.RESPONSE_PARAM_LATEST_CLIENT_VERSION)))
            new SyncNotificationManager(context).showUpdateAvailableNotification(getString(R.string.update_available_content_text));

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_ERROR_CODE)) {
            ResponseErrorCode responseErrorCode = ResponseErrorCode.getErrorByCode(headerMap.get(ServerURL.RESPONSE_PARAM_ERROR_CODE));

            String action = headerMap.get(ServerURL.RESPONSE_PARAM_ACTION);
            if (action != null && action.equals(ServerURL.RESPONSE_ACTION_CLEAN_DATA)) {
                clearSeriesTasks();
                startTask(new SyncServiceTask<>(SyncServiceTask.TypeOfTask.SERIES_SYNC,
                        new SyncRequestType(SyncRequestType.TypeOfRequest.CLEAN_DATA),
                        responseErrorCode.getMessage()));
            } else if (action != null && action.equals(ServerURL.RESPONSE_ACTION_CLEAN_DATA)) {
                clearSeriesTasks();
                startTask(new SyncServiceTask<>(SyncServiceTask.TypeOfTask.SERIES_SYNC,
                        new SyncRequestType(SyncRequestType.TypeOfRequest.LOST_CLEAN_DATA),
                        responseErrorCode.getMessage()));
            } else {
                throw new CustomSyncException(responseErrorCode.getMessage());
            }
        }

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_AUTH_KEY))
            realmHelperSyncThread.getSyncMetaData(false).setAuthKey(headerMap.get(ServerURL.RESPONSE_PARAM_AUTH_KEY));

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_LAST_SYNC_DATE))
            realmHelperSyncThread.getSyncMetaData(false).setLastSyncDate(headerMap.get(ServerURL.RESPONSE_PARAM_LAST_SYNC_DATE));

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_LAST_SYNC_MSG_DATE))
            realmHelperSyncThread.getSyncMetaData(false).setLastSyncMsgDate(headerMap.get(ServerURL.RESPONSE_PARAM_LAST_SYNC_MSG_DATE));

        if (headerMap.containsKey(ServerURL.RESPONSE_PARAM_LAST_SYNC_BOARD_ITEM_DATE))
            realmHelperSyncThread.getSyncMetaData(false).setLastSyncPostsDate(headerMap.get(ServerURL.RESPONSE_PARAM_LAST_SYNC_BOARD_ITEM_DATE));

        if (getCurrentTask().getSubTask().getTypeOfRequest() == SyncRequestType.TypeOfRequest.SEND_GD_MSG_STATES) {
            List<SyncGdMsgStateModel> syncGdMsgStateModels = realmHelperSyncThread.getAll(SyncGdMsgStateModel.class);
            realmHelperSyncThread.deleteObjectList(syncGdMsgStateModels, false);
        }
    }

    /**
     * Вызывается после получения ответа, в потоке загрузки.
     * Для вывода ошибки необходимо сгененировать исключение CustomSyncException("локализованное название ошибки")
     *
     * @param lines тело блока в виде строк. Следует распарсить и вставить в БД.
     * @throws com.gsbelarus.gedemin.lib.sync.protocol.exception.CustomSyncException прерывает загрузку данных, отображает диолог об ошибке
     */
    @Override
    public void onBlockLoaded(List<String> lines) throws CustomSyncException {
        LogUtil.d("lines count = " + lines.size());

        final int[] categoryColors = getResources().getIntArray(R.array.dividing_lines);

        final String packageModel = "com.gsbelarus.gedemin.salary.entity.model";
        SyncDataHandler dataHandler = new RealmSyncDataHandler(gsonParser, packageModel, realmHelperSyncThread.getRealm())
                .subscribe(PayslipModel.class.getSimpleName(), new SyncDataHandler.HandlingListener() {
                    @Override
                    public void onSave(Object object) {
                        PayslipModel payslip = (PayslipModel) object;
                        if (payslip.isFinal()) {
                            Calendar date = Calendar.getInstance();
                            date.setTime(payslip.getPayDate());
                            newPayslipAvailableText = getString(R.string.notification_new_payslip, DateFormatHelper.getMonthString(context, date));
                        }
                    }
                })
                .subscribe(GdMsgModel.class.getSimpleName(), new SyncDataHandler.HandlingListener() {
                    @Override
                    public void onSave(Object object) {
                        GdMsgModel gdMsg = (GdMsgModel) object;
                        if (gdMsg.isUnread() && gdMsg.getSender() == GdMsgModel.Sender.SERVER) {
                            inboxMsgNotificationTexts.put(gdMsg.getUid(), gdMsg.getSubjectName() + ". ");
                        }
                        gdMsg.setGdMsgString(gdMsg.toString());
                    }
                })
                .subscribe(SyncGdMsgStateModel.class.getSimpleName(), new SyncDataHandler.HandlingListener() {
                    @Override
                    public void onSave(Object object) {
                        SyncGdMsgStateModel stateModel = (SyncGdMsgStateModel) object;
                        // удаляем нотификацию о сообщении, для которого пришел статус
                        if (inboxMsgNotificationTexts.containsKey(stateModel.getGdMsgKey()))
                            inboxMsgNotificationTexts.remove(stateModel.getGdMsgKey());

                        realmHelperSyncThread.applyGdMsgState(stateModel, false);
                        //TODO делать прочитанным на сервере
                    }
                })
                .subscribe(EmployeeModel.class.getSimpleName(), new SyncDataHandler.HandlingListener() {
                    @Override
                    public void onSave(Object object) {
                        EmployeeModel employeeModel = (EmployeeModel) object;
                        ContactModel tmpContactModel = new ContactModel();
                        tmpContactModel.setUid(employeeModel.getUid());
                        ContactModel contactModel = ModelFactory.newInstanceContactModel(
                                tmpContactModel, employeeModel.getFirstname(), employeeModel.getSurname(), employeeModel.getMiddlename(), "");
                        realmHelperSyncThread.getRealm().copyToRealmOrUpdate(contactModel);
                    }
                });

        try {
            for (String line : lines) {
                LogUtil.d(line);
                dataHandler.dataHandle(line);
            }
        } catch (Exception e) {
            if (e instanceof ClassNotFoundException || e instanceof NoSuchMethodException) {
                LogUtil.e(e.getMessage());
            } else {
                throw new CustomSyncException(e.getMessage() == null ? e.getCause().getMessage() : e.getMessage());
            }
        }
    }

    /**
     * Закрыть транзакцию успешно, выполняется в потоке загрузки
     */
    @Override
    public void onEndTransaction() {
        LogUtil.d();

        if (getCurrentTask().getSubTask() != null) {
            switch (getCurrentTask().getSubTask().getTypeOfRequest()) {
                case DATA_REQUEST:
                    currencyHelper.updatesRatesToBackend(realmHelperSyncThread, false);
                    break;
            }
        }

        realmHelperSyncThread.getRealm().commitTransaction();
        realmHelperSyncThread.getRealm().close();
        realmHelperSyncThread = null;
    }

    /**
     * Закрыть транзакцию с откатом изменений, выполняется в потоке загрузки
     */
    @Override
    public void onCancelTransaction() {
        LogUtil.d();
        realmHelperSyncThread.getRealm().cancelTransaction();
        realmHelperSyncThread.getRealm().close();
        realmHelperSyncThread = null;
    }

    /**
     * Выполняется после загрузки, имеет доступ к UI потоку
     *
     * @param status статус с каким завершилась синхронизация
     *               (NO_INTERNET_CONNECTION, INVALID_RESPONSE, EMPTY_RESPONSE, TIMEOUT, NOT_REQUIRED,
     *               ELSE_FAILED, SUCCESSFUL, STOP_SYNC, CUSTOM_SYNC_FAILED)
     */
    @Override
    public void onPostExecute(final SyncStatus status) {
        currencyHelper.onFinishUpdateDB();

        if (status.getTypeOfStatus() == SyncStatus.TypeOfStatus.SUCCESSFUL) {

            switch (getCurrentTask().getSubTask().getTypeOfRequest()) {
                case MSG_REQUEST:
                    ShortcutBadger.with(getApplicationContext()).count(realmHelperUIThread.getUnreadGdMsgs().size());

                    break;
                case AUTH_REQUEST:
                    new GCMHelper(context).deleteRegistration();

                    break;
            }

            new GCMHelper(context).checkRegistration(realmHelperUIThread);

            if (!inboxMsgNotificationTexts.isEmpty()) {
                new SyncNotificationManager(context).showNewMsgNotification(inboxMsgNotificationTexts, Main.class, GdMsgsListFragment.class);
                inboxMsgNotificationTexts.clear();
            }
            if (newPayslipAvailableText != null) {
                if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsFragment.NOTIFY_NEW_PAYSLIP, true))
                    new SyncNotificationManager(context).showNewPaylslipAvailableNotification(newPayslipAvailableText, Main.class, PayslipFragment.class);
                newPayslipAvailableText = null;
            }

        }

        SyncRequestType requestType = getCurrentTask().getSubTask();
        List<SyncGdMsgStateModel> syncGdMsgStateModels = realmHelperUIThread.getAll(SyncGdMsgStateModel.class);
        if (requestType != null && requestType.getTypeOfRequest() == SyncRequestType.TypeOfRequest.MSG_REQUEST &&
                !syncGdMsgStateModels.isEmpty()) {

            // после загрузки сообщений и их состояний отправляем свои состояния
            startTask(new SyncServiceTask<SyncRequestType>(SyncServiceTask.TypeOfTask.SERIES_SYNC)
                    .setSubTask(new SyncRequestType(SyncRequestType.TypeOfRequest.SEND_GD_MSG_STATES)));
        } else {

            SyncDataService.super.onPostExecute(status);
        }

        LogUtil.d(status.getTypeOfStatus().toString(), status.getMessage());
    }

    /**
     * @return параметры для запросов, вызывается перед каждой синхронизацией
     */
    @Override
    protected SyncProperty getSyncProperty() {
        SyncMetaDataModel syncMetaDataModel = realmHelperUIThread.getSyncMetaData(true);
        DeviceInfoModel deviceInfoModel = realmHelperUIThread.getDeviceInfo(true);
        EmployeeModel lastEmployeeModel = realmHelperUIThread.getLastEmployee(true);

        SyncProperty.SyncOneBlockBuilder syncOneBlockBuilder = new SyncProperty.SyncOneBlockBuilder();
        if (getCurrentTask().getSubTask() != null) {
            Map<String, String> requestParams = new HashMap<>();
            requestParams.put(ServerURL.REQUEST_PARAM_ALIAS, syncMetaDataModel.getAlias());
            requestParams.put(ServerURL.REQUEST_PARAM_IP_ADDRESS, WebServiceManager.getIpAddress());

            switch (getCurrentTask().getSubTask().getTypeOfRequest()) {
                case MSG_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SYNC_GD_MSG);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    requestParams.put(ServerURL.REQUEST_PARAM_LAST_SYNC_MSG_DATE, syncMetaDataModel.getLastSyncMsgDate());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(3);
                    break;
                case BOARD_ITEM_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SYNC_BOARD_ITEM);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    requestParams.put(ServerURL.REQUEST_PARAM_LAST_SYNC_BOARD_ITEM_DATE, syncMetaDataModel.getLastSyncPostsDate());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));
                    requestParams.put(ServerURL.REQUEST_PARAM_MAX_SYNC_BOARD_ITEM_DAYS, String.valueOf(SettingsFragment.getBoardSyncPeriodDaysPref(context)));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(3);
                    break;
                case DATA_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SYNC);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    requestParams.put(ServerURL.REQUEST_PARAM_LAST_SYNC_DATE, syncMetaDataModel.getLastSyncDate());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(3);
                    break;
                case AUTH_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SIGN_IN);
                    requestParams.put(ServerURL.REQUEST_PARAM_DEVICE_ID, deviceInfoModel.getDeviceId());
                    requestParams.put(ServerURL.REQUEST_PARAM_PHONE_NUMBER, deviceInfoModel.getPhoneNumber());
                    requestParams.put(ServerURL.REQUEST_PARAM_DEVICE_MODEL_NAME, deviceInfoModel.getDeviceModel());
                    requestParams.put(ServerURL.REQUEST_PARAM_PASSPORT_ID, lastEmployeeModel.getPassportId());
                    requestParams.put(ServerURL.REQUEST_PARAM_LIST_NUMBER, lastEmployeeModel.getListNumber());
                    requestParams.put(ServerURL.REQUEST_PARAM_FIRSTNAME, lastEmployeeModel.getFirstname());
                    requestParams.put(ServerURL.REQUEST_PARAM_MIDDLENAME, lastEmployeeModel.getMiddlename());
                    requestParams.put(ServerURL.REQUEST_PARAM_SURNAME, lastEmployeeModel.getSurname());
                    requestParams.put(ServerURL.REQUEST_PARAM_INVITE_CODE, syncMetaDataModel.getInviteCode() == null ? "" : syncMetaDataModel.getInviteCode());
                    requestParams.put(ServerURL.REQUEST_PARAM_TRY_WITHOUT_INVITE, syncMetaDataModel.getInviteCode() == null ? "1" : "0");
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));
                    requestParams.put(ServerURL.REQUEST_PARAM_LOST_MODE, syncMetaDataModel.isLostMode() ? "1" : "0");

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(1);
                    break;
                case SEND_GD_MSG_STATES:
                    List<SyncGdMsgStateModel> syncGdMsgStateModels = realmHelperUIThread.getAll(SyncGdMsgStateModel.class);

                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat(ServerURL.SERVER_DATE_TIME_FORMAT);
                    try {
                        builder.registerTypeAdapter(Class.forName("io.realm.SyncGdMsgStateModelRealmProxy"), new SyncGdMsgStateModelSerializer());
                    } catch (ClassNotFoundException ignore) {
                    }
                    Gson gson = builder.excludeFieldsWithoutExposeAnnotation().create();

                    //String jsonData = gson.toJson(syncGdMsgStateModels);                          TODO
                    StringBuilder jsonData = new StringBuilder("[");

                    for (int i = 0; i < syncGdMsgStateModels.size(); i++) {
                        if (i != 0)
                            jsonData.append(",");
                        jsonData.append(gson.toJson(syncGdMsgStateModels.get(i)));
                    }
                    jsonData.append("]");

                    LogUtil.d(jsonData.toString());

                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_SEND_GD_MSG_STATES);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    if (!realmHelperUIThread.getSyncMetaData(true).getGcmRegistrationId().isEmpty())
                        requestParams.put(ServerURL.REQUEST_PARAM_GCM_REG_ID, realmHelperUIThread.getSyncMetaData(true).getGcmRegistrationId());
                    requestParams.put(ServerURL.REQUEST_PARAM_GD_MSG_STATE_ARRAY, jsonData.toString());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(3);

                    break;
                case LOST_CLEAN_DATA:
                case CLEAN_DATA:
                    syncOneBlockBuilder = null;
                    break;
                case DENY_ACCESS_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_DENY_ACCESS);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));
                    requestParams.put(ServerURL.REQUEST_PARAM_LOCK_AUTH_ID, String.valueOf(getCurrentTask().getSubTask().getData()));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(1);
                    break;
                case INVITE_REQUEST:
                    requestParams.put(ServerURL.REQUEST_PARAM_TOKEN, ServerURL.REQUEST_TOKEN_NEW_INVITE);
                    requestParams.put(ServerURL.REQUEST_PARAM_AUTH_KEY, syncMetaDataModel.getAuthKey());
                    requestParams.put(ServerURL.REQUEST_PARAM_APP_VERSION_CODE, String.valueOf(RealmHelper.getVersionCode()));

                    syncOneBlockBuilder
                            .setUrl(WebServiceManager.buildRequestUrl(syncMetaDataModel.getScheme(), syncMetaDataModel.getAddressServer(), requestParams))
                            .setDelayBetweenReconnect(20 * 1000)
                            .setCountConnections(1);
                    break;
            }
        }

        return syncOneBlockBuilder == null ? null : syncOneBlockBuilder.build();
    }

    @Override
    protected void onCreateDemoDB() {
        if (SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.DEMO_MODE) {
            RealmHelper.executeWithClosing(new RealmHelper.RealmHelperExecutor() {
                @Override
                public void execute(RealmHelper realmHelper) {
                    if (realmHelper.getSyncMetaData(true).getAuthKey().isEmpty() && realmHelper.getLastPayslip() == null) {

                        realmHelper.clearDB(true);
                        realmHelper.initializeDemoData();

                        for (GdMsgModel model : realmHelper.getUnreadGdMsgs()) {
                            inboxMsgNotificationTexts.put(model.getUid(), model.getSubjectName() + ".");
                        }

                        new SyncNotificationManager(context).showNewMsgNotification(inboxMsgNotificationTexts, Main.class, GdMsgsListFragment.class);
                        inboxMsgNotificationTexts.clear();

                        getCurrentTask().setSuccessfulMessage("");
                    } else
                        getCurrentTask().setSuccessfulMessage(getString(R.string.successful_sync_demo_message));
                }
            });
            CurrencyHelper.getInstance(context).updatesRates(null);
        } else {
            getCurrentTask().setSuccessfulMessage(getString(R.string.successful_sync_demo_message));
        }
    }

    public static boolean isCompatibleVersions(String serverVersion) {
        try {
            if (Double.valueOf(serverVersion) < ServerURL.MIN_SUPPORT_SERVER_VERSION)
                return false;
        } catch (NumberFormatException e) {
            LogUtil.d(e);
        }
        return true;
    }

    private boolean isNeedShowUpdateNotification(String latestVersionStr) {
        try {
            final String TAG = "last_latest_client_version"; //TODO
            int latestVersion = Integer.valueOf(latestVersionStr);

            if (RealmHelper.getVersionCode() < latestVersion) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

                if (pref.getInt(TAG, 0) < latestVersion) {
                    pref.edit().putInt(TAG, latestVersion).apply();
                    return true;
                }
            }

        } catch (NumberFormatException e) {
            LogUtil.d(e);
        }
        return false;
    }

    /**
     * @return в каком режиме проводить синхронизацию. Вызывается перед каждой синхронизацей.
     */
    @Override
    protected boolean isWorkMode() {
        return SettingsFragment.getPrefMode(context) == SettingsFragment.PrefMode.WORK_MODE;
    }

    @Override
    protected SyncNotificationAdapter getSyncNotificationAdapter() {
        return new SyncNotificationManager(context);
    }
}
