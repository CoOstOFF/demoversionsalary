package com.gsbelarus.gedemin.salary.service;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.util.GCMHelper;

public class GcmInstanceIDService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        LogUtil.d("onTokenRefresh");
        GCMHelper gcmHelper = new GCMHelper(getApplicationContext());
        gcmHelper.updateRegistration();
    }
}
