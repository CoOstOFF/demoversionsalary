package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class Settings extends ThemedActivity {  //TODO lib

    private SettingsFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();
        setScrollFlagsToolbar(0);

        if (savedInstanceState == null) {
            includeFragment(fragment = new SettingsFragment(), TAG);
        } else {
            fragment = findFragment(TAG);
        }
    }

    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, BaseActivity.SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        switch (syncServiceStatus.getTypeOfServiceStatus()) {
            case START_SYNC:
                if (fragment != null && fragment.isAdded())
                    fragment.startSync();
                break;
            case FINISH_SYNC:
                if (fragment != null && fragment.isAdded())
                    fragment.finishSync();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }
}
