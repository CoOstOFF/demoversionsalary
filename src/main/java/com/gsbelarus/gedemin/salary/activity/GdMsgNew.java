package com.gsbelarus.gedemin.salary.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

import java.util.Date;

public class GdMsgNew extends ThemedActivity {

    public static final String TAG_FRAGMENT_CLASS = "fragment_class";
    public static final String TAG_MSG_DATE = "gd_msg_date";

    @Nullable
    @Override
    protected Integer getAppBarLayoutResource() {
        return R.layout.appbar_gdmsg_new;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();
        setScrollFlagsToolbar(0);

        if (savedInstanceState == null) {

            if (BundleHelper.containsArguments(getIntent().getExtras(), TAG_FRAGMENT_CLASS)) {
                Class fragmentClass = BundleHelper.get(getIntent().getExtras(), TAG_FRAGMENT_CLASS);

                try {
                    assert fragmentClass != null;
                    BaseFragment baseFragment = (BaseFragment) fragmentClass.newInstance();
                    if (BundleHelper.containsArguments(getIntent().getExtras(), TAG_MSG_DATE))
                        showFragment(baseFragment, BundleHelper.<Date>get(getIntent().getExtras(), TAG_MSG_DATE));
                    else
                        includeFragment(baseFragment);

                } catch (InstantiationException e) {
                    LogUtil.d(e.toString());
                } catch (IllegalAccessException e) {
                    LogUtil.d(e.toString());
                }
            }
        }
    }

    private void showFragment(BaseFragment fragment, Date msgData) {
        Bundle args = new Bundle();
        args.putSerializable(TAG_MSG_DATE, msgData);
        fragment.setArguments(args);
        includeFragment(fragment);
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gdmsg_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                Intent settingIntent = new Intent(context, Settings.class);
                startActivity(settingIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
