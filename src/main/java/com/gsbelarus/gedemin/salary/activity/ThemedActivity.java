package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;

import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;

abstract public class ThemedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        switch (SettingsFragment.getThemeMode(getApplicationContext())) {
            case DARK:
                setTheme(R.style.App_Theme_Dark);
                break;
            case LIGHT:
                setTheme(R.style.App_Theme);
                break;
        }

        super.onCreate(savedInstanceState);
    }
}
