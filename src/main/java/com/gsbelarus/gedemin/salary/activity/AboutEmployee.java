package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.AboutEmployeeFragment;
import com.gsbelarus.gedemin.salary.ui.FloatingActionFrameLayout;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class AboutEmployee extends ThemedActivity {

    private AboutEmployeeFragment fragment;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about_employee;
    }

    @Nullable
    @Override
    protected Integer getAppBarLayoutResource() {
        return R.layout.appbar_about_employee;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();
        setVisibilityAppBarShadow(false);

        if (savedInstanceState == null) {
            includeFragment(fragment = new AboutEmployeeFragment(), TAG);
        } else {
            fragment = findSupportFragment(TAG);
        }

        FloatingActionFrameLayout avatarFrame = (FloatingActionFrameLayout) findViewById(R.id.about_employee_avatar_frame);
        if (avatarFrame != null && getAppBarLayout() != null) {
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) avatarFrame.getLayoutParams();
            params.setAnchorId(getAppBarLayout().getId());
            avatarFrame.setLayoutParams(params);
            avatarFrame.bringToFront();
            avatarFrame.invalidate();
        }
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC &&
                syncServiceStatus.getStatus().getTypeOfStatus() != SyncStatus.TypeOfStatus.NO_INTERNET_CONNECTION)
            fragment.reloadFragmentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GCMHelper(this).checkPlayServices(this);
    }
}
