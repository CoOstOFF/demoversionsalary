package com.gsbelarus.gedemin.salary.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Html;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.fragment.registration.RegistrationPagerFragment;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class RegistrationPager extends ThemedActivity {

    private RegistrationPagerFragment fragment;

    private boolean performingRequest;

    @Nullable
    @Override
    protected Integer getAppBarLayoutResource() {
        return R.layout.appbar_registration;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();

        if (savedInstanceState == null) {
            includeFragment(fragment = new RegistrationPagerFragment(), TAG);
            showMaterialAuthDialog();
        } else {
            fragment = findSupportFragment(TAG);
        }

        if (getAppBarLayout() != null)
            getAppBarLayout().setExpanded(true);
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        boolean showErrorDialog = true;
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC) {
            performingRequest = false;
            if (fragment != null)
                showErrorDialog = fragment.finishSync(syncServiceStatus);
        }
        if (showErrorDialog)
            SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);

        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.START_SYNC) {
            performingRequest = true;

            /** закрываем активити если началась синхронизация (значит активити открыто из настроек) */
            if (syncServiceStatus.getTask().getSubTask() != null &&
                    ((SyncRequestType) syncServiceStatus.getTask().getSubTask()).getTypeOfRequest() !=
                            SyncRequestType.TypeOfRequest.AUTH_REQUEST)
                finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!performingRequest) {
            boolean flag = true;
            if (fragment != null)
                flag = fragment.onBackPressed();
            if (!flag)
                super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }

    public void showMaterialAuthDialog() {
        new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml("Для работы программы на сервере предприятия " +
                        "должен быть установлен комплекс \"Учет труда и заработной платы\" " +
                        "компании <a href='" + SettingsFragment.GS_URL + "'>..., Ltd</a>."))
                .setPositiveButton("Продолжить", null)
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create()
                .show();
    }
}
