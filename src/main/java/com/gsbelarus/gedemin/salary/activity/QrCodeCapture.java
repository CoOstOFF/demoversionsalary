package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.registration.QrCodeCaptureFragment;

public class QrCodeCapture extends ThemedActivity {

    private QrCodeCaptureFragment fragment;

    @Nullable
    @Override
    protected Integer getAppBarLayoutResource() {
        return R.layout.appbar_qr_code_capture;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_qr_code_capture;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setupSubActivity();
        setScrollFlagsToolbar(0); //TODO lib

        setVisibilityAppBarShadow(false);
        if (savedInstanceState == null) {
            includeFragment(fragment = new QrCodeCaptureFragment(), TAG);
        } else {
            fragment = findSupportFragment(TAG);
        }
    }
}
