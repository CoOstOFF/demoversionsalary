package com.gsbelarus.gedemin.salary.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro2;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.fragment.IntroFragment;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.util.GCMHelper;

public class IntroPager extends AppIntro2 {

    public static final String PREF_SHOW_INTRO = "show_intro";

    private static final int REQUEST_CODE_ACTIVITY = 1;

    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(IntroFragment.newInstance(R.layout.fragment_intro_payslip));
        addSlide(IntroFragment.newInstance(R.layout.fragment_intro_workschedule));
        addSlide(IntroFragment.newInstance(R.layout.fragment_intro_messages));
        addSlide(IntroFragment.newInstance(R.layout.fragment_intro_sync));
        addSlide(IntroFragment.newInstance(R.layout.fragment_intro_mode_select, new IntroFragment.OnCreateViewListener() {

            @Override
            public void onCreateView(View view) {

                View workModeButton = view.findViewById(R.id.intro_work_mode_button);
                if (workModeButton != null)
                    workModeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startRegistration();
                        }
                    });

                View demoModeButton = view.findViewById(R.id.intro_demo_mode_button);
                if (demoModeButton != null)
                    demoModeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(PREF_SHOW_INTRO, false).apply();
                            SettingsFragment.putPrefMode(getApplicationContext(), SettingsFragment.PrefMode.DEMO_MODE);

                            startApplication();

                            startService(SyncServiceModel.getIntentWithTask(new Intent(getApplicationContext(), SyncDataService.class),
                                    SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));
                        }
                    });
            }
        }));
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onSlideChanged() {

    }

    @Override
    public void onDonePressed() {
        startRegistration();
    }

    private void startRegistration() {
        Intent intent = new Intent(getApplicationContext(), RegistrationPager.class);
        startActivityForResult(intent, REQUEST_CODE_ACTIVITY);
    }

    private void startApplication() {
        Intent intent = new Intent(getApplicationContext(), Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_ACTIVITY:
                if (resultCode == RESULT_OK)
                    startApplication();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }
}