package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.salary.fragment.AuthDevicesFragment;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class AuthDevices extends ThemedActivity {

    private AuthDevicesFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();
        setScrollFlagsToolbar(0);

        if (savedInstanceState == null) {
            includeFragment(fragment = new AuthDevicesFragment(), TAG);
        } else {
            fragment = findSupportFragment(TAG);
        }
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC) {

            if (fragment != null)
                fragment.reloadFragmentData();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }
}
