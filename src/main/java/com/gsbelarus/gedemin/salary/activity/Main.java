package com.gsbelarus.gedemin.salary.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceTask;
import com.gsbelarus.gedemin.lib.ui.fragment.BaseFragment;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.database.RealmHelper;
import com.gsbelarus.gedemin.salary.entity.SyncRequestType;
import com.gsbelarus.gedemin.salary.entity.model.EmployeeModel;
import com.gsbelarus.gedemin.salary.fragment.StatisticsFragment;
import com.gsbelarus.gedemin.salary.fragment.calendar.CalendarFragment;
import com.gsbelarus.gedemin.salary.fragment.dialogs.RateItDialogFragment;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.GdMsgsListFragment;
import com.gsbelarus.gedemin.salary.fragment.payslip.PayslipFragment;
import com.gsbelarus.gedemin.salary.fragment.settings.SettingsFragment;
import com.gsbelarus.gedemin.salary.service.SyncDataService;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.PermissionHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

import io.realm.Realm;
import me.leolin.shortcutbadger.ShortcutBadger;

public class Main extends ThemedNavigationDrawerActivity {

    public static final String PREF_APP_VERSION = "app_version";

    public static final String CONTACT_EMAIL = "...";
    public static final String CONTACT_SUBJECT = "... support";

    private BaseFragment currentFragment;
    private RealmHelper realmHelper;
    private Integer waitingSelectedNavItemId = null;
    private boolean finishedSync;

    @Override
    protected int getNavHeaderLayoutResource() {
        return R.layout.navview_header;
    }

    @Override
    protected int getDrawerMenuResource() {
        return R.menu.view_drawer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean showIntro = getSharedBoolean(IntroPager.PREF_SHOW_INTRO, true);
        if (showIntro) {
            Intent intent = new Intent(this, IntroPager.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            showDrawerForLearning(false);
        }

        realmHelper = new RealmHelper(Realm.getDefaultInstance());

        if (savedInstanceState != null) {
            currentFragment = findSupportFragment(TAG);
            setToolbarTitle(getSelectedNavItemId());
            selectMsgCounter(getSelectedNavItemId(), R.id.nav_messages);

        } else if (getIntent().hasExtra("openFragment") &&
                getIntent().getStringExtra("openFragment").equals(GdMsgsListFragment.class.getSimpleName())) {
            selectNavItem(R.id.nav_messages);

        } else {
            selectNavItem(R.id.nav_payslip);
        }

        Tracker tracker = ((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.setScreenName(getClass().getSimpleName());
        tracker.send(new HitBuilders.AppViewBuilder().build());

        updateNavMenuMailCounter();
        updateNavHeaderData();

        if (savedInstanceState == null && !realmHelper.getSyncMetaData(true).getAuthKey().isEmpty()) {
            RateItDialogFragment.show(this, getFragmentManager());
        }

        addDrawerStateListener(new DrawerStateListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (waitingSelectedNavItemId != null) {
                    selectNavItem(waitingSelectedNavItemId);
                    waitingSelectedNavItemId = null;
                }
            }
        });
    }

    public void updateNavMenuMailCounter() {
        long count = realmHelper.getUnreadGdMsgs().size();
        TextView countText = (TextView) getNavView().getMenu().findItem(R.id.nav_messages).getActionView();
        if (countText != null) {
            if (count > 0) {
                countText.setText(String.valueOf(count));
                countText.setVisibility(View.VISIBLE);
            } else {
                countText.setVisibility(View.GONE);
            }
        }

        ShortcutBadger.with(getApplicationContext()).count((int) count);
    }

    @Override
    protected void initNavHeaderData() {
        TextView tvProfileSummary = (TextView) getNavHeaderLayout().findViewById(R.id.navview_header_profile_summary);
        String profileSummary = getString(R.string.navview_header_profile_summary).toLowerCase();   // :style
        tvProfileSummary.setText(profileSummary);

        View profileLayout = getNavHeaderLayout().findViewById(R.id.navview_header_profile_layout);
        if (profileLayout != null)
            profileLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent drawerPersonInfoIntent = new Intent(Main.this, AboutEmployee.class);
                            startActivity(drawerPersonInfoIntent);
                        }
                    }
            );
    }

    private void updateNavHeaderData() {
        String profileName = getString(R.string.navview_header_profile_name_default);
        EmployeeModel employeeModel = realmHelper.getLastEmployee(true);

        switch (SettingsFragment.getPrefMode(context)) {
            case WORK_MODE:
                if (!realmHelper.getSyncMetaData(true).getAuthKey().isEmpty() &&
                        !employeeModel.getFirstname().isEmpty() &&
                        !employeeModel.getSurname().isEmpty())
                    profileName = employeeModel.getFirstname() + " " + employeeModel.getSurname();
                break;
            case DEMO_MODE:
                if (!employeeModel.getFirstname().isEmpty() &&
                        !employeeModel.getSurname().isEmpty())
                    profileName = employeeModel.getFirstname() + " " + employeeModel.getSurname();
                break;
        }

        TextView tvProfileName = (TextView) getNavHeaderLayout().findViewById(R.id.navview_header_profile_name);
        tvProfileName.setText(profileName);
    }

    @Override
    protected void onNavItemSelected(MenuItem menuItem) {
        if (getSelectedNavItemId() != menuItem.getItemId()) {

            if (!isOpenedDrawer()) {
                selectNavItem(menuItem.getItemId());
            } else {
                waitingSelectedNavItemId = menuItem.getItemId();
            }
        }
        closeDrawer();
    }

    private void selectMsgCounter(int selectedItemId, int waitingSelectedNavItemId) {
        // change counter color // FIXME: 23.03.2016 pre-lollipop get attr in select
        TextView tvCounter = (TextView) getNavView().getMenu().findItem(waitingSelectedNavItemId).getActionView();
        tvCounter.setSelected(selectedItemId == waitingSelectedNavItemId);
    }

    @Override
    public void selectNavItem(int selectedItemId) {
        super.selectNavItem(selectedItemId);

        selectMsgCounter(selectedItemId, R.id.nav_messages);

        BaseFragment fragment = null;

        switch (selectedItemId) {
            case R.id.nav_payslip:
                fragment = new PayslipFragment();
                break;
            case R.id.nav_statistic:
                fragment = new StatisticsFragment();
                break;
            case R.id.nav_workschedule:
                fragment = CalendarFragment.getInstance(CalendarFragment.CalendarType.WORK_SCHEDULE);
                break;
            case R.id.nav_timesheet:
                fragment = CalendarFragment.getInstance(CalendarFragment.CalendarType.TIMESHEET);
                break;
            case R.id.nav_messages:
                fragment = new GdMsgsListFragment();
                break;
            case R.id.nav_sync:
                if (PermissionHelper.ReadPhoneState.checkAndRequest(this))
                    startService(SyncServiceModel.getIntentWithTask(new Intent(context, SyncDataService.class),
                            SyncServiceTask.TypeOfTask.SINGLE_SYNC, new SyncRequestType(SyncRequestType.TypeOfRequest.DATA_MSG_BOARDITEM_REQUEST)));
                break;
            case R.id.nav_settings:
                Intent settingsIntent = new Intent(Main.this, Settings.class);
                startActivity(settingsIntent);
                break;
        }

        if (fragment != null) {
            if (getAppBarLayout() != null)
                getAppBarLayout().setExpanded(true);

            setToolbarTitle(selectedItemId);
            replaceFragment(currentFragment = fragment, TAG);
        }
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC) {

            finishedSync = true;

            updateNavMenuMailCounter();
            updateNavHeaderData();
            if (currentFragment != null)
                currentFragment.reloadFragmentData();

        } else if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.START_SYNC) {
            finishedSync = false;
        }
    }

    public boolean isFinishedSync() {
        return finishedSync;
    }

    @Override
    protected void onResume() {
        super.onResume();

        GCMHelper gcmHelper = new GCMHelper(this);
        gcmHelper.checkRegistration(realmHelper);
        gcmHelper.checkPlayServices(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        realmHelper.getRealm().close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                /** Если фрагмент Сообщения, даем возможность переопределить действие на кнопку home */
                if (currentFragment instanceof GdMsgsListFragment) {
                    return false;
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
