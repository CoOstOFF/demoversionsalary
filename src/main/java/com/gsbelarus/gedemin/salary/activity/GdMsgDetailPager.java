package com.gsbelarus.gedemin.salary.activity;

import android.os.Bundle;
import android.view.Menu;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.lib.ui.activity.BaseActivity;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.entity.model.GdMsgModel;
import com.gsbelarus.gedemin.salary.fragment.gdmsg.GdMsgDetailPagerFragment;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class GdMsgDetailPager extends ThemedActivity {

    private GdMsgDetailPagerFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSubActivityWithTitle();
        setScrollFlagsToolbar(0);

        if (savedInstanceState == null) {
            Integer gdMsgId = 0;
            String textFilter = null;
            GdMsgModel.Sender senderFilter = null;

            if (BundleHelper.containsArguments(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_GD_MSG_ID))
                gdMsgId = BundleHelper.get(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_GD_MSG_ID);
            if (BundleHelper.containsArguments(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_TEXT_FILTER))
                textFilter = BundleHelper.get(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_TEXT_FILTER);
            if (BundleHelper.containsArguments(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_SENDER_FILTER))
                senderFilter = BundleHelper.get(getIntent().getExtras(), GdMsgDetailPagerFragment.TAG_SENDER_FILTER);

            fragment = GdMsgDetailPagerFragment.newInstance(gdMsgId, senderFilter, textFilter);
            fragment.setHasOptionsMenu(true);
            includeFragment(fragment, TAG);
        } else
            fragment = findSupportFragment(TAG);
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, BaseActivity.SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC &&
                syncServiceStatus.getStatus().getTypeOfStatus() != SyncStatus.TypeOfStatus.NO_INTERNET_CONNECTION)
            fragment.reloadFragmentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GCMHelper(this).checkPlayServices(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gdmsg, menu);
        return true;
    }
}