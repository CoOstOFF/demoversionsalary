package com.gsbelarus.gedemin.salary.activity;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.salary.R;
import com.gsbelarus.gedemin.salary.fragment.StatisticsFragment;
import com.gsbelarus.gedemin.salary.util.GCMHelper;
import com.gsbelarus.gedemin.salary.util.SyncStatusDialogHelper;

public class Statistics extends ThemedActivity {

    private StatisticsFragment fragment;

    @Override
    protected int getLayoutResource() {
        // TODO проверить после обновления support design lib
        // начиная с v23.2.0 fitsSystemWindows для CoordinatorLayout "учитывается" даже если статус бар не прозрачный,
        // а динамическая смена работает некорректно
        return R.layout.activity_without_fits_system_windows;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[] attrs = {R.attr.colorStatusBar};

            TypedArray ta = this.obtainStyledAttributes(attrs);

            int statusBarColor = ta.getColor(0, Color.BLACK);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(statusBarColor);
            ta.recycle();
        }
        setupSubActivityWithTitle();

        if (savedInstanceState == null)
            includeFragment(fragment = new StatisticsFragment(), TAG);
        else
            fragment = findSupportFragment(TAG);
    }

    @Override
    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
        super.onReceiveSyncStatus(syncServiceStatus, syncStatusDialog);

        SyncStatusDialogHelper.show(this, syncServiceStatus, syncStatusDialog);
        if (syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.FINISH_SYNC &&
                syncServiceStatus.getStatus().getTypeOfStatus() != SyncStatus.TypeOfStatus.NO_INTERNET_CONNECTION)
            fragment.reloadFragmentData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GCMHelper(this).checkPlayServices(this);
    }
}
