package com.gsbelarus.gedemin.salary.config;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public abstract class ServerURL {

    public static final double MIN_SUPPORT_SERVER_VERSION = 2.41; // 2.42

    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd";
    public static final String SERVER_DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";


    public static final String RESPONSE_PARAM_SERVER_VERSION = "server_version";
    public static final String RESPONSE_PARAM_ERROR_CODE = "error_code";
    public static final String RESPONSE_PARAM_LAST_SYNC_DATE = "last_sync_date";
    public static final String RESPONSE_PARAM_LAST_SYNC_MSG_DATE = "last_sync_msg_date";
    public static final String RESPONSE_PARAM_LAST_SYNC_BOARD_ITEM_DATE = "last_sync_board_item_date";
    public static final String RESPONSE_PARAM_AUTH_KEY = "auth_key";
    public static final String RESPONSE_PARAM_LATEST_CLIENT_VERSION = "latest_client_version";
    public static final String RESPONSE_PARAM_INVITE_CODE = "invite_code";
    public static final String RESPONSE_PARAM_GDMSG_ID = "gdmsg_id";
    public static final String RESPONSE_PARAM_GDMSG_TIMESTAMP = "gdmsg_timestamp";

    public static final String RESPONSE_PARAM_ACTION = "action";
    public static final String RESPONSE_ACTION_CLEAN_DATA = "clean_data";

    public static final String GCM_RESPONSE_PARAM_ACTION = RESPONSE_PARAM_ACTION;
    public static final String GCM_RESPONSE_ACTION_CLEAN_DATA = RESPONSE_ACTION_CLEAN_DATA;
    public static final String GCM_RESPONSE_ACTION_REMOVE_PERMITS = "remove_permits";
    public static final String GCM_RESPONSE_ACTION_ADD_PERMITS = "add_permits";
    public static final String GCM_RESPONSE_ACTION_SYNC_MSGS = "sync_msgs";
    public static final String GCM_RESPONSE_ACTION_SYNC_BOARD_ITEMS = "sync_board_items";
    public static final String GCM_RESPONSE_ACTION_SYNC = "sync";
    public static final String GCM_RESPONSE_ACTION_LOST_CLEAN_DATA = "lost_clean_data";


    public static final String REQUEST_PARAM_TOKEN = "token";
    public static final String REQUEST_PARAM_ALIAS = "alias";
    public static final String REQUEST_PARAM_IP_ADDRESS = "ip_address";
    public static final String REQUEST_PARAM_GD_MSG = "gd_msg";
    public static final String REQUEST_PARAM_GCM_REG_ID = "gcm_reg_id";
    public static final String REQUEST_PARAM_GD_MSG_STATE_ARRAY = "gd_msg_state_array";
    public static final String REQUEST_PARAM_LAST_SYNC_DATE = RESPONSE_PARAM_LAST_SYNC_DATE;
    public static final String REQUEST_PARAM_LAST_SYNC_MSG_DATE = RESPONSE_PARAM_LAST_SYNC_MSG_DATE;
    public static final String REQUEST_PARAM_LAST_SYNC_BOARD_ITEM_DATE = RESPONSE_PARAM_LAST_SYNC_BOARD_ITEM_DATE;
    public static final String REQUEST_PARAM_MAX_SYNC_BOARD_ITEM_DAYS = "max_sync_board_item_days";
    public static final String REQUEST_PARAM_AUTH_KEY = RESPONSE_PARAM_AUTH_KEY;
    public static final String REQUEST_PARAM_PASSPORT_ID = "passport_id";
    public static final String REQUEST_PARAM_LIST_NUMBER = "list_number";
    public static final String REQUEST_PARAM_FIRSTNAME = "firstname";
    public static final String REQUEST_PARAM_MIDDLENAME = "middlename";
    public static final String REQUEST_PARAM_SURNAME = "surname";
    public static final String REQUEST_PARAM_INVITE_CODE = RESPONSE_PARAM_INVITE_CODE;
    public static final String REQUEST_PARAM_TRY_WITHOUT_INVITE = "try_without_invite";
    public static final String REQUEST_PARAM_PHONE_NUMBER = "phone_number";
    public static final String REQUEST_PARAM_DEVICE_ID = "device_id";
    public static final String REQUEST_PARAM_DEVICE_MODEL_NAME = "device_model_name";
    public static final String REQUEST_PARAM_APP_VERSION_CODE = "app_version_code";
    public static final String REQUEST_PARAM_LOST_MODE = "lost_mode";
    public static final String REQUEST_PARAM_LOCK_AUTH_ID = "lock_auth_id";

    public static final String REQUEST_TOKEN_SET_REG_ID = "gdmnSalarySetGCMRegId";
    public static final String REQUEST_TOKEN_DENY_ACCESS = "gdmnSalaryDenyAccess";
    public static final String REQUEST_TOKEN_SIGN_IN = "gdmnSalarySignIn";
    public static final String REQUEST_TOKEN_NEW_INVITE = "gdmnSalaryGetNewInvite";
    public static final String REQUEST_TOKEN_SYNC = "gdmnSalarySync";
    public static final String REQUEST_TOKEN_SYNC_BOARD_ITEM = "gdmnSalarySyncBoardItem";
    public static final String REQUEST_TOKEN_SYNC_GD_MSG = "gdmnSalarySyncMsg";
    public static final String REQUEST_TOKEN_SEND_MSG = "gdmnSalarySendMsg";
    public static final String REQUEST_TOKEN_SEND_GD_MSG_STATES = "gdmnSalarySendMsgStates";
    public static final String REQUEST_TOKEN_SEND_BOARD_ITEM = "gdmnSalarySendBoardItem";


    public static String RELAY_SERVER_URL = "***";

    private static String IGNORED_SERVER_PARTS_PATTERN =
            "\\sоао|\\sооо|\\sодо|\\sтда|\\soao|\\sooo|оао\\s|ооо\\s|одо\\s|тда\\s|oao\\s|ooo\\s|\\'|\\\"|\\s\\+"; // рус бел eng  //TODO tmp


    private static Map<String, String> getUrls() {
        Map<String, String> urls = new HashMap<>();

        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");
        urls.put("...", "...");

        return urls;
    }

    public static String formatServerName(String serverName) {
        return Pattern.compile(IGNORED_SERVER_PARTS_PATTERN).matcher(serverName.toLowerCase()).replaceAll(" ").trim();
    }

    public static String findAddressByCompany(String company) {
        String str = formatServerName(company);

        return getUrls().containsKey(str) ? getUrls().get(str) : "";
    }

    public static String findCompanyByAddress(String address) {
        if (getUrls().containsValue(address)) {
            for (Map.Entry<String, String> entry : getUrls().entrySet()) {

                if (entry.getValue().equals(address)) {
                    return entry.getKey();
                }
            }
        }

        return "";
    }
}