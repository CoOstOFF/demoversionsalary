package com.gsbelarus.gedemin.salary.config;

import android.support.annotation.StringRes;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;
import com.gsbelarus.gedemin.salary.App;
import com.gsbelarus.gedemin.salary.R;

public enum ResponseErrorCode {

    //0
    INTERNAL_ERROR(R.string.response_error_unknown),
    //1
    INVALID_REQUEST(R.string.response_error_invalid_request),
    //2
    INVALID_DATA(R.string.response_error_invalid_data),
    //3
    NO_PERMISS(R.string.response_error_no_permiss),
    //4
    NO_PERMISS_DATA_WILL_BE_DELETED(R.string.response_error_no_permiss_data_will_be_deleted),
    //5
    UNABLE_GET_INVITE_WAS_SENT(R.string.response_error_unable_get_invite_was_sent),
    //6
    INVALID_INVITE_WAS_ACTIVATED(R.string.response_error_invalid_invite_was_activated),
    //7
    INVALID_INVITE(R.string.response_error_invalid_invite),
    //8
    INVALID_INVITE_EXPIRED(R.string.response_error_need_invite),
    //9
    NEED_INVITE(R.string.response_error_need_invite),
    //10
    EMPL_NOT_FOUND(R.string.response_error_empl_not_found),
    //11
    EMPL_NOT_FOUND_INVALID_ID(R.string.response_error_empl_not_found_invalid_id),
    //12
    EMPL_NOT_FOUND_INVALID_NAME(R.string.response_error_empl_not_found_invalid_name),
    //13
    EMPL_NOT_FOUND_INVALID_LN(R.string.response_error_empl_not_found_invalid_ln),
    //14
    NEED_UPDATE_APP(R.string.response_error_need_update_app),
    //15
    SERVER_NOT_FOUND(R.string.response_error_server_not_found),
    //16
    SERVER_UNAVAILABLE(R.string.response_error_server_unavailable),
    //17
    EMPTY_CODE(R.string.response_error_empty_code),
    //18
    UNKNOWN(R.string.response_error_unknown);

    private int messageRes;

    ResponseErrorCode(@StringRes int messageRes) {
        this.messageRes = messageRes;
    }

    private @StringRes int getMessageRes() {
        return messageRes;
    }

    public String getMessage() {
        return App.getContext().getString(getMessageRes());
    }

    public static ResponseErrorCode getErrorByCode(String codeStr) {
        try {
            int code = Integer.valueOf(codeStr);
            if (code >= 0 && code < ResponseErrorCode.values().length)
                return ResponseErrorCode.values()[code];

        } catch (NumberFormatException e) {
            LogUtil.d(e.getMessage());
        }
        return ResponseErrorCode.UNKNOWN;
    }


}
